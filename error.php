<?php 

include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
?>
<link rel="stylesheet" href="css/jquery-ui.css" />
<script src="js/jquery-ui.js"></script>
<style>
a
{
	text-decoration:none;
}
</style>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container" id="inner_main_div">
 			<div class="cell_con">
				 <div id="content_1" class="content">
<?php echo "<p class='error_msg red'>Sorry! Some of the information you entered was incorrect. Please re-enter your information and try to upgrade again.</p>";?>
                 </div>
	        </div>
        </div>
	<?php include('newLounge_right.php');?>
    </div>
</div>

<div class="footer"><?php include('footer.php');?>
</div>
<?php include('budfolio_footer.php');?>

</body>
</html>
