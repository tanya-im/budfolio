<?php session_start();
// Include function and config file.
include_once("config.php");
include_once("function.php");
// Show lounge listing for all users
if($_POST['SearchData']=='SearchData' and $_POST['type']=='all')
{
	if(!empty($_POST['searchtext']))
	{
		$_SESSION['searchtext']=$_POST['searchtext'];
		$_SESSION['loungecounter']=1;
	}
	else{
		unset($_SESSION['searchtext']);
		unset($_SESSION['loungecounter']);
		return false;
	}
	$perpage=PerPageForLoungMyLocal;
	$searchtext=$_SESSION['searchtext'];
	$page=$_SESSION['loungecounter'];
	LoungeSearchRes($page,$searchtext,$perpage);
}
if($_POST['SearchData']=='SearchData' and $_POST['type']=='following')
{
	if(!empty($_POST['searchtext']))
	{
		$_SESSION['searchtext']=$_POST['searchtext'];
		$_SESSION['loungecounter']=1;
	}
	else{
		unset($_SESSION['searchtext']);
		unset($_SESSION['loungecounter']);
		return false;
	}
	$perpage=PerPageForLoungMyLocal;
	$searchtext=$_SESSION['searchtext'];
	$page=$_SESSION['loungecounter'];
	LoungeFollowSearchRes($page,$searchtext,$perpage);
}
if($_POST['SearchData']=='SearchData' and $_POST['type']=='message')
{
	if(!empty($_POST['searchtext']))
	{
		$_SESSION['searchtext']=$_POST['searchtext'];
		$_SESSION['loungecounter']=1;
	}
	else{
		unset($_SESSION['searchtext']);
		unset($_SESSION['loungecounter']);
		return false;
	}
	$perpage=PerPageForLoungMyLocal;
	$searchtext=$_SESSION['searchtext'];
	$page=$_SESSION['loungecounter'];
	LoungeMessageSearchRes($page,$searchtext,$perpage);
}
die;?>