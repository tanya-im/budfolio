<?php
// Include header,auth,transection and constant array file.
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
// Get user detail
$selectprofile="select * from users where user_id='".$_SESSION['user_id']."'";
$profile=mysql_query($selectprofile);
$profiledata=mysql_fetch_array($profile);
?>
<div class="inner-con">
	<?php include_once("left_sidebar.php");?>
    <div class="innercon_center">
    	<div class="imagechane">
        	<div id="profilepreview" class="imgdisplay">
			<?php 
                // Show user profiloe pic
                $getuserpic="select photo_url from users where user_id='".$_SESSION['user_id']."'";
                $res=mysql_query($getuserpic)or die(mysql_error());
                $picpath=mysql_fetch_array($res);
                if(!empty($picpath[0]))echo '<img src="images/profileimages/original/'.$picpath[0].'"/>';
				else { echo '<img src="images/css_images/profile_pic.png"/>';}
           ?>
    		</div>
            <div class="profilepiconsetting">
            	<a href="javascript:void(0);" id="setprofilepicture" class="photo_btn">Set Profile Picture</a>
            </div> 
    	</div>	
        <!-- User profile pic form-->
        <form id="profileimageform" method="post" enctype="multipart/form-data" action='ajaxprofileimage.php'>
        	<input class="hideme" type="file" name="profileimg" id="profileimg" size="13" value="Upload" />
        </form>
        <!--End-->
    	<div id="chgpwdmsg"></div> 
        <!---------------- Update user profile form------------------------->
        <form method="post" name="profileform" id="profileform" onsubmit="return user_profile_update();">
        	<h3 class="martop10"> Bio </h3>
        	<div class="formcon">
            	<dl>
                    <div class="textcon">
                       	<textarea id="bio" name="bio" maxlength="300"><?php echo $profiledata['bio']; ?></textarea></div>
                	 <div class="dbox_msg">300 characters</div>
                     <span class="marrit15"> <a href="javascript:void(0);" class="edit_btn" id="savebio"> Update </a></span>
                </dl>
            </div>
            <h3 class="martop10"> Change Password </h3>
        	<div class="formcon">
                <dl>
                    <dt> Old Password </dt>
                    <dd><span class="innerinput"><input  type="password" name="oldpassword" id="oldpassword" maxlength="25" placeholder="Enter Old Password" ></span>
                    </dd>
                </dl>
                <dl>
                    <dt> New Password </dt>
                    <dd> <span class="innerinput"> <input type="password" name="newpassword" id="newpassword" maxlength="25" placeholder="Enter New Password" > </span> </dd>
                </dl>
                <dl>
                    <dt> Confirm Password </dt>
                    <dd> <span class="innerinput"> <input type="password" name="confirmpassword" id="confirmpassword" maxlength="25" placeholder="Enter Confirm Password" > </span> </dd>
                </dl>
                <dl>
                    <dt>&nbsp; </dt>
                    <dd> <a href="#" class=" org_btn" id="change_password_btn"> Create New Password </a> </dd>
                </dl>
			</div>
			<div class="formcon"> 
                <dl>
                    <dt> Email </dt>
                    <dd> <span class="innerinput"> <input type="text"  name="email" id="email" value="<?php echo $profiledata['email_address']; ?>" placeholder="Enter Email" maxlength="55" disabled="disabled"> </span> 
                    <span> <a href="javascript:void(0);" id="editprofile" class="edit_btn"> Edit </a></span> </dd>
                </dl>
                <dl>
                    <dt> Zip Code </dt>
                    <dd> <span class="innerinput"> <input type="text" name="zipcode" id="zipcode" value="<?php echo $profiledata['zip_code']; ?>" placeholder="Enter Zip Code" maxlength="10" disabled="disabled"> </span> </dd>
                </dl>
                <dl>
                    <dt> State </dt>
                    <dd> <span class="innerinput"> <input type="text" name="state" id="state" value="<?php echo $profiledata['state']; ?>" placeholder="Enter State" maxlength="30" disabled="disabled"> </span> </dd>
                </dl>
                <dl>
                    <dt> Country </dt>
                    <dd> <span class="innerinput"> <input type="text" name="country" id="country" value="<?php echo $profiledata['country']; ?>" placeholder="Enter Country" maxlength="30" disabled="disabled"> </span> </dd>
                </dl>
                <p class="martop10 fllft"> This info is used for geo advertising only. It is not shared or seen by any other users or third parties.</p>
			</div>
            <div class="formcon"> 
                <h3> Privacy </h3>
                    <dl>
                        <dt class="marpad_top0"> <input type="radio" name="privacy" id="public" value="public" <?php if($profiledata['privacy']=='public'){ ?> checked="checked" <?php } ?>>Public   </dt>
                        <dd class="marpad_top0"> <input type="radio" name="privacy" id="private" value="private" <?php if($profiledata['privacy']=='private'){ ?> checked="checked" <?php } ?>> Private</dd>
                    </dl>
                    <p class="martop10 fllft"> A private profiles data will not be linked to the &quot;lounge&quot; feature of the site. Public user has all uploaded strains added to the lounge feed.</p>
            </div>
            <div class="formcon" style="display:none;"> 
                <h3> Emails From BudFolio</h3>
                    <dl>
                        <dt class="marpad_top0"> <input type="radio" <?php if($profiledata['receive_email_from_budfolio']=='yes'){ ?> checked="checked" <?php } ?> name="email_confirm" value="yes" id="email_confirm_yes" tabindex="7"> Yes </dt>
                        <dd class="marpad_top0"> <input type="radio" <?php if($profiledata['receive_email_from_budfolio']=='no'){ ?> checked="checked" <?php } ?> name="email_confirm" value="no" id="email_confirm_no" tabindex="8"> No</dd>
                    </dl>
                    <p class="martop10 fllft"> Allow budfolio.com to email you regarding site updates and promotions. Budfolio will not sell your email, or personal information to any 3rd parties for any use.</p>
            </div>
			<div class="formcon_last"> 
				<h3> Contact Budfolio</h3>
            	<dl>
            		<dt class="marpad_top0">
                		<input type="radio" name="feedbacktype" id="comment" value="comment"> Comment  
                	</dt>
            		<dd class="marpad_top0"> 
                		<span><input type="radio" name="feedbacktype" id="issue" value="issue"> Issue</span>&nbsp;&nbsp; &nbsp;<span> <input type="radio" name="feedbacktype" id="suggestion" value="suggestion" checked="checked"> Suggestion </span>
            		</dd>
            	</dl>
				<div class="textcon"><textarea id="feedbackarea" name="feedback" maxlength="1000"></textarea> </div>
                	 <div class="dbox_msg">1000 characters</div>
					<span class="marrit15"> <a href="javascript:void(0);" class="edit_btn" id="UpdateProfile"> Submit </a></span>
				</div>
			</form>
        	<!-- End User profile form-->
		</div>             
	<?php include_once("right_sidebar.php");?>
</div>
<?php include_once("footer.php");?>
<!-- Add validation file-->
<script type="text/javascript" src="js/profile_validation.js"></script>