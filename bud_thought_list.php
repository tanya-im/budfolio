<?php
/*
	File Name - bud thought list of a user 
	Date - 30 th april
	Name - Tanya Kumpawat
*/

// Include header,auth and config file. #C8C8C8
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include('function.php');
include_once("lounge_functions.php");
include_once("timeAgoUtil.php"); 
$_SESSION['loungecounter']=1;


$user_id = $_REQUEST['t'];
?>

<style>
a
{
	text-decoration:none;
}
</style>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container" id="inner_main_div">
 			<div class="cell_con">
            	<div class="gray_headingbar">
                	<div class="add_dis_btn" style="float:left;position:relative;right:0px;"> 
                    	<a style="float:left; margin-left: 13px;margin-top: 2px;" name="back_lounge" onclick="goOneStepBAck();" class="edit_btn" href="javascript:void(0);"> Back </a>
                	 </div>
                 <h2 style="width:77%;"> Bud thoughts </h2> </div>
                <div class="greenbar_bg"> </div>
               <div id="content_1" class="content">
                
<?php 
			$perpage = PerPageForLoungMyLocal;
			// Show lounge listing for all users
			$results = bud_thought_list_of_user(1,$user_id,$perpage);//function 
		 	$rows = mysql_num_rows($results);
		
		
			   	if($rows)
			   	{
					$i = 0;
                   	while($budThoughtdata = mysql_fetch_array($results))
                   	{	
					
					$date = funTimeAgo(strtotime($budThoughtdata['post_date']));$i++;
						include('notifications_bud_thought_list.php');
						
					}//end of while
				 }//end of if ($rows)
				 else
				 {
				 	echo '<div  id="pd30" class="cell center">No budthoughts found.</div>'; 		 
				 }
	?>
        </div>
	        </div>
        </div>
	<?php include('newLounge_right.php');?>
    </div>
</div>

<div class="footer"><?php include('footer.php');?>
</div>
<?php include('budfolio_footer.php');?>

<script type="text/javascript" src="js/row_col.js"></script>
<script type="text/javascript" src="js/row_col_for_edit.js"></script>
<script>
	(function($){
		$(window).load(function(){
			$('.scroll_container').scrollExtend(
				{	'target': 'div#content_1',
					'url': 'bud_thought_list_ajax.php?flag=1&user_id='+<?php echo $user_id;?>,
					'loadingIndicatorClass': 'scrollExtend-loading',
					'loadingIndicatorEnabled': true
				}
			);
		});
	})(jQuery);
	</script>
</body>

</html>
