<?php 
/* 
	*File dESCIPTION - external File for lounge functions and query
*/

/*
	Function Description - to get the list of lounge of all strains and 
					       bud thought of all users except the logged in user.
				
	Function name -	 loungeAllUsers			
*/
function loungeAllUsers($page,$userId,$perpage)
{
	$calc = $perpage * $page;
	$start = $calc - $perpage;
 	$queryone="SELECT strain_id, strain_name,dispensary_name,post_date,species,overall_rating,
			   strains.user_id as userId,NULL AS picture_url, 1 AS TYPE
			   FROM strains
			   INNER JOIN users ON users.user_id = strains.user_id
			   WHERE strains.flag = 'active'
			   AND users.flag ='active'
			   AND users.privacy = 'public'
			   UNION ALL
			   SELECT bud_thought_id, bud_thought, NULL AS dispensary_name,
			   post_date, NULL AS species,
			   NULL AS overall_rating,tbt.user_id as userId, picture_url, 0 AS TYPE
			   FROM tbl_bud_thought tbt
			   INNER JOIN users ON users.user_id = tbt.user_id
			   WHERE users.flag = 'active'
			   AND users.privacy = 'public'
			   AND 	delete_status=0
			   ORDER BY post_date DESC
			   Limit $start, $perpage";
		//echo	$queryone."<br>";		
    $results=mysql_query($queryone);
	return $results;
			  
}

/*
	Function Description - to get the list of lounge of strains and 
					       bud thought of followers of users except the logged in user.
				
	Function name -	 loungeUserFollowers			
*/
function loungeUserFollowers($page,$userId,$perpage)
{	
	$calc = $perpage * $page;
	$start = $calc - $perpage;
	$queryone = "SELECT strain_id, strain_name,dispensary_name,post_date,species,overall_rating,
				 strains.user_id as userId,NULL AS picture_url, 1 AS TYPE
				 FROM strains
				 INNER JOIN user_followup on user_followup.follower_id = strains.user_id
				 INNER JOIN users ON users.user_id = user_followup.follower_id 
				 WHERE strains.flag='active'
				 AND user_followup.user_id='".$userId."'
				 AND users.flag='active'
				 AND users.privacy = 'public'
				 UNION ALL
				 SELECT bud_thought_id, bud_thought, NULL AS dispensary_name, post_date, NULL AS species,
				 NULL AS overall_rating,tbt.user_id as userId, picture_url, 0 AS TYPE
				 FROM tbl_bud_thought tbt
				 INNER JOIN users ON users.user_id = tbt.user_id
			  	 INNER JOIN user_followup on user_followup.follower_id=tbt.user_id 
				 WHERE user_followup.user_id='".$userId."'
				 AND  users.flag = 'active' 
				 AND users.privacy = 'public'
				 AND delete_status=0
				 ORDER BY post_date DESC  
				 Limit $start, $perpage";
				 //echo $queryone;
    $results=mysql_query($queryone);
	return $results;
			  
}

/*
	Function Description - to get the list of lounge of strains and 
					       bud thought of followers of users except the logged in user.
				
	Function name -	 loungeLocalUser
*/
function loungeLocalUser($userIds)
{
	$queryone="SELECT strain_id, strain_name,dispensary_name,post_date,species,overall_rating,
			   strains.user_id as userId,NULL AS picture_url, 1 AS TYPE
			   FROM strains
			   INNER JOIN users ON users.user_id = strains.user_id
			   WHERE strains.flag = 'active'
			   and users.flag='active'
			   AND users.privacy = 'public'
			   AND strains.user_id in (".$userIds.")
			   UNION ALL
			   SELECT bud_thought_id, bud_thought, NULL AS dispensary_name,post_date,
			   NULL AS species,NULL AS overall_rating,tbt.user_id as userId,
			   picture_url, 0 AS TYPE
			   FROM tbl_bud_thought tbt
			   INNER JOIN users ON users.user_id = tbt.user_id
			   WHERE tbt.user_id in (".$userIds.")
			   AND  users.flag = 'active'
			   AND users.privacy = 'public'
			   AND delete_status=0
			   ORDER BY post_date DESC";

	$results=mysql_query($queryone);
	return $results;
			  
}
function loungeNotificationsList($page,$userId,$perpage)
{	
	$calc = $perpage * $page;
	$start = $calc - $perpage;
	$queryone="SELECT *
			   FROM `tbl_bud_thought`
			   WHERE bud_thought_id
			   IN (
				   SELECT bud_thought_id
				   FROM tbl_comments
				   WHERE `bud_thought` LIKE '%".$_SESSION['user_name']."%'
			   )
			   or `bud_thought` LIKE '%".$_SESSION['user_name']."%'
			   AND delete_status=0
			   ORDER BY post_date DESC 
			   Limit $start, $perpage";
	//echo $queryone."<br>";
	$results=mysql_query($queryone);
	return $results;
}

function loungePushNotificationList($page,$userId,$perpage)
{	
	$calc = $perpage * $page;
	$start = $calc - $perpage;
	$queryone="SELECT pn_id,user_id,sender_user_id,type,
		  			  type_id,notification_msg,date_time
		  	   FROM tbl_push_notification
			   WHERE 
			   user_id ='".$userId."'
			   AND delete_status='0'
			   ORDER BY date_time DESC Limit $start, $perpage";
//echo $queryone."<br>";
	$results=mysql_query($queryone);
	return $results;
}

function loungeHashTag($page,$strValidSearchtext,$perpage)
{
	$calc = $perpage * $page;
	$start = $calc - $perpage;
 	 $queryone="SELECT strain_id, strain_name,dispensary_name,post_date,species,overall_rating,
			   strains.user_id as userId,NULL AS picture_url, 1 AS TYPE
			   FROM strains
			   				  INNER JOIN users ON users.user_id = strains.user_id
			   				  WHERE strains.flag = 'active'
			                  AND users.flag ='active'
			                  AND privacy = 'public'
                              AND (strains.strain_name LIKE '%".$strValidSearchtext."%' 
							  || 
							  strains.dispensary_name LIKE '%".$strValidSearchtext."%'
							  ||
							  strains.linage LIKE '%".$strValidSearchtext."%' 
							  ||
							  strains.description LIKE '%".$strValidSearchtext."%'
							  ||
							  strains.tested_by LIKE '%".$strValidSearchtext."%')
			   UNION ALL
			   SELECT bud_thought_id, bud_thought, NULL AS dispensary_name,
			   post_date, NULL AS species,
			   NULL AS overall_rating,tbt.user_id as userId, picture_url, 0 AS TYPE
			  FROM tbl_bud_thought tbt
			   				INNER JOIN users ON users.user_id = tbt.user_id
			   				WHERE users.flag = 'active'
              				AND bud_thought like '%".$strValidSearchtext."%'
							AND delete_status='0'
			   				UNION 
			  				SELECT 
								bud_thought_id,
								bud_thought,
								NULL AS dispensary_name,
			  					post_date,
								NULL AS species,
			   					NULL AS overall_rating,
								tbt.user_id as userId,
								picture_url,
								0 AS TYPE
			   			 	FROM tbl_bud_thought tbt
			   				INNER JOIN users ON users.user_id = tbt.user_id
			   				WHERE users.flag = 'active' AND bud_thought_id
			  				IN (
						  		SELECT bud_thought_id
						  		FROM tbl_comments
						  		WHERE `bud_thought` LIKE '%".$strValidSearchtext."%'
			  				)
							AND delete_status='0'
							ORDER BY post_date desc
			   Limit $start, $perpage";
		//echo	$queryone."<br>";		
    $results=mysql_query($queryone);
	return $results;
			  

}



function bud_thought_list_of_user($page,$userId,$perpage)
{	
	$calc = $perpage * $page;
	$start = $calc - $perpage;
	$queryone="SELECT *
			   FROM `tbl_bud_thought`
			   WHERE 
			   user_id ='".$userId."'
			   AND delete_status='0' 
			   ORDER BY post_date DESC 
			   Limit ".$start.",".$perpage." ";
	//echo $queryone."<br>";
	$results=mysql_query($queryone) or die ($queryone." : ".mysql_error());
	return $results;

	
}
?>