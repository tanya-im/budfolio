<?php
/*
 * PHP function to change UTF8 strings into emojified strings.
 * 
 * -------------------------------------------------------------
 * File:     emojify.php
 * Name:     emojify
 * Purpose:  Turns a UTF8 string into emoji images
 * -------------------------------------------------------------
 */
function emojify($string) {
	$return_string;
   	$return_string = preg_replace("/([\\xC0-\\xF7]{1,1}[\\x80-\\xBF]+)/e", '_utf8_to_emojiimages("\\1")', $string);
	/*manage number and flag images*/
	$return_string = str_replace("020e3", '<img src="images/emoji/0030-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("120e3", '<img src="images/emoji/0031-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("220e3", '<img src="images/emoji/0032-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("320e3", '<img src="images/emoji/0033-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("420e3", '<img src="images/emoji/0034-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("520e3", '<img src="images/emoji/0035-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("620e3", '<img src="images/emoji/0036-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("720e3", '<img src="images/emoji/0037-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("820e3", '<img src="images/emoji/0038-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("920e3", '<img src="images/emoji/0039-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("#20e3", '<img src="images/emoji/0023-20e3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1e81f1f3", '<img src="images/emoji/1f1e8-1f1f3.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1e91f1ea", '<img src="images/emoji/1f1e9-1f1ea.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1ea1f1f8", '<img src="images/emoji/1f1ea-1f1f8.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1eb1f1f7", '<img src="images/emoji/1f1eb-1f1f7.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1ec1f1e7", '<img src="images/emoji/1f1ec-1f1e7.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1ee1f1f9", '<img src="images/emoji/1f1ee-1f1f9.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1ef1f1f5", '<img src="images/emoji/1f1ef-1f1f5.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1f01f1f7", '<img src="images/emoji/1f1f0-1f1f7.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1f71f1fa", '<img src="images/emoji/1f1f7-1f1fa.png" alt="emoji" class="emoji" />', $return_string);
	$return_string = str_replace("1f1fa1f1f8", '<img src="images/emoji/1f1fa-1f1f8.png" alt="emoji" class="emoji" />', $return_string);				
	return $return_string;
}

function _utf8_to_emojiimages($data) {
    $ret = 0;	
    foreach((str_split(strrev(chr((ord($data{0}) % 252 % 248 % 240 % 224 % 192) + 128) . substr($data, 1)))) as $k => $v)
        $ret += (ord($v) % 128) * pow(64, $k);
	/*check for the number and flag images*/
	if(dechex((int)$ret) != "20e3" && dechex((int)$ret) != "1f1e8" && dechex((int)$ret) != "1f1f3" && dechex((int)$ret) != "1f1e9" && dechex((int)$ret) != "1f1ea" && dechex((int)$ret) != "1f1ea" && dechex((int)$ret) != "1f1f8" && dechex((int)$ret) != "1f1eb" && dechex((int)$ret) != "1f1f7" && dechex((int)$ret) != "1f1ec" && dechex((int)$ret) != "1f1e7" && dechex((int)$ret) != "1f1ee" && dechex((int)$ret) != "1f1f9" && dechex((int)$ret) != "1f1ef" && dechex((int)$ret) != "1f1f5" && dechex((int)$ret) != "1f1f0" && dechex((int)$ret) != "1f1fa") {	
		
			return '<img src="images/emoji/' . dechex((int)$ret) .'.png" alt="emoji" class="emoji" />';
	} else {
		return dechex((int)$ret);	
	}
}
?>
