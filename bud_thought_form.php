<?php 
//session_start();
// Include header,auth and config file. #C8C8C8
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include_once("function.php");
require_once 'includes/mobileDetect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
include_once('includes/config.php');

?>
<style>
/*#E1E1E1*/

.ui-helper-hidden-accessible { display:none; }
.ui-autocomplete { 
    /* these sets the height and width */
	max-height:130px;

    /* these make it scroll for anything outside */
    overflow-y:auto;
	overflow-x:hidden;
}
#ui-id-1{
	background-color:#ccc;}
a {
    border: 0 none;
    font-size: 18px;
    outline: medium none;
}
a:hover{
	font-size: 18px;
	color:#090;
	}
.thumbWrapper
{
	height:0px;
}
.previewWindow
{
		height:0px;
}
#thumbnail
{
			height:0px;
}
</style>
<link rel="stylesheet" type="text/css" href="css/crop_css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/crop_css/imgareaselect-default.css" />
<link rel="stylesheet" type="text/css" href="css/crop_css/styles.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

<script type="text/javascript">
			var useMobile=<?php if($deviceType=='tablet'||$deviceType=='phone') { echo 'true';} else {echo 'false';}?>;
</script>


<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container" id="inner_main_div">
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2 style="width:60% float:right;">Bud Thought</h2> <a href="schedule_bud_thought_form.php" class="setting_btn" style="position:absolute;right:10px; top:10px;" >Schedule Post</a>
				</div>
        		<div id="content_1" class="content">
                
                <div id="budthoughterror"></DIV>
                <div class="budthought_popup"> 
                	
                    <!--start of bud thought image croping portion-->
            		<div class="wrapper" style="width:100%;">
                        <div class="uploader">

							<!-- first step upload image begin -->
							<div id="big_uploader">
								<form name="upload_big" id="upload_big" class="uploaderForm" method="post"
                             	enctype="multipart/form-data" action="upload.php?act=upload" target="upload_target">
									<div class="fileWrapper" style="width: 97.6%;">
										<a class="fileButton"><img src="images/bud_thought/add_photobtn.png" width="44" height="34"></a>
										<input name="photo" id="file" class="fileInput" size="27" type="file" />	
									</div>
									<input type="hidden" name="width" value="<?=$canvasWidth?>" />
									<input type="hidden" name="height" value="<?=$canvasHeight?>" />              
									<input type="submit" name="action" value="Upload Image now" class="inputSubmit" />
								</form>
								<div id="notice" class="notice" >Uploading...</div>
							</div>
						<!-- first step upload image end -->
				
						<div class="content">
					
                          <!-- second step selection begin -->
                          <div id="uploaded">
                          
                          	<div id="div_upload_big" style="width:<?=$bigWidthPrev?>px;height:<?=$bigHeightPrev?>px;"></div></div>
                        <!--  <div style="border:1px solid #CCC;width:100%;float:left;" id="preview_thumb">-->
                              <!-- preview the selection begin -->
                              <div class="previewWindow" style="float:left;width:46%;border-right:1px solid #CCC;visibility:hidden;">
                                <strong>Selection preview</strong>
                                <div class="previewWrapper" style="width:<?=$bigWidthPrev/2?>px;height:<?=$bigHeightPrev?>px;">
                                      <div id="preview"></div>
                                </div>
                              </div>
                             <!-- preview the selection end -->
                          
                              
                          
                          <!-- second step selection end -->
                          
                          
                          
                          <!-- third step generated thumb begin -->
                          <div id="thumbnail" style="float:left;width:46%;">
                              <div class="thumbWrapper" style="width:<?=$bigWidthPrev/2?>px;height:<?=$bigHeightPrev?>px;visibility:hidden;">
                                 <strong> Generated thumbnail</strong>
                                  <div id="div_upload_thumb"></div>
                              </div>
                          
                          </div>
                          <!-- third step generated thumb end -->
                          
                          
                          <div class="uploadThumbWrapper">
                              <?php if($deviceType=='tablet'||$deviceType=='phone') { ?>
                              <div class="mobileSelection">
                                  <a id="selLeft">left</a>
                                  <a id="selRight">right</a>
                                  <a id="selUp">up</a>
                                  <a id="selDown">down</a>
                                  <a id="selResize">bigger</a>
                                  <a id="selResizeSmall">smaller</a>
                              </div>
                              <?php } ?>
                              <form name="upload_thumb" id="upload_thumb" class="uploaderForm" method="post" action="upload.php?act=thumb" target="upload_target">
                                  <input type="hidden" name="img_src" id="img_src" class="img_src" /> 
                                  <input type="hidden" name="height" value="0" id="height" class="height" />
                                  <input type="hidden" name="width" value="0" id="width" class="width" />
                                  <input type="hidden" id="y1" class="y1" name="y" />
                                  <input type="hidden" id="x1" class="x1" name="x" />
                                  <input type="hidden" id="y2" class="y2" name="y1" />
                                  <input type="hidden" id="x2" class="x2" name="x1" />                         
                                  <input type="submit" id="save_cropped" value="Save Cropped image " class="create_thumb" style="height:37px;" />
                              </form>
                              <div id="notice2" class="notice">Cropped image saving in progress...</div>
                          </div>
					<!--	/*</div> */-->
                          
					
						</div>
				
				<!-- hidden iframe begin -->
				<iframe id="upload_target" name="upload_target" src=""></iframe>
				<!-- hidden iframe end -->

			</div>

		</div>
              	
                <div class="bud_textarea ui-widget"><!--ui-widget -->	
            		<!--<div id="tags" name="budThoughtText" >
                    </div>-->
              	<textarea placeholder="Enter bud thought here..." tabindex="1" id="tags" name="budThoughtText" maxlength="200" cols="100" rows="3"></textarea> 
                <!--<input id="suggestionfield" type="hidden" />-->
                </div>
                <div class="character_limit">200 Character</div>
                <div class="add_postbtn" style="width:57%;">
                		<!--<a id="addBudThoughtImg" href="javascript:void(0);" class="fllft"> <img src="images/bud_thought/add_photobtn.png" width="44" height="34"></a>-->
                        <!-- share buttons -->
                        
                         <?php include_once("fb_budthought_share.html"); ?>
                         <?php include_once("twitter.php"); ?>
                        
                        <!-- share buttons -->
						<a href="javascript:void(0);"  id="budthoughtbtn"> <img src="images/bud_thought/post_btn.png" width="74" height="34"> </a>
                </div>
			</div></div>
           	</div>
        </div>    
    
 <?php include('newLounge_right.php');?>
</div>
  </div>
<?php $strFollowingsListQuery = "SELECT user_name
							  FROM users";
   $FollowingsListSQL = mysql_query($strFollowingsListQuery) or die($strFollowingsListQuery." : ".mysql_error());			       //executing strLoginSQL
    while($row = mysql_fetch_assoc($FollowingsListSQL)){
		$val[] = $row['user_name'];
	}
?>

<div class="footer"><?php include('footer.php');?></div>

<script>

$(function() {
  var availableTags=Array();

<?php 
	$i=0;
	foreach($val as $k=>$v){
	?>availableTags[<?php echo $i;?>]="<?php echo stripslashes($v);?>";
	<?php $i++;
}
?>

function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}

$('#tags').bind( "keypress", function( event ) {
	if(event.keyCode == 40) {
		console.log("this : "+this);
	}/*if(event.keyCode == 40) {*/
});
$('#tags').bind( "keydown", function( event ) {
	if ( event.keyCode === $.ui.keyCode.TAB &&
	$( this ).data( "ui-autocomplete" ).menu.active ) {
	event.preventDefault();
	}
}).triggeredAutocomplete({
	source: function( request, response ) {
		//var result=$.ui.autocomplete.filter(availableTags, extractLast( request.term ) );
		//response(result.slice(0,5));
		response($.ui.autocomplete.filter(availableTags, extractLast( request.term )));
	},
	focus: function (event, ui) {
		event.preventDefault(); 
		$(this).val(ui.item.suggestion);
	},
	select: function( event, ui ) {
        document.location.href = ui.item.url;
    },
    context: this,
	open: function (event, ui) {
         $(".ui-menu").css("width" ,"200px");
    }
	});

});
</script>
	<script type="text/javascript" src="js/crop_js/jquery.imgareaselect.min.js"></script>
		<script type="text/javascript" src="js/crop_js/effects.js"></script>
		<?php if($deviceType=='tablet'||$deviceType=='phone') { ?>
		<script type="text/javascript" src="js/crop_js/touch.js"></script>
		<?php } ?>
<?php include('budfolio_footer.php');?> 

	