<?php
// Include header,auth and config file.
include_once("newHeader.php");
include_once("config.php");
include_once("function.php");
include_once("lounge_functions.php");
$_SESSION['strainLibCounter']=1; 
$_SESSION['searchtext']='';
?>
<style>
h4
{
	color:#000000;
}
.inn_cell
{
min-height:45px;
}
#boxes .window {
height:383px;

}
.inn_cell img.scale-with-grid {
   /* height: auto;
    margin-left: 40%;
    margin-top: 34px;
    max-width: 100%;*/
}

#strain_lib_popup .window{
    width: 60%;
}

</style>

<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	
    	   <div class="inner_conlft scroll_container">
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2>Strain Library </h2> </div>
				<div class="greenbar_bg"> 
                	<form>
                    	<div class="grenlft_con">
                        
                      	<div class="input-append"> <div class="searchbox"> <img src="images/new_web/serach_icon.png" class="scale-with-grid"> <input type="text" id="sl_search_text" name="sl_search_text"> </div> <a href="javascript:void(0);" class="searchSL search_btn pushtop9"> Search </a> </div>
                        </div>
                    	
                   </form> 
                </div>
       			<div id="content_1" class="content lounge">
		<?php 
			
			include('strain_liberary_content.php');
		?>
    </div>
            </div>
          </div>
        <!-- Inlude Right sidebar--> <?php include('newLounge_right.php');?><!-- End Inlude Right sidebar-->
    </div>
</div>
<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>
<script>
	(function($){
		$(window).load(function(){
			/*$("#content_1").mCustomScrollbar({
				scrollButtons:{
					enable:true
				}
			});*/
		$('.scroll_container').scrollExtend(
		{	'target': 'div#content_1',
			'url': 'strain_liberary_content.php?flag=1',
			'loadingIndicatorClass': 'scrollExtend-loading',
			'loadingIndicatorEnabled': true
		}
		);
		});
	})(jQuery);
</script>
<div id="boxes"></div>
<div id="mask"></div> 
</body>

</html>
