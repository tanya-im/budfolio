<?php
// Include header,auth and config file.
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include('function.php');
$_SESSION['searchtext']='';
$_SESSION['datacount']='';
?>
<link rel="stylesheet" href="css/jquery-ui.css" />
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/organictabs.jquery.js"></script>

<script type="text/javascript">
// Run the script on DOM ready:
/*$(function(){
	$('input').customInput();
});*/
</script>
<style>
a
{
	text-decoration:none;
}
</style>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft">
 			<div class="cell_con">
               <div class="gray_headingbar"> <h2> Dispensary List</h2> 
                <?php if($_SESSION['dispansary']=='1') {?><div class="add_dis_btn"> <a href="javascript:void(0);" class="edit_btn open_add_dis" name="add_dispensary"> Add Dispensary </a></div> <?php  } ?></div>
               <div class="greenbar_bg"><?php include('menu_fillters.php');?> </div>
               <div id="content_1" class="content">
   					<?php include('local_menu_content.php');?>
			   </div>
		    </div>
        </div>
	    <?php include('newLounge_right.php');?>
    </div>
</div>
<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>

<script type="text/javascript" src="js/row_col.js"></script>
<script type="text/javascript" src="js/row_col_for_edit.js"></script>
<script>
	(function($){
		$(window).load(function(){
			$("#content_1").mCustomScrollbar({
				scrollButtons:{
					enable:true
				}
			});
		});
	})(jQuery);
	</script>
    <script>
$(function() {
	$( "#tabs" ).tabs({
						beforeLoad: function( event, ui ) {
ui.jqXHR.error(function() {
ui.panel.html(
"Couldn't load this tab. We'll try to fix this as soon as possible. " +
"If this wouldn't be a demo." );});}});
});
</script>
<div id="boxes"></div>
<div id="mask"></div> 
</body>

</html>
