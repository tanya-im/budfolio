<?php
// Include header,auth and config file.
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include_once("function.php");
include_once("timeAgoUtil.php");
include_once("lounge_functions.php"); 
include_once("check_subscription_for_lounge.php");
$_SESSION['loungecounter']=1;
$_SESSION['searchtext']='';
?>
<style>
.content
{
	margin-top:15px;
}

.error_msg_all
{
	margin-top:4px;
}

.msg_cell {
    border-bottom: 0px solid #C8C8C8;
    float: left;
    margin: -1px 0 5px;
    padding: 5px 2%;
    width: 96%;
}
</style>

<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    
      <?php
	if($_SESSION['dispansary']==1)
	{	
		if($timeDiffrence > 0 and $subTYpe=='d')
		{
				$isSubscription='1';
	?>
                <div id ="scroll" style="color:#2B6A03;margin-bottom: 6px;
    margin-top: 0;"><marquee scrolldelay="200">14-day trial is active:<a href="check_out.php"> Click here to upgrade </a> and continue sharing photos, specials, and more in the Lounge</marquee></div>
                <?php 
		}
		else if ($timeDiffrence < 0 and $subTYpe=='d' )
		{
				$isSubscription='0';		
					
	?>
				<div id ="scroll" style="color:#2B6A03;margin-bottom: 6px;margin-top: 0;"> <marquee scrolldelay="200"> Your trial period has ended:<a href="check_out.php"> Click here to upgrade</a> and continue sharing photos, specials, and more in the Lounge</marquee></div>
	<?php
        }
		else if ($timeDiffrence < 0 and $subTYpe!='d' ){
		$isSubscription='0';		
					
	?>
				<div id ="scroll" style="color:#2B6A03;margin-bottom: 6px;margin-top: 0;"> <marquee scrolldelay="200"> Your subscription has ended:<a href="check_out.php"> Click here to upgrade</a> and continue sharing photos, specials, and more in the Lounge</marquee></div>
	<?php
        }
	}// end of $_session['dispensary']
		?>
    
    
    
    	<div class="inner_conlft scroll_container">
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2> Lounge</h2>  
                
                  <div class="refresh_button"> <a href="javascript:void(0);" onClick="window.location.reload()"> <img src="images/new_web/bf_refresh_btn.png" class="scale-with-grid"> </a> </div>
                    
               <?php 
			    if(($_SESSION['dispansary']==1) and !empty($_SESSION['user_id']))  
             	 { 
				 
				 ?>   
                 <div class="budthough_btn"> <a <?php if($timeDiffrence>0) { ?> href="bud_thought_form.php" <?php }else{ ?>
                 href="javascript:void(0);" onclick="subscription_over_alert();" <?php }   ?>> <img src="images/new_web/budthought_btn.png" class="scale-with-grid"> </a> </div>
               <?php }
			   else if  ($_SESSION['user_id'] and $_SESSION['dispensary']!=1)
			   {
				   
				?>
                <div class="budthough_btn"> <a href="bud_thought_form.php"><img src="images/new_web/budthought_btn.png" class="scale-with-grid"> </a> </div>
			   <?php
               }
			   ?></div>
                <div class="greenbar_bg"><?php include('Lounge_Filter.php');?> </div>
                <div class="greenbar_bg_new"><div class="input-append"> <div class="searchbox" style="width:626px;"> <img src="images/new_web/serach_icon.png" class="scale-with-grid"> <input type="text" id="sl_search_text" name="sl_search_text" placeholder="Username, strain, hashtags"> </div> <a href="javascript:void(0);" id="search_here"  class="pushtop9 search_btn <?php if($filename=='outerLounge.php' || $filename=='newLounge.php'){?>lounge_search <?php }else if($filename=='loungeFollowupList.php'){?>loungeFollowSearch<?php  }else {?> loungeMessageSearch <?php } ?>" > Search <!--<img src="images/new_web/search_btn.png" class="scale-with-grid">--> </a> </div></div>
                <div id="content_1" class="content">
	<?php 
	// Show lounge listing for all users
	$perpage = PerPageForLoungMyLocal;
	$results = loungePushNotificationList(1,$_SESSION['user_id'],$perpage);//function 
	$rows = mysql_num_rows($results);
	if($rows)
	{
		$i = 0;
        while($LoungeList = mysql_fetch_array($results))
        {	
			$i++;
			$date = funTimeAgo(strtotime($LoungeList['date_time']));
			
			$querythree="select user_name,zip_code,state,photo_url 
					 from users 
					 where user_id='".$LoungeList['sender_user_id']."' 
					 limit 0,1";
			$userdata=mysql_query($querythree);
			$udata=mysql_fetch_array($userdata);
		
			if(!empty($udata['photo_url']))
        	{
				$img_src='images/profileimages/original/'.$udata['photo_url'];
			}
			else
			{ 
				$img_src="images/css_images/profile_pic.png";
			}
			
			if($LoungeList['type']==0 || $LoungeList['type']==2 || $LoungeList['type']==3 || $LoungeList['type']==4 )
			{
				$href="loungePushNotificationItem.php?t=".$LoungeList['type']."&tid=".$LoungeList['type_id'];
			}// end of if (type==0|2|3|4)
			else if ($LoungeList['type']==1) // type==1 -> like strain 
			{
				$href="loungePushNotificationItem.php?t=".$LoungeList['type']."&tid=".$LoungeList['type_id'];			
			}
			else//type=5 -> follow user
 			{
					$href="user_popup_page.php?userId=".$LoungeList['sender_user_id'];
			}
			
			
		?>
		<div class="inn_cell" id="budthought_<?php echo $LoungeList['type_id'];?>">
       		 <div class="gray_bg_cellnew"> 
             	<span class="lounge_profile"> 
                	<img src="<?php echo $img_src;?>" class="scale-with-grid" >
                </span>
                <span class="name_link"><a href="user_popup_page.php?userId=<?php echo $LoungeList['user_id'];?>" id="userids_<?php echo $LoungeList['user_id'];?>"> <?php echo truncatestr($udata['user_name'],20);?></a></span> 
                <span class="datetime"> <?php echo $date;?> ago</span>
             </div>
             <div class="msg_cell"><a href="<?php echo $href;?>"><span class="user_comment"><?php echo $LoungeList['notification_msg'];?></span> </a></div>
		</div>
		<?php }//end of while
	}
	else{
			echo '<div class="cell"><center style="padding-top:25px;">No result found.</center></div>';
	}?>
	</div>            	
    <!--    End loung elisting -->
			</div>
    	</div>
    	 <?php include('newLounge_right.php');?>
	</div>
</div>
<div class="footer"></div>
<?php include('budfolio_footer.php');?>

<script>
	(function($){
		$(window).load(function(){
			/*$("#content_1").mCustomScrollbar({
				scrollButtons:{
					enable:true
				}
			});*/
			$('.scroll_container').scrollExtend(
			{	
			'target': 'div#content_1',
			'url': 'loungePushNotificationAjax.php',
			'loadingIndicatorClass': 'scrollExtend-loading',
			'loadingIndicatorEnabled': true
			});
		});
	})(jQuery);

$(document).ready(function() {	

	$("#searchtext").live('keyup',function(){
		$("#searcherror").html("&nbsp;");
	});
	

});
</script>
<script type="text/javascript" src="js/loungeComment.js"></script>
<div id="boxes"></div>
<div id="mask"></div> 
</body>

</html>