<?php
/*
	File Description - file to show user profile
	File Name - showuserprofileAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	$intUserId = trim($inputArray['UserId']);
	
	
	// Input validations
	if(!empty($intUserId))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
	// Check user id is valid or not		
		$uservalidation=CheckValidUser($intValidUserId);
		if(!$uservalidation)
		{
			$error = array('success' => "0", "msg" => "Invalid user id.");
			$this->response($this->toJson($error), 406, "application/json");
		}
		$GetUserDetailQuery="select * from users where user_id='".$intValidUserId."'";
		$GetUserDetailSql=mysql_query($GetUserDetailQuery)or die(mysql_error());
		$userdetail=mysql_fetch_array($GetUserDetailSql);
		
		

		// Set image url
		if(!empty($userdetail['photo_url']))
		{		
			$thumbimagesurl= BASEURL.$userprofileimagefoldername_thumbnail.$userdetail['photo_url'];
			$orgimagesurl= BASEURL.$userprofileimagefoldername_original.$userdetail['photo_url'];
		}
		else
		{
			$thumbimagesurl='';
			$orgimagesurl='';
		}

		$settingArray= array();

		if($userdetail['user_type']==2){ //if user type == dispensary user
		
			$getUserSettings ="SELECT * FROM user_settings WHERE user_id='".$intUserId."'";

			$getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());

			$settingData = mysql_fetch_assoc($getSettingsResponse);
			$settingArray=array(
						'DeliveryService'=>$settingData['DeliveryService']!=''?$settingData['DeliveryService']:'0',
				'StoreFront'=>$settingData['StoreFront']!=''?$settingData['StoreFront']:'0',
				'AcceptCreditCard'=>$settingData['AcceptCreditCard']!=''?$settingData['AcceptCreditCard']:'0',
				'AcceptATMonSite'=>$settingData['AcceptATMonSite']!=''?$settingData['AcceptATMonSite']:'0',
				'18YearsOld'=>$settingData['18YearsOld']!=''?$settingData['18YearsOld']:'0',
				'21YearsOld'=>$settingData['21YearsOld']!=''?$settingData['21YearsOld']:'0',
				'HandicapAssesseble'=>$settingData['HandicapAssesseble']!=''?$settingData['HandicapAssesseble']:'0',
				'Security'=>$settingData['Security']!=''?$settingData['Security']:'0'	
										  );
					
		}else
		{
						$settingArray=array(
											  'DeliveryService'=>'0',
											  'StoreFront'=>'0',
											  'AcceptCreditCard'=>'0',
											  'AcceptATMonSite'=>'0',
											  '18YearsOld'=>'0',
											  '21YearsOld'=>'0',
											  'HandicapAssesseble'=>'0',
											  'Security'=>'0'
										  );
		}


		$UserDetailArray[] =  array(
										'UserId'=> $userdetail['user_id'],
										'UserName'=>$userdetail['user_name'],
										'UserType'=>$userdetail['user_type'],
										'Email'=>$userdetail['email_address'],
										'ZipCode'=> $userdetail['zip_code'], 
										'city'=>$userdetail['city'], 
										'State'=> $userdetail['state'],
										'Privacy'=> $userdetail['privacy'],
										'Bio'=> $userdetail['bio'],
										'Country'=> $userdetail['country'],
										'EmailsFromBudfolio'=>$userdetail['receive_email_from_budfolio'],
										'ProfileImagethumb'=>$thumbimagesurl,
										'ProfileImageoriginal'=>$orgimagesurl,
										'settings'=>$settingArray
										
								);// end of array
		//if every thing is good then return  success=1 and Profiledetail
		$result = array( "success" => "1", "Profiledetail" => $UserDetailArray);
		$this->response($this->toJson($result),200,"application/json");
	}
	else
	{
		// If Empty user id
		$error = array('success' => "0", "msg" => "Empty user id.");
		$this->response($this->toJson($error), 406, "application/json");
	}
?>