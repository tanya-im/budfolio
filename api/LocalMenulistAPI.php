<?php
/*
	File Description - Local Menu List
	File Name - LocalMenulistAPI.php
*/
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	
	// Input validations
	if(!empty($intUserId) && !empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
	// Check user id is valid or not		
		$uservalidation=CheckValidUser($intValidUserId);
		if($uservalidation)
		{
			// Get local menu list within the 15 or 20 miles radious			
			$localusersid=get_local_menu_dispenaries($intValidUserId);
			$countlocaluser=count($localusersid);
			if($countlocaluser>0)
			{	
				$dispds='';
				for($l=0;$l<$countlocaluser;$l++)
				{
					$dispds.=$localusersid[$l]['dispensary_id'];
					if($l<($countlocaluser-1))
					{
						$dispds.=',';
					}
				}
				$perpage =$intPageSize;
				$page=$intPageNo;
				$calc = $perpage * $page;
				$start = $calc - $perpage;
				
				// Sql to count record no. 				
				$StrDispnoQuery="select Count(*) As Total 
								 from dispensaries 
								 where dispensary_id in (".$dispds.") and flag='Active'";

				$DispnoSql = mysql_query($StrDispnoQuery)or die(mysql_error());
				$DipnoResult=mysql_fetch_array($DispnoSql)or die(mysql_error());
				$DispnoData=$DipnoResult[0];
				$totalPages = ceil($DispnoData / $intPageSize);
				
				//SQL query to get Lounge list
				$strMenuListQuery = "select dispensary_id,dispensary_name,city,state,zip,image,
									latitude,longitude,phone_number,added_by
									from dispensaries 
									where flag='Active' 
									and dispensary_id in (".$dispds.") 
									group by dispensary_name
									order by dispensary_name Limit $start, $perpage";
				$arrayMenuListArray=array();
				$MenuListSQL = mysql_query($strMenuListQuery) or die(mysql_error());
				if(mysql_num_rows($MenuListSQL) > 0)
				{
					while($DispensaryList=mysql_fetch_array($MenuListSQL))
					{
						
						if($DispensaryList['added_by']!=0){
					$getUserSettings ="SELECT * FROM user_settings 
									   WHERE user_id='".$DispensaryList['added_by']."'";

				  $getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());
	  
				  $settingData = mysql_fetch_assoc($getSettingsResponse);
				  $settingArray=array(
											  'DeliveryService'=>$settingData['DeliveryService'],
											  'StoreFront'=>$settingData['StoreFront'],
											  'AcceptCreditCard'=>$settingData['AcceptCreditCard'],
											  'AcceptATMonSite'=>$settingData['AcceptATMonSite'],
											  '18YearsOld'=>$settingData['18YearsOld'],
											  '21YearsOld'=>$settingData['21YearsOld'],
											  'HandicapAssesseble'=>$settingData['HandicapAssesseble'],
											  'Security'=>$settingData['Security']
										  );
					
					}else
					{
						$settingArray=array(
											  'DeliveryService'=>'0',
											  'StoreFront'=>'0',
											  'AcceptCreditCard'=>'0',
											  'AcceptATMonSite'=>'0',
											  '18YearsOld'=>'0',
											  '21YearsOld'=>'0',
											  'HandicapAssesseble'=>'0',
											  'Security'=>'0'
										  );
					}
					
						
					if(!empty($DispensaryList['image']))
					  {
							$DispensaryImageUrl=BASEURL.$dispensaryimagefoldername_original.$DispensaryList['image'];
							$DispensaryThumbImageUrl=BASEURL.$dispensaryimagefoldername_thumbnail.$DispensaryList['image'];
					  }
					  else
					  {
						  $DispensaryImageUrl='';
						  $DispensaryThumbImageUrl='';
					  }
					
					  // Fet followup flag
					  $FWPflag=getFollowUpList($DispensaryList['dispensary_id'],$intValidUserId);
					  
					  // Prepare array for response						
					  $arrayDispensaryListArray[] =array(
												 	'DispensaryId'=> $DispensaryList['dispensary_id'],
													'DispensaryName'=> $DispensaryList['dispensary_name'], 																                                                    'City'=> $DispensaryList['city'],
													'State'=> $DispensaryList['state'],
													'ZipCode'=> $DispensaryList['zip'],
													'flag'=>$FWPflag,
													'DispensaryImageUrl'=> $DispensaryImageUrl,
													'DispensaryThumbImageUrl'=>$DispensaryThumbImageUrl,
													'PhoneNumber'=>$DispensaryList['phone_number'],
													'settings'=>$settingArray
												);// end of array
					}
// If success everythig is good send header as "OK" and MenuDispensaryList list in reponse
					$result = array('success' => '1','TotalPageNo'=>$totalPages,'MenuDispensaryList' => $arrayDispensaryListArray,);
					$this->response(stripslashes($this->toJson($result)),200,"application/json");
				}	
				else
				{
					// If Empty Dispensary List
					$error = array('success' => "0", "msg" => "Empty Menu Dispensary List.");
					$this->response($this->toJson($error), 400,"application/json");
				}
			}
			else
			{
				// Empty Dispensary List
				$error = array('success' => "0", "msg" => "Empty Local Menu List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>