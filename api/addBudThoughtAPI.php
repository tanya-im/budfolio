<?php
/*pro

	File Description - File to add bud thought for a user 
	File Name - addBudThoughtAPI.php
	creation date - 7th may
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
/*	$arr = array ("user_id"=>"1",
				  "bud_thought"=>"this is trial",
				  "bud_thought_id"=>"","flag"=>"1");
	 echo json_encode($arr);
	 die;*/
	
	//converting that json into array
 	$intUserId = $_REQUEST['UserId'];		
	$strBudThought = $_REQUEST['bud_thought'];
 	$intBudThoughtId = $_REQUEST['bud_thought_id'];

	// Input validations
	if(!empty($intUserId))
	{
		$date = date('Y-m-d H:i:s');
		$strValidBudThought = mysql_real_escape_string($strBudThought);
		
		// for Uploading image
		$strValidPictureURL ='';
		// for insertiing/updating profile pic
		if(isset($_FILES['budThoughtPicture']['name'])){//if profile picture is sent
			  
			$imagefile = $_FILES['budThoughtPicture']['name'];//taking only the base file name
				  
			$strGetImgName = explode('.',$imagefile);// for extracting the extension of the doc/image

			// 12 aug
			$extention = explode(".", $imagefile);
			$arrayconunt=(count($extention)-1);
			if(!empty($extention[$arrayconunt]))
			{
				$ext = $extention[$arrayconunt];
			}
			//12 aug

			$fileName = time().rand()."_".$intUserId.".".$ext; //appending file with current time
				  
			$fileTempName_one = $_FILES['budThoughtPicture']['tmp_name'];
			
			$bpath='../images/budthought/'.$fileName;
			
			if(move_uploaded_file($fileTempName_one,$bpath)){
				
				$strValidPictureURL = $fileName;
				//21st june
				$dest_t='../images/budthought/thumbnail/'.$fileName;
				resize_image($bpath,$dest_t,100,100);
				//21st june

			}else
			{
				// If no records such records available 
				$strError = array("success" => "0" ,"msg" => "Bud thought image does not uploaded.");
				$this->response($this->toJson($strError), 204,"application/json");
			}
		 }
		
		if ($intBudThoughtId==0){
			$table_name = "tbl_bud_thought";
			$appendFieldInQuery = "post_date='".$date."'";
		}else{
			$table_name = "tbl_comments";
			$appendFieldInQuery = "create_date_time='".$date."', bud_thought_id ='".$intBudThoughtId."'";
		}
		
		//SQL query to insert budThought into the Db
		$strAddBudThoughtSQL = " INSERT INTO $table_name 
									  set user_id = '".$intUserId."',
									  bud_thought = '".$strValidBudThought."',
									  picture_url=  '".$strValidPictureURL."',
									  $appendFieldInQuery
									  ";
								  
		$sql = mysql_query($strAddBudThoughtSQL)or die(mysql_error());//executing strLoginSQL
		
		$budThoughtId = mysql_insert_id();// inserted bud thought id
		
		if($sql == 1)
		{
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1","picture_url" => BASEURL.BTIMGPATH.$strValidPictureURL,"bud_thought_id"=>$budThoughtId);
			$this->response($this->toJson($result),200,"application/json");

		} // closing mysql_num_rows
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "Bud thought not inserted.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	} //end of input validations
	else
	{
		// If either username or passowrd is empty
		$error = array('success' => "0", "msg" => "Please enter user id.");
		$this->response($this->toJson($error), 400,"application/json");
	}
?>