<?php
/*
	File Description - Follow UnFollow Strain
	File Name - FollowUpLoungeAPI.php
*/

// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		$this->response('Invalid request',406);
	}
//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	$intFollowerId = $inputArray['FollowerId'];
	$intFlag = $inputArray['Flag'];		
	
// Input validations
	if(!empty($intUserId) && !empty($intFollowerId))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		$intFollowerId = mysql_real_escape_string($intFollowerId);
		$intValidFlag = mysql_real_escape_string($intFlag);
//SQL query to check the  USER exists or not
		$strUserNameSQL = "SELECT user_name FROM users WHERE user_id ='".$intValidUserId."'";
		$sql = mysql_query($strUserNameSQL)or die(mysql_error());//executing strUserNameSQL
		if(mysql_num_rows($sql) > 0)
		{
			$CheckStrainIdQuery = "SELECT user_name FROM users WHERE user_id ='".$intFollowerId."'";
			$CheckStrainIdSql = mysql_query($CheckStrainIdQuery)or die(mysql_error());//executing strUserNameSQL
			if(mysql_num_rows($CheckStrainIdSql) > 0)
			{
				if($intFlag==1)
				{
					$CheckFollowupQuery = "select * from user_followup where follower_id='".$intFollowerId."' and user_id='".$intValidUserId."' ";
					$CheckFollowupSql = mysql_query($CheckFollowupQuery)or die(mysql_error());//executing strUserNameSQL	
					$RecordStatus=mysql_num_rows($CheckFollowupSql);
					if($RecordStatus==0)
					{
						$FollowupQuery = "insert into user_followup(user_id,follower_id)values('".$intValidUserId."','".$intFollowerId."')";
						$FollowupSql = mysql_query($FollowupQuery)or die(mysql_error());//executing strUserNameSQL
					}
					//if every thing looks good then success=1 and user id
					$result = array( "success" => "1", 'UserId' => $intValidUserId,"Status"=>"Follow");
					$this->response($this->toJson($result),200,"application/json");
				}
				else
				{
					$FollowupQuery = "delete FROM user_followup WHERE user_id ='".$intValidUserId."' and follower_id='".$intFollowerId."'";
					$FollowupSql = mysql_query($FollowupQuery)or die(mysql_error());
					//executing strUserNameSQL
					//if every thing looks good then success=1 and user id
					$result = array( "success" => "1", 'UserId' => $intValidUserId,"Status"=>"UnFollow");
					$this->response($this->toJson($result),200,"application/json");
				}
			}
			else
			{
				// If Invalid strain id
				$error = array('success' => "0", "msg" => "Invalid Follower id.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or FollowerId  empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>