<?php
/*
	File Description - Follow UnFollow Menu
	File Name - FollowUpMenuAPI.php
*/

// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		$this->response('Invalid request',406);
	}
//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	$intMenuId = $inputArray['MenuId'];
	$intFlag = $inputArray['Flag'];		
	
// Input validations
	if(!empty($intUserId) && !empty($intMenuId))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		$intValidMenuId = mysql_real_escape_string($intMenuId);
		$intValidFlag = mysql_real_escape_string($intFlag);
//SQL query to check the  USER exists or not
		$strUserNameSQL = "SELECT user_name FROM users WHERE user_id ='".$intValidUserId."'";
		$sql = mysql_query($strUserNameSQL)or die(mysql_error());//executing strUserNameSQL
		if(mysql_num_rows($sql) > 0)
		{
			$CheckMenuIdQuery = "SELECT dispensary_name FROM  dispensaries WHERE dispensary_id ='".$intValidMenuId."'";
			$CheckMenuIdSql = mysql_query($CheckMenuIdQuery)or die(mysql_error());//executing strUserNameSQL
			if(mysql_num_rows($CheckMenuIdSql) > 0)
			{
				if($intFlag==1)
				{
					$CheckFollowupQuery = "select * from menu_followup where dispensary_id='".$intValidMenuId."' and user_id='".$intValidUserId."' ";
					$CheckFollowupSql = mysql_query($CheckFollowupQuery)or die(mysql_error());//executing strUserNameSQL	
					$RecordStatus=mysql_num_rows($CheckFollowupSql);
					if($RecordStatus==0)
					{
						$FollowupQuery = "insert into menu_followup(dispensary_id,user_id)values('".$intValidMenuId."','".$intValidUserId."')";
						$FollowupSql = mysql_query($FollowupQuery)or die(mysql_error());//executing strUserNameSQL
					}
					//if every thing looks good then success=1 and user id
					$result = array( "success" => "1", 'UserId' => $intValidUserId,"Status"=>"Follow");
					$this->response($this->toJson($result),200,"application/json");
				}
				else
				{
					$strUserNameSQL = "delete FROM menu_followup WHERE user_id ='".$intValidUserId."' and dispensary_id='".$intValidMenuId."'";
					$sql = mysql_query($strUserNameSQL)or die(mysql_error());//executing strUserNameSQL
					//if every thing looks good then success=1 and user id
					$result = array( "success" => "1", 'UserId' => $intValidUserId,"Status"=>"UnFollow");
					$this->response($this->toJson($result),200,"application/json");
				}
			}
			else
			{
				// If Invalid strain id
				$error = array('success' => "0", "msg" => "Invalid Menu id.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or MenuId empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>