<?php
/*
	File Description - Medicinal use list
	File Name - medicinaluseAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	$intUserId = $inputArray['UserId'];		
// Input validations
	/*if(!empty($intUserId))
	{*/
		$intValidUserId = mysql_real_escape_string($intUserId);
		// to check whether the user id is exist.
		if($intValidUserId!=0)
		{
			$uservalidation=CheckValidUser($intValidUserId);
		}
		/*if($uservalidation)
		{*/
			if(is_array($medicinaluse) && !empty($medicinaluse))
			{
				$arrayMedicinalUse=array();
				for($i=0;count($medicinaluse)>$i;$i++)
				{   
					$arrayMedicinalUse[$i]=$medicinaluse[$i];
				}
				sort($arrayMedicinalUse,SORT_STRING);
				// If success everythig is good send header as "OK" and medicinal use list in reponse
				$result = array('success' => '1','MedicinalUseList' =>$arrayMedicinalUse);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// If Empty Medicinal Use List
				$error = array('success' => "0", "msg" => "Empty Medicinal Use List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		/*}
		else
		{
			// If invalid user id  
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If empty user id
		$error = array('success' => "0", "msg" => "UserId empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	*/
?>