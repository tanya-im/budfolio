<?php
/*
	File Description - followers list for the dispensary popup
	File Name - disFollowerListAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array

	//Taking all values into local variable				
	$intDispensaryId = $inputArray['DispensaryId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];	

	// Input validations
	if(!empty($intDispensaryId))
	{
		$strFollowersCountLIst = "SELECT count(user_id)
							 FROM users
							 WHERE user_id
							 IN (
							  
							   SELECT `user_id`
							   FROM `menu_followup`
							   WHERE `dispensary_id` = '".$intDispensaryId."'
							 )";
							   
		$resCount = mysql_query($strFollowersCountLIst) or die($strFollowersCountLIst." : ".mysql_error());

		$strFollowerCountResult = mysql_fetch_array($resCount);
		$strFollwerData = $strFollowerCountResult[0];
		$totalPages = ceil($strFollwerData / $intPageSize);
			
		$perpage = $intPageSize;
		$page = $intPageNo;
		$calc = $perpage * $page;
		$start = $calc - $perpage;
			
		$strFollowersCountLIst = "SELECT *
							 	  FROM users
							      WHERE user_id
							 	  IN (
								  
								   SELECT `user_id`
								   FROM `menu_followup`
								   WHERE `dispensary_id` = '".$intDispensaryId."'
								 ) ";
		if(!empty($perpage) and !empty($page)){
					$strFollowersCountLIst.=" LIMIT $start, $perpage";
		}					   
		$resCount = mysql_query($strFollowersCountLIst) or die($strFollowersCountLIst." : ".mysql_error());
		
		if(mysql_num_rows($resCount)>0)
		{
			$arrayFollowers = array();
			while($rowFollower = mysql_fetch_assoc($resCount))
			{
				if(!empty($rowFollower['photo_url']))
				{
				   $thumbimagesurl=BASEURL.USER_PROFILE_IMG_THUMB.$rowFollower['photo_url'];
				   $orgimagesurl=BASEURL.USER_PROFILE_IMG_ORI.$rowFollower['photo_url'];
				}else
				{
					$profilePic ='';
				}
				$arrayFollowers[] = array(
								  'UserId' => $rowFollower['user_id'],
								  'Email' => ($rowFollower['email_address']!=''?$rowFollower['email_address']:''),
								  'UserName' =>($rowFollower['user_name']!=''?$rowFollower['user_name']:''),
								  'ProfileImagethumb'=>($thumbimagesurl!=''?$thumbimagesurl:''),
								  'ProfileImageoriginal'=>($orgimagesurl!=''?$orgimagesurl:'')
				);
			}
			//if every thing is ok then send success=1 and strain  id in response.
			$result = array( "success" => "1","followersList"=>$arrayFollowers);
			$this->response($this->toJson($result),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "0","followersList"=> array());
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Please select dispensary.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>