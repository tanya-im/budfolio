<?php
/*
	File Description - budthought list and comment list if user is mentioned in it 
	File Name - notificationsAPI.php
	creation date - 25th june
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	$dataJson = file_get_contents("php://input"); //getting json input 
	$inputArray = json_decode($dataJson,true); //converting that json into array
	//converting that json into array
 	$intUserId = $inputArray['UserId'];		
	
	// Input validations
	if(!empty($intUserId))
	{
		$userName = getUserName($intUserId);
		// search bud thought if user is mentioned in it 
		$strBudThoughtSQL ="SELECT * FROM `tbl_bud_thought`
							WHERE bud_thought LIKE '%".$userName."%' order by post_date desc";

		//execute query
		$resultBudThoughts = mysql_query($strBudThoughtSQL) or die(mysql_error()." ".$strBudThoughtSQL);					   					     
		while($bt = mysql_fetch_assoc($resultBudThoughts))
		{
			
			  $date = date('d|m|Y | h:i A',strtotime($bt['post_date']));//conevrting date time into string format
			   $likeCount = getLikeCount($bt['bud_thought_id']);
			   $commentCount = getCommentCount($bt['bud_thought_id']);
			   $likeFlag = isUserLiked($bt['bud_thought_id'],$intUserId);
				
			   if($bt['picture_url']!='')
			   {
					 $imgURL = BASEURL.BTIMGPATH.$bt['picture_url'];
					 $thumbURL = BASEURL.BTTIMGPATH.$bt['picture_url'];
			   }
			   else
			   {
					$imgURL = '';
					$thumbURL ='';
			   }


				$getPathSQL = "SELECT photo_url FROM users WHERE user_id='".$bt['user_id']."'";
					$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());

					$row = mysql_fetch_assoc($response);
 
					if(!empty($row['photo_url'])){
	
							$userImgPath = $row['photo_url'];
					}else
					{
						 $userImgPath = '';
					}

				 
				$arrayList[]= array(
								'type'=>'0',
								'bud_thought_id' => $bt['bud_thought_id'],
								'BudThought' 	 => str_replace('"','/',stripslashes(str_replace("\\","/",$bt['bud_thought']))), 
								'pictureUrl'     => $userImgPath!=''?BASEURL."images/profileimages/original/".$userImgPath:'',
								'thumbnailUrl'   =>$userImgPath!=''?BASEURL."images/profileimages/thumbnail/".$userImgPath:'',//for thumbnail url on 21st june
								'UserId'         => $bt['user_id'],
								'UserName'       => getUserName($bt['user_id']),
								'LikeCount'      => $likeCount,
								'CommentCount'   => $commentCount,
								'likeFlag'       => $likeFlag,
							    'date'           => $date,
								'budThoughtUrl'  => $thumbURL
				); 
		}//end of while
						
		//search user in comments of bud thought
		$strCommentSQL = "SELECT *
						  FROM `tbl_bud_thought`
						  WHERE bud_thought_id
						  IN (
						  
						  SELECT bud_thought_id
						  FROM tbl_comments
						  WHERE `bud_thought` LIKE '%".$userName."%'
						  )";

		$resComment = mysql_query($strCommentSQL) or die($strCommentSQL." : ".mysql_error());
					 
		while($cmnt = mysql_fetch_assoc($resComment))
		{
			$date = date('d F Y / h:i A',strtotime($cmnt['post_date']));//conevrting date time into string format
				  $likeCount = getLikeCount($cmnt['bud_thought_id']);
				  $commentCount = getCommentCount($cmnt['bud_thought_id']);
				  $likeFlag = isUserLiked($cmnt['bud_thought_id'],$intUserId);
				  
				   if($cmnt['picture_url']!='')
				   {
					   $imgURL = BASEURL.$budThoughtImgPath.$cmnt['picture_url'];
				   }
				  else
				  {
					  $imgURL = '';
				  }
				   
				  $arrayList[]= array(
					  'type'=>'0',
				  'bud_thought_id' => $cmnt['bud_thought_id'],
								'BudThought' 	 => str_replace('"','/',stripslashes(str_replace("\\","/",$cmnt['bud_thought']))), 
								'pictureUrl'     => $imgURL,
								'thumbnailUrl'   => $thumbURL,//for thumbnail url on 21st june
								'UserId'         => $cmnt['user_id'],
								'UserName'       => getUserName($cmnt['user_id']),
								'LikeCount'      => $likeCount,
								'CommentCount'   => $commentCount,
								'likeFlag'       => $likeFlag,
							    'date'           => getUserName($cmnt['user_id']).' / '.$date
				  ); 
		}//end of while comment
		if(count($arrayList)>0)
		{
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1",'LoungeList'=>$arrayList);
			$this->response($this->toJson($result),200,"application/json");
				 
		}// end of if(count($arrayUserNames) >0)	 
		else
		{
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1",'LoungeList'=>array());
			$this->response($this->toJson($result),200,"application/json");
		}
	} // closing mysql_num_rows
	else
	{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "User id is empty.");
			$this->response($this->toJson($strError), 204,"application/json");
	}	
	
?>