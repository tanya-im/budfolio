<?php
/*
	File Description - Delete Strain
	File Name - removestrainAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intStrainId = $inputArray['StrainId'];
	$intUserId = $inputArray['UserId'];
	
	// Input validations
	if(!empty($intStrainId) && !empty($intUserId))
	{
		$intValidintStrainId = mysql_real_escape_string($intStrainId);
		$intValidUserId = mysql_real_escape_string($intUserId);
		
		//SQL query to get Strain detail
		$strStrainDetailQuery = "SELECT * FROM strains WHERE strain_id ='".$intValidintStrainId."' and user_id='".$intValidUserId."' Limit 0,1";
		$StrainDetailSQL = mysql_query($strStrainDetailQuery) or die(mysql_error());//executing strStrainDetailQuery
		if(mysql_num_rows($StrainDetailSQL) > 0)
		{
			
			$strBlockStrainQuery = "update strains set flag='deactive' where strain_id ='".$intValidintStrainId."'";
			$strBlockStrainSql=mysql_query($strBlockStrainQuery)or(die(mysql_error()));
			// If success everythig is good send header as "OK" and user details
			// sending StrainId in reponse
			if($strBlockStrainSql)
			{
				$result = array('success' => '1','StrainId' =>$intValidintStrainId,"msg"=>"Strain Block");
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}
			else
			{
				// If Strain not block
				$error = array('success' => "0", "msg" => "Strain not delete. Please try again.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid StrainId
			$error = array('success' => "0", "msg" => "Invalid StrainId.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "StrainId or UserId empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>