<?php
/*
	File Description - Dispensary Detail
	File Name - dispensarydetailAPI.php
*/

// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable				
	$intDispensaryId = $inputArray['DispensaryId'];
	
	//Input validations
	if(!empty($intDispensaryId))
	{
		// sanitization of values to set in sql
		$intValidintDispensaryId = mysql_real_escape_string($intDispensaryId);
		
		//SQL query to get Strain detail
		$strDispensaryDetailQuery = "SELECT * 
									 FROM dispensaries 
									 WHERE flag='Active' 
									 and  dispensary_id ='".$intValidintDispensaryId."' 
									 Limit 0,1";
		$DispensaryDetailSQL = mysql_query($strDispensaryDetailQuery) or die(mysql_error());
		$DispensaryDetailArray=array();
		$menuheadingArray=array();
		$DispensaryDetail=array();
		$number=0;
		if(mysql_num_rows($DispensaryDetailSQL) > 0)
		{
			while($DispensaryDeatil=mysql_fetch_array($DispensaryDetailSQL))
			{
				//SQL query to get menu header detail
				 $strDispensaryHeading = "SELECT * FROM menu_heading 
										 WHERE dispensary_id ='".$DispensaryDeatil['dispensary_id']."' 
										 order by menu_heading_id asc ";
				$DispensaryHeadingsSQL = mysql_query($strDispensaryHeading);
				
				
				//23rd may
				if(mysql_num_rows($DispensaryHeadingsSQL)>0)
				{
					while($headingdata=mysql_fetch_array($DispensaryHeadingsSQL))
					{
						
					  // SQL query to get dispensary detail
					  $strDispensariesDetailQuery = "SELECT * FROM dispensaries_detail 
													 WHERE menu_heading_id ='".$headingdata['menu_heading_id']."' 	order by dispensary_detail_id asc ";
					  $SQLDispensariesDetail = mysql_query($strDispensariesDetailQuery);
					  //executing strDispensariesDetailQuery
					  $headingflag=0;
					  $mn=0;
					  while($DispensariesDetailData=mysql_fetch_array($SQLDispensariesDetail))
					  {
						  if($headingflag==0)
						  {
							 $menuheadingArray[$number]['heading']=unserialize($DispensariesDetailData['dispensaries_detail_data']);
							 
						  }
						  else
						  {
							  $menuheadingArray[$number]['detail'][$mn]['menudata']=unserialize($DispensariesDetailData['dispensaries_detail_data']);
  // Unserialize data							
							  $data=unserialize($DispensariesDetailData['dispensaries_detail_data']);
  // Select all users id that added same strain with added by admin in menu despensary 							
							  $Queryuserno="select distinct user_id from strains where strain_name='".$data[0]."'";							
							  $Sqluserno=mysql_query($Queryuserno)or die(mysql_error()); 
							  $Resuserno=mysql_num_rows($Sqluserno);
							  $menuheadingArray[$number]['detail'][$mn]['userdata']['userrating']=$Resuserno;
  // Select strain detail							
							  $Querysmell="select smell_rating,count(smell_rating) as rating from strains where strain_name='".$data[0]."' group by smell_rating ORDER BY rating desc LIMIT 0 , 1";
							  $Sqlsmell=mysql_query($Querysmell)or die(mysql_error()); 
							  $Ressmell=mysql_fetch_array($Sqlsmell);
							  $menuheadingArray[$number]['detail'][$mn]['userdata']['smell_rating']=$Ressmell['smell_rating'];
  // Get test rate rating sql 							
							  $Querytaste="select taste_rate,count(taste_rate) as rating from strains where strain_name='".$data[0]."' group by taste_rate ORDER BY rating desc LIMIT 0 , 1";
							  $Sqltaste=mysql_query($Querytaste)or die(mysql_error()); 
							  $Restaste=mysql_fetch_array($Sqltaste);
							  $menuheadingArray[$number]['detail'][$mn]['userdata']['taste_rate']=$Restaste['taste_rate'];
  // Select over all rating sql 							
							  $QueryOverallRating="select count(overall_rating) as rating from strains where strain_name='".$data[0]."' group by overall_rating LIMIT 0 , 1";
							  $SqlOverallRating=mysql_query($QueryOverallRating)or die(mysql_error()); 
							  $ResOverallRating=mysql_fetch_array($SqlOverallRating);
  // Get some of overall rating							
							  $QueryOverallRating_avg="select sum(overall_rating) as avgsum from strains where strain_name='".$data[0]."' group by overall_rating  LIMIT 0 , 1";
							  $SqlOverallRating_avg=mysql_query($QueryOverallRating_avg)or die(mysql_error()); 
							  $ResOverallRating_avg=mysql_fetch_array($SqlOverallRating_avg);
  // Calculate avg rating							
							  $avg=$ResOverallRating_avg['avgsum']/$ResOverallRating['rating'];
							  $menuheadingArray[$number]['detail'][$mn]['userdata']['avg_rating']=$avg;
							  $mn++;
						  }
						  $headingflag++;
					  }
					  $number++;
				  }
				}/*else
				{
					// If Invalid DispensaryId
					$error = array('success' => "0", "msg" => "No rating found for selected dispensary.");
					$this->response($this->toJson($error), 400,"application/json");
					
				}*///23rd may
				if(!empty($DispensaryDeatil['image']))
				{
					$imgurl=BASEURL.$dispensaryimagefoldername_original.$DispensaryDeatil['image'];
					$thumimageurl=BASEURL.$dispensaryimagefoldername_thumbnail.$DispensaryDeatil['image'];
				}
				else
				{
					$imgurl='';
					$thumimageurl='';
				}

				if($DispensaryDeatil['added_by']!=0){
					$getUserSettings ="SELECT * FROM user_settings 
									   WHERE user_id='".$DispensaryDeatil['added_by']."'";
				
				  $getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());
	  
				  $settingData = mysql_fetch_assoc($getSettingsResponse);
				   $settingArray=array(
				'DeliveryService'=>$settingData['DeliveryService']!=''?$settingData['DeliveryService']:'0',
				'StoreFront'=>$settingData['StoreFront']!=''?$settingData['StoreFront']:'0',
				'AcceptCreditCard'=>$settingData['AcceptCreditCard']!=''?$settingData['AcceptCreditCard']:'0',
				'AcceptATMonSite'=>$settingData['AcceptATMonSite']!=''?$settingData['AcceptATMonSite']:'0',
				'18YearsOld'=>$settingData['18YearsOld']!=''?$settingData['18YearsOld']:'0',
				'21YearsOld'=>$settingData['21YearsOld']!=''?$settingData['21YearsOld']:'0',
				'HandicapAssesseble'=>$settingData['HandicapAssesseble']!=''?$settingData['HandicapAssesseble']:'0',
				'Security'=>$settingData['Security']!=''?$settingData['Security']:'0'	
										  );
					
					}else
					{
						$settingArray=array(
											  'DeliveryService'=>'0',
											  'StoreFront'=>'0',
											  'AcceptCreditCard'=>'0',
											  'AcceptATMonSite'=>'0',
											  '18YearsOld'=>'0',
											  '21YearsOld'=>'0',
											  'HandicapAssesseble'=>'0',
											  'Security'=>'0'
										  );
					}
					


				//Deispensary detail array
				$DeispensaryDetailArray[] =  array(
											'DispensaryId'=> $DispensaryDeatil['dispensary_id'],
											'DispensaryName'=>$DispensaryDeatil['dispensary_name'],
											'CustomerName'=> $DispensaryDeatil['customer_name'], 
											'StreetAddress'=> $DispensaryDeatil['street_address'],
											'PhoneNumber'=> $DispensaryDeatil['phone_number'],
											'Email'=>$DispensaryDeatil['email'],
											'Website'=> $DispensaryDeatil['website'],
											'City'=> $DispensaryDeatil['city'],
											'State'=> $DispensaryDeatil['state'],
											'ZipCode'=> $DispensaryDeatil['zip'],
											'HoursOfOperation'=> $DispensaryDeatil['hours_of_operation'],
											'Directions'=>$DispensaryDeatil['directions'],
											'MenuHeadingArray'=> $menuheadingArray,
											'DispensaryImageUrl'=> $imgurl,
											'DispensaryThumbImageUrl'=> $thumimageurl,
											'Bio'=>$DispensaryDeatil['bio'],
											'ReviewCount'=>getReviewsCount($DispensaryDeatil['dispensary_id']),
											'PhotoCount'=>getPhotosCount($DispensaryDeatil['dispensary_name'])!=''?getPhotosCount($DispensaryDeatil['dispensary_name']):'0',
											'FollowersCount'=>getFollowersCount($DispensaryDeatil['dispensary_id']),
											'settings'=>$settingArray

										);// end of array//=>getPhotosCount($DispensaryDeatil['dispensary_name']),
			}
			

				// If success everythig is good send header as "OK" and sending Dispensary details in reponse
				$result = array('success' => '1','DispensaryDetail' => $DeispensaryDetailArray);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");

		
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "0", "msg" => "Invalid DispensaryId.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Dispensary Id empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>