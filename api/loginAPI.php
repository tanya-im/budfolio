<?php
/*
	File Description - File to check credentials of user and allow to login
	File Name - loginAPI.php
*/

// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true);

	//converting that json into array
	$strUserName = $inputArray['UserName'];		
	$strPassword = $inputArray['Password'];
	
	// Input validations
	if(!empty($strUserName) and !empty($strPassword))
	{
		$strValidUserName = mysql_real_escape_string($strUserName);
		$strValidPassword = mysql_real_escape_string($strPassword);
		

		//SQL query to check the  USER exists or not
		$strLoginSQL = "SELECT user_id,user_name,confirm_code,user_type FROM users WHERE user_name ='".$strValidUserName."' AND password = '".md5($strValidPassword)."'";
		$sql = mysql_query($strLoginSQL)or die(mysql_error());//executing strLoginSQL
		if(mysql_num_rows($sql) > 0)
		{
			$resultSet = mysql_fetch_assoc($sql);
			
			//check email is confirm or not
			if(!empty($resultSet['confirm_code']))
			{
				
				// If email is not cofirm.
				$error = array('success' => "0", "msg" => "Please confirm your email address before login.");
				$this->response($this->toJson($error), 400,"application/json");
			}
			$date = date('Y-m-d H:i:s');
			$updatelogindate="update users set last_login_date='".$date."' where user_id='".$resultSet['user_id']."'";
			$datestatus=mysql_query($updatelogindate);
			
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1",
							'UserId' => $resultSet['user_id'],
							'UserName'=>$resultSet['user_name'],
							'userType'=>$resultSet['user_type']
				);
			$this->response($this->toJson($result),200,"application/json");

		} // closing mysql_num_rows
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "Invalid Username or password.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	} //end of input validations
	else
	{
		// If either username or passowrd is empty
		$error = array('success' => "0", "msg" => "Username or password is empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}
?>