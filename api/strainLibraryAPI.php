<?php
/*
	File Description - default strain library
	File Name - strainLiberaryAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable				
	 $intUnixTime = $inputArray['unix_time'];
	
	// Input validations
	if(isset($intUnixTime))
	{
		//SQL query to get Strain library
		$strStrainLiberarySQL = "SELECT *,UNIX_TIMESTAMP(date_time) as unixTime
								 FROM strain_library 
								 WHERE UNIX_TIMESTAMP(date_time) > '".$intUnixTime."'
								 ORDER BY strain_name";
		
		$StrainLiberarySQL = mysql_query($strStrainLiberarySQL) or die($strStrainLiberarySQL." : ".mysql_error());
		
		$StrainLibArray=array();
		
		$unix_time =0;
		if(mysql_num_rows($StrainLiberarySQL) > 0)
		{
			while($strainLiberary = mysql_fetch_array($StrainLiberarySQL))
			{
				$unix_time = $strainLiberary['unixTime'];
				//Strain Liberary array
				$StrainLibArray[] =  array(
											'starin_lib_id'=> $strainLiberary['strain_lib_id'],
											'strain_name'=>$strainLiberary['strain_name'],
											'species'=> $strainLiberary['species'], 
											'lineage'=> $strainLiberary['lineage'],
											'reviewCount' => strainLibReviewsCount(mysql_real_escape_string($strainLiberary['strain_name']))
										);// end of array
			}
			// If success everythig is good send header as "OK" and sending Dispensary details in reponse
			$result = array('success' => '1',
							'strain_liberary' => $StrainLibArray,
							'unix_time'=> $unix_time
				);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "1",'strain_liberary' =>array());
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Time is empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>