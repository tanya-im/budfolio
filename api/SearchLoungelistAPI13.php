<?php
/*
	File Description - Search Lounge List
	File Name - SearchLoungelistAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		$this->response('Invalid request',406);
	}
//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	$strSearchtext = $inputArray['Search'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	
// Input validations
	if(!empty($intUserId) && !empty($strSearchtext) && !empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		$strValidSearchtext = mysql_real_escape_string($strSearchtext);
// Check user id is valid or not		
		$uservalidation=CheckValidUser($intValidUserId);
		if($uservalidation)
		{
			// Get users within the 15 or  20 miles radious
			$zipdata=get_zip_search($strValidSearchtext,10);
			$countlocaluser=count($zipdata);
			if($countlocaluser>0)
			{	
				$uids='';
				for($l=0;$l<$countlocaluser;$l++)
				{
					$uids.=$zipdata[$l]['UserId'];
					if($l<($countlocaluser-1))
					{
						$uids.=',';
					}
				}
				//Sql to  count the search result				
				$StrLoungeSearchQuery="select Count(*) As Total from strains INNER JOIN users on users.user_id=strains.user_id where strains.flag='".Active."' and privacy='public' and(strains.strain_name LIKE '%".$strValidSearchtext."%' || strains.dispensary_name LIKE '%".$strValidSearchtext."%' || users.zip_code LIKE '%".$strValidSearchtext."%' || strains.user_id in (".$uids."))";	
			}
			else
			{
				//Sql to count the search result				
				$StrLoungeSearchQuery="select Count(*) As Total from strains INNER JOIN users on users.user_id=strains.user_id where strains.flag='".Active."' and privacy='public' and(strains.strain_name LIKE '%".$strValidSearchtext."%' || strains.dispensary_name LIKE '%".$strValidSearchtext."%' || users.zip_code LIKE '%".$strValidSearchtext."%')";
			}	
			$SearchResultNoSql = mysql_query($StrLoungeSearchQuery)or die(mysql_error());
			$SearchResult=mysql_fetch_array($SearchResultNoSql)or die(mysql_error());
			$SearchData=$SearchResult[0];
			$totalPages = ceil($SearchData / $intPageSize);
		
			$perpage=$intPageSize;
			$page=$intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;

			if($countlocaluser>0)
			{
				//Sql to get the search result				
				$strSearchLoungeListQuery = "select strain_id,strain_name,dispensary_name, post_date, species,overall_rating, strains.user_id from strains INNER JOIN users on users.user_id=strains.user_id where strains.flag='".Active."' and  privacy='public' and(strains.strain_name LIKE '%".$strValidSearchtext."%' || strains.dispensary_name LIKE '%".$strValidSearchtext."%' || users.zip_code LIKE '%".$strValidSearchtext."%' || strains.user_id in (".$uids.")) order by strain_id desc  Limit $start, $perpage";
			}
			else
			{
//Sql to get the search result				
				$strSearchLoungeListQuery = "select strain_id,strain_name,dispensary_name, post_date, species,overall_rating, strains.user_id from strains INNER JOIN users on users.user_id=strains.user_id where strains.flag='".Active."' and  privacy='public' and(strains.strain_name LIKE '%".$strValidSearchtext."%' || strains.dispensary_name LIKE '%".$strValidSearchtext."%' || users.zip_code LIKE '%".$strValidSearchtext."%') order by strain_id desc  Limit $start, $perpage";
				
			}
			$SearchLoungeListSql = mysql_query($strSearchLoungeListQuery) or die(mysql_error());
			$arraySearchLoungeArray=array();
			if(mysql_num_rows($SearchLoungeListSql) > 0)
			{
				while($LoungeList=mysql_fetch_array($SearchLoungeListSql))
				{
					$primaryimages=GetPrimaryImages($LoungeList['strain_id']);//Get primary image.
					$userdetail=GetUserDetail($LoungeList['user_id']);// Get user detail
					if($userdetail['Privacy']=='public')
					{
						$arraySearchLoungeArray[] =  array(
														'StrainId'=> $LoungeList['strain_id'],
														'StrainName'=> $LoungeList['strain_name'], 
														'Dispensory'=> $LoungeList['dispensary_name'],
														'Species'=> $LoungeList['species'],
														'OverallRating'=> $LoungeList['overall_rating'],
														'StrainsImageUrl'=>$primaryimages[0],
														'StrainsThumbImageUrl'=>$primaryimages[1],
														'UserId'=>$LoungeList['user_id'],
														'UserName'=>$userdetail['UserName'],
														'ZipCode'=>$userdetail['ZipCode'],
														'type'=>'1'//for strain
													);// end of array
					}
				}
				// If success everythig is good send header as "OK" and search result in reponse
$result = array('success' => '1','LoungeList' => $arraySearchLoungeArray,'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// If Empty search result
				$error = array('success' => "0", "msg" => "No match found with this criteria.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or Search Text or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>