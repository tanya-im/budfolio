<?php
/*
	File Description - strain lib Following list
	File Name - followingStrainLibAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable				
	$intStarinLibId = $inputArray['starin_lib_id'];
	$intUserId = $inputArray['user_id'];
	
	// Input validations
	if(isset($intStarinLibId))
	{
		$strainInfoArray = getStrainLibName($intStarinLibId);
		//SQL query to get Strain library
		$strStrainLiberarySQL = "SELECT strain_id,strain_name,user_id,dispensary_name
								     FROM strains
								     WHERE strain_name = '".$strainInfoArray['strain_name']."'
								     AND user_id
								     IN (
								      SELECT follower_id
								      FROM user_followup
								      WHERE user_id ='".$intUserId."'
								    )
								 ";
		$StrainLiberarySQL = mysql_query($strStrainLiberarySQL) or die($strStrainLiberarySQL." : ".mysql_error());
		
		$StrainLibArray=array();
		$number=0;
		if(mysql_num_rows($StrainLiberarySQL) > 0)
		{
			while($strainLiberary = mysql_fetch_array($StrainLiberarySQL))
			{
				//Strain Liberary array
				$StrainLibArray[] =  array(
											'user_id'=> $strainLiberary['user_id'],
											'user_name'=>getUserName($strainLiberary['user_id']),
											'dispensary_name'=> $strainLiberary['dispensary_name']
										);// end of array
			}
			 
			// If success everythig is good send header as "OK" and sending user list in reponse
			$result = array('success' => '1','users_list' => $StrainLibArray);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "0", "msg" => "No strains found in starin liberary.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainlibId empty
		$error = array('success' => "0", "msg" => "Please enter string to be search.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>