<?php
/* 
	File Description - file to add strain images
	File Name - addstrainimagesAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
//Taking all values into local variable		
	$intStrainId= $_REQUEST['StrainId'];
	$intprimaryImage= $_REQUEST['PrimaryImage'];
	if(empty($intStrainId))
	{
		$error = array('success' => "0", "msg" => "Empty strain id.");
		$this->response($this->toJson($error), 406, "application/json");
	}
	$flag=false;
// Sanatize the values 	
	$intValidStrainId = mysql_real_escape_string($intStrainId);
// Check which one is primary images set 0 in others.	
	if($intprimaryImage==1)
	{
		$one=1;
	}
	else
	{
		$one=0;
	}
	if($intprimaryImage==2)
	{
		$two=1;
	}
	else
	{
		$two=0;
	}
	if($intprimaryImage==3)
	{
		$three=1;
	}
	else
	{
		$three=0;
	}
   // Check strain id is valid or not.	
	$strSQLChkStrainId ="SELECT strain_name FROM strains WHERE strain_id='".$intValidStrainId."'";
	$SQLResChkStrainId = mysql_query($strSQLChkStrainId);// execution of query intValidStrainId
	if(mysql_num_rows($SQLResChkStrainId)< 1)
	{
		$error = array('success' => "0", "msg" => "Invalid strain id.");
		$this->response($this->toJson($error), 406, "application/json");
	}
    // Remove old strain images sql
	$RemoveOldStrainImagesQuery ="delete FROM strainimages WHERE strain_id='".$intValidStrainId."'";
	$RemoveOldStrainImagesSql = mysql_query($RemoveOldStrainImagesQuery) or die(mysql_error());

    // Adding  Inserting strain images
	$ImageId=array();
	if(isset($_FILES['StrainsImage1']))
	{
		$name = $_FILES['StrainsImage1']['name'];
		$size = $_FILES['StrainsImage1']['size'];
		if(strlen($name))
		{
			list($txt, $ext) = explode(".", $name);
			$extention = explode(".", $name);
			$arrayconunt=(count($extention)-1);
			if(!empty($extention[$arrayconunt]))
			{
				$ext=$extention[$arrayconunt];
			}
			if(in_array(strtolower($ext),$valid_formats))
			{
				if($size < ImageSize)
				{
					$actual_image_name = time().rand().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $_FILES['StrainsImage1']['tmp_name'];
					if(move_uploaded_file($tmp,'../'.$strainimagefoldername_original.$actual_image_name))
					{
						$strAddImagesQuery="insert into strainimages(image_name,strain_id,primary_image) values('".$actual_image_name."','".$intValidStrainId."','".$one."')";
						$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());
						$ImageId = mysql_insert_id();
						$src='../'.$strainimagefoldername_original.$actual_image_name;
						$dest_m='../'.$strainimagefoldername_medium.$actual_image_name;
						$dest_t='../'.$strainimagefoldername_thumbnail.$actual_image_name;
						resize_image($src,$dest_m,300,300);
						resize_image($src,$dest_t,115,115);
						$ImageStatus[1]['ImageId']=$ImageId;
						$ImageStatus[1]['success']=1;
						$ImageStatus[1]['msg']="Image one upload successfully."; 
						$flag=true; 
					}
					else	
					{
						$ImageStatus[1]['ImageId']=0;
						$ImageStatus[1]['success']=0;
						$ImageStatus[1]['msg']="Image one upload failed. Please try agian."; 
					}
				}
				else
				{
					$ImageStatus[1]['ImageId']=0;
					$ImageStatus[1]['success']=0;
					$ImageStatus[1]['msg']="Image one file size max 3 MB."; 
					$error = array('success' => "0", "msg" => "");
				}
			}
			else
			{
				$ImageStatus[1]['ImageId']=0;
				$ImageStatus[1]['success']=0;
				$ImageStatus[1]['msg']="Image one Invalid file format."; 
			}
		}
		else
		{
			$ImageStatus[1]['ImageId']=0;
			$ImageStatus[1]['success']=0;
			$ImageStatus[1]['msg']="Please select image one."; 
		}		
	
	}
	if(isset($_FILES['StrainsImage2']))
	{
		$name = $_FILES['StrainsImage2']['name'];
		$size = $_FILES['StrainsImage2']['size'];
		if(strlen($name))
		{
			list($txt, $ext) = explode(".", $name);
			$extention = explode(".", $name);
			$arrayconunt=(count($extention)-1);
			if(!empty($extention[$arrayconunt]))
			{
				$ext=$extention[$arrayconunt];
			}
			if(in_array(strtolower($ext),$valid_formats))
			{
				if($size< ImageSize)
				{
					$actual_image_name = time().rand().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $_FILES['StrainsImage2']['tmp_name'];
					if(move_uploaded_file($tmp,'../'.$strainimagefoldername_original.$actual_image_name))
					{
						$strAddImagesQuery="insert into strainimages(image_name,strain_id,primary_image) values('".$actual_image_name."','".$intValidStrainId."','".$two."')";
						$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());
						$ImageId = mysql_insert_id();
						$src='../'.$strainimagefoldername_original.$actual_image_name;
						$dest_m='../'.$strainimagefoldername_medium.$actual_image_name;
						$dest_t='../'.$strainimagefoldername_thumbnail.$actual_image_name;
						resize_image($src,$dest_m,300,300);
						resize_image($src,$dest_t,115,115);
						$ImageStatus[2]['ImageId']=$ImageId;
						$ImageStatus[2]['success']=1;
						$ImageStatus[2]['msg']="Image two upload successfully."; 
						$flag=true;  
					}
					else	
					{
						$ImageStatus[2]['ImageId']=0;
						$ImageStatus[2]['success']=0;
						$ImageStatus[2]['msg']="Image two upload failed. Please try agian."; 
					}
				}
				else
				{
					$ImageStatus[2]['ImageId']=0;
					$ImageStatus[2]['success']=0;
					$ImageStatus[2]['msg']="Image two file size max 3 MB."; 
					$error = array('success' => "0", "msg" => "");
				}
			}
			else
			{
				$ImageStatus[2]['ImageId']=0;
				$ImageStatus[2]['success']=0;
				$ImageStatus[2]['msg']="Image two Invalid file format."; 
			}
		}
		else
		{
			$ImageStatus[2]['ImageId']=0;
			$ImageStatus[2]['success']=0;
			$ImageStatus[2]['msg']="Please select image two."; 
		}		
	
	}
	if(isset($_FILES['StrainsImage3']))
	{
		$name = $_FILES['StrainsImage3']['name'];
		$size = $_FILES['StrainsImage3']['size'];
		if(strlen($name))
		{
			list($txt, $ext) = explode(".", $name);
			$extention = explode(".", $name);
			$arrayconunt=(count($extention)-1);
			if(!empty($extention[$arrayconunt]))
			{
				$ext=$extention[$arrayconunt];
			}
			if(in_array(strtolower($ext),$valid_formats))
			{
				if($size< ImageSize)
				{
					$actual_image_name = time().rand().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $_FILES['StrainsImage3']['tmp_name'];
					if(move_uploaded_file($tmp,'../'.$strainimagefoldername_original.$actual_image_name))
					{
						$strAddImagesQuery="insert into strainimages(image_name,strain_id,primary_image) values('".$actual_image_name."','".$intValidStrainId."','".$three."')";
						$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());
						$ImageId = mysql_insert_id();
						$src='../'.$strainimagefoldername_original.$actual_image_name;
						$dest_m='../'.$strainimagefoldername_medium.$actual_image_name;
						$dest_t='../'.$strainimagefoldername_thumbnail.$actual_image_name;
						resize_image($src,$dest_m,300,300);
						resize_image($src,$dest_t,115,115);
						$ImageStatus[3]['ImageId']=$ImageId;
						$ImageStatus[3]['success']=1;
						$ImageStatus[3]['msg']="Image three upload successfully."; 
						$flag=true;  
					}
					else	
					{
						$ImageStatus[3]['ImageId']=0;
						$ImageStatus[3]['success']=0;
						$ImageStatus[3]['msg']="Image three upload failed. Please try agian."; 
					}
				}
				else
				{
					$ImageStatus[3]['ImageId']=0;
					$ImageStatus[3]['success']=0;
					$ImageStatus[3]['msg']="Image three file size max 3 MB."; 
					$error = array('success' => "0", "msg" => "");
				}
			}
			else
			{
				$ImageStatus[3]['ImageId']=0;
				$ImageStatus[3]['success']=0;
				$ImageStatus[3]['msg']="Image three Invalid file format."; 
			}
		}
		else
		{
			$ImageStatus[3]['ImageId']=0;
			$ImageStatus[3]['success']=0;
			$ImageStatus[3]['msg']="Please select image three."; 
		}		
	
	}

	$strGetImagePathSQL = "SELECT image_name FROM strainimages WHERE strain_id='".$intStrainId."'
						   ORDER BY image_id desc limit 0,1";

	$resImagePath = mysql_query($strGetImagePathSQL) or die($strGetImagePathSQL." : ".mysql_error());
	
	$resImage = mysql_fetch_assoc($resImagePath);

	$image = BASEURL.$strainimagefoldername_thumbnail.$resImage['image_name'];


	if($flag==true)
	{
		$result = array( "success" => "1", 'ImageStatus' => $ImageStatus,"Status"=>"All images uploaded successfully.",'Image'=>$image);
		$this->response($this->toJson($result),200,"application/json");
	}
	else
	{
		$result = array( "success" => "0", 'ImageDetail' => $ImageId,"Status"=>"Images not uploaded successfully.");
		$this->response($this->toJson($result),200,"application/json");
	}	