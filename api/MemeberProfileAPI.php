<?php
/*
	File Description - Meber Profile with strain list PRODUCTion
	File Name - MemeberProfileAPI.php
*/

// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	$intUserId = $inputArray['UserId'];
	$intMemberId = $inputArray['MemberId'];
// Input validations
	if(!empty($intUserId) && !empty($intMemberId))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		$intValidMemberId = mysql_real_escape_string($intMemberId);
		
// to check whether the user id is exist.
		$uservalidation=CheckValidUser($intValidUserId);
		if(!$uservalidation)
		{
			$error = array('success' => "0", "msg" => "Invalid user id.");
			$this->response($this->toJson($error), 406, "application/json");
		}
		
		$FollowUpQuery = "select * from user_followup where follower_id='".$intValidMemberId."' and user_id='".$intValidUserId."' ";
		$FollowUpSql=mysql_query($FollowUpQuery);
		if(mysql_num_rows($FollowUpSql)==0)
		{
			$falg=1;
		}
		else
		{
			$falg=0;
		}
		$MemberProfileQuery ="SELECT * FROM users WHERE user_id='".$intValidMemberId."'";
		$MemberProfileSql = mysql_query($MemberProfileQuery);// execution of query strSQLChkUserId
		$MemberProfileData=mysql_fetch_array($MemberProfileSql);
		if(!empty($MemberProfileData['photo_url']))
		{
			$thumbimagesurl=BASEURL.$userprofileimagefoldername_thumbnail.$MemberProfileData['photo_url'];
			$orgimagesurl=BASEURL.$userprofileimagefoldername_original.$MemberProfileData['photo_url'];
		}
		else
		{
			$thumbimagesurl='';
			$orgimagesurl='';
		}

		
		$UserDetailArray=array();
		$UserDetailArray[] =  array(
										'UserId'=> $MemberProfileData['user_id'],
										'UserName'=>$MemberProfileData['user_name'],
										'Bio'=> $MemberProfileData['bio'],
										'Flag'=>$falg,
										'ProfileImagethumb'=>$thumbimagesurl,
										'ProfileImageoriginal'=>$orgimagesurl,
										'indiciaCount' => getSpeciesCount($intMemberId,'indicia'),
										'sativaCount'=> getSpeciesCount($intMemberId,'sativa'),
										'hybridCount '=>getSpeciesCount($intMemberId,'hybrid'),
										'dispnsaryCount'=> getDispCountOfUser($intMemberId),
										'followingsCount'=>getUserFollowersCount($intMemberId),
										'photosCount'=>getStrainsPhotosCount($intMemberId)

								);// end of array
		//if every thing is good then return  success=1 and Profiledetail
		$result = array( "success" => "1", "MemberProfile" => $UserDetailArray);
		$this->response($this->toJson($result),200,"application/json");
	}
	else
	{
		$error = array('success' => "0", "msg" => "Member id or user id empty.");
		// If Empty user id
		$this->response($this->toJson($error), 406, "application/json");
	}
?>