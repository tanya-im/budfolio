<?php
/*
	File Description - Budfolio List
	File Name - mybudfoliolistAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	
	// Input validations
	if(!empty($intUserId) && !empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		$uservalidation=CheckValidUser($intValidUserId);
// Check user id is valid or not		
		if($uservalidation)
		{
			$strstrainnoQuery="select Count(*) As Total from strains where flag='".Active."' and user_id!='".$intValidUserId."'";
			$strstrainnoSql = mysql_query($strstrainnoQuery);
			$strstrainnoResult=mysql_fetch_array($strstrainnoSql);
			$strstrainnoData=$strstrainnoResult[0];
			$totalPages = ceil($strstrainnoData / $intPageSize);
			
			$perpage=$intPageSize;
			$page=$intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;
			//SQL query to get Lounge list
			$strLoungeListQuery = "SELECT user_id,strain_id,strain_name,dispensary_name,species,overall_rating FROM strains WHERE flag='".Active."' and user_id!='".$intValidUserId."' order by strain_id desc Limit $start, $perpage";
			$arrayLoungeListArray=array();
			$LoungeListSQL = mysql_query($strLoungeListQuery) or die(mysql_error());//executing strLoginSQL
			if(mysql_num_rows($LoungeListSQL) > 0)
			{
				while($LoungeList=mysql_fetch_array($LoungeListSQL))
				{
					$primaryimages=GetPrimaryImages($LoungeList['strain_id']);//Get primary image.
					$userdetail=GetUserDetail($LoungeList['user_id']);// Get user detail
					if($userdetail['Privacy']=='public')
					{
						$arrayLoungeListArray[] =  array(
														'StrainId'=> $LoungeList['strain_id'],
														'StrainName'=> $LoungeList['strain_name'], 
														'Dispensory'=> $LoungeList['dispensary_name'],
														'Species'=> $LoungeList['species'],
														'OverallRating'=> $LoungeList['overall_rating'],
														'StrainsImageUrl'=>$primaryimages[0],
														'StrainsThumbImageUrl'=>$primaryimages[1],
														'UserId'=>$LoungeList['user_id'],
														'UserName'=>$userdetail['UserName'],
														'ZipCode'=>$userdetail['ZipCode']
													);// end of array
					}
				}
				// If success everythig is good send header as "OK" and LoungeList in reponse
$result = array('success' => '1','LoungeList' => $arrayLoungeListArray,'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// Empty Lounge List
				$error = array('success' => "0", "msg" => "Empty Lounge List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>