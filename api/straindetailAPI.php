<?php
/*
	File Description - Strain Detail
	File Name - straindetailAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}

	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	$intStrainId = $inputArray['StrainId'];

	// Input validations
	if(!empty($intStrainId))
	{
		$intValidintStrainId = mysql_real_escape_string($intStrainId);

		//SQL query to check valid user getting strain detail 
		$strStrainDetailQuery = "SELECT * FROM strains WHERE flag='Active' 
								 and strain_id ='".$intValidintStrainId."' Limit 0,1";
		$StrainDetailSQL = mysql_query($strStrainDetailQuery) or die(mysql_error());
		$StrainDetailArray=array();
		if(mysql_num_rows($StrainDetailSQL) > 0)
		{
			while($StrainDeatil=mysql_fetch_array($StrainDetailSQL))
			{
                //SQL query to get Strain detail
				$strAllImages = "SELECT * FROM strainimages WHERE 
								strain_id ='".$StrainDeatil['strain_id']."' order by image_id asc limit 0,3";
				$AllImagesSQL = mysql_query($strAllImages);//executing strAllImages
				$imagesurl=array();
				$imgOrignals = array();
				$pimgno=3;
				if(mysql_num_rows($AllImagesSQL)>0)
				{
					$i=0;
					while($i<3){
						$AllImages = mysql_fetch_array($AllImagesSQL);
						$imagesurl['ImagesUrl'][] = $AllImages['image_name']!=''?(BASEURL.$strainimagefoldername_thumbnail.$AllImages['image_name']):'';
						$imgOrignals['ImagesUrl'][]=$AllImages['image_name']!=''? (BASEURL."images/uploads/original/".$AllImages['image_name']):'';
						$i++;
					}
					/*while($AllImages = mysql_fetch_array($AllImagesSQL))
					{
						// Set images url					
						$imagesurl['ImagesUrl'][] = $AllImages['image_name']!=''?(BASEURL.$strainimagefoldername_thumbnail.$AllImages['image_name']):'';
						$imgOrignals['ImagesUrl'][]=$AllImages['image_name']!=''? (BASEURL."images/uploads/original/".$AllImages['image_name']):'';	 
						// Check primary image and set it in array					
						if($AllImages['primary_image']==1)
						{
							$imagesurl['PrimaryImage']=$pimgno;
							$imgOrignals['PrimaryImage']=$pimgno;
						}
						$pimgno--;
					}*/
				}
				else
				{
					$imagesurl['ImagesUrl']=array();
					$imagesurl['PrimaryImage']=0;
					$imgOrignals['ImagesUrl']=array();
					$imgOrignals['PrimaryImage']=0;
				}	

				// Get experiance detail				
				$strExperienceQuery = "SELECT experience_name,experience_id FROM experience WHERE strain_id ='".$StrainDeatil['strain_id']."' order by experience_id asc ";
				$SQLExperience = mysql_query($strExperienceQuery);//executing strExperienceQuery
				$experience=array();
				while($allexperience=mysql_fetch_array($SQLExperience))
				{
					$experience[]=$allexperience['experience_name'];
				}

				// Get medicinal use detail				
				$strMedicinalUseQuery = "SELECT medicinal_id,medicinal_type FROM medicinal_use WHERE strain_id ='".$StrainDeatil['strain_id']."' order by medicinal_id asc ";
				$SQLMedicinalUse = mysql_query($strMedicinalUseQuery);//executing strMedicinalUseQuery
				$medicinaluse=array();
				while($AllMedicinalUse=mysql_fetch_array($SQLMedicinalUse))
				{
					$medicinaluse[]=$AllMedicinalUse['medicinal_type'];
				}

				// Prepare array for strain detail				
				$StrainDetailArray[] =  array(
												 		'StrainId'=> $StrainDeatil['strain_id'],
														'UserId'=>$StrainDeatil['user_id'],
														'StrainName'=> stripslashes($StrainDeatil['strain_name']), 
														'Dispensory'=> stripslashes($StrainDeatil['dispensary_name']),
														'DispensaryId'=> $StrainDeatil['dispensary_id'],
														'Species'=> $StrainDeatil['species'],
														'Linage'=>$StrainDeatil['linage'],
														'SmellRating'=> $StrainDeatil['smell_rating'],
														'TasteRate'=> $StrainDeatil['taste_rate'],
														'StrengthRate'=> $StrainDeatil['strength_rate'],
														'OverallRating'=> $StrainDeatil['overall_rating'],
														'ExperienceArray'=>$experience,
														'Consumption'=> $StrainDeatil['consumption_name'],
														'MedicinalUseArray'=> $medicinaluse,
														'TestedBy'=>$StrainDeatil['tested_by'],
														'THC'=> $StrainDeatil['thc'],
														'CBD'=> $StrainDeatil['cbd'],
														'CBN'=> $StrainDeatil['cbn'],
														'THCa'=> $StrainDeatil['thca'],
														'CBHa'=> $StrainDeatil['cbha'],
														'Moisture'=> $StrainDeatil['moisture'],
														'Description'=> stripslashes($StrainDeatil['description']),
														'StrainsImagesUrlArray'=> $imagesurl,
														'StrainOrignalImagesUrl'=> $imgOrignals
												);// end of array
			}
			// If success everythig is good send header as "OK" and StrainDetail in reponse
			$result = array('success' => '1','StrainDetail' => $StrainDetailArray);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		}
		else
		{
			// If Invalid StrainId
			$error = array('success' => "0", "msg" => "Invalid StrainId.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "StrainId empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>