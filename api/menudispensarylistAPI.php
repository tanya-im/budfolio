<?php
/* production
	File Description - Menu Dispensary list
	File Name - menudispensarylistAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
    
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true);     //converting that json into array

	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	$intLat = $inputArray['latitude'];
	$intLong =$inputArray['longitude'];
	
    // Input validations
	if(!empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		$intValidPageSize = mysql_real_escape_string($intPageSize);
		$intValidPageNo = mysql_real_escape_string($intPageNo);
		
		if($intValidUserId!=0){
			// Check user id is valid or not		
			$uservalidation=CheckValidUser($intValidUserId);
		}
		
		if((empty($intLat) and empty($intLong)) or ($intLat==0 and $intLong==0)){
			
			/*// Get no of records.			
			$strdispensariesnoQuery="SELECT dispensary_id,dispensary_name,city,state,zip,image,
									   phone_number,added_by
									   FROM dispensaries WHERE flag='Active' 
									   group by dispensary_name
									   order by dispensary_name";*/
			/*$strdispensariesnoSql = mysql_query($strdispensariesnoQuery);
			$strdispensariesnoResult = mysql_fetch_array($strdispensariesnoSql);
			$strdispensariesnoData = $strdispensariesnoResult[0];
			$totalPages = ceil($strdispensariesnoData / $intPageSize);
			
			// Set strat and end limit			
			$perpage=$intPageSize;
			$page=$intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;*/
			
			//SQL query to get Menu list
			$strDispensaryListQuery = "SELECT dispensary_id,dispensary_name,city,state,zip,image,
									   phone_number,added_by
									   FROM dispensaries WHERE flag='Active' 
									   group by dispensary_name
									   order by dispensary_name ";


			$arrayDispensaryListArray=array();
			$DispensaryListSQL = mysql_query($strDispensaryListQuery) or die(mysql_error());
			
			$strdispensariesnoData = mysql_num_rows($DispensaryListSQL);
			$totalPages = ceil($strdispensariesnoData / $intPageSize);
			
			// Set strat and end limit			
			$perpage=$intPageSize;
			$page=$intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;
			
			//7th aug
			
			$strDispensaryListQuery .=" Limit $start, $perpage";
			
			$DispensaryListSQL = mysql_query($strDispensaryListQuery) or die(mysql_error());
			if(mysql_num_rows($DispensaryListSQL) > 0)
			{
				
				
				while($DispensaryList=mysql_fetch_array($DispensaryListSQL))
				{
					$settingArray= array();
					if($DispensaryList['added_by']!=0){
					$getUserSettings ="SELECT * FROM user_settings 
									   WHERE user_id='".$DispensaryList['added_by']."'";
				
				  $getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());
	  
				  $settingData = mysql_fetch_assoc($getSettingsResponse);
				  $settingArray=array(
				'DeliveryService'=>$settingData['DeliveryService']!=''?$settingData['DeliveryService']:'0',
				'StoreFront'=>$settingData['StoreFront']!=''?$settingData['StoreFront']:'0',
				'AcceptCreditCard'=>$settingData['AcceptCreditCard']!=''?$settingData['AcceptCreditCard']:'0',
				'AcceptATMonSite'=>$settingData['AcceptATMonSite']!=''?$settingData['AcceptATMonSite']:'0',
				'18YearsOld'=>$settingData['18YearsOld']!=''?$settingData['18YearsOld']:'0',
				'21YearsOld'=>$settingData['21YearsOld']!=''?$settingData['21YearsOld']:'0',
				'HandicapAssesseble'=>$settingData['HandicapAssesseble']!=''?$settingData['HandicapAssesseble']:'0',
				'Security'=>$settingData['Security']!=''?$settingData['Security']:'0'	
										  );
					
					}else
					{
						$settingArray=array(
											  'DeliveryService'=>'0',
											  'StoreFront'=>'0',
											  'AcceptCreditCard'=>'0',
											  'AcceptATMonSite'=>'0',
											  '18YearsOld'=>'0',
											  '21YearsOld'=>'0',
											  'HandicapAssesseble'=>'0',
											  'Security'=>'0'
										  );
					}
					
					
					//SQL query to get Followup flag list
					$getfollowflagQuery="select * from menu_followup 
					where dispensary_id='".$DispensaryList['dispensary_id']."' and user_id='".$intValidUserId."'";
					$faldSql=mysql_query($getfollowflagQuery);
					$flagdatacount=mysql_num_rows($faldSql);
					if($flagdatacount>0)
					{
						$followFlag=1;
					}

					else
					{
						$followFlag=0;
					}
			// Set image url.					
			if(!empty($DispensaryList['image']))
			{
			$DispensaryImageUrl=BASEURL.$dispensaryimagefoldername_original.$DispensaryList['image'];
		    $DispensaryThumbImageUrl=BASEURL.$dispensaryimagefoldername_thumbnail.$DispensaryList['image'];
			}
			else
			{
				$DispensaryImageUrl='';	
				$DispensaryThumbImageUrl='';
			}
			$FWPflag=getFollowUpList($DispensaryList['dispensary_id'],$intValidUserId);
					// Prepare array for response 					
					$arrayDispensaryListArray[] = array('DispensaryId'=> $DispensaryList['dispensary_id'],
														 'DispensaryName'=> $DispensaryList['dispensary_name'], 
														 'City'=> $DispensaryList['city'],
														 'State'=> $DispensaryList['state'],
														 'ZipCode'=> $DispensaryList['zip'],
														 'flag'=>$FWPflag,
														 'DispensaryImageUrl'=> $DispensaryImageUrl,
														 'DispensaryThumbImageUrl'=>$DispensaryThumbImageUrl,
														 'PhoneNumber'=>$DispensaryList['phone_number'],
														 'settings'=>$settingArray);
				}
// If success everythig is good send header as "OK" and Menu list in reponse
				$result = array('success' => '1','TotalPageNo'=>$totalPages,'MenuDispensaryList' => $arrayDispensaryListArray,);

				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// If Empty Menu List
				$error = array('success' => "0", "msg" => "Empty Menu Dispensary List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}//end of if lat long empty
		else
		{	
		
			$strDataInRangeSQL = "SELECT dispensary_id,dispensary_name,city,state,zip,image,phone_number,added_by,";
			$strDataInRangeSQL .= " ( 3959 * acos( cos( radians( ".$intLat." ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$intLong.") ) + sin( radians( ".$intLat." ) ) * sin( radians( latitude ) ) ) ) AS distance";
			$strDataInRangeSQL .= "  from dispensaries where flag='Active'";
			$strDataInRangeSQL .= " HAVING distance < 100 ORDER BY distance";

			
			$SearchResultNoSql = mysql_query($strDataInRangeSQL) or die($strDataInRangeSQL." : ".mysql_error());
			
		     $SearchData = mysql_num_rows($SearchResultNoSql);
			$totalPages = ceil($SearchData / $intPageSize);
		
			$perpage = $intPageSize;
			$page = $intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;
		
			$strDataInRangeSQL .= " Limit $start, $perpage";

			
			$SearchResultNoSql = mysql_query($strDataInRangeSQL) or die($strDataInRangeSQL." : ".mysql_error());
			$arraySearchMenuArray=array();
			if(mysql_num_rows($SearchResultNoSql) > 0)
			{
				while($MenuList=mysql_fetch_array($SearchResultNoSql))
				{
					$settingArray = array();
					if($MenuList['added_by']!=0){
					$getUserSettings ="SELECT * FROM user_settings 
									    WHERE user_id='".$MenuList['added_by']."'";
					
				    $getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());
	  
					$settingData = mysql_fetch_assoc($getSettingsResponse);
				   $settingArray=array(
				'DeliveryService'=>$settingData['DeliveryService']!=''?$settingData['DeliveryService']:'0',
				'StoreFront'=>$settingData['StoreFront']!=''?$settingData['StoreFront']:'0',
				'AcceptCreditCard'=>$settingData['AcceptCreditCard']!=''?$settingData['AcceptCreditCard']:'',
				'AcceptATMonSite'=>$settingData['AcceptATMonSite']!=''?$settingData['AcceptATMonSite']:'',
				'18YearsOld'=>$settingData['18YearsOld']!=''?$settingData['18YearsOld']:'',
				'21YearsOld'=>$settingData['21YearsOld']!=''?$settingData['21YearsOld']:'',
				'HandicapAssesseble'=>$settingData['HandicapAssesseble']!=''?$settingData['HandicapAssesseble']:'',
				'Security'=>$settingData['Security']!=''?$settingData['Security']:''	
										  );
					
					}else
					{
						$settingArray=array(
											  'DeliveryService'=>'0',
											  'StoreFront'=>'0',
											  'AcceptCreditCard'=>'0',
											  'AcceptATMonSite'=>'0',
											  '18YearsOld'=>'0',
											  '21YearsOld'=>'0',
											  'HandicapAssesseble'=>'0',
											  'Security'=>'0'
										  );
					}					
					
						//Set image url
						if(!empty($MenuList['image']))
						{
						  $MenuImageUrl=BASEURL.'images/dispensary_images/original/'.$MenuList['image'];
						  $MenuThumbImageUrl=BASEURL.'images/dispensary_images/thumbnail/'.$MenuList['image'];
						}
						else
						{
							$MenuImageUrl='';
							$MenuThumbImageUrl='';
						}
						
						// Fet followup flag
						$FWPflag=getFollowUpList($MenuList['dispensary_id'],$intValidUserId);
						$arraySearchMenuArray[] =  array(
														 'DispensaryId'=> $MenuList['dispensary_id'],
														 'DispensaryName'=> $MenuList['dispensary_name'], 
														 'City'=> $MenuList['city'],
														 'State'=> $MenuList['state'],
														 'ZipCode'=> $MenuList['zip'],
														 'flag'=>$FWPflag,
														 'DispensaryImageUrl'=> $MenuImageUrl,
														 'DispensaryThumbImageUrl'=>$MenuThumbImageUrl,
														 'PhoneNumber'=>$MenuList['phone_number'],
														 'settings'=>$settingArray
													  );// end of array
					}
					// If success everythig is good send header as "OK" and MenuDispensaryList list in reponse
					$result = array(
								'success' => '1',
								'TotalPageNo'=>$totalPages,
								'MenuDispensaryList' => $arraySearchMenuArray
								);
					$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
					// If Empty Menu List
					$error = array('success' => "0", "msg" => "Empty Menu List.");
					$this->response($this->toJson($error), 400,"application/json");
			}//end of if (mysql_num_rows($SearchLoungeListSql) > 0)
		
		
		
		
		
		
		}
	}
	else
	{
		// UserId or DispensaryId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId, DispensaryId, pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>