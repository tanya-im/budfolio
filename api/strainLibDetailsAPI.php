<?php
/*
	File Description - get the details of starinLib Details and user list
	File Name - starinLibDetailsAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable				
	 $intStarinLibId = $inputArray['starin_lib_id'];
	
	// Input validations
	if(isset($intStarinLibId))
	{
		$strainInfoArray = getStrainLibName($intStarinLibId);
		
		//SQL query to get Strain name used by users 
		$strStrainLiberarySQL = " SELECT strain_id, strain_name, user_id, dispensary_name, species, linage, 
								  avg( overall_rating )
								  FROM strains
								  WHERE strain_name ='".$strainInfoArray['strain_name']."'
								  GROUP BY user_id";


								
								
		$resStrainLiberarySQL = mysql_query($strStrainLiberarySQL) or die($strStrainLiberarySQL." : ".mysql_error());
		
		$StrainLibArray=array();
		
		while($strainLiberary = mysql_fetch_array($resStrainLiberarySQL))
		{
				//Strain Liberary array
				$StrainLibArray[] =  array(
											'user_id'=> $strainLiberary['user_id'],
											'user_name'=>getUserName($strainLiberary['user_id']),
											'dispensary_name'=> $strainLiberary['dispensary_name'],
											'ratting'=>$strainLiberary['overall_rating']!=''?$strainLiberary['overall_rating']:'0'
										);// end of array
		}
		// If success everythig is good send header as "OK" and sending Dispensary details in reponse
			$result = array('success' => '1',
							'strain_name' => $strainInfoArray['strain_name'],
							'species' => $strainInfoArray['species'],
							'lineage' => $strainInfoArray['lineage'],
							'apearance'=>$strainInfoArray['apearance']!=''?$strainInfoArray['apearance']:'',
							'smell'=>$strainInfoArray['smell']!=''?$strainInfoArray['smell']:'',
							'taste'=>$strainInfoArray['taste']!=''?$strainInfoArray['taste']:'',
							'users_list'=>$StrainLibArray);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Time is empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>