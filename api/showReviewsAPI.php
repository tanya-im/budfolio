<?php
/*
	File Description - show a review Apion dispensary popup
	File Name - showReviewsAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array

	//Taking all values into local variable				
	 $intDispensaryId = $inputArray['DispensaryId'];
	 $intPageSize = $inputArray['PageSize'];
     $intPageNo = $inputArray['PageNo'];	
	 
	// Input validations
	if(!empty($intDispensaryId))
	{
		$strReviewCountSQL = "SELECT count(user_id) as counts FROM tbl_dispensary_review 
						 	  WHERE dispensary_id='".$intDispensaryId."'";
							   
		$resCount = mysql_query($strReviewCountSQL) or die($strReviewCountSQL." : ".mysql_error());

		$strReviewCountResult = mysql_fetch_array($resCount);
		$strReviewData = $strReviewCountResult[0];
		$totalPages = ceil($strReviewData / $intPageSize);
			
		$perpage = $intPageSize;
		$page = $intPageNo;
		$calc = $perpage * $page;
		$start = $calc - $perpage;
			
		
		
		
		 $strGetReviewSQL  = "SELECT * FROM tbl_dispensary_review 
						 	  WHERE dispensary_id='".$intDispensaryId."' ORDER BY date_time desc";
		if(!empty($perpage) && !empty($page)){
			$strGetReviewSQL  .="  LIMIT $start,$perpage";
		}
		
		$resGetreviews = mysql_query($strGetReviewSQL) or die($strGetReviewSQL." : ".$strGetReviewSQL);
		
		if(mysql_num_rows($resGetreviews)>0)
		{
			$arrayReview = array();
			while($reviewRow = mysql_fetch_assoc($resGetreviews))
			{
				
				$arrayReview[] = array(
										'user_name'  => getUserName($reviewRow['user_id']),
										'strain_quality' => $reviewRow['strain_quality'],
										'strain_variety' =>  $reviewRow['strain_variety'],
										'service'		 => $reviewRow['service'],
										'atmosphere'     => $reviewRow['atmosphere'],
										'Privacy'		 => (($reviewRow['privacy']!='')?$reviewRow['privacy']:'0'),
										'Pricing'		 =>(($reviewRow['pricing']!='')?$reviewRow['pricing']:'0'),
										'review_text'    => $reviewRow['review_text']														
				);
			}
			//if every thing is ok then send success=1 and strain  id in response.
			$result = array( "success" => "1","reviewList"=>$arrayReview);
			$this->response($this->toJson($result),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "0","reviewList"=> array());
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Please select dispensary.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>