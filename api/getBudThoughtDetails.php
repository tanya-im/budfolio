<?php
/*
	File Description - File to get bud thought details of a user 
	File Name - budThoughtDetailsAPI.php
	creation date - 8th may
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}

	$dataJson = file_get_contents("php://input"); //getting json input

	$inputArray = json_decode($dataJson,true);
	
	//converting that json into array
	$intBudthoughtId = $inputArray['bud_thought_id'];
	$intUSerId = $inputArray['UserId'];
	
	// Input validations
	if(!empty($intBudthoughtId))
	{
		 //SQL query to get budThought 
		 $strGetBudThoughtSQL = "SELECT `user_id`,`bud_thought`,`picture_url`,`post_date` 								 						 FROM `tbl_bud_thought`
								 WHERE `bud_thought_id` ='".$intBudthoughtId."'";
								 
		$sql = mysql_query( $strGetBudThoughtSQL)or die( $strGetBudThoughtSQL." ".mysql_error());//executing strLoginSQL
		
		if(mysql_num_rows($sql)>0)
		{
			
			$budThgtDetails = mysql_fetch_assoc($sql);// getting all the bud thought detials
			$date = date('d|m|Y | h:i A',strtotime($budThgtDetails['post_date']));//conevrting date time into string format
			$likeCount = getLikeCount($intBudthoughtId);
			$commentCount = getCommentCount($intBudthoughtId);
			$likeFlag = isUserLiked($intBudthoughtId,$intUSerId);
			// get all the replies on the bud thought
			$strGetRepliesSQL = "SELECT * FROM tbl_comments WHERE bud_thought_id='".$intBudthoughtId."'";
			
			$resReply = mysql_query($strGetRepliesSQL) or die($strGetRepliesSQL." ".mysql_error());
			
			if($resReply){
				
				$arrayReplies = array();
				while($rowReply = mysql_fetch_assoc($resReply)){
				
					if($rowReply['picture_url']!=''){
							  $imgURL = BASEURL.BTTIMGPATH.$rowReply['picture_url'];
					}
					else
					{
								$imgURL = '';
					}
					$dateComment = date('d M Y / h:i A',strtotime($rowReply['create_date_time']));//conevrting date time into string format
				
					$arrayReplies[]= array(
								'comment_id'  => $rowReply['comment_id'],
								'comment'	  => $rowReply['bud_thought'],
								'user_id '    => $rowReply['user_id'],
								'user_name'   => getUserName($rowReply['user_id']),
								'comment_time'=> $dateComment,
								'picture_url' => $imgURL
					 );// end of the array
				}// end of while loop
			}// end of if($resreply==1)
			
			$arrayBuThought = array(
								"user_id" => $budThgtDetails['user_id'],
								'user_name' => getUserName($budThgtDetails['user_id']),
								"bud_thought" => $budThgtDetails['bud_thought'],
								"picture_url" => ($budThgtDetails['picture_url']!=''?  BASEURL.BTTIMGPATH.$budThgtDetails['picture_url']:''),
				"originalimage" => ($budThgtDetails['picture_url']!=''?  BASEURL.BTIMGPATH.$budThgtDetails['picture_url']:''),
								 "date"=> $date,
								 "LikeCount" => $likeCount,
								 "CommentCount" => $commentCount,
								 "likeFlag" =>$likeFlag,
								 "reply"=>$arrayReplies 
							  );
				
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1",'bud_thought_details' => $arrayBuThought);
			$this->response($this->toJson($result),200,"application/json");

		} // closing ($sql == 1)
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "No such bud thought found.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	} //end of input validations
	else
	{
		// If either username or passowrd is empty
		$error = array('success' => "0", "msg" => "Please enter bud thought.");
		$this->response($this->toJson($error), 400,"application/json");
	}
?>