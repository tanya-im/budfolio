<?php
/*
	File Description - gives Followers of a user
	File Name - FollowersListAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	
	// Input validations
	if(!empty($intUserId))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		
		//SQL query to get Lounge list(strains and bud thought together date wise)
		$strFollowingsListQuery = "SELECT * FROM users
								   WHERE user_id IN
								  (
								  	SELECT `follower_id`
									FROM `user_followup`
									WHERE user_id = '".$intUserId."'	
								  )";

		$FollowingsListSQL = mysql_query($strFollowingsListQuery) or die($strFollowingsListQuery." : ".mysql_error());			       //executing strLoginSQL
			
			$arrayFollowingsListArray=array();
			
			if(mysql_num_rows($FollowingsListSQL) > 0)
			{
				while($FollowingsList = mysql_fetch_array($FollowingsListSQL))
				{
					
					if(!empty($FollowingsList['photo_url']))
					{			
						$thumbImageurl=BASEURL.USER_PROFILE_IMG_THUMB.$FollowingsList['photo_url'];
					}
					else
					{
						$thumbImageurl='';
					}
					
					$arrayFollowingsListArray[]= array(
						'UserId' => $FollowingsList['user_id'],
						'UserName'  => $FollowingsList['user_name'],
						'ProfileImageThumbnail'=>$thumbImageurl
					);
					

				}//end of while
				
				
				// If success everythig is good send header as "OK" and LoungeList in reponse
$result = array('success' => '1','FollowingList' => $arrayFollowingsListArray);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// Empty Lounge List
				$error = array('success' => "1",'FollowingList' => array());
				$this->response($this->toJson($error), 400,"application/json");
			}
		
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId is empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>