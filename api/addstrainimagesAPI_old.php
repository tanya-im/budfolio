<?php
/*
	File Description - file to add strain images
	File Name - addstrainimagesAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	$intStrainId= $_REQUEST['StrainId'];
	if(empty($intStrainId))
	{
		$error = array('success' => "0", "msg" => "Empty strain id.");
		$this->response($this->toJson($error), 406, "application/json");
	}
	$intValidStrainId = mysql_real_escape_string($intStrainId);
	$strSQLChkStrainId ="SELECT strain_name FROM strains WHERE strain_id='".$intValidStrainId."'";
	$SQLResChkStrainId = mysql_query($strSQLChkStrainId);// execution of query intValidStrainId
	if(mysql_num_rows($SQLResChkStrainId)< 1)
	{
		$error = array('success' => "0", "msg" => "Invalid strain id.");
// If no records "Error msg" status
		$this->response($this->toJson($error), 406, "application/json");
	}
// Adding  Inserting strain images
	if(isset($_FILES))
	{
		$name = $_FILES['StrainsImage']['name'];
		$size = $_FILES['StrainsImage']['size'];
		if(strlen($name))
		{
			list($txt, $ext) = explode(".", $name);
			$extention = explode(".", $name);
			$arrayconunt=(count($extention)-1);
			if(!empty($extention[$arrayconunt]))
			{
				$ext=$extention[$arrayconunt];
			}
			if(in_array(strtolower($ext),$valid_formats))
			{
				if($size< ImageSize)
				{
					$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $_FILES['StrainsImage']['tmp_name'];
					if(move_uploaded_file($tmp,'../'.$strainimagefoldername_original.$actual_image_name))
					{
						$strAddImagesQuery="insert into strainimages(image_name,strain_id) values('".$actual_image_name."','".$intValidStrainId."')";
						$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());
						$ImageId = mysql_insert_id();
						$src='../'.$strainimagefoldername_original.$actual_image_name;
						$dest_m='../'.$strainimagefoldername_medium.$actual_image_name;
						$dest_t='../'.$strainimagefoldername_thumbnail.$actual_image_name;
						resize_image($src,$dest_m,300,300);
						resize_image($src,$dest_t,100,100);
						//if registration is successfull then success=1 and user id
						$result = array( "success" => "1", 'ImageId' => $ImageId);
						$this->response($this->toJson($result),200,"application/json");
					}
					else	
					{
						$error = array('success' => "0", "msg" => "Image upload failed. Please try agian.");
						// If no records "Error msg" status
						$this->response($this->toJson($error), 406, "application/json");
					}
				}
				else
				{
					$error = array('success' => "0", "msg" => "Image file size max 3 MB");
					// If no records "Error msg" status
					$this->response($this->toJson($error), 406, "application/json");					
				}
			}
			else
			{
				$error = array('success' => "0", "msg" => "Invalid file format.");
				// If no records "Error msg" status
				$this->response($this->toJson($error), 406, "application/json");	
			}
		}
		else
		{
			$error = array('success' => "0", "msg" => "Please select image.");
			// If no records "Error msg" status
			$this->response($this->toJson($error), 406, "application/json");
		}		
	
	}