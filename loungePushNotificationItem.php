<?php
// Include header,auth and config file.
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include_once("function.php");
include_once("timeAgoUtil.php");
include_once("lounge_functions.php"); 
$_SESSION['loungecounter']=1;
$_SESSION['searchtext']='';
?>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container">
        <div id="msg"></div>
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2> Lounge</h2>  <div class="budthough_btn"> <a href="bud_thought_form.php"> <img src="images/new_web/budthought_btn.png" class="scale-with-grid"> </a> </div></div>
                <div class="greenbar_bg"><?php include('Lounge_Filter.php');?> </div>
                <div class="greenbar_bg_new">		<div class="input-append"> <div class="searchbox" style="width:626px;"> <img src="images/new_web/serach_icon.png" class="scale-with-grid"> <input type="text" id="sl_search_text" name="sl_search_text" placeholder="Username, strain, hashtags"> </div> <a href="javascript:void(0);" class="pushtop9 search_btn <?php if($filename=='outerLounge.php' || $filename=='newLounge.php'){?>lounge_search <?php }else if($filename=='loungeFollowupList.php'){?>loungeFollowSearch<?php  }else {?> loungeMessageSearch <?php } ?>" > Search <!--<img src="images/new_web/search_btn.png" class="scale-with-grid">--> </a> </div></div>
                <div id="content_1" class="content">
             		<?php
			
					if($_GET['t']==0 || $_GET['t']==2 || $_GET['t']==3 || $_GET['t']==4)
					{
						
						$fetchDataSQl= "SELECT 
												bud_thought_id as strain_id,
												bud_thought as strain_name,
												NULL AS dispensary_name,
			   									post_date, NULL AS species,
			   									NULL AS overall_rating,
												tbt.user_id as userId,
												picture_url,
												0 AS TYPE
			   							FROM tbl_bud_thought tbt
			   							INNER JOIN users ON users.user_id = tbt.user_id
			   							WHERE users.flag = 'active'
			   							AND delete_status='0'
										AND bud_thought_id='".$_GET['tid']."'
										LIMIT 0,1";
						
					}else if ($_GET['t']==1)
					{
						$fetchDataSQl= "SELECT strain_id,
											   strain_name,
											   dispensary_name,
											   post_date,
											   species,
											   overall_rating,
			   		                           strains.user_id as userId,
											   NULL AS picture_url,
											   1 AS TYPE
			   							FROM strains
										INNER JOIN users ON users.user_id = strains.user_id
			   							WHERE users.flag = 'active'
			   						   	AND  strains.flag = 'active'
			   							AND strain_id='".$_GET['tid']."'
										LIMIT 0,1";
					}
					$result =mysql_query($fetchDataSQl) or die($fetchDataSQl." : ".mysql_error());
					
                   	$straindata = mysql_fetch_array($result);
                  
					//$dateTime = explode(' ',$straindata['post_date']);
					$date = date('d|m|Y | h:i A',strtotime($straindata['post_date']));
					if($straindata['TYPE']==1) 	
					{
							 //includeing external file for showing strains
							 include("newloung_strain_details.php");
							 
					}//end of if(type==1 //strain)
					else
					{
							
							//includeing external file for showing bud thoughts 
							include('newLounge_budThought_details.php');	 
							
					}//end of else//bud thought
						
					
			
?>            	
    <!--    End loung elisting -->
	<div class="clear">&nbsp;</div>
   <!-- <div style="border:1px solid green;" class="scrollExtend-loading"><img src="images/loading-bars.gif" /></div>-->
                </div>
            </div>
        </div>
        
        <?php include('newLounge_right.php');?>
    </div>
</div>
<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>
<div id="boxes"></div>
<div id="mask"></div> 
</body>

</html>
