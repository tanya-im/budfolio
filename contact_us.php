<?php session_start();
// Include header,auth and config file.
include_once("newHeader.php");
//require_once("auth.php");
include_once("config.php");
include('function.php');
?>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con" >
    	<div class="inner_conlft scroll_container" id="inner_main_div">
        	<div class="cell_con"><div class="gray_headingbar">  <h2> Contact Us </h2> </div>
             <p style="margin-top:3%; float:right;"> All the fields are mandatory </p> 
            <div style="text-align:center;" >
            
            <table style="width:100%;border-collapse:collapse; float: left;">
            	<tr style="width:100%; margin-left:2%;border:none;"><td style="width:15%; text-align:right;border:none;">Name:</td><td style="width:60%;border:none;"><input type="text" id="conatctName" style="width:70%" /></td></tr>
                <tr><td style="text-align: right;border:none;">Email Id:</td><td style="border:none;"><input type="text" id="conatctEmail" style="width:70%" /></td></tr>
                <tr><td style="text-align: right;border:none;">Contact No:</td><td style="border:none;"><input type="text" id="conatctNo" maxlength="12" style="width:70%"/></td></tr>
                <tr><td style="text-align: right;border:none;">Address:</td><td style="border:none;"><textarea type="" id="address" style="width:70%;"></textarea></td></tr>
                <tr><td style="text-align: right;border:none;">Comment:</td><td style="border:none;"><textarea type="" id="conatctComment" style="width:70%;"></textarea></td></tr>
                <tr><td style="border:none;"></td><td style="border:none;float: right;margin-right: 14%;"><input id="saveInfo" type="button" value="submit" /></td></tr>
            </table>
            
           	</div> 
         </div>
       </div>
	<?php include('newLounge_right.php');?>
    </div>
</div>
<script>
$("#saveInfo").click(function(){
	
	var errotText ='';
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var phoneReg = /^[0-9]+$/;
	
	var name=$("#conatctName").val();
	var email=$("#conatctEmail").val();
	var conatctNo=$("#conatctNo").val();
	var address=$("#address").val();
	var Comment=$("#conatctComment").val();
	
	
	if($.trim($('#conatctName').val())=='')
		{
			 errotText+='Please provide name.\n';
		}
	if($.trim($('#conatctEmail').val())=='')
		{
			 errotText+='Please provide email ID.\n';
		} 
	if($.trim($('#conatctEmail').val())!='' && !emailReg.test($.trim($('#conatctEmail').val())))
		{
			 errotText+='Please provide an valid email ID.\n';
		}  
	if($.trim($('#conatctNo').val())=='')
		{
			errotText+='Please provide phone no.\n';
		} 
	
	if($('#conatctNo').val()!='' && !phoneReg.test($('#conatctNo').val()))
		{
			errotText+='Please provide a valid phone number.\n';
		}
	if($.trim($('#address').val())=='')
		{
			 errotText+='Please provide address.\n';
		} 
	if($.trim($('#conatctComment').val())=='')
		{
			 errotText+='Please provide Comment.\n';
		} 
		
	if(errotText!='')
		{
			var ErroFinal='Please correct the following errors: \n\n';
			ErroFinal = ErroFinal+errotText
			alert(ErroFinal);
			return false
		}
	else
	{
		//alert('error'); 
	}  
		
	$.ajax({
			type: "POST",
			url: "contactInfoInsert.php",
			data: 'name='+name+'&email='+email+'&conatctNo='+conatctNo+'&address='+address+'&Comment='+Comment+'&contactInfo=Info',
			success: function(data)
			{
				alert("mail sent");
				window.location.href="contact_us.php";
			}
	});
});
</script>

<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>
     