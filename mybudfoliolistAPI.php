<?php
/*
	File Description - Budfolio List
	File Name - mybudfoliolistAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array

	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	// Input validations
	if(!empty($intUserId) && !empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		// to check whether the user id is exist.
		$uservalidation=CheckValidUser($intValidUserId);
		if($uservalidation)
		{
			$strstrainnoQuery="select Count(*) As Total from strains 
							  where flag='Active' and user_id='".$intValidUserId."'";
			$strstrainnoSql = mysql_query($strstrainnoQuery);
			$strstrainnoResult=mysql_fetch_array($strstrainnoSql);
			$strstrainnoData=$strstrainnoResult[0];
			$totalPages = ceil($strstrainnoData / $intPageSize);
			
			$perpage=$intPageSize;
			$page=$intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;
//SQL query to get budfolio list
			$strBudfolioListQuery = "SELECT strain_id,strain_name,dispensary_name,species,overall_rating 
									FROM strains WHERE flag='Active' and user_id ='".$intValidUserId."' 
									order by strain_id desc  Limit $start, $perpage";
			$arrayBudfolioListArray=array();
			$BudfolioListSQL = mysql_query($strBudfolioListQuery) or die(mysql_error());//executing Query
			if(mysql_num_rows($BudfolioListSQL) > 0)
			{
				while($BudfolioList=mysql_fetch_array($BudfolioListSQL))
				{
//SQL query to get budfolio list
					//$primaryimages=GetPrimaryImages($BudfolioList['strain_id']);
					$thumbImg = GetThreeImages($BudfolioList['strain_id']);
					$arrayBudfolioListArray[] =  array(
												 		'StrainId'=> $BudfolioList['strain_id'],
														'StrainName'=> $BudfolioList['strain_name'], 
														'Dispensory'=> $BudfolioList['dispensary_name'],
														'Species'=> $BudfolioList['species'],
														'OverallRating'=> $BudfolioList['overall_rating'],
														'Thumb1'=> (!empty($thumbImg[0]))? $thumbImg[0] :'',
														'Thumb2'=> (!empty($thumbImg[1]))? $thumbImg[1] :'',
														'Thumb3'=> (!empty($thumbImg[2]))? $thumbImg[2] :''
												);// end of array
				}
// If success everythig is good send header as "OK" and user details
// sending medicinal use list in reponse
				$result = array('success' => '1','MyBudfolioList' => $arrayBudfolioListArray,'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// If Empty Budfolio List
				$error = array('success' => "0", "msg" => "Empty Budfolio List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If user id empty
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>