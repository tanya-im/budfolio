/**
 *
 * Crop Image While Uploading With jQuery
 * 
 * Copyright 2013, Resalat Haque
 * http://www.w3bees.com/
 *
 */

// set info for cropping image using hidden fields
function setInfo(i, e) {
	$('#xbt').val(e.x1);
	$('#ybt').val(e.y1);
	$('#wbt').val(e.width);
	$('#hbt').val(e.height);
}

$(document).ready(function() {
	var p = $("#uploadPreviewBT");

	// prepare instant preview
	$("#commentImg").change(function(){
		// fadeOut or hide preview
		p.fadeOut();

		// prepare HTML5 FileReader
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("commentImg").files[0]);

		oFReader.onload = function (oFREvent) {
	   		p.attr('src', oFREvent.target.result).fadeIn();
		};
	});

	// implement imgAreaSelect plug in (http://odyniec.net/projects/imgareaselect/)
	$('img#uploadPreviewBT').imgAreaSelect({
		// set crop ratio (optional)
		aspectRatio: '1:1',
		onSelectEnd: setInfo
	});
});