$(document).ready(function(){
	$(".fadinmsg").delay(9000).fadeOut("slow");
	
});

function alluser()
{
	window.location='newLounge.php';
}
function allUserWidOutLogin()
{
	window.location='outerLounge.php';
}

function localuser()
{
	window.location='local_user_lounge.php';
}
function LoungeFollowUp()
{
	window.location='loungeFollowupList.php';
}

function LoungeNotification()
{
	window.location='loungeNotificationList.php';
}

function LoungePushNotification()
{
	window.location='loungePushNotificationList.php';
}

function mylistMenu()
{
	window.location='my_menu_list.php';
}


function alluserMenu()
{
	window.location='menu.php';
}
function alluserMenuDispensary()
{
	window.location='dispensary_menu.php';
}
function outerMenu()
{
	window.location='outereMenu.php';
}
function localuserMenu()
{
	window.location='local_menu.php';
}
function MenuFollowUp()
{
	window.location='MenuFollowUp.php';
}

ddaccordion.init({
	headerclass: "expandable", //Shared CSS class name of headers group that are expandable
	contentclass: "categoryitems", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded:false, //index of content(s) open by default [index1, index2, etc]. [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		$(".categoryitems").hide();
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		
	}
});