function addRow(tableID)
{
	var tblno=tableID.split("_");
	var table = document.getElementById(tableID);
	var crtdtrno=$("#createdtr_"+tblno[1]).val();
	crtdtrno=parseInt(crtdtrno)+1;
	console.log(crtdtrno);
	$("#createdtr_"+tblno[1]).val(crtdtrno);
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	var colCount = table.rows[0].cells.length;
	for(var i=0; i<colCount; i++)
	{
		var newcell = row.insertCell(i);
		newcell.innerHTML = table.rows[1].cells[i].innerHTML;
		switch(newcell.childNodes[0].type)
		{
			case "text":
				newcell.childNodes[0].value = "";
				newcell.childNodes[0].name = "menudetail_"+tblno[1]+"_"+crtdtrno+"[]";
			break;
			case "checkbox":
			console.log('checkbox');
				newcell.childNodes[0].name = "menudetail_"+tblno[1]+"_"+(crtdtrno)+"[]";
				newcell.childNodes[0].checked = false;
			break;
			case "button":
			console.log('button');
				newcell.childNodes[0].name = "menudetail_"+tblno[1]+"_"+(crtdtrno)+"[]";
				newcell.childNodes[0].id= "chk_"+tblno[1]+"_"+(crtdtrno);
			break;
			case "select-one":
			console.log('select-one');
				newcell.childNodes[0].selectedIndex = 0;
			break;
			case "hidden":
				newcell.childNodes[0].name = "menudetail_"+tblno[1]+"_"+(crtdtrno)+"[]";
				newcell.childNodes[0].id= "hide_"+tblno[1]+"_"+(crtdtrno);
			break;
		}
	}
}
function addColumn(tblId)
{
	var tblno=tblId.split("_");
	var table = document.getElementById(tblId);
	var tblBodyObj = document.getElementById(tblId).tBodies[0];
	var collength=table.rows[0].cells.length;
	if(collength>=8)
	{
		alert("Max column number is 7. You can not aad more than 7 column.");
		return false;
	}
	for (var i=0; i<tblBodyObj.rows.length; i++)
	{
		var newnm=tblBodyObj.rows[i].cells[1].childNodes[0].name;
		//alert(tblBodyObj.rows[i].cells[1].childNodes[0]);alert(tblBodyObj.rows[i].cells[1].childNodes[0].value);
		
		var newcelltextboxname=newnm.split("_");
		var newCell = tblBodyObj.rows[i].insertCell(-1);
		newCell.innerHTML = table.rows[0].cells[1].innerHTML;
		newCell.childNodes[0].value = "";
		newCell.childNodes[0].name = "menudetail_"+tblno[1]+"_"+newcelltextboxname[2];
	}
}
function deleteColumn(tblId)
{
	var allRows = document.getElementById(tblId).rows;
	for (var i=0; i<allRows.length; i++) 
	{
		if (allRows[i].cells.length <= 2)
		{
			alert("You can not delete all the columns.");
			break;
		}
		allRows[i].deleteCell(-1);
	}
}
function deleteRow(tableID)
{
	try
	{
		var flag=false;
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		for(var i=0; i<rowCount; i++)
		{
			var row = table.rows[i];
			var chkbox = row.cells[0].childNodes[0];
			
			if(null != chkbox && true == chkbox.checked)
			{
				flag=true;
				if(rowCount <= 1) 
				{
					alert("You can not delete all the rows.");
					break;
				}
				table.deleteRow(i);
				rowCount--;
				i--;
				
			}
		}
		if(flag==false)
		{
			alert("Please select row to delete.");
		}
	}
	catch(e)
	{
		alert(e);
	}
}

$("#removeoption").click(function(){
	
	return false;
	
	var r=confirm("Are you sure you want to remove these menu dispensary option?");
	if (r==true)
	{
		$('.rmtbl').each(function() {
       		var id=$(this).attr("id");
			var tblid=id.split("_");
			if ($('#removetable_'+tblid[1]).is(':checked'))
    		{
        	   	$("#tblwrapper_"+tblid[1]).remove();
       		}
		});
	}
});

function validate_disp()
{
	if($("#dispensaryname").val()=='')
	{
		$("#dispensaryname").focus();
		alert("Please enter the value");
		return false;
	}
	if($("#businessname").val()=='')
	{
		$("#businessname").focus();
		alert("Please enter the value");
		return false;
	}
	if($("#email").val()=='')
	{
		$("#email").focus();
		alert("Please enter the value");
		return false;
	}
	if($("#website").val()=='')
	{
		$("#website").focus();
		alert("Please enter the value");
		return false;
	}
	
	return true;
}
function createnewdispcancel(url)
{
	window.location.href=''+url+'';	
}

$(document).ready(function() {
	$('#dispensaryname').keyup(function(){ 
	var dispval=$(this).val();
	if(dispval=='')
	{
		return false;
	}
	$.ajax({
			type: "POST",
			url: "validate_dispancry_name.php",
			data: 'dispval='+dispval,
			async:   false,
			success: function(data)
			{
				if(data=='exist')
				{
					$("#dispnamediv").html('<span class="dispnamevalidate">Dispensary name already exists.</span>');
				}
				else
				{
					$("#dispnamediv").html("");
				}
			}
		});
	});
	
	$('#dispensaryname').focusout(function(){ 
	var dispval=$(this).val();
	if(dispval=='')
	{
		return false;
	}
	$.ajax({
			type: "POST",
			url: "validate_dispancry_name.php",
			data: 'dispval='+dispval,
			async:   false,
			success: function(data)
			{
				if(data=='exist')
				{
					$("#dispnamediv").html('<span class="dispnamevalidate">Dispensary name already exists.</span>');
				}
				else
				{
					$("#dispnamediv").html("");
				}
			}
		});
	});
});

//7th june -remove option
function remove_option()
{

	var r=confirm("Are you sure you want to remove these menu dispensary option?");
	if (r==true)
	{
		$('.rmtbl').each(function() {
       		var id=$(this).attr("id");
			var tblid=id.split("_");
			if ($('#removetable_'+tblid[1]).is(':checked'))
    		{
        	   	$("#tblwrapper_"+tblid[1]).remove();
       		}
		});
	}	
}
$('.hide_menu_row').live('click',function(){
	
	console.log('abc');
	var table_row_no = $(this).attr("name"); console.log(table_row_no);
	var removedBracketID = table_row_no.split('[]');
	
	var id_hidden_field = removedBracketID[0].split('_');
// alert('#hide_'+id_hidden_field[1]+'_'+id_hidden_field[2]);
	if($(this).hasClass('show_this_row'))
	{
		
	    $('#hide_'+id_hidden_field[1]+'_'+id_hidden_field[2]).val('hide');
		$(this).addClass('hide_this_row');
		$(this).removeClass('show_this_row');
		$(this).attr('title',"Show this row");
		$(this).val('H');
		//$(this).attr("name").css({'background':'#0C6'});
	
	}
	else
	{
		$('#hide_'+id_hidden_field[1]+'_'+id_hidden_field[2]).val('');
		$(this).addClass('show_this_row');
		$(this).removeClass('hide_this_row');
		$(this).attr('title',"Hide this row");
		$(this).val('S');
	}
})