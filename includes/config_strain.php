<?php
// factor for the real size of the uploaded image
$sizefactor=3;

// size of the big, preview and thumb container
$bigWidthPrev=600;
$bigHeightPrev=300;

// canvas size for the uploaded image
$canvasWidth=$bigWidthPrev*$sizefactor;
$canvasHeight=$bigHeightPrev*$sizefactor;

// file type error
$fileError='Filetype not allowed. Please upload again. Only GIF, JPG and PNG files are allowed.';
$sizeError='File is too big. Please upload again. Maximum filesize is 1.3MB.';

// image upload folders
$imgbig='./images/uploads/big/'; // folder for the uploads after cropping
$imgtemp='./images/uploads/tmp/'; // temp-folder before cropping
$imgthumb='./images/uploads/original/'; // folder with big uploaded images*/

$path_medium="images/uploads/medium/";
$path_thumbnail="images/uploads/thumbnail/";
$path_cropped="images/uploads/cropped/";
/*// image upload folders
$imgthumb='upload/'; // folder for the uploads after cropping
$imgtemp='upload/tmp/'; // temp-folder before cropping
$imgbig='upload/big/'; // folder with big uploaded images
*/

// max file-size for upload in bytes, default: 3mb
$maxuploadfilesize=3200000;

// background color of the canvas as rgb, default:white
$canvasbg=array('r'=>255,'g'=>255,'b'=>255);
?>
