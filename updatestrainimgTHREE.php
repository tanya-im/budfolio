<?php
session_start();
include_once("config.php");
include_once("function.php");
// define the image folder path
$path = "images/uploads/original/";
$path_medium="images/uploads/medium/";
$path_thumbnail="images/uploads/thumbnail/";
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
	$name = $_FILES['updatephotoimgthree']['name'];
	$size = $_FILES['updatephotoimgthree']['size'];
	if(strlen($name))
	{
		// check image exists or not
		list($txt, $ext) = explode(".", $name);
		$extention = explode(".", $name);
		$arrayconunt=(count($extention)-1);
		if(!empty($extention[$arrayconunt]))
		{
			$ext=$extention[$arrayconunt];
		}
		// Check image formate valid or not  
		if(in_array(strtolower($ext),$valid_formats))
		{
			// check image size not more than 3 Mb.
			if($size< ImageSize)
			{
				$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
				$tmp = $_FILES['updatephotoimgthree']['tmp_name'];
				// Move to uploaded file from tem dir to destination dir.
				if(move_uploaded_file($tmp, $path.$actual_image_name))
				{
					$src=$path.$actual_image_name;
					$dest_m=$path_medium.$actual_image_name;
					$dest_t=$path_thumbnail.$actual_image_name;
					resize_image($src,$dest_m,300,300);
					resize_image($src,$dest_t,100,100);
					$_SESSION['images'][2]=$actual_image_name;
					echo '<span id="strainimageid_3" class="removeupdateimage">&nbsp;</span>';
					echo "<img src='images/uploads/original/".$actual_image_name."'  class='preview'>";
				}
				else	
				{
					echo "failed";
				}
			}
			else
			{
				echo "Image file size max 6  MB";					
			}
		}
		else
		{
			echo "Invalid file format..";	
		}
	}
	else
	{
		echo "Please select image..!";
	}		
	exit;
}
?>