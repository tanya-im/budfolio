<?php session_start(); ob_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Budfolio-Index</title>
<!-- Include css and js files-->
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/scroller.css" />
<link rel="stylesheet" type="text/css" href="css/css.css" /><!-- 21st june-->
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<!--<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<!-- End-->
<!--[if IE]>
<style type="text/css">
.userinput input{
 padding: 7px 0 0 0px;
    height:20px;
    margin:0px 0px 0px 5px;
}

.passwordinput input{
 padding: 7px 0 0 0px;
    height:20px;
    margin:0px 0px 0px 5px;
}
.btn{
	margin-left:40px;
}    
</style>
<![endif]-->
</head>
<body>
	<div class="main_con">
    	<div class="header"> 
        	<div class="logo">
            	<!--<a href="index.php"><img src="images/css_images/logo.png" width="498" height="127" ></a>-->
                <img src="images/css_images/logo.png" width="364" height="118" >
            </div>
            <div class="header_link">
            	<ul>	
<?php 			if(isset($_SESSION['user_name'])){?>
					<li>
						<a href="logout.php">
							<img src="images/css_images/logout_icon.png" width="24" height="24">&nbsp;&nbsp;Logout</a>
					</li>
<?php 			}else{?>
					<li><a target="_blank" href="https://twitter.com/budfolio"><img src="images/css_images/twt_icon.png" width="33" height="33"></a></li>
                    <li><a href="uconst"><img src="images/css_images/fb_icon.png" width="33" height="33"></a></li>
                    <li><a href="uconst"><img src="images/css_images/insta_icon.png" width="33" height="33"></a></li>
<?php 			}?>   
				</ul>
            </div>
		</div>
<?php if(isset($_SESSION['user_name'])){ if(!empty($_SESSION['user_name'])){?>
<?php $requestname=explode("/",$_SERVER['SCRIPT_NAME'])	;
 	  $count=count($requestname);
	  $count=$count-1;
?>        
        <div class="nav_con"> 
        	<ul>
            	<li>
                	<a href="home.php" <?php if($requestname[$count]=='home.php'){ ?> id="my_active" <?php } ?>  class="my">My Budfolio </a>
               </li>
               
               <li> 
                 <a href="lounge.php" class="lounge" <?php if($requestname[$count]=='lounge.php' || $requestname[$count]=='local_user_lounge.php'){?> id="lounge_active" <?php } ?>> Lounge </a>
               </li>
                
               <li> 
               <a <?php if($_SESSION['dispansary']=='1'){?>href="dispensary_menu.php" <?php }else{?>href="menu.php" <?php } ?> class="menu" <?php if($requestname[$count]=='menu.php'){?>  <?php }?>> Menu </a>    
               </li>
                
              <li style="margin-left:253px;" id="but_thught"> 
             	<?php if($requestname[$count]=='lounge.php' || $requestname[$count]=='local_user_lounge.php' || $requestname[$count]=='loungeFollowupList.php'|| $requestname[$count]=='loungeNotificationList.php'){?> 
                	<a href="javascript:void(0);"  onclick="OpenBudThoughtBox();"> 
             			<img src="images/bud_thought/budhtought_btn.png" width="59" height="41"></a>
                <?php } ?>
               </li>
               <li><a href="strain_liberary.php">Strain Liberary</a></li>
            </ul>
        </div>
<?php	} 	}//echo phpinfo();?>