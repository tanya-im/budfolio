<?php
/* 
	File Description - file to add strain images
	File Name - addstrainimagesAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	
	//Taking all values into local variable		
	$intStrainId= $_REQUEST['StrainId'];
	
	$flag=false;
	
	if(empty($intStrainId))
	{
		$error = array('success' => "0", "msg" => "Empty strain id.");
		$this->response($this->toJson($error), 406, "application/json");
	}
	// Sanatize the values 	
	$intValidStrainId = mysql_real_escape_string($intStrainId);
	
	// Check strain id is valid or not.	
	$strSQLChkStrainId ="SELECT strain_name 
						 FROM strains WHERE strain_id='".$intValidStrainId."' 
						 and flag='active'";
	$SQLResChkStrainId = mysql_query($strSQLChkStrainId);// execution of query intValidStrainId
	if(mysql_num_rows($SQLResChkStrainId)< 1)
	{
		$error = array('success' => "0", "msg" => "Invalid strain id.");
		$this->response($this->toJson($error), 406, "application/json");
	}
	
	//for image one
	$img_1_status =$_REQUEST['StrainsImageStatus_1'];// 0-add,1-update,2-delete
	$old_img_url_1= $_REQUEST['StrainsImageOldUrl_1'];
	
	
	if($img_1_status==0){// add image to db and upload to server
		
		if(isset($_FILES['StrainsImageData_1']))
		{
			echo "in img";
			$name = $_FILES['StrainsImageData_1']['name'];
			$size = $_FILES['StrainsImageData_1']['size'];
			/*if(strlen($name))
			{*/
				list($txt, $ext) = explode(".", $name);
				$extention = explode(".", $name);
				$arrayconunt=(count($extention)-1);
				if(!empty($extention[$arrayconunt]))
				{
					$ext=$extention[$arrayconunt];
				}
				/*if(in_array(strtolower($ext),$valid_formats))
				{*/
					/*if($size < ImageSize)
					{*/
				
						$actual_image_name = time().rand().substr(str_replace(" ", "_", $txt), 5).".".$ext;
						$tmp = $_FILES['StrainsImageData_1']['tmp_name'];
						if(move_uploaded_file($tmp,'../'.$strainimagefoldername_original.$actual_image_name))
						{
							
							echo "in upload"; 
							$strAddImagesQuery="insert into strainimages
												(image_name,strain_id,primary_image) 
												values(
												'".$actual_image_name."',
												'".$intValidStrainId."',
												'0')";
							echo $strAddImagesQuery;
							$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());
							$ImageId = mysql_insert_id();
							
							$src='../'.$strainimagefoldername_original.$actual_image_name;
							$dest_m='../'.$strainimagefoldername_medium.$actual_image_name;
							$dest_t='../'.$strainimagefoldername_thumbnail.$actual_image_name;
							resize_image($src,$dest_m,300,300);
							resize_image($src,$dest_t,115,115);
							$ImageStatus[1]['ImageId']=$ImageId;
							$ImageStatus[1]['success']=1;
							$ImageStatus[1]['msg']="Image one upload successfully."; 
							$flag=true; 
						}
						else	
						{
							$ImageStatus[1]['ImageId']=0;
							$ImageStatus[1]['success']=0;
							$ImageStatus[1]['msg']="Image one upload failed. Please try agian."; 
						}
					/*}
					else
					{
					  $ImageStatus[1]['ImageId']=0;
					  $ImageStatus[1]['success']=0;
					  $ImageStatus[1]['msg']="Image one file size max 3 MB."; 
					  $error = array('success' => "0", "msg" => "");
					}*/
				/*}
				else
				{
				  $ImageStatus[1]['ImageId']=0;
				  $ImageStatus[1]['success']=0;
				  $ImageStatus[1]['msg']="Image one Invalid file format."; 
				}*/	
					
			/*}
			else
			{
				$ImageStatus[1]['ImageId']=0;
				$ImageStatus[1]['success']=0;
				$ImageStatus[1]['msg']="Please select image one."; 
			}*/		
	
		}
	
	}else if($img_1_status=='1'){                       // UPDATEING IMAGE
		//extracting old imgaename only
		$oldUrlImg = explode('/',$old_img_url_1);
		$len = count($oldUrlImg);
		
		
		if(isset($_FILES['StrainsImageData_1']))
		{
			echo "in update get img";
			$name = $_FILES['StrainsImageData_1']['name'];
			$size = $_FILES['StrainsImageData_1']['size'];
			if(strlen($name))
			{
				list($txt, $ext) = explode(".", $name);
				$extention = explode(".", $name);
				$arrayconunt=(count($extention)-1);
				if(!empty($extention[$arrayconunt]))
				{
					$ext=$extention[$arrayconunt];
				}
				/*if(in_array(strtolower($ext),$valid_formats))
				{
					if($size < ImageSize)
					{*/
				
					$actual_image_name = time().rand().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $_FILES['StrainsImageData_1']['tmp_name'];
					if(move_uploaded_file($tmp,'../'.$strainimagefoldername_original.$actual_image_name))
					{
						echo "in upload update";
						$strAddImagesQuery="UPDATE strainimages
											SET image_name ='".$actual_image_name."'
											WHERE strain_id='".$intValidStrainId."'
											AND image_name='".$oldUrlImg[$len-1]."'
											";
											
						echo $strAddImagesQuery;					
						$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());
						$ImageId = mysql_insert_id();
						
						$src='../'.$strainimagefoldername_original.$actual_image_name;
						$dest_m='../'.$strainimagefoldername_medium.$actual_image_name;
						$dest_t='../'.$strainimagefoldername_thumbnail.$actual_image_name;
						resize_image($src,$dest_m,300,300);
						resize_image($src,$dest_t,115,115);
						$ImageStatus[1]['ImageId']=$ImageId;
						$ImageStatus[1]['success']=1;
						$ImageStatus[1]['msg']="Image one upload successfully."; 
						$flag=true; 
					}
					else	
					{ echo "in else update";
						$ImageStatus[1]['ImageId']=0;
						$ImageStatus[1]['success']=0;
						$ImageStatus[1]['msg']="Image one upload failed. Please try agian."; 
					}
					/*}
					else
					{
					  $ImageStatus[1]['ImageId']=0;
					  $ImageStatus[1]['success']=0;
					  $ImageStatus[1]['msg']="Image one file size max 3 MB."; 
					  $error = array('success' => "0", "msg" => "");
					}
				}
				else
				{
				  $ImageStatus[1]['ImageId']=0;
				  $ImageStatus[1]['success']=0;
				  $ImageStatus[1]['msg']="Image one Invalid file format."; 
				}*/	
					
			
					
			}
			else
			{
				$ImageStatus[1]['ImageId']=0;
				$ImageStatus[1]['success']=0;
				$ImageStatus[1]['msg']="Please select image one."; 
			}		
	
		}
	}//end of else if 
	else								//DELETING IMAGE
	{
		//extracting old imgaename only
		$oldUrlImg = explode('/',$old_img_url_1);
		$len = count($oldUrlImg);
		
		
		$deleteOldImgSQL = "DELETE FROM strainimages 
							WHERE strain_id='".$intValidStrainId."'
							AND image_name ='".$oldUrlImg[$len-1]."'";
		$resDeleteOldImgSQL	= mysql_query($deleteOldImgSQL) or die($deleteOldImgSQL." : ".mysql_error());
		$flag=true; 				
	}
	/*------------------------end of image one-----------------------------------*/
	
	
	
	echo $strGetImagePathSQL = "SELECT image_name FROM strainimages WHERE strain_id='".$intStrainId."'
						   ORDER BY image_id desc limit 0,1";

	$resImagePath = mysql_query($strGetImagePathSQL) or die($strGetImagePathSQL." : ".mysql_error());
	
	$resImage = mysql_fetch_assoc($resImagePath);

	$image = BASEURL.$strainimagefoldername_thumbnail.$resImage['image_name'];


	if($flag==true)
	{
		$result = array( "success" => "1", 'ImageStatus' => $ImageStatus,"Status"=>"All images uploaded successfully.",'Image'=>$image);
		$this->response($this->toJson($result),200,"application/json");
	}
	else
	{
		$result = array( "success" => "0", 'ImageDetail' => $ImageId,"Status"=>"Images not uploaded successfully.");
		$this->response($this->toJson($result),200,"application/json");
	}	
	?>