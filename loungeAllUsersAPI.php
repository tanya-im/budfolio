<?php
/*
	File Description - Lounge list (for all users)
	File Name - loungeListAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	
	// Input validations
	if(!empty($intUserId) && !empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		// Check user id is valid or not		
		$uservalidation=CheckValidUser($intValidUserId);
		
		if($uservalidation)
		{
			
		 	$strstrainnoQuery="SELECT strain_id FROM strains
					   		  INNER JOIN users ON users.user_id = strains.user_id
					          WHERE strains.flag = 'active'
					          AND privacy = 'public'
							  AND users.flag='active'
							  UNION ALL
							  SELECT bud_thought_id
							  FROM tbl_bud_thought tbt
							  INNER JOIN users ON users.user_id = tbt.user_id
							  WHERE users.flag='active'";
			$strstrainnoSql = mysql_query($strstrainnoQuery);

			$strstrainnoData = mysql_num_rows($strstrainnoSql);
			$totalPages = ceil($strstrainnoData / $intPageSize);
			
			$perpage = $intPageSize;
			$page = $intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;
			//SQL query to get Lounge list(strains and bud thought together date wise)
			$strLoungeListQuery = "SELECT strain_id, 			                strain_name,dispensary_name,post_date,species,overall_rating,
			   strains.user_id as userId,NULL AS picture_url, 1 AS TYPE
			   FROM strains
			   INNER JOIN users ON users.user_id = strains.user_id
			   WHERE strains.flag = 'active'
			   AND users.flag ='active'
			   AND privacy = 'public'
			   UNION ALL
			   SELECT bud_thought_id, bud_thought, NULL AS dispensary_name,
			   post_date, NULL AS species,
			   NULL AS overall_rating,tbt.user_id as userId, picture_url, 0 AS TYPE
			   FROM tbl_bud_thought tbt
			   INNER JOIN users ON users.user_id = tbt.user_id
			   WHERE users.flag = 'active'
			   ORDER BY post_date DESC Limit $start, $perpage";
						   

			$LoungeListSQL = mysql_query($strLoungeListQuery) or die(mysql_error());//executing strLoginSQL
			
			$arrayLoungeListArray=array();
			
			if(mysql_num_rows($LoungeListSQL) > 0)
			{
				
				while($LoungeList = mysql_fetch_array($LoungeListSQL))
				{
					
					$userdetail = GetUserDetail($LoungeList['userId']);// Get user detail
					
					if($LoungeList['TYPE']==1)
					{
						$primaryimages = GetPrimaryImages($LoungeList['strain_id']);//Get primary image.
					
						$arrayLoungeListArray[] =  array(
														'type'=>$LoungeList['TYPE'],
														'StrainId'=> $LoungeList['strain_id'],
														'StrainName'=> $LoungeList['strain_name'], 
														'Dispensory'=> $LoungeList['dispensary_name'],
														'Species'=> $LoungeList['species'],
														'OverallRating'=> $LoungeList['overall_rating'],
														'StrainsImageUrl'=>$primaryimages[0],
														'StrainsThumbImageUrl'=>$primaryimages[1],
														'UserId'=>$LoungeList['userId'],
														'UserName'=>$userdetail['UserName'],
														'ZipCode'=>$userdetail['ZipCode']
													);// end of array
					
				   }//end of ($LoungeList['TYPE']==1)
				  	else{//start of bud thought
					 
							$likeCount = getLikeCount($LoungeList['strain_id']);
							$commentCount = getCommentCount($LoungeList['strain_id']);
							$date = date('d F Y / h:i A',strtotime($LoungeList['post_date']));//conevrting date time into string format
							//imgae complet path
							if($LoungeList['picture_url']!=''){
							  $imgURL = BASEURL.$budThoughtImgPath.$LoungeList['picture_url'];
							}
							else
							{
								$imgURL = '';
							}
							
							$likeFlag = isUserLiked($LoungeList['strain_id'],$intUserId);
							
							$arrayLoungeListArray[] =  array(
														  'type'=>$LoungeList['TYPE'],
														  'bud_thought_id' => $LoungeList['strain_id'],
														  'BudThought' =>  str_replace('"','\"',stripslashes($LoungeList['strain_name'])), 
														  'pictureUrl' => $imgURL,
														  'UserId' => $LoungeList['userId'],
														  'UserName' => $userdetail['UserName'],
														  'LikeCount' => $likeCount,
														  'CommentCount'=> $commentCount,
														  'likeFlag' =>$likeFlag,
														  'date'=> $userdetail['UserName'].' / '.$date
													  );// end of array
					
				   }//end of else
				}//end of while
				
				
				// If success everythig is good send header as "OK" and LoungeList in reponse
$result = array('success' => '1','LoungeList' => $arrayLoungeListArray,'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// Empty Lounge List
				$error = array('success' => "0", "msg" => "Empty Lounge List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>