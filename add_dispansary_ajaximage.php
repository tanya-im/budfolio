<?php session_start(); ob_start();
include_once("function.php");
include_once("config.php");
// define the image folder path
$path = "images/dispensary_images/original/";
$path_medium="images/dispensary_images/medium/";
$path_thumbnail="images/dispensary_images/thumbnail/";
// Check request methode
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
	$name = $_FILES['disImg']['name'];
	$size = $_FILES['disImg']['size'];
	
	// check image exists or not
	if(strlen($name))
	{
		list($txt, $ext) = explode(".", $name);
		$extention = explode(".", $name);
		$arrayconunt=(count($extention)-1);
		if(!empty($extention[$arrayconunt]))
		{
			$ext=$extention[$arrayconunt];
		}
		// Check image formate valid or not  
		if(in_array(strtolower($ext),$valid_formats))
		{
			
			// check image size not more than 3 Mb.
			if($size< ImageSize)
			{
				
				$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
				 $tmp = $_FILES['disImg']['tmp_name'];
				// Move to uploaded file from tem dir to destination dir.
				$a=move_uploaded_file($tmp, $path.$actual_image_name);
				if($a)
				{
					
					//height /width to show
					$widthShow =655;
					$heightShow =138;
					
					$src=$path.$actual_image_name;
					$dest_m = $path_medium.$actual_image_name;
					$dest_t = $path_thumbnail.$actual_image_name;
					
					//actual image h/w
					list($width, $height, $type, $attr) = getimagesize($path.$actual_image_name);
					
					/*if(($width > $widthShow) || ($height > $heightShow))//if any of height and width is > thn original img height and width
					{*/
						if($heightShow <= $height)
						{
							
							$ratio = $heightShow/$height;
							$hThumb =$heightShow;
							$wThumb = $width*$ratio;
							
						}
						else if($widthShow <= $width)
						{
							
							$ratio = $widthShow/$width;
							$wThumb = $widthShow;
							$hThumb = $height*$ratio;
							
						}else
						{
							$hThumb = $height;
							$wThumb = $width;
						}
						resize_image($src,$dest_m,300,300);
						
						resize_image($src,$dest_t,$wThumb,$hThumb); 
					
					$_SESSION['dispancryphotoimg'] = $actual_image_name;
					echo "<img src='images/dispensary_images/original/".$actual_image_name."'  class='preview'>";
				}
				else	
				{
					echo "failed";
				}
			}
			else
			{
				echo "Image file size max 6  MB";					
			}
		}
		else
		{
			echo "Invalid file format..";	
		}
	}
	else
	{
		echo "Please select image..!";
	}		
	exit;
}
?>