<?php 
//session_start();
// Include header,auth and config file. #C8C8C8
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include_once("function.php");
require_once 'includes/mobileDetect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
include_once('includes/config.php');

?>
<style>
/*#E1E1E1*/

.ui-helper-hidden-accessible { display:none; }
.ui-autocomplete { 
    /* these sets the height and width */
	max-height:130px;

    /* these make it scroll for anything outside */
    overflow-y:auto;
	overflow-x:hidden;
}
#ui-id-1{
	background-color:#ccc;}
a {
    border: 0 none;
    font-size: 18px;
    outline: medium none;
}
a:hover{
	font-size: 18px;
	color:#773e00;
	}
.thumbWrapper
{
	height:0px;
}
.previewWindow
{
		height:0px;
}
#thumbnail
{
			height:0px;
}
#showScheduledPosts .table_schedule{
	background-color: #eee;
}
#showScheduledPosts .table_schedule:nth-child(2n+2){
	background-color: #ddd;
}
</style>
<link rel="stylesheet" type="text/css" href="css/crop_css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/crop_css/imgareaselect-default.css" />
<link rel="stylesheet" type="text/css" href="css/crop_css/styles.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>

<script type="text/javascript">
			var useMobile=<?php if($deviceType=='tablet'||$deviceType=='phone') { echo 'true';} else {echo 'false';}?>;
</script>


	<div id="wraper">
		<div class="clear"></div> 
		<div id="main_con">
			<div class="inner_conlft scroll_container" id="inner_main_div">
				<div class="cell_con">
					<div class="gray_headingbar"> <h2>Schedule Bud Thought</h2></div>
					<div id="content_1" class="content">
					
					
					<div class="budthought_popup"> 
						
						<!--start of bud thought image croping portion-->
						<div class="wrapper" style="width:100%;">
							<div class="uploader">

								<!-- first step upload image begin -->
								<div id="big_uploader">
									<form name="upload_big" id="upload_big" class="uploaderForm" method="post"
									enctype="multipart/form-data" action="upload.php?act=upload" target="upload_target">
										<div class="fileWrapper" style="width: 97.6%;">
											<a class="fileButton"><img src="images/bud_thought/add_photobtn.png" width="44" height="34"></a>
											<input name="photo" id="file" class="fileInput" size="27" type="file" />	
										</div>
										<input type="hidden" name="width" value="<?=$canvasWidth?>" />
										<input type="hidden" name="height" value="<?=$canvasHeight?>" />              
										<input type="submit" name="action" value="Upload Image now" class="inputSubmit" />
									</form>
									<div id="notice" class="notice" >Uploading...</div>
								</div>
							<!-- first step upload image end -->
					
							<div class="content">
						
							  <!-- second step selection begin -->
							  <div id="uploaded">
							  
								<div id="div_upload_big" style="width:<?=$bigWidthPrev?>px;height:<?=$bigHeightPrev?>px;"></div></div>
							<!--  <div style="border:1px solid #CCC;width:100%;float:left;" id="preview_thumb">-->
								  <!-- preview the selection begin -->
								  <div class="previewWindow" style="float:left;width:46%;border-right:1px solid #CCC;visibility:hidden;">
									<strong>Selection preview</strong>
									<div class="previewWrapper" style="width:<?=$bigWidthPrev/2?>px;height:<?=$bigHeightPrev?>px;">
										  <div id="preview"></div>
									</div>
								  </div>
								 <!-- preview the selection end -->
								 
							  <!-- second step selection end -->
							  
							  <!-- third step generated thumb begin -->
							  <div id="thumbnail" style="float:left;width:46%;">
								  <div class="thumbWrapper" style="width:<?=$bigWidthPrev/2?>px;height:<?=$bigHeightPrev?>px;visibility:hidden;">
									 <strong> Generated thumbnail</strong>
									  <div id="div_upload_thumb"></div>
								  </div>
							  
							  </div>
							  <!-- third step generated thumb end -->
							  
							  
							  <div class="uploadThumbWrapper">
								  <?php if($deviceType=='tablet'||$deviceType=='phone') { ?>
								  <div class="mobileSelection">
									  <a id="selLeft">left</a>
									  <a id="selRight">right</a>
									  <a id="selUp">up</a>
									  <a id="selDown">down</a>
									  <a id="selResize">bigger</a>
									  <a id="selResizeSmall">smaller</a>
								  </div>
								  <?php } ?>
								  <form name="upload_thumb" id="upload_thumb" class="uploaderForm" method="post" action="upload.php?act=thumb" target="upload_target">
									  <input type="hidden" name="img_src" id="img_src" class="img_src" /> 
									  <input type="hidden" name="height" value="0" id="height" class="height" />
									  <input type="hidden" name="width" value="0" id="width" class="width" />
									  <input type="hidden" id="y1" class="y1" name="y" />
									  <input type="hidden" id="x1" class="x1" name="x" />
									  <input type="hidden" id="y2" class="y2" name="y1" />
									  <input type="hidden" id="x2" class="x2" name="x1" />                         
									  <input type="submit" id="save_cropped" value="Save Cropped image " class="create_thumb" style="height:37px;" />
								  </form>
								  <div id="notice2" class="notice">Cropped image saving in progress...</div>
							  </div>
						<!--	/*</div> */-->
							  
						
							</div>
					
					<!-- hidden iframe begin -->
					<iframe id="upload_target" name="upload_target" src=""></iframe>
					<!-- hidden iframe end -->

				</div>

			</div>
					
				<div class="bud_textarea ui-widget"><!--ui-widget -->	
						<!--<div id="tags" name="budThoughtText" >
						</div>-->
					<textarea placeholder="Enter bud thought here..." tabindex="1" id="tags" name="budThoughtText" maxlength="200" cols="100" rows="3"></textarea> 
					<!--<input id="suggestionfield" type="hidden" />-->
					<div id="budthoughterror"></DIV>
					</div>
					<div class="character_limit">200 Character</div>
					<div class="add_postbtn" style="width:64%;">
							<!--<a id="addBudThoughtImg" href="javascript:void(0);" class="fllft"> <img src="images/bud_thought/add_photobtn.png" width="44" height="34"></a>-->
							<!-- share buttons -->
							<div style="float:left;margin:13px 10px 0 0;">
							<input type="checkbox"  id="schduleBudthought" value="budfolioShare" style="float:left; margin:16px 10px 0 0;"> 
							<a id="" class="setting_btn " href="javascript:void(0);"> Budfolio </a></div>
							 <?php include_once("schedule_fb_budthought_share.html"); ?>
							 <?php include_once("schedule_twitter.php"); ?>&nbsp;&nbsp;
							
							<!-- share buttons -->
							
					</div>
					
				</div>
				</div>
				<div id="add_schedule" style="display:none;">
					<div class="input_fields_wrap" >						
							<a href="javascript:void(0)" type="button" style=" margin-left: 2%;" class="add_field_button setting_btn"  />Add More Schedule date</a> 
							<a href="javascript:void(0)" type="button" style=" margin-left: 2%;" id="viewSchedulePosts" class="setting_btn"  />View Schedule Posts</a> <br />
							<div style="margin-top:1%"><input type="text" name="schedule_1" id="schedule_1" onclick="dateTimePicker(this.id)" placeholder="Please Enter Date in y-m-d h:m:s formate" style=" background-image: linear-gradient(to bottom, #ffffff, #b4b4b5);  border: 1px solid #e4e4e4;   float: left;    height: 40px;    margin: 1% 2% 0;    padding: 0;    width: 70%;"/><br /></div>
					</div>
					
					<div  style="float: right; width: 100%; text-align: right;margin-top: 3%;"><a href="javascript:void(0);"  class="setting_btn"  id="schedulebudthoughtbtn" style="font-size: 18px; margin-right: 4%;" > Schedule Post</a></div>
				</div>
				<div id="showScheduledPosts" style="width:100%; float: left;padding: 2% 1% 1% 4%;" >
					<div style="float:right; margin: 13px 60px 0 0;">							
							<a id="addSchedules" class="setting_btn " href="javascript:void(0);"> Add Schedule </a>
					</div>
					
					<div style="float:left; width:92%; margin:0; padding:0; box-sizing:border-box;margin-top: 20px;" class="topstrain_header">
						<div style="width:24%;float:left; padding: 2%;box-sizing:border-box;">Budthought</div>
						<div style="width:39%;float:left; padding: 2%;box-sizing:border-box;">Scheduled Dates</div>
						<div style="width:20%;float:left; padding: 2%;box-sizing:border-box;">Action</div>
					</div>
					
					<?php 
								// select all posts schedule by user.
								 $schedulePostsSQL = "SELECT * FROM `schedule_post_tbl` where user_id='".$_SESSION['user_id']." ' and is_deleted !='1' ";
								 $schedulePostsRES = mysql_query($schedulePostsSQL) or die($schedulePostsSQL." : ".mysql_error());
								 
								 if(mysql_num_rows($schedulePostsRES)>0) {
										 $i=0;
										 while($row = mysql_fetch_assoc($schedulePostsRES))	 {
										 
								?>
								<div style="float:left; width:92%; margin:0; padding:0; box-sizing:border-box;" class="table_schedule">
									<div style="width:25%;float:left; text-align:center; padding-top:15px;"><?php echo $row['schedule_post']; ?></div>
									<div style="width:39%;float:left; text-align:center; padding-top:15px;">
										<?php if($row['first_schedule_date']!='0000-00-00 00:00:00'){ echo $row['first_schedule_date']; }else{echo "";} ?><br>
										<?php if($row['second_schedule_date']!='0000-00-00 00:00:00'){echo $row['second_schedule_date']; }else{echo "";}?><br>
										<?php if($row['thired_schedule_date']!='0000-00-00 00:00:00'){echo $row['thired_schedule_date']; }else{echo "";}?><br>
										<?php if($row['fourth_schedule_date']!='0000-00-00 00:00:00'){echo $row['fourth_schedule_date']; }else{echo "";}?>
									</div>
									<div style="width:25%;float:left;text-align:center; padding-top:15px;">
										<!-- <a id="<?php echo $row['schedule_post_id']; ?>" onclick="" class="setting_btn " href="javascript:void(0);"> Edit </a> -->&nbsp &nbsp 
										<a id="<?php echo $row['schedule_post_id']; ?>" onclick="deleteSchedulepost(this.id);" class="setting_btn " href="javascript:void(0);"> Delete </a>
									</div>
								</div>	
					<?php 
											$i++;
										}
									}
								
								?>
				</div>				
				</div>
			</div>    

		<?php include('newLounge_right.php');?>
		</div>
	</div>
<?php $strFollowingsListQuery = "SELECT user_name
							  FROM users";
   $FollowingsListSQL = mysql_query($strFollowingsListQuery) or die($strFollowingsListQuery." : ".mysql_error());			       //executing strLoginSQL
    while($row = mysql_fetch_assoc($FollowingsListSQL)){
		$val[] = $row['user_name'];
	}
?>

<div class="footer"><?php include('footer.php');?></div>
<script>
//onclick="showAddSchedulediv(this.id,parentId='showScheduledPosts')"
$("#addSchedules").click(function(){
	$("#showScheduledPosts").hide();
	$("#add_schedule").show();
});
$('#viewSchedulePosts').click(function(){
	$("#showScheduledPosts").show();
	$("#add_schedule").hide();

});

function deleteSchedulepost(id){
	$.ajax({
				type: "POST",
				url: "deleteSchedulepost.php",
				data: 'deleteSchedulepost=deleteSchedule&sc_id='+id,
				success: function(data)
				{
					console.log(data);
					window.location.href="schedule_bud_thought_form.php";
					return false;
				}
		});
}

$(function() {
    var max_fields = 4; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div style="float: left; margin-top:1%; width: 100%;"><input type="text" onclick="dateTimePicker(this.id)" placeholder="Please Enter Date in y-m-d h:m:s formate" id="schedule_'+x+'" name="schedule_'+x+'" style=" background-image: linear-gradient(to bottom, #ffffff, #b4b4b5);  border: 1px solid #e4e4e4;   float: left;    height: 40px;    margin: 1% 2% 0;    padding: 0;    width: 70%;"/><br /> <a href="#" class="remove_field" style="color:red;font-size: 14px;font-size: 20px;    margin-left: 2%;">X</a></div>'); //add input box
        }else{
			alert("You can schedule only 4 date for a post ");
		}
    });
    
    $(wrapper).delegate(".remove_field","click", function(e) { //user click on remove text
        e.preventDefault(); 
		$(this).parent('div').remove(); x--;
    })
});


$(function() {
  var availableTags=Array();

<?php 
	$i=0;
	foreach($val as $k=>$v){
	?>availableTags[<?php echo $i;?>]="<?php echo stripslashes($v);?>";
	<?php $i++;
}
?>

function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}

$('#tags').bind( "keypress", function( event ) {
	if(event.keyCode == 40) {
		console.log("this : "+this);
	}/*if(event.keyCode == 40) {*/
});
$('#tags').bind( "keydown", function( event ) {
	if ( event.keyCode === $.ui.keyCode.TAB &&
	$( this ).data( "ui-autocomplete" ).menu.active ) {
	event.preventDefault();
	}
}).triggeredAutocomplete({
	source: function( request, response ) {
		//var result=$.ui.autocomplete.filter(availableTags, extractLast( request.term ) );
		//response(result.slice(0,5));
		response($.ui.autocomplete.filter(availableTags, extractLast( request.term )));
	},
	focus: function (event, ui) {
		event.preventDefault(); 
		$(this).val(ui.item.suggestion);
	},
	select: function( event, ui ) {
        document.location.href = ui.item.url;
    },
    context: this,
	open: function (event, ui) {
         $(".ui-menu").css("width" ,"200px");
    }
	});

});
</script>
<script type="text/javascript" src="js/crop_js/jquery.imgareaselect.min.js"></script>
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
<script>
/*function timedate(){
	
	var dt = new Date();
	var date=dt.getDate();
	var month=dt.getMonth()+1;
	var year=dt.getFullYear();
	var hours=dt.getHours();
	var minutes=dt.getMinutes();
	var sec=dt.getSeconds();
	
	var datetime=date+"/"+month+"/"+year+" "+hours+":"+minutes+":"+sec;
	
	return datetime;
}

function dateTimePicker(id){
	//alert(console.log($('#'+id)))
	$('#'+id).datetimepicker({format:'d/m/Y H:i:s',value:timedate()});//set current datetime
}*/

</script>
<script type="text/javascript" src="js/crop_js/effects.js"></script>
		<?php if($deviceType=='tablet'||$deviceType=='phone') { ?>
<script type="text/javascript" src="js/crop_js/touch.js"></script>
		<?php } ?>
<?php include('budfolio_footer.php');?> 

	