<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-ui.triggeredAutocomplete.js"></script>
<script type="text/javascript" src="js/myjs.js"></script>
<script type="text/javascript" src="js/neJs/organictabs.jquery.js"></script>
<script type="text/javascript" src="js/neJs/bjqs-1.3.min.js"></script>
<script type="text/javascript" src="js/cycle-plugin.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script type="text/javascript" src="js/mycustom.js"></script>
<script type="text/javascript" src="js/FadingAndScrollFunctionCall.js"></script>
<script type="text/javascript" src="js/jquery.scrollExtend.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41565808-1', 'budfolio.com');
  ga('send', 'pageview');

</script>
