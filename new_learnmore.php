<?php 
// Include header and config files. 
include_once("newHeader.php");
include_once("config.php");?>
 <link href="css/new_style_lounge.css" rel="stylesheet" type="text/css" />   
    <div id="wraper">
        <div id="new_main_con">
        	<div class="learntop_con">
            	<div class="fleft" style="width:68%;"> 
                	<div class="learn_heading marbtm10"> What's in your Budfolio? </div>
                	<div class="learn_main_text">Start your strain journal and join the conversation today. Budfolio is a fast, interactive and fun way to share your cannabis experience. <br><br>
Take photos and rate your strains based on smell, strength, experience and more.<br><br>
Smoke, rate and share with our cannabis community.</div>
                </div> 
				<div class="fltrit" style="width:30%;"> <img src="images/new_lounge/img1.png"></div>
            </div>
            <div class="learnmore_conlft">
            	<div class="fleft">
                    <div class="learn_heading txtcnt"> Budfolio is the best for Mobile Devices </div>
                    <div class="txtcnt"><img src="images/new_lounge/headline.png"></div>
                    <div class="fleft" style="width:20%; margin-top:-2"><img src="images/new_lounge/img2.png"></div>
                    <div class="fleft" style="width:80%; margin-top: 40px;"> 
                        <div class="learn_heading marbtm10"> Hang out in the Lounge </div>
                        <div class="learn_main_text">Joining the cannabis community just got easier! You can <br> post, like and comment on photos, or make friends <br> and follow others straight from the lounge.</div>
                        
                    </div>
                </div>
                
                <div class="txtcnt"><img src="images/new_lounge/headline.png"></div>
                
                <div class="hand_con" style="min-height:323px;">
                    <div class="fleft" style="width:62%; margin-top: 46px;"> 
                        <div class="learn_heading marbtm10">Keep a strain journal</div>
                        <div class="learn_main_text">
                        Easily keep track all the cannabis you try by photos, ratings, effects and more!
                        </div>    
                    </div>
                    <div class="hand_img" style="width:35%; top:-1px"> <img src="images/new_lounge/img3.png" height="323"></div>
                    <div class="btmbar_dividedline"><img src="images/new_lounge/headline.png"></div>
                </div>
                
                
                <div class="hand_con" style="min-height:323px;">
                    <div class="iphone_img2" style="width:49%;"> <img src="images/new_lounge/img4.png" height="323"></div>
                    <div class="fltrit" style="width: 51%;margin: 35px 0px 0px;"> 
                        <div class="learn_heading marbtm10">Find Dispensary and delivery services </div>
                        <div class="learn_main_text">Easily search local dispensaries and delivery services. View menu’s, customer reviews, photos and more from the dispensary list.</div>
                       <!-- <span class="blk_link"> <a href="https://budfolio.com/index.php"> Sign up for free now </a> </span>-->
                    </div>
                    <div class="btmbar_dividedline1"><img src="images/new_lounge/headline.png"></div>
                </div>
                <!--<div class="btm_graycon">
                	<div class="fleft" style="width:50%;margin-top: 5px;">
                	
                	<div class="learn_heading marbtm10">Download the App </div>
                    <div>
                        &nbsp; &nbsp; <a href="#"><img src="images/new_lounge/itunes_btn.png"></a> &nbsp; &nbsp; 
                        <a href="#"><img src="images/new_lounge/google_btn.png"></a>
                    </div>
                    
                </div>
                
               		<div class="fleft"  style="width:50%;margin-top: 5px;">
               			<div class="learn_heading marbtm10">Start Now</div>
                    	<div>
                        <a href="index.php"><img src="images/new_lounge/register_btn.png"></a> &nbsp;
                        <a href="https://www.facebook.com/pages/Budfolio/126841610710369"><img src="images/new_lounge/fb_icon_btn.png"></a> &nbsp;
                        <a href="https://twitter.com/budfolio"><img src="images/new_lounge/twt_icon_btn.png"></a>
                    </div>
                   </div>
                </div>-->
                <div class="btm_graycon">
                	<div class="fleft" style="width: 49%;text-align: center;border-right: 1px solid gray;">
                	<div class="learn_heading marbtm10">Download the App </div>
                    <div>
                        &nbsp; &nbsp; <a href="https://itunes.apple.com/us/app/budfolio/id687645140?mt=8"><img src="images/new_lounge/itunes_btn.png"></a> &nbsp; &nbsp; 
                        <a href="https://play.google.com/store/apps/details?id=com.budfolio.app&hl=en"><img src="images/new_lounge/google_btn.png"></a>
                    </div>
                </div>
                
                	<div style="width:50%; margin:0px; padding:0px; float:left; text-align:center;">
                	<div class="learn_heading marbtm10">Start Now</div>
                    <div>
                        <a href="member_user_registration.php"><img src="images/new_lounge/register_btn.png"></a> &nbsp;
                        <a href="https://www.facebook.com/pages/Budfolio/126841610710369"><img src="images/new_lounge/fb_icon_btn.png"></a> &nbsp;
                        <a href="https://twitter.com/budfolio"><img src="images/new_lounge/twt_icon_btn.png"></a>
                    </div>
                </div>
                </div>
            </div>
        </div>
	</div>
    
	<div class="footer">        
        <div class="clear">&nbsp;</div>
        <div class="fllft"> © 2013 Budfolio LLC </div>
        <div class="footerlink">
            <ul>
                <li> <a href="mailto:advertising.budfolio@gmail.com" target="_new">Advertisers</a> </li>
                <li>| </li>
                <li> <a href="terms_condition.php">Terms</a> </li>
            </ul>
        </div>
	</div>
</div>
</body>
</html>
