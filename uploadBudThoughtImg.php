<?php session_start(); ob_start();
include_once("function.php");
include_once("config.php");
// define the image folder path
$path = "images/budthought/";

// Check request methode
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
	$name = $_FILES['commentImg']['name'];
	$size = $_FILES['commentImg']['size'];
	// check image exists or not
	if(strlen($name))
	{
		list($txt, $ext) = explode(".", $name);
		$extention = explode(".", $name);
		$arrayconunt=(count($extention)-1);
		if(!empty($extention[$arrayconunt]))
		{
			$ext=$extention[$arrayconunt];
		}
		// Check image formate valid or not  
		if(in_array(strtolower($ext),$valid_formats))
		{
			// check image size not more than 3 Mb.
			if($size< ImageSize)
			{
				$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
				$tmp = $_FILES['commentImg']['tmp_name'];
				// Move to uploaded file from tem dir to destination dir.
				$a=move_uploaded_file($tmp, $path.$actual_image_name);
				if($a)
				{
					$src=$path.$actual_image_name;
					$_SESSION['bud_thought_img']=$actual_image_name;
					
					echo "<img src='images/budthought/".$actual_image_name."'  class='preview'>";
				}
				else	
				{
					echo "failed";
				}
			}
			else
			{
				echo "Image file size max 6  MB";					
			}
		}
		else
		{
			echo "Invalid file format..";	
		}
	}
	else
	{
		echo "Please select image..!";
	}		
	exit;
}
?>