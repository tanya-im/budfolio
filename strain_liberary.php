<?php
// Include header,auth and config file.
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include_once("function.php");
include_once("lounge_functions.php"); 
$_SESSION['strainLibCounter']=1;
$_SESSION['searchtext']='';
?>
<script type="text/javascript" src="js/customInput.jquery.js"></script>
<script type="text/javascript">
// Run the script on DOM ready:
$(function(){
	$('input').customInput();
});
</script>
<style>
h4
{
	color:#000000;
	width:100%;
}


.userprofileblock
{
	width:90%;
}
.userprofile_sec2{
	width:45%;
}
.userprofile_sec3
{
	width:50%;
}

</style>

<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	
    	   <div class="inner_conlft scroll_container">
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2>Strain Library </h2> </div>
				<div class="greenbar_bg_single"> 
                	<form>
                    	<div class="grenlft_con">
                        
                      	<div class="input-append"> <div class="searchbox"> <img src="images/new_web/serach_icon.png" class="scale-with-grid"> <input type="text" id="sl_search_text" name="sl_search_text"> </div>
						
						<a id="search_here" href="javascript:void(0);" class="pushtop9 search_btn searchSL"> Search  </a>
						
						 </div>
                        </div>
                    	
                   </form> 
                </div>
       			<div id="content_1" class="content lounge">
			<?php 
			
			include('strain_liberary_content.php');?>
    </div>
            </div>
          </div>
        <!-- Inlude Right sidebar--> <?php include('newLounge_right.php');?><!-- End Inlude Right sidebar-->
    </div>
</div>
<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>
<script>
	(function($){
		$(window).load(function(){
			$('.scroll_container').scrollExtend(
			{	'target': 'div#content_1',
				'url': 'strain_liberary_content.php?flag=1',
				'loadingIndicatorClass': 'scrollExtend-loading',
				'loadingIndicatorEnabled': true
			}
		);
		});
	})(jQuery);
</script>

</body>

</html>
