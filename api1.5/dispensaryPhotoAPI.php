<?php
/*
	File Description - image list for dispensary
	File Name - photosAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array

	//Taking all values into local variable				
 	$intDispensaryId = $inputArray['DispensaryId'];

	// Input validations
	if(!empty($intDispensaryId))
	{
		
		//get dispensary name 
		$getDispensaryNameSQL = "SELECT dispensary_name FROM dispensaries 
								 WHERE dispensary_id='".$intDispensaryId."'";
		$disNameRes = mysql_query($getDispensaryNameSQL) or die($getDispensaryNameSQL." : ".mysql_error());
		
		$disNameRow = mysql_fetch_assoc($disNameRes);
		
		$disName = $disNameRow['dispensary_name'];
		 
		$strGetAllPhotosSQl = "SELECT * 
							  FROM  `strainimages` 
							  WHERE  `strain_id` 
							  IN (
							  
							  SELECT  `strain_id` 
							  FROM  `strains` 
							  WHERE (
							  `dispensary_name` =  '".addslashes($disName)."'
							  OR dispensary_id =  '".$intDispensaryId."'
							  )
							  AND flag =  'active'
							  )
							  AND image_name!=''
							  ";
			//echo 	$strGetAllPhotosSQl;			  
		$resGetAllPhotos = mysql_query($strGetAllPhotosSQl) or die($strGetAllPhotosSQl." : ".mysql_error());
		
		if(mysql_num_rows($resGetAllPhotos)>0)
		{
			$arrayPhotos = array();
			while($reviewPic = mysql_fetch_assoc($resGetAllPhotos))
			{
								
				$photoPath = BASEURL."images/uploads/thumbnail/".$reviewPic['image_name'];
				$orignalphotoPath = BASEURL."images/uploads/original/".$reviewPic['image_name'];
				$arrayPhotos[] = array(
										'image_id' => $reviewPic['image_id'],
										'thumb_image' =>$photoPath,
										'orignal_image'=>$orignalphotoPath
				);
			}
			/*$arrayPhotos[0]=array('image_id'=>'1',
							 'image'=>'http://budfolio.com/qa/images/uploads/thumbnail/13660938751631894326.jpg');
			$arrayPhotos[1]=array('image_id'=>'2',
							 'image'=>'http://budfolio.com/qa/images/uploads/thumbnail/1366093878190094328.jpg');
			$arrayPhotos[2]=array('image_id'=>'3',
							 'image'=>'http://budfolio.com/qa/images/uploads/thumbnail/13660938771436794327.jpg');
			$arrayPhotos[3]=array('image_id'=>'4',
							 'image'=>'http://budfolio.com/qa/images/uploads/thumbnail/13660938791669394330.jpg');
			$arrayPhotos[4]=array('image_id'=>'5',
							 'image'=>'http://budfolio.com/qa/images/uploads/thumbnail/13660941261413994577.jpg');
*/

			//if every thing is ok then send success=1 and strain  id in response.
			$result = array( "success" => "1","photoList"=>$arrayPhotos);
			$this->response($this->toJson($result),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "1","photoList"=> array());
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Please select dispensary.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>