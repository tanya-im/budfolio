<?php
/*
	File Description - file to update user profile
	File Name - updatesettingAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	// Get all values in local variable	
	$intUserId 	= $_REQUEST['UserId'];
	$intUserType = $_REQUEST['userType'];
	$strEmail 	= $_REQUEST['Email'];
	$intZip 	= $_REQUEST['Zip'];
	$strCity    = $_REQUEST['city'];
	$strState 	= $_REQUEST['State'];
	$strPrivacy	= $_REQUEST['Privacy'];
	$strBio		= $_REQUEST['Bio'];
	$strCountry = $_REQUEST['Country'];


	$strReceiveEmailFromBudFolio = $_REQUEST['ReceiveEmailFromBudFolio'];

	//new added keys
	$intDeliveryService = $_REQUEST['DeliveryService'];
	$intStoreFront = $_REQUEST['StoreFront'];
 	$intAcceptCreditCard = $_REQUEST['AcceptCreditCard'];
	$intAcceptATMonSite = $_REQUEST['AcceptATMonSite'];
 	$int18YearsOld = $_REQUEST['18YearsOld'];
	$int21YearsOld = $_REQUEST['21YearsOld'];
	$intSecurity=$_REQUEST['Security'];
	$intHandicapAssesseble =$_REQUEST['HandicapAssesseble'];


	// Input validations
	if(!empty($intUserId))
	{


		// sanitization of values to insert in db
		$intValidUserId = mysql_real_escape_string($intUserId);
		$strValidEmail = mysql_real_escape_string($strEmail);
		$strValidAddress = mysql_real_escape_string($strAddress);
		$intValidZip = mysql_real_escape_string($intZip);		
		$strValidState = mysql_real_escape_string($strState);
		$strValidCity = mysql_real_escape_string($strCity);
		$strValidPrivacy	= mysql_real_escape_string($strPrivacy);
		$strValidBio = mysql_real_escape_string($strBio);
		$strValidCountry = mysql_real_escape_string($strCountry);
		
		$strValidReceiveEmailFromBudFolio= mysql_real_escape_string($strReceiveEmailFromBudFolio);
        
		// Check user id is valid or not		
		$uservalidation=CheckValidUser($intValidUserId);
		if(!$uservalidation)
		{
			// If no records "Error msg" status
			$error = array('success' => "0", "msg" => "Invalid user id.");
			$this->response($this->toJson($error), 406, "application/json");
		}
		
		// to check whether the email address is exist.
		$strSQLChkEmail ="SELECT user_name 
						  FROM users 
						  WHERE email_address='".$strValidEmail."' and 
						  user_id!='".$intValidUserId."'";

		

		$SQLResChkEmail = mysql_query($strSQLChkEmail);// execution of query strSQLChkEmail

		if(mysql_num_rows($SQLResChkEmail)>0)
		{
			// If no records "Error msg" status
			$error = array('success' => "0",
				           "msg" => "This email address already in use. Please try again with another email address");

			$this->response($this->toJson($error), 406, "application/json");
		}
		
		$latlongdata = getlatlong($intZip,$strState);
		if($latlongdata['status']!='OK')
		{
			// If no lat lng "Error msg" status
			$error = array('success' => "0", "msg" => "Invalid zip code.");
			$this->response($this->toJson($error), 406, "application/json");
		}
		$lat= mysql_real_escape_string($latlongdata['results'][0]['geometry']['location']['lat']);
		$lng= mysql_real_escape_string($latlongdata['results'][0]['geometry']['location']['lng']);
		
		// Update user profile.
		$strUpdateUserProfileQuery ="update users set 
									email_address='".$strValidEmail."',
									address='".$strValidAddress."', 
									zip_code='".$intValidZip."',
									city ='".$strValidCity."',
									state='".$strValidState."',
									privacy='".$strValidPrivacy."', receive_email_from_budfolio='".$strValidReceiveEmailFromBudFolio."',
									latitude='".$lat."',
									longitude='".$lng."',
									bio='".$strValidBio."',
									country='".$strValidCountry."'
									where user_id='".$intValidUserId."'";

		$SQLUpdateUserProfile = mysql_query($strUpdateUserProfileQuery)or die(mysql_error());
		
		if($intUserType==2){
	
		
		//setting all options to zero
		$strUpdateSettingSQL = "UPDATE user_settings SET
								DeliveryService='".$intDeliveryService."',
					            StoreFront='".$intStoreFront."',
								AcceptCreditCard='".$intAcceptCreditCard."',
								AcceptATMonSite='".$intAcceptATMonSite."',
								18YearsOld='".$int18YearsOld."',
								21YearsOld='".$int21YearsOld."',
								HandicapAssesseble='".$intHandicapAssesseble."',
								Security='".$intSecurity."'
								WHERE 
								user_id='".$intUserId."'";
					
		$resSettings = mysql_query($strUpdateSettingSQL) or die($strUpdateSettingSQL." : ".mysql_error());	
		}

		if(!$SQLUpdateUserProfile)
		{ 
			// If no records "Error msg" status
			$error = array('success' => "0", "msg" => "Profile not updated , please try agian.");
			$this->response($this->toJson($error), 406, "application/json");
		}
		
		// Adding  Inserting strain images
		if(isset($_FILES))
		{
			$name = $_FILES['UserProfileImage']['name'];
			$size = $_FILES['UserProfileImage']['size'];
			if(strlen($name))
			{
				list($txt, $ext) = explode(".", $name);
				$extention = explode(".", $name);
				$arrayconunt=(count($extention)-1);
				if(!empty($extention[$arrayconunt]))
				{
					$ext=$extention[$arrayconunt];
				}
				if(in_array(strtolower($ext),$valid_formats))
				{
					if($size< ImageSize)
					{
						$actual_image_name = time().$name;//substr(str_replace(" ", "_", $txt), 5).".".$ext
						$tmp = $_FILES['UserProfileImage']['tmp_name'];
						if(move_uploaded_file($tmp,'../'.$userprofileimagefoldername_original.$actual_image_name))
						{
							$strAddImagesQuery="UPDATE users SET photo_url='".$actual_image_name."' WHERE 
							user_id='".$intValidUserId."'";
							$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());
							$src='../images/profileimages/original/'.$actual_image_name;
							$dest_m='../images/profileimages/thumbnail/'.$actual_image_name;
							$dest_t='../images/profileimages/thumbnail/'.$actual_image_name;
							resize_image($src,$dest_m,115,115);
							//resize_image($src,$dest_t,150,150);
						}
						else	
						{
							$error = array('success' => "0", "msg" => "Image upload failed. Please try agian.");
							// If no records "Error msg" status
							$this->response($this->toJson($error), 406, "application/json");
						}
					}
					else
					{
						$error = array('success' => "0", "msg" => "Image file size max 3 MB");
						// If no records "Error msg" status
						$this->response($this->toJson($error), 406, "application/json");					
					}
				}
				else
				{
					$error = array('success' => "0", "msg" => "Invalid file format.");
					// If no records "Error msg" status
					$this->response($this->toJson($error), 406, "application/json");	
				}
			}
			/*else
			{
				$error = array('success' => "0", "msg" => "Please select image.");
				// If no records "Error msg" status
				$this->response($this->toJson($error), 406, "application/json");
			}*/		
		}
	
		//if pfile update successfully
		$result = array( "success" => "1", 'UserId' => $intValidUserId);
		$this->response($this->toJson($result),200,"application/json");
	}
	else
	{
		$error = array('success' => "0", "msg" => "Email address,zipcode,state ,Country or Bio empty.");
		// If no records "Error msg" status
		$this->response($this->toJson($error), 406, "application/json");
	}
?>















































