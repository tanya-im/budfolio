<?php
/*
	File Description - File for forgot password 
	File Name - forgotpasswordAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true);
//converting that json into array
	$strEmailAddress = $inputArray['EmailId'];		
// Input validations
	if(!empty($strEmailAddress))
	{
		$strValidEmailAddress = mysql_real_escape_string($strEmailAddress);
//SQL query to check the  USER exists or not
		$strForgotPasswordSQL = "SELECT user_id,user_name,confirm_code FROM users WHERE email_address ='".$strValidEmailAddress."'";
		$sql = mysql_query($strForgotPasswordSQL)or die(mysql_error());//executing strLoginSQL
		if(mysql_num_rows($sql) > 0)
		{
			$resultSet = mysql_fetch_assoc($sql);
			//If email is not confirm return back false. 			
			if(!empty($resultSet['confirm_code']))
			{
				$error = array('success' => "0", "msg" => "Please confirm your email address first.");
				$this->response($this->toJson($error), 400,"application/json");
			}
			
			$confirmcode = hash('sha256',$resultSet['user_id'].$resultSet['user_name'].time());
			$queryone = "update users set forgot_password='".$confirmcode."' where user_id='".$resultSet['user_id']."'";
			$res = mysql_query($queryone)or die(mysql_error());
			$url = BASEURL.'reset_password.php?fgtpwd='.$confirmcode.'&cid='.$resultSet['user_id'];
			$subject = 'Forgot Password';
			$msg = $resultSet['user_name'].",<br /><br />
			Sorry you're having problems logging into Budfolio. Please <a href='".$url."'>click here</a> to reset your password.<br /><br />Regards,<br> Budfolio";
			$linkmsg = " Click here to reset password ";
			$sendbyemail=SenderEmail;
			$sent_unsent=email($sendbyemail,$strValidEmailAddress,$subject,$msg);
			if($sent_unsent)
			{
				$emailstatus="sent";
				$statusmsg="Please check your email.";
			}
			else
			{
				$emailstatus="unsent";
				$statusmsg="Email not sent.Please try again later.";
			}
			
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1", 'UserId' => $resultSet['user_id'],'UserName'=>$resultSet['user_name'],"EmailStatus"=>$emailstatus,"msg"=>$statusmsg);
			$this->response($this->toJson($result),200,"application/json");

		} // closing mysql_num_rows
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "Email address not exists.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	} //end of input validations
	else
	{
		// If either username or passowrd is empty
		$error = array('success' => "0", "msg" => "Email address is empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}
?>