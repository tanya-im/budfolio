<?php
/*
	File Description - File to like a bud thought 
	File Name - likeBudThoughtAPI.php
	creation date - 7th may
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true);
	

	//converting that json into array

 	$intUserId = $inputArray['UserId'];
	$intBudThoughtId = $inputArray['bud_thought_id'];
	$flag  = $inputArray['Flag'];//if 1 means liking bud thought and if 2 means remove like 

	
		
			$alreadyLikeByUser = isUserLiked($intBudThoughtId,$intUserId);
			if($alreadyLikeByUser ==0){//if user is liking bud thought first time
				
			  //SQL query to like budThought  if flag ==1
			  $strLikeBudThoughtSQL = "INSERT INTO tbl_like_thoughts
									   set user_id = '".$intUserId."',
									   bud_thought_id = '".$intBudThoughtId."'";
				
				
		       $ownerUserId = getBudthoughtOwner($intBudThoughtId);
				
				$UserWhoLiked = getUserName($intUserId);
				$msg = $UserWhoLiked." liked your bud thought";
				
				 
				$getUserInfoForNotiSQL="SELECT device_token_for_iphone,device_token_for_android 
										 FROM users 
										 WHERE user_id='".$ownerUserId."'";
				$resNote= mysql_query($getUserInfoForNotiSQL) or die($getUserInfoForNotiSQL." : ".mysql_error());
				$NoteUser = mysql_fetch_assoc($resNote);
				$checkNotificationSentFlag =0;
				if($NoteUser['device_token_for_iphone']!='')
				{
						sendNotificationToiPhone($ownerUserId,$msg,'like bud thought',$intUserId,'0',$intBudThoughtId);
						$checkNotificationSentFlag =1;
				}if($NoteUser['device_token_for_android']!='')
				{	
					gcm_send($ownerUserId,$msg,'like bud thought',$intUserId,'0',$intBudThoughtId);
					$checkNotificationSentFlag =1;
				}
			
				// store push notification information into DB
				if($checkNotificationSentFlag ==1)
				{
					$date = date('Y-m-d H:i:s');
					$insertPushActivitySQL= "INSERT INTO tbl_push_notification SET
											 user_id='".$ownerUserId."',
											 sender_user_id='".$intUserId."',
											 type='0',
											 type_id='".$intBudThoughtId."',
											 notification_msg='".$msg."',
											 date_time='".$date."'";
					$resPushActivity = mysql_query($insertPushActivitySQL) or die($insertPushActivitySQL." : ".mysql_error());				
				}
			

			}else
			{
				// removing like from the bud thought and like entry from the table will get delete 
		 		$strLikeBudThoughtSQL = "DELETE FROM tbl_like_thoughts WHERE 
									     bud_thought_id = '".$intBudThoughtId."' 
									     AND user_id = '".$intUserId."'";
										 
				
				// if reverting like from bud thought then 
				//it should get delete from push notification table as well
				$removePushActivitySQL= "DELETE FROM tbl_push_notification WHERE
											 user_id='".$ownerUserId."' AND
											 sender_user_id='".$intUserId."' AND
											 type='0' AND
											 type_id='".$intBudThoughtId."' AND
											 notification_msg='".$msg."'";
				$resPushActivity = mysql_query($removePushActivitySQL) or die($removePushActivitySQL." : ".mysql_error());
										 
										 
		 	}
		
	 $sql = mysql_query($strLikeBudThoughtSQL) or die($strLikeBudThoughtSQL." : ".mysql_error());//executing $strLikeBudThoughtSQL

		if($sql == 1)
		{
			
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1");
			$this->response($this->toJson($result),200,"application/json");

		} // end of if ($sql == 1)
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "Unable to like bud thought.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	/*} //end of input validations
	else
	{
		// If either username or passowrd is empty
		$error = array('success' => "0", "msg" => "Please enter bud thought id.");
		$this->response($this->toJson($error), 400,"application/json");
	}*/
?>