<?php
/*
	File Description - Lounge list (for all users)
	File Name - loungeListAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array
	
	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	$intLat = $inputArray['latitude'];
	$intLong =$inputArray['longitude'];
	
	// Input validations
	if(!empty($intPageSize) && !empty($intPageNo) && !empty($intLat) && !empty($intLong))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		// Check user id is valid or not		
		if($intValidUserId!=0){
			$uservalidation=CheckValidUser($intValidUserId);
		}
			
		$strstrainnoQuery="(SELECT strain_id,strain_name,dispensary_name,
		  						 dispensary_id,post_date,species,overall_rating,
								 strains.user_id as userId,NULL AS picture_url, 1 AS TYPE,
                                 3959 * acos( cos( radians( ".$intLat."  ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$intLong.") ) + sin( radians(".$intLat." ) ) * sin( radians( latitude ) ) )  AS distance
								 FROM strains
								 INNER JOIN users ON users.user_id = strains.user_id
								 WHERE strains.flag = 'active'
								 AND users.flag ='active'
								 AND privacy = 'public'
								 HAVING distance < 25
								 ORDER BY post_date DESC,distance)
                             	 UNION ALL
								 (SELECT bud_thought_id, bud_thought,NULL as dispensary_name, NULL AS dispensary_id,
								 post_date, NULL AS species,
								 NULL AS overall_rating,tbt.user_id as userId, picture_url, 0 AS TYPE,
								  3959 * acos( cos( radians(  ".$intLat." ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$intLong.") ) + sin( radians( ".$intLat.") ) * sin( radians( latitude ) ) )  AS distance
								 FROM tbl_bud_thought tbt
								 INNER JOIN users ON users.user_id = tbt.user_id
								 WHERE users.flag = 'active'
								 and  privacy = 'public'
								 HAVING distance < 25
								 ORDER BY post_date DESC,distance)";
		  $strstrainnoSql = mysql_query($strstrainnoQuery);

		  $strstrainnoData = mysql_num_rows($strstrainnoSql);
		  $totalPages = ceil($strstrainnoData / $intPageSize);
			
		  $perpage = $intPageSize;
		  $page = $intPageNo;
		  $calc = $perpage * $page;
		  $start = $calc - $perpage;
		  
		  $strstrainnoQuery .= " Limit $start, $perpage";
		  
		 // echo $strstrainnoQuery;
		  
		  $LoungeListSQL = mysql_query($strstrainnoQuery) or die(mysql_error());//executing strLoginSQL
			
			$arrayLoungeListArray=array();
			
			if(mysql_num_rows($LoungeListSQL) > 0)
			{
				while($LoungeList = mysql_fetch_array($LoungeListSQL))
				{
					$userdetail = GetUserDetail($LoungeList['userId']);// Get user detail
					
					//for user profile image path
					$getPathSQL = "SELECT photo_url FROM users WHERE user_id='".$LoungeList['userId']."'";
					$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());
					$row = mysql_fetch_assoc($response);
 					if(!empty($row['photo_url']))
					{
						$userImgPath = BASEURL."images/profileimages/thumbnail/".$row['photo_url'];
						$userImgOriginal=BASEURL."images/profileimages/original/".$row['photo_url'];
					}else
					{
						$userImgPath = '';
						$userImgOriginal='';
					}
					//end of user image

					if($LoungeList['TYPE']==1)//start of strain
					{
						$primaryimages = GetPrimaryImages($LoungeList['strain_id']);//Get primary image.
						
						if(!empty($primaryimages[0])){
							$img = $primaryimages[0];
							$imgThumb =  $primaryimages[1];
							$isImage ='1';
						}else
						{
							$img="";
							$imgThumb = "";
							$isImage='0';
								
						}

												
						if($LoungeList['dispensary_id']=='0')
						{
							$dispensaryName = $LoungeList['dispensary_name'];
						}else
						{
							$dispensaryName = getDispensaryName($LoungeList['dispensary_id']);
						}
						
						$date = funTimeAgo(strtotime($LoungeList['post_date']));
						$likeCount = getLikeCountStrain($LoungeList['strain_id']);

						$likeFlag = isUserLikeStrain($LoungeList['strain_id'],$intUserId);

			$encodedStarinNAme =base64_encode(str_replace("\\", "",$LoungeList['strain_name']));
			$encodedDispensaryName =base64_encode(str_replace("\\", "",$dispensaryName));



						$arrayLoungeListArray[] =  array(
							  'type'=>$LoungeList['TYPE'],
							  'StrainId'=> $LoungeList['strain_id'],
							  'StrainName'=> $encodedStarinNAme , 
							  'Dispensory'=> $encodedDispensaryName,
							  'Species'=> str_replace("\\", "",$LoungeList['species']),
							  'OverallRating'=> $LoungeList['overall_rating'],
							  'StrainsImageUrl'=>$img!=''?$img:'',
							  'StrainsThumbImageUrl'=> $imgThumb!=''?$imgThumb:'',
							  'UserId'=>$LoungeList['userId'],
							  'UserName'=>str_replace("\\", "",$userdetail['UserName']),
							  'ZipCode'=>$userdetail['ZipCode'],
							  'DispensaryId'=> ($LoungeList['dispensary_id']!=''?(string)$LoungeList['dispensary_id']:'0'),
							  'LikeCount' => $likeCount,
							  'LikeArray'=>  getLikeStrainUserList($LoungeList['strain_id']),
							  'likeFlag' =>$likeFlag,
							  'date'=> $date,
							  'pictureUrl'=>$userImgPath,
							  'pictureUrlThumb'=>$userImgOriginal,
							  'isImage'=>$isImage
						);// end of array
					
				   }//end of ($LoungeList['TYPE']==1)
				  	else{//start of bud thought
					 
						 if(!empty($LoungeList['picture_url']))
						 {
							 $budThoughtURL = BASEURL."images/budthought/".$LoungeList['picture_url'];
					  		 $isImage ='1';
					  
						 }else
						 {
							 $budThoughtURL= "";
							  $isImage ='0';
						 }

							$likeCount = getLikeCount($LoungeList['strain_id']);
							$commentCount = getCommentCount($LoungeList['strain_id']);
							//$date = date('d|m|Y | h:i A',strtotime($LoungeList['post_date']));//conevrting date time into string format
							$date = funTimeAgo(strtotime($LoungeList['post_date']));
							if(!empty($userImgPath))
							{
								$img = BASEURL."images/profileimages/original/".$userImgPath;
								$imgThumb = BASEURL."images/profileimages/thumbnail/".$userImgPath;
							}else
							{
									$img ='';
									$imgThumb ='';
							}
							if($intUserId!=0){
								$likeFlag = isUserLiked($LoungeList['strain_id'],$intUserId);
							}else
							{
								$likeFlag = 0;
							}
							$arrayLoungeListArray[] =  array(
			'type'=>$LoungeList['TYPE'],
			'bud_thought_id' => $LoungeList['strain_id'],
			'BudThought' => base64_encode(str_replace("\\", "",$LoungeList['strain_name'])), 
			'pictureUrl' => $userImgOriginal!=''?$userImgOriginal:'',
								'thumbnailUrl'=> $userImgPath!=''?$userImgPath:'',//for thumbnail url on 21st june
			'UserId' => $LoungeList['userId'],
			'UserName' => str_replace("\\", "",$userdetail['UserName']),
			'LikeCount' => $likeCount,
			'CommentCount'=> $commentCount,
			'likeFlag' =>$likeFlag,
			'date'=> $date,
			'budThoughtUrl'=> $budThoughtURL,
			'LikeArray'=>getLikeBudThoughtUserList($LoungeList['strain_id']),
			'CommentList'=>getCommentList($LoungeList['strain_id']),
			'isImage'=>$isImage
							);// end of array
					
				   }//end of else
				}//end of while
				
				
				// If success everythig is good send header as "OK" and LoungeList in reponse
				$result = array('success' => '1',
				'LoungeList' => $arrayLoungeListArray,
				'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// Empty Lounge List
				$error = array('success' => "0", "msg" => "Empty Lounge List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or pageSize or PageNo empty or lat long empty");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>