<?php
/*
	File Description - Delete Strain
	File Name - removestrainAPI.php
*/

	// Cross validation if the request method is POST 
	//else it will return "Invalid request" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intBudThoughtID = $inputArray['bud_thought_id'];
	$intUserId = $inputArray['UserId'];
	
	// Input validations
	if(!empty($intBudThoughtID) && !empty($intUserId))
	{
		
		$checkBTOwnerSQL ="SELECT user_id FROM tbl_bud_thought
						   WHERE bud_thought_id ='".$intBudThoughtID."'
						   LIMIT 0,1";
		$exeBTOwnerSQL = mysql_query($checkBTOwnerSQL) or die ($checkBTOwnerSQL." : ".mysql_error());
		
		if(mysql_num_rows($exeBTOwnerSQL))
		{
			$userIDRow = mysql_fetch_assoc($exeBTOwnerSQL);
			
			if($userIDRow['user_id']==$intUserId)
			{
		
				$intValidBudThoughtID = mysql_real_escape_string($intBudThoughtID);
				$intValidUserId = mysql_real_escape_string($intUserId);
		
				// delete all the activity done on ste strain thta have to delete  
				$deleteActivitySQL ="UPDATE tbl_push_notification 
									 SET 
									   delete_status='1'		
									   WHERE 
										 (type=0 OR type=2 OR type=3 OR type=4)
									   AND type_id='".$intValidBudThoughtID."'";
	      $resActivitySQL = mysql_query($deleteActivitySQL) or die($deleteActivitySQL." : ".mysql_error());
	
	
			$strBlockBTQuery = "UPDATE tbl_bud_thought 
								SET delete_status='1' 
								WHERE bud_thought_id ='".$intValidBudThoughtID."'";
			$strBlockStrainSql= mysql_query($strBlockBTQuery)or die(mysql_error());
			
			// If success everythig is good send header as "OK" and user details
			// sending bud thought in reponse
			$result = array(
								'success' => '1',
								'bud_thought_id' =>$intValidBudThoughtID,
								"msg"=>"Bud thought deactivated ");
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
			
			}
			else
			{
				// If Bud thought id or user id empty
				$error = array('success' => "0",
								"msg" => "User can't delete this bud thought.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}else
		{
			// If Bud thought id or user id empty
			$error = array('success' => "0", "msg" => "No such Bud thought exist.");
			$this->response($this->toJson($error), 400,"application/json");
		}
	}
	else
	{
		// If Bud thought id or user id empty
		$error = array('success' => "0", "msg" => "Bud thought or UserId empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>