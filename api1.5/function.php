<?php
// Comman function file................ QA>....................
include_once("config.php");
include_once("atRateUser.php");
$globalimagesize=(1024*1024)*3;
define('ImageSize',$globalimagesize);
$path='https://budfolio.com/qa1/';


// Email function  
function email($sender,$to,$subject=null,$msg=null,$confirmcode=null,$linkmsg=null)
{	
	$to = $to;
		
	$subject = $subject;
		
	$headers = "From:" . $sender . "\r\n";
	$headers .= "Reply-To: ". $sender."\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$via = "-fbudfolio@budfolio.com";	
	$message = '<html><body>';
	$message .= '<h1>Hello,</h1>';
	$message .= '</body></html>';
	$message = '<html><body>';
	$message .= '<table rules="all" border="0" cellpadding="10">';
	$message .= "<tr style='background: #eee;'><td>".$msg."</td></tr>";
	$message .= "</table>";
	$message .= "</body></html>";
	
	$sent_unsent=mail($to, $subject, $message, $headers);
	return $sent_unsent;
}

// Get Lat,Long from zip code 
function getlatlong($zipcode,$state=null)
{
	$url='http://maps.googleapis.com/maps/api/geocode/json?address=';
	if(!empty($zipcode))
	{
		$zipcode=str_replace(" ","",$zipcode);
		$url.=$zipcode;
	}
	if(!empty($state))
	{
		$statename=str_replace(" ","%20",$state);
		$url.='+'.$statename;
	}
	$url.='+USA';
	$url.="&sensor=false";
	
	$latlongdata = file_get_contents($url);
	return json_decode($latlongdata,true);
}

// Get Lat,Long from zip code and state city 
function getlatlongbyaddress($searchkey)
{
	$url='http://maps.googleapis.com/maps/api/geocode/json?address=';
	
	if(is_numeric($searchkey))
	{
		$url.=$searchkey;
	}
	else
	{
		$statename=str_replace(" ","%20",$searchkey);
		$url.=$statename;
	}
	$url.="&sensor=false";
	$latlongdata = file_get_contents($url);
	return json_decode($latlongdata,true);
}

// Calculate distance from one lat,long to other lat,long 
function get_distance($lat1, $lng1, $lat2, $lng2, $miles = true)
{
	$pi80 = M_PI / 180;
	$lat1 *= $pi80;
	$lng1 *= $pi80;
	$lat2 *= $pi80;
	$lng2 *= $pi80;

	$r = 6372.797; // mean radius of Earth in km
	$dlat = $lat2 - $lat1;
	$dlng = $lng2 - $lng1;
	$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
	$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
	$km = $r * $c;

	return ($miles ? ($km * 0.621371192) : $km);
}

// Get top strain raters
function get_top_strain_raters($userid,$no=null)
{
	$loginuserlatlongquery="SELECT latitude,longitude FROM users where status!='admin' and flag!='deactive' and  user_id='".$userid."'";
	$loginuserlatlongsql=mysql_query($loginuserlatlongquery)or die(mysql_error());
	$loginuserlatlongres=mysql_fetch_array($loginuserlatlongsql);
	$lat_one=$loginuserlatlongres['latitude'];
	$long_one=$loginuserlatlongres['longitude'];
	$rows_one = mysql_num_rows($loginuserlatlongsql);
	if($rows_one<1)
	{
		return false;
	}
	$maxstrainquery="SELECT count( `user_id` ) AS total_strains, `user_id` FROM `strains` where flag='".Active."' GROUP BY `user_id` ORDER BY count( `user_id` ) DESC";
	$maxstrainsql=mysql_query($maxstrainquery)or die(mysql_error());
	$rows = mysql_num_rows($maxstrainsql);
	$arr=array();
	$i=0;
	while($maxstrainresult=mysql_fetch_array($maxstrainsql))
	{
		$get_latlongofuserquery="select latitude,longitude,user_name,user_id from users where status!='admin' and flag!='deactive' and user_id='".$maxstrainresult['user_id']."'";
		$get_latlongofusersql=mysql_query($get_latlongofuserquery);
		while($get_latlongofuserresult=mysql_fetch_array($get_latlongofusersql))
		{
			$miles=get_distance($lat_one,$long_one,$get_latlongofuserresult['latitude'],$get_latlongofuserresult['longitude']);
			if($miles<10)
			{
				if(!empty($no))
				{
					if(($no-1)<$i)
					{
						return $arr;
					}
				}	
				$arr[$i]['UserId']=$get_latlongofuserresult['user_id'];
				$arr[$i]['UserName']=$get_latlongofuserresult['user_name'];
				$arr[$i]['miles']=$miles;
				$arr[$i]['strains']=$maxstrainresult['total_strains'];
				$i++;
			}
		}
	}
	return $arr;
}

// Get top strain raters
function get_local_menu_dispenaries($userid)
{
	$loginuserlatlongquery="SELECT latitude,longitude FROM users 
							where status!='admin' and flag!='deactive' 
							and user_id='".$userid."'";

	$loginuserlatlongsql = mysql_query($loginuserlatlongquery)or die(mysql_error());
	$loginuserlatlongres = mysql_fetch_array($loginuserlatlongsql);
	$lat_one = $loginuserlatlongres['latitude'];
	$long_one = $loginuserlatlongres['longitude'];
	$rows_one = mysql_num_rows($loginuserlatlongsql);
	if($rows_one<1)
	{
		return false;
	}
	$dispquery="SELECT latitude,longitude,dispensary_id FROM `dispensaries` where flag='".Active."' ORDER BY dispensary_id DESC";
	$dispsql=mysql_query($dispquery)or die(mysql_error());
	$rows = mysql_num_rows($dispsql);
	$arr=array();
	$i=0;
	while($dispresult=mysql_fetch_array($dispsql))
	{
		
		$miles=get_distance($lat_one,$long_one,$dispresult['latitude'],$dispresult['longitude']);
		if($miles<10)
		{
			$arr[$i]['dispensary_id']=$dispresult['dispensary_id'];
			$i++;
		}
	}
	
	return $arr;
}


//Resize the images in thumb or medium size. 
function resize_image($src,$dest,$width=100,$height=100)
{
	if(!$image_data = getimagesize($src))
    {
		return false;
    }
	//print_r($image_data['mime']);
	switch($image_data['mime'])
    {
		case 'image/gif':
							$quality=90;
							resize_gif_image($src,$dest,$height,$width,$quality);
		break;
		case 'image/jpeg';
							$quality=90;
							resize_jpg_image($src,$dest,$height,$width,$quality);
		break;
		case 'image/png':
							$quality=9;
							resize_png_image($src,$dest,$height,$width,$quality);
		break;
	}	
}

//Resize jpg images
function resize_jpg_image($src,$dest,$height,$width,$quality)
{
	$my_input_file = $src;
	$my_output_file = $dest;
	$jpeg_quality = $quality;
	$thumb_height = $height;
	$size = getimagesize($my_input_file);
	//$thumb_width = ($size[0] / $size[1]) * $thumb_height; 
    $thumb_width = $width;
	$src_img = imagecreatefromjpeg($my_input_file);
	$dst_img = imagecreatetruecolor($thumb_width,$thumb_height);
	imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_width, $thumb_height, $size[0], $size[1]);
	imagejpeg($dst_img,$my_output_file, $jpeg_quality);
	imagedestroy($src_img);
	imagedestroy($dst_img);
}

//Resize png images
function resize_png_image($src,$dest,$height,$width,$quality)
{
	$my_input_file = $src;
	$my_output_file = $dest;
	$png_quality = $quality;
	$thumb_height = $height;
	$size = getimagesize($my_input_file);
	//$thumb_width = ($size[0] / $size[1]) * $thumb_height; 
	$thumb_width = $width; 
	$src_img = imagecreatefrompng($my_input_file);
	$dst_img = imagecreatetruecolor($thumb_width,$thumb_height);
	
	$background = imagecolorallocate($dst_img, 0, 0, 0);// integer representation of the color black (rgb: 0,0,0)
	imagecolortransparent($dst_img, $background);// removing the black from the placeholder
	imagealphablending($dst_img, false);// turning off alpha blending (to ensure alpha channel information 
	// is preserved, rather than removed (blending with the rest of the  image in the form of black))
	// turning on alpha channel information saving (to ensure the full range of transparency is preserved)
	imagesavealpha($dst_img, true);
	imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_width, $thumb_height, $size[0], $size[1]);
	imagepng($dst_img, $my_output_file, $png_quality);
	imagedestroy($src_img);
	imagedestroy($dst_img);
}

//Resize gif images
function resize_gif_image($src,$dest,$height,$width,$quality)
{
	$my_input_file = $src;
	$my_output_file = $dest;
	$jpeg_quality = $quality;
	$thumb_height = $height;
	$size = getimagesize($my_input_file);
	//$thumb_width = ($size[0] / $size[1]) * $thumb_height; 
	$thumb_width = $width; 
	$src_img = imagecreatefromgif($my_input_file);
	$dst_img = imagecreatetruecolor($thumb_width,$thumb_height);
	imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_width, $thumb_height, $size[0], $size[1]);
	imagegif($dst_img, $my_output_file, $jpeg_quality);
	imagedestroy($src_img);
	imagedestroy($dst_img);
}

function insert_parse_links($text)
{

$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);

// pad it with a space so we can match things at the start of the 1st line.
$ret = ' ' . $text;

// matches an "xxxx://yyyy" URL at the start of a line, or after a space.
// xxxx can only be alpha characters.
// yyyy is anything up to the first space, newline, comma, double quote or <
$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);

// matches a "www|ftp.xxxx.yyyy[/zzzz]" kinda lazy URL thing
// Must contain at least 2 dots. xxxx contains either alphanum, or "-"
// zzzz is optional.. will contain everything up to the first space, newline,
// comma, double quote or <.
$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);

// matches an email@domain type address at the start of a line, or after a space.
// Note: Only the followed chars are valid; alphanums, "-", "_" and or ".".
$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);

// Remove our padding..
$ret = substr($ret, 1);
return $ret;
}


function addhttp($url)
{	
   if (!preg_match("~^(?:f|ht)tps?://~i", $url)) 
   {
       $url = "https://" . $url;
   }
   return $url;
}







/*
** $str -String to truncate
** $length - length to truncate
** $trailing - the trailing character, default: "..."
*/
function truncatestr ($str, $length=25, $trailing='...')
{
	// take off chars for the trailing
	$length-=strlen($trailing);
	if (strlen($str)> $length)
	{
		// string exceeded length, truncate and add trailing dots
		return substr($str,0,$length).$trailing;
	}
	else
	{
		// string was already short enough, return the string
		$res = $str;
	}
	return $res;
}


function getPagination($count,$pageno=null)
{
	if(empty($pageno))
	{
		$paginationCount= floor($count / PAGE_PER_NO);
		$paginationModCount= $count % PAGE_PER_NO;
	}
	else
	{
		$paginationCount= floor($count / $pageno);
		$paginationModCount= $count % $pageno;
	}
	if(!empty($paginationModCount))
	{
		$paginationCount++;
	}
	return $paginationCount;
}

// Function that return date. Ex : $day='Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' 
function last_dayofweek($day)
{
    // return timestamp of last Monday...Friday
    // will return today if today is the requested weekday
    $day = strtolower(substr($day, 0, 3));
    if (strtolower(date('D')) == $day)
        return strtotime("today");
    else
        return strtotime("last {$day}");
}

function agotime($time)
{
   $time=strtotime($time);
   $periods = array("sec", "min", "hr", "day", "week", "month", "year", "decade");
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

       $difference     = $now - $time;
       $tense         = "ago";

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);

   if($difference != 1 && $periods[$j] != 'sec' && $periods[$j] != 'min') {
       $periods[$j].= "s";
   }

   return "$difference $periods[$j]";
}

function get_zip_search($zipcode,$rmiles=null)
{
	if(empty($rmiles))
	{
		$radious=LoungeSearchRadious;
	}
	else
	{
		$radious=$rmiles;
	}
	$uidsarr=array();
	$i=0;
	$latlongdata=getlatlong($zipcode);
	if($latlongdata['status']!='OK')
	{
		return $uidsarr;
	}
	$latone=$latlongdata['results'][0]['geometry']['location']['lat'];
	$lngone=$latlongdata['results'][0]['geometry']['location']['lng'];
	
	$get_latlongofuserquery="select latitude,longitude,user_name,user_id from users where  status!='admin' and flag!='deactive' ";
	$get_latlongofusersql=mysql_query($get_latlongofuserquery);
	while($get_latlongofuserresult=mysql_fetch_array($get_latlongofusersql))
	{
		$miles=get_distance($latone,$lngone,$get_latlongofuserresult['latitude'],$get_latlongofuserresult['longitude']);
		if($miles < $radious)
		{
			$uidsarr[$i]['UserId']=$get_latlongofuserresult['user_id'];
			$uidsarr[$i]['UserName']=$get_latlongofuserresult['user_name'];
			$uidsarr[$i]['miles']=$miles;
			$i++;
		}
	}
	return $uidsarr;
}

function get_Search_menu_dispenaries($searchkey,$rmiles=null)
{
	if(empty($rmiles))
	{
		$radious=MenuSearchRadious;
	}
	else
	{
		$radious=$rmiles;
	}
	$arr=array();
	$i=0;
	$latlongdata=getlatlongbyaddress($searchkey);
	if($latlongdata['status']!='OK')
	{
		return $arr;
	}
	$lat_one=$latlongdata['results'][0]['geometry']['location']['lat'];
	$long_one=$latlongdata['results'][0]['geometry']['location']['lng'];
	
	$dispquery="SELECT latitude,longitude,dispensary_id FROM `dispensaries` where flag='".Active."' ORDER BY dispensary_id DESC";
	$dispsql=mysql_query($dispquery)or die(mysql_error());
	$rows = mysql_num_rows($dispsql);
	while($dispresult=mysql_fetch_array($dispsql))
	{
		$miles=get_distance($lat_one,$long_one,$dispresult['latitude'],$dispresult['longitude']);
		if($miles < $radious)
		{
			$arr[$i]['dispensary_id']=$dispresult['dispensary_id'];
			$i++;
		}
	}
	return $arr;
}

// Check user id is valid or not : Perameter : user Id
function CheckValidUser($intValidUserId)
{
	//SQL query to check the  USER exists or not
	$strUserNameSQL = "SELECT user_name FROM users WHERE user_id ='".$intValidUserId."'";
	$sql = mysql_query($strUserNameSQL)or die(mysql_error());//executing strUserNameSQL
	if(mysql_num_rows($sql) > 0)
	{
		return true; 
	}
	else
	{
		return false;
	}

}

// Get strain primary image if primary image not set, Get latest strain image. Perameter : strain Id    
function GetPrimaryImages($StrainId)
{
	global $path;
	$imageArray=array();
	//SQL query to get Lounge primary images
	$PrimaryImageQuery = "SELECT image_name,image_id FROM strainimages WHERE 
	strain_id ='".$StrainId."' and  image_name!='' order by image_id asc limit 0,1";
	$PrimaryImageSql = mysql_query($PrimaryImageQuery);//executing strLatestImage
	if(mysql_num_rows($PrimaryImageSql) > 0)
	{
		$ImageUrl = mysql_fetch_array($PrimaryImageSql);
		if(empty($ImageUrl['image_name']))
		{
			$imageArray[0]='';
			$imageArray[1]='';
		}
		else
		{
			$imageArray[0]=$path.'images/uploads/original/'.$ImageUrl['image_name'];
			$imageArray[1]=$path.'images/uploads/thumbnail/'.$ImageUrl['image_name'];
		}
		return $imageArray;
	}
	
	//SQL query to get Lounge latest images
	$LatestImageQuery = "SELECT image_name,image_id FROM strainimages WHERE 
	strain_id ='".$StrainId."' order by image_id desc limit 0,1";
	$LatestImageSql = mysql_query($LatestImageQuery);//executing strLatestImage
	if(mysql_num_rows($LatestImageSql) > 0)
	{
		$ImageUrl=mysql_fetch_array($LatestImageSql);
		if(empty($ImageUrl['image_name']))
		{
			$imageArray[0]='';
			$imageArray[1]='';
		}
		else
		{
			$imageArray[0]=$path.'images/uploads/original/'.$ImageUrl['image_name'];
			$imageArray[1]=$path.'images/uploads/thumbnail/'.$ImageUrl['image_name'];
		}
		return $imageArray;
	}
	$imageArray[0]='';
	$imageArray[1]='';
	return $imageArray;
}  
// Get user detail. Perameter UserId
function GetUserDetail($UserId)
{
	$user=array();
	//SQL query to get User detail 
	$UserDetailQuery = "SELECT user_name,zip_code,privacy FROM users WHERE user_id ='".$UserId."'  limit 0,1";
	$UserDetailSql = mysql_query($UserDetailQuery);
	if(mysql_num_rows($UserDetailSql) > 0)
	{
		$resUserName=mysql_fetch_array($UserDetailSql);
		$user['UserName']=$resUserName['user_name'];
		$user['ZipCode']=$resUserName['zip_code'];
		$user['Privacy']=$resUserName['privacy'];
	}
	else
	{
		$user['UserName']='';
		$user['ZipCode']='';
		$user['Privacy']='';
	}
	return $user;
}

function getFollowUpList($dispensaryid,$userid)
{
	$followupQuery="select * from  menu_followup where dispensary_id='".$dispensaryid."' and user_id='".$userid."'";
	$followupSql=mysql_query($followupQuery);
	$followupResult=mysql_num_rows($followupSql);
	if($followupResult>0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
function UpdateAdViews($adid)
{
	// Update page views
	$updatePageViewsQuery="update advertisement set current_views=current_views+1 where  advertisement_id='".$adid."' ";
	$updatePageViewsSql=mysql_query($updatePageViewsQuery)or die(mysql_error());	
}


function ShowAds($type,$allowzip)
{
	$AdIdsArray=array();
	$Flag=false;
	$HTML='';
	if($allowzip==false){
		$Query="select * from  advertisement 
				where ad_type='".$type."' and image_name!='' 
				and website_url!='' 
				and status='".Active."' 
				and ad_expire_date >='".date("Y-m-d")."' 
				and current_views < max_page_views order by RAND() limit 0,1 ";
		//echo $Query;
		$Sql = mysql_query($Query)or die(mysql_error());
		$adrowno = mysql_num_rows($Sql);
		if($adrowno>0)
		{
			while($data=mysql_fetch_array($Sql))
			{
				$Flag=true;

				UpdateAdViews($data['advertisement_id']);

				//$HTML="<img src='images/ad_images/".$data['image_name']."' />";
				$HTML.='<a target="_blank" class="clickcount" id="clickcount_'.$data['advertisement_id'].'"  href="'.addhttp($data['website_url']).'">
					<img src="images/ad_images/'.$data['image_name'].'" /></a>';
			}
			
		}
	}
	else{
	// Show landing page ad.
		$Query="select * from  advertisement 
				where ad_type='".$type."' 
				and status='Active' 
				and ad_expire_date >='".date("Y-m-d")."' 
				and  current_views < max_page_views
				order by RAND() limit 0,1 ";
 //echo $Query."<br>";
		$Sql = mysql_query($Query)or die(mysql_error());
		$RowNo = mysql_num_rows($Sql);
		if($RowNo>0)
		{
			while($Result=mysql_fetch_array($Sql))
			{ 	
				if(!empty($Result['zipcode_setting']))
				{
					$getuserzip = "select zip_code from users where user_id='".$_SESSION['user_id']."'";
					$usql = mysql_query($getuserzip)or die(mysql_error());
					$udata = mysql_fetch_array($usql);
					$zipcodes = explode(",",$Result['zipcode_setting']);
					if(in_array($udata['zip_code'],$zipcodes))
					{
						$AdIdsArray[]=$Result['advertisement_id'];
					}
				}
				else
				{
					if(empty($Result['active_view_radius']))
					{
						$mls =ActiveViewRadiousAdmin;
					}
					else
					{
						$mls =$Result['active_view_radius'];
						$admiles = preg_replace("/[^\d]/", "", $mls);
					}		 
					$miles=get_distance($_SESSION['lat'],$_SESSION['long'],$Result['latitude'],$Result['longitude']);
					if($miles<$admiles)
					{
						$AdIdsArray[]=$Result['advertisement_id'];
					}
				}	
			}
			if(count($AdIdsArray) > 0)
			{
				$AdidsNo = count($AdIdsArray);
				$adids='';
				for($l=0;$l<$AdidsNo;$l++)
				{	
					$adids.=$AdIdsArray[$l];
					if($l<($AdidsNo-1))
					{
						$adids.=',';
					}
				}
				$Query = "select * from  advertisement 
						  where advertisement_id in (".$adids.") or zipcode_setting in(".$adids.") 
						  and image_name !='' 
						  and website_url!='' 
						  order by RAND() limit 0,1";
				$sql=mysql_query($Query) or die(mysql_error($Query));
			
				while($data=mysql_fetch_array($sql))
				{
					$Flag=true;
					UpdateAdViews($data['advertisement_id']);
					//$HTML="<img src='images/ad_images/".$data['image_name']."' />";
					$HTML.='<a target="_blank" class="clickcount" id="clickcount_'.$data['advertisement_id'].'"  href="'.addhttp($data['website_url']).'">
					<img src="images/ad_images/'.$data['image_name'].'" /></a>';
				}
			}
		}
	}	
	$opt[0]=$Flag;
	$opt[1]=$HTML;
	return $opt; 
}

function GetFileName($scriptname)
{
	$requestname=explode("/",$scriptname);
 	$count=count($requestname);
	$count=$count-1;
	return $requestname[$count];
}

function LoungeSearchRes($searchtext)
{
	
	$strValidSearchtext=mysql_real_escape_string(trim($searchtext));
	$zipdata=get_zip_search($strValidSearchtext);
	$countlocaluser=count($zipdata);
	if($countlocaluser>0)
	{	
		$uids='';
		for($l=0;$l<$countlocaluser;$l++)
		{
			$uids.=$zipdata[$l]['UserId'];
			if($l<($countlocaluser-1))
			{
				$uids.=',';
			}
		}
		/*$queryone="select strain_id,strain_name,dispensary_name, post_date, species,overall_rating, strains.user_id from strains INNER JOIN users on users.user_id=strains.user_id where strains.flag='".Active."' and  privacy='public' and(strains.strain_name LIKE '%".$strValidSearchtext."%' || strains.dispensary_name LIKE '%".$strValidSearchtext."%' || users.zip_code LIKE '%".$strValidSearchtext."%' || strains.user_id in (".$uids.")) order by strain_id desc Limit $start,$perpage";*/
		$queryone="SELECT strain_id, strain_name,dispensary_name,post_date,species,overall_rating,
			   strains.user_id as userId,NULL AS picture_url, 1 AS TYPE
			   FROM strains
			   INNER JOIN users ON users.user_id = strains.user_id
			   WHERE strains.flag = 'active'
			   AND users.flag ='active'
			   AND privacy = 'public'
               AND (strains.strain_name LIKE '%".$strValidSearchtext."%' || strains.dispensary_name LIKE '%".$strValidSearchtext."%' || users.zip_code LIKE '%".$strValidSearchtext."%' || strains.user_id in (".$uids."))
			   UNION ALL
			   SELECT bud_thought_id, bud_thought, NULL AS dispensary_name,
			   post_date, NULL AS species,
			   NULL AS overall_rating,tbt.user_id as userId, picture_url, 0 AS TYPE
			   FROM tbl_bud_thought tbt
			   INNER JOIN users ON users.user_id = tbt.user_id
			   WHERE users.flag = 'active'
               and bud_thought like '%".$strValidSearchtext."%'";
	}
	else
	{
		/*$queryone="select strain_id,strain_name,dispensary_name, post_date, species,overall_rating, strains.user_id from strains INNER JOIN users on users.user_id=strains.user_id where strains.flag='".Active."' and  privacy='public' and(strains.strain_name LIKE '%".$strValidSearchtext."%' || strains.dispensary_name LIKE '%".$strValidSearchtext."%') order by strain_id desc Limit $start,$perpage";*/
		$queryone ="SELECT strain_id, strain_name,dispensary_name,post_date,species,overall_rating,
			   strains.user_id as userId,NULL AS picture_url, 1 AS TYPE
			   FROM strains
			   INNER JOIN users ON users.user_id = strains.user_id
			   WHERE strains.flag = 'active'
			   AND users.flag ='active'
			   AND privacy = 'public'
               AND (strains.strain_name LIKE '%".$strValidSearchtext."%' || strains.dispensary_name LIKE '%".$strValidSearchtext."%')
			   UNION ALL
			   SELECT bud_thought_id, bud_thought, NULL AS dispensary_name,
			   post_date, NULL AS species,
			   NULL AS overall_rating,tbt.user_id as userId, picture_url, 0 AS TYPE
			   FROM tbl_bud_thought tbt
			   INNER JOIN users ON users.user_id = tbt.user_id
			   WHERE users.flag = 'active'
               and bud_thought like '%".$strValidSearchtext."%'";
	}
	
	$results=mysql_query($queryone)or die (mysql_error());
	$rows = mysql_num_rows($results);
	if($rows > 0)
	{
		$i = 0;
		while($straindata=mysql_fetch_array($results))
		{	
			$i++;	
        	echo ' <div class="inn_cell">';
        	$querythree="select user_name,zip_code,state,photo_url from users where user_id='".$straindata['user_id']."' limit 0,1";
			$userdata=mysql_query($querythree);
			$udata=mysql_fetch_array($userdata);
			?>
            <!--user name-->
            <div class="gray_bg_cell"> <h4> <a href="javascript:void(0);" class="userprofile" id="userids_<?php echo $straindata['userId'];?>"> <?php echo truncatestr($udata['user_name'],20);?></a>  </h4> </div>
            <!--user name-->
            
            <!--image -->
            <?php 
			if(!empty($udata['photo_url']))
        	{
			?>
			<div class="loungeimg dis_photos">
            	<img src="<?php echo 'images/profileimages/original/'.$udata['photo_url'];?>" /> 
            </div>
			<?php 	}
			else
			{ 
				echo '<div class="loungeimg">
						<img src="images/css_images/profile_pic.png" />
				 </div>';					
			}
			?>
            <!--image-->
            
            <!--cell left-->
            <div class="cell_left">
       		 <?php echo truncatestr($straindata['strain_name'],20); ?><br>
             <?php echo $straindata['species'];?> <br>
			<?php if(!empty($straindata['dispensary_name']))?>
              
              <?php 		
                 $dispancry_link_query="select dispensary_id 
                                         from dispensaries 
                                         where dispensary_name='".$straindata['dispensary_name']."'
                                         Limit 0,1";
                  $dispancry_link_sql = mysql_query($dispancry_link_query);
                  $dispancry_link_result = mysql_fetch_array($dispancry_link_sql);
                  if(!empty($dispancry_link_result['dispensary_id']))
                  {
                      echo "<a href='javascript:void(0);' id='displink_".$dispancry_link_result['dispensary_id']."' class='dispensary-popup gray_link'>".truncatestr($straindata['dispensary_name'],20)."</a>";
                  }
                 /* else
                  {
                      echo truncatestr($straindata['dispensary_name'],20);
                  }*/
              ?>
             </div>
            <!--end of cell left-->
            
            <!--cell right-->
            <div class="cell_right">
            
              <?php for($rating=1;$rating < 6;$rating++){
                                                        if($straindata['overall_rating']>=$rating) {?>
              <img src="images/css_images/start_sel.png">
              <?php 	} else {?>
              <img src="images/css_images/start_dsel.png">
              <?php	} }?>
             </div>
            <!--end of cell right-->
             <div class="arrow" > <a href="javascript:void(0);"> <img src="images/new_web/arrow.png" <?php if(!empty($_SESSION['user_id'])){?> class="loung_straindetail scale-with-grid" <?php }else{?> class="alertRequest scale-with-grid" <?php } ?> id="strainid_<?php echo $straindata['strain_id']; ?>"> </a></div>
             
              <?php
		  $strLikeStrainSQL ="SELECT count(*) as count FROM tbl_strain_like 
		   					  WHERE strain_id ='".$straindata['strain_id']."'";
		  $resStrainSQL = mysql_query($strLikeStrainSQL) or die($strLikeStrainSQL." : ".mysql_error());
		  $rowCount = mysql_fetch_assoc($resStrainSQL);
		  $likeCount= $rowCount['count'];
		  	 
		  ?>
             
             
             <div class="btn_area_single"> 
                  <div class="fllft"> <a href="javascript:void(0);" <?php if(!empty($_SESSION['user_id'])){?>class="like_btn strain_btn" <?php }else{?> class="alertRequest like_btn" <?php } ?> id="likeBudThought_<?php echo $straindata['strain_id'];?>"> like </a> <span class="no_display" id="like_count_<?php echo $straindata['strain_id']; ?>">  <?php echo $likeCount;?> </span> &nbsp; &nbsp; &nbsp;   </div> 
                  </div>
            
            
		</div>
<?php }  
	}
	else
	{
		//echo '<div class="cell"><center style="padding-top:25px;">No result found.</center></div>';
	}
}

function MenuSearchRes($searchtext)
{
	
	$strValidSearchtext=mysql_real_escape_string(trim($searchtext));
	$zipdata=get_Search_menu_dispenaries($strValidSearchtext);
	$countlocaluser=count($zipdata);
	/*if($countlocaluser>0)
	{	
		$uids='';
		for($l=0;$l<$countlocaluser;$l++)
		{
			$uids.=$zipdata[$l]['dispensary_id'];
			if($l<($countlocaluser-1))
			{
				$uids.=',';
			}
		}
		$queryone="select dispensary_id,dispensary_name,city,state,zip,image,latitude,longitude,phone_number from dispensaries where flag='Active' and(city LIKE '%".$strValidSearchtext."%' || state LIKE '%".$strValidSearchtext."%' || zip LIKE '%".$strValidSearchtext."%' || dispensary_id in (".$uids.")) order by dispensary_name";
	}
	else
	{*/
		$queryone="SELECT DISTINCT (
							dispensary_id
							), dispensary_name, city, state, zip, image, phone_number, added_by
							FROM (
							SELECT dispensary_id, dispensary_name, city, state, zip, image, phone_number, added_by
							FROM dispensaries
							WHERE flag = 'Active'
							AND dispensary_name LIKE '".$strValidSearchtext."%'
							UNION
							SELECT dispensary_id, dispensary_name, city, state, zip, image, phone_number, added_by
							FROM dispensaries
							WHERE flag = 'Active'
							AND city = '".$strValidSearchtext."' || state = '".$strValidSearchtext."' || zip = '".$strValidSearchtext."'
							) AS temp_table";
	//}
	
	$results=mysql_query($queryone)or die (mysql_error());
	$rows = mysql_num_rows($results);
	if($rows > 0)
	{
		$i = 0;
		while($menudata=mysql_fetch_array($results))
		{	
			$i++;
			include('commanDispensary.php');//data replce by menusearch_resultdata.php
       }
	}
	else
	{
		echo '<div class="cell"><center style="padding-top:25px;">No result found.</center></div>';
	}
}
/*
	File name- functions.php
	File Description - For external functions 
*/

/*
	Function name - getLikeCount
	Function Description - to get all the like counts on the given Bud thoght
	Date - 15th may
*/
function getLikeCount($budThoughtId){
	
	//query to fetch like count
	$countLikeSQL = "SELECT count(*) as likeCount
					 FROM `tbl_like_thoughts` 
					 where `bud_thought_id`='".$budThoughtId."'";
				 
	$resultLikeCount = mysql_query($countLikeSQL);
	$rowLike = mysql_fetch_assoc($resultLikeCount);
	$likeCount = $rowLike['likeCount'];
	
	return $likeCount;
}


/*
	Function name - getCommentCount
	Function Description - to get all the Commnets counts on the given Bud thought
	Date - 15th may

*/
function getCommentCount($budThoughtId)
{
	//query to get the like count and comment count	
		$countCmmntSQL = "SELECT count(*) as commentCount 
						  FROM `tbl_comments` 
						  where `bud_thought_id`='".$budThoughtId."'";
		$resultCommentCount = mysql_query($countCmmntSQL);
		$rowComment = mysql_fetch_assoc($resultCommentCount);
		$commentCount = $rowComment['commentCount'];
		return $commentCount;
}
/*
	function to get the userName
*/
function getUserName($userId)
{
	 	$strGetUserNameSQl ="SELECT user_name FROM users WHERE user_id='".$userId."'";
		$resGetUserNameSQl = mysql_query($strGetUserNameSQl) or die($strGetUserNameSQl." : ".mysql_error());
		
		$rowUser = mysql_fetch_assoc($resGetUserNameSQl);
		return $rowUser['user_name'];
}
/*
	Function isUserLiked()
*/
function isUserLiked($budThoughtId,$userID)
{
	$strIsLikeSQl = "SELECT like_id FROm tbl_like_thoughts 
					WHERE user_id='".$userID."'
					AND bud_thought_id='".$budThoughtId."'";
	$resIsLikeSQl = mysql_query($strIsLikeSQl) or die($strIsLikeSQl." : ".mysql_error());
	
	if(mysql_num_rows($resIsLikeSQl)>0){
		return 1;
	}else
	{
		return 0;
	}
}

//get the counts of species 
 function getSpeciesCount($userId,$speciesType)
 {
	 if(strtolower($speciesType)=='hybrid'){
		$appendQuery = "species LIKE '%".$speciesType."%'";
	 }else
	 {
		 $appendQuery = "species ='".$speciesType."'";
	 }
	 $strGetCountsSQL ="SELECT count( `species` ) as counts, species
						FROM `strains`
						WHERE user_id ='".$userId."' and flag='active'
						and ".$appendQuery;
	
	$resCountsSQL = mysql_query($strGetCountsSQL) or die($strGetCountsSQL." :".mysql_error());
 	$countSpecies = mysql_fetch_assoc($resCountsSQL);
	return $countSpecies['counts'];
 }

 function getStrainLibName($strainLibId)
 {
	 	$strgetNameSQL = "SELECT * FROM strain_library
						  WHERE strain_lib_id='".$strainLibId."'";
		$resGetName = mysql_query($strgetNameSQL) or die($strgetNameSQL." : ".mysql_error());
		
		$strain_lib = mysql_fetch_assoc($resGetName);
		
		return $strain_lib;
		
 }

 /*
	Function isUserLikeStrain()
*/
 /*
	Function isUserLikeStrain()
*/
function isUserLikeStrain($strainId,$userID)
{
	$strIsLikeSQl = "SELECT starin_like_id FROM tbl_strain_like 
					 WHERE user_id='".$userID."'
					 AND strain_id='".$strainId."'";
	$resIsLikeSQl = mysql_query($strIsLikeSQl) or die($strIsLikeSQl." : ".mysql_error());
	
	if(mysql_num_rows($resIsLikeSQl)>0){
		return 1;
	}else
	{
		return 0;
	}
} 
/*
	function get dispensary Id
*/
function getDispensaryId($strDispensaryName)
{
	$strGetId = "SELECT dispensary_id FROM dispensaries WHERE dispensary_name='".$strDispensaryName."' LIMIT 0,1";
	$resGetId = mysql_query($strGetId) or die($strGetId." : ".mysql_error());

	$rowId = mysql_fetch_assoc($resGetId);
	return $rowId['dispensary_id']; 
}

/*
	Function name - getLikeCountStrain
	Function Description - to get all the like counts on the given Strain
	Date - 14th june
*/
function getLikeCountStrain($strainId){
	
	//query to fetch like count
	$countLikeSQL = "SELECT count(*) as likeCount
					 FROM tbl_strain_like 
					 where strain_id='".$strainId."'";
				 
	$resultLikeCount = mysql_query($countLikeSQL);
	$rowLike = mysql_fetch_assoc($resultLikeCount);
	$likeCount = $rowLike['likeCount'];
	
	return $likeCount;
}
/*
	function - to get dispensary photos count
	Discription - no of photos in strains of a dispensary
*/
function getPhotosCount($dispensaryName)
{
	$disNameArray = explode('-',$dispensaryName);
	if(count($disNameArray)>0)
	{
		$disName= $disNameArray[0];
		$disId =getDispensaryId($disName);
	}
	else
	{
		$disName=$dispensaryName;
	}
	$strGetCount ="SELECT count(*) as imgCount
								  FROM `strainimages`
								  WHERE `strain_id`
								  IN (
										SELECT `strain_id`
										FROM `strains`
										WHERE (`dispensary_name` = '".addslashes($disName)."'
												or dispensary_id='".$disId."')
										AND flag='active'
										
								 ) AND image_name!=''";
				 
	$resGetCount = mysql_query($strGetCount) ;//or die($strGetCount." : ".mysql_error())
	
	$row = mysql_fetch_assoc($resGetCount);
	return $row['imgCount'];
}


/*
	function - to get dispensary reviews count
*/
function getReviewsCount($dispensaryId)
{
	/*$strGetCount = "SELECT count(*) as count 
					FROM `tbl_dispensary_review` 
					WHERE `dispensary_id`='".$dispensaryId."'";
					
	$resGetCount = mysql_query($strGetCount) or die($strGetCount." : ".mysql_error());
	
	$row = mysql_fetch_assoc($resGetCount);
	return $row['count'];*/
	
	$dispensaryName = getDispensaryName($dispensaryId);
		
	$strReviewCountSQL ="SELECT strain_id 
							 FROM strains
							 WHERE flag='active' 
							 and dispensary_name LIKE  '".$dispensaryName."%'
							 OR dispensary_id = '".$dispensaryId."'
							 union all 
							 SELECT distinct(user_id)
							 FROM tbl_dispensary_review 
							 WHERE dispensary_id='".$dispensaryId."'";
		
	$resCount = mysql_query($strReviewCountSQL) or die($strReviewCountSQL." : ".mysql_error());

	$strReviewData = mysql_num_rows($resCount);
	return $strReviewData;
	
	
	
}
/*
	function - to get followers count
*/ 
 function getFollowersCount($dispensaryId)
 {
	 	$strGetFollowersCount = "SELECT count(`user_id`) as count
							   FROM `menu_followup`
							   WHERE `dispensary_id` = '".$dispensaryId."'";
		$resFollowersCount = mysql_query($strGetFollowersCount) or die($strGetFollowersCount." : ".mysql_error());
		
		$rowCount = mysql_fetch_assoc($resFollowersCount);
		
		return $rowCount['count'];
		
 }
 
/*
 	function name - getStrainsPhotosCount($userId)
 
 */
function  getStrainsPhotosCount($userId)
{
	$strGetCount = "SELECT count( image_id ) as imgCount
					FROM strainimages
					WHERE strain_id
					IN (
					
					SELECT `strain_id`
					FROM `strains`
					WHERE `user_id` = '".$userId."'
					and flag='active'
					)and image_name!=''";
	$resCount = mysql_query($strGetCount) or die($strGetCount." : ".mysql_error());
	$rowCount = mysql_fetch_assoc($resCount);
	return $rowCount['imgCount'];
}
/*
	get users followers count
*/
function getUserFollowersCount($userId)
{
	$strGetFollowersCount ="SELECT count(follower_id) as count
							FROM user_followup WHERE user_id='".$userId."'";
	$resFollowersCount = mysql_query($strGetFollowersCount) or die($strGetFollowersCount." : ".mysql_error());
	$rowCount = mysql_fetch_assoc($resFollowersCount);
	return $rowCount['count'];
}


function StrainLiberarySearchRes($searchtext)
{
	
	$strValidSearchtext=mysql_real_escape_string(trim($searchtext));
		$queryone="SELECT *
			   	   FROM strain_library
			   	   WHERE strain_name LIKE '%".$strValidSearchtext."%'
			       ";
	
	$results = mysql_query($queryone)or die (mysql_error());
	$rows = mysql_num_rows($results);
	if($rows > 0)
	{
	?>
		
    <?php 
		while($straindata = mysql_fetch_array($results))
		{ 
	?>
    		   <div class="inn_cell" id="SL_<?php echo $straindata['strain_lib_id'];?>">
               		<div class="cell_left"> <h4> <?php echo $straindata['strain_name'];?> </h4></div>
               		<div class="cell_left"> <span class="grntxt"> <?php echo $straindata['species'];?> </span></div>
                    <div class=" fltrit martoprit2"> <a href="#dialoguser" name="modal"> <img src="images/new_Web/arrow.png" class="scale-with-grid"> </a></div>
                	<!--<td class="strain_lib" id="strainLib_<?php // echo $straindata['strain_lib_id'];?>"></td>-->
                </div>
     <?php 
		}
	 ?>
        
	<?php }
	else
	{
		echo '<div class="inn_cell"><center style="padding-top:25px;">No result found.</center></div>';
	}
}

function getDispCountOfUser($userId)
{
	$strgetCount ="SELECT DISTINCT count( `dispensary_name` ) AS counts, dispensary_name
				   FROM `strains`
                   WHERE user_id='".$userId."'
                   AND dispensary_name != ''
				   and flag='active'";
	$resGetCount =mysql_query($strgetCount) or die($strgetCount." : ".mysql_error());
	
	$resCount =mysql_fetch_assoc($resGetCount);
	return $resCount['counts'];
}

// 11TH JULY Get strain image if primary image not set, Get latest strain image. Perameter : strain Id    
function GetThreeImages($StrainId)
{
	global $path;
	$imageArray=array();
	
	//SQL query to get Lounge primary images
	$PrimaryImageQuery = "SELECT image_name,image_id FROM strainimages WHERE 
	strain_id ='".$StrainId."' order by image_id desc limit 0,3";

	$PrimaryImageSql = mysql_query($PrimaryImageQuery);//executing strLatestImage
	if(mysql_num_rows($PrimaryImageSql) > 0)
	{
		$i=0;
		while($ImageUrl=mysql_fetch_array($PrimaryImageSql))
		{
			$imageArray[$i]=(!empty($ImageUrl['image_name']))?$path.'images/uploads/thumbnail/'.$ImageUrl['image_name']:'';
			$i++;
		}			
		return $imageArray;
	}else
	{
		$imageArray[0]='';
		$imageArray[1]='';
		$imageArray[2]='';
	}
}  

//function strain liberary reviews count  
//date 22nd july
function strainLibReviewsCount($strainLibName){
	
	//SQL query to get Strain name used by users 
	$strStrainLiberarySQL = "SELECT count( DISTINCT (
							user_id	
							) ) AS counts
							FROM strains
							WHERE strain_name = '".$strainLibName."'";
	$resStrainLiberarySQL = mysql_query($strStrainLiberarySQL) or die($strStrainLiberarySQL." : ".mysql_error());
	
	$rowCount = mysql_fetch_assoc($resStrainLiberarySQL);
	
	return $rowCount['counts'];

}//end of function 



//for geolocation dispensary
//date  22nd july
function geoLocationDispensary($lat,$long){

$dispquery = "SELECT latitude,longitude,dispensary_id FROM `dispensaries` 
			  WHERE flag='Active' 
			  ORDER BY dispensary_id DESC";
				
	$dispsql = mysql_query($dispquery)or die(mysql_error());
	$rows = mysql_num_rows($dispsql);
	$arr = array();
	$i = 0;
	while($dispresult=mysql_fetch_array($dispsql))
	{
		$miles=get_distance($lat,$long,$dispresult['latitude'],$dispresult['longitude']);
		if($miles<10)
		{
			$arr[$i]['dispensary_id']=$dispresult['dispensary_id'];
			$i++;
		}
	}
	return $arr;
}
function getUserImagePath($userId){
 
 $getPathSQL = "SELECT photo_url FROM users WHERE user_id='".$userId."'";
 $response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());

 $row = mysql_fetch_assoc($response);
 
	 if(!empty($row['photo_url'])){
	
		$imgPath = $row['photo_url'];
	 }else
	 {
		 $imgPath = '';
	 }
	 return $imgPath;
 }



 
//function getDispensaryName 
//date 8th aug
function getDispensaryName($dispensaryId)
{
	$strGEtNameSQL = "SELECT dispensary_name FROM dispensaries WHERE dispensary_id='".$dispensaryId."'";
	$res = mysql_query($strGEtNameSQL) or die($strGEtNameSQL." : ".mysql_error());
	$row = mysql_fetch_assoc($res);
	return $row['dispensary_name'];
}




//function - getLikeStrainUserList
//date - 17th aug
function getLikeStrainUserList($strainId)
{
	$strGetUserLIstSQL = "SELECT user_id
						  FROM tbl_strain_like
						  WHERE strain_id = '".$strainId."' order by starin_like_id desc
						";
						  
	$resUserList = mysql_query($strGetUserLIstSQL) or die($strGetUserLIstSQL." : ".mysql_error());
	
	$likeStarinUserList = array();					  
	while($rowUser = mysql_fetch_assoc($resUserList))
	{
		$userImg =getUserImagePath($rowUser['user_id']);
		if($userImg!='')
		{
			$path=BASEURL."images/profileimages/thumbnail/".$userImg;

		}else
		{
			$path='';
		}
			$likeStarinUserList[]=array(
								'id'=> $rowUser['user_id'],
								'user_name'=>getUserName($rowUser['user_id'])!=''?getUserName($rowUser['user_id']):'',
								'thumb_url'=>$path
				 );
	}
	
	return array_reverse($likeStarinUserList);
						  
}


//function - getLikeBudThoughtUserList
//date - 17th aug
function getLikeBudThoughtUserList($budThoughtId)
{
	$strGetUserLIstSQL = "SELECT user_id
						  FROM `tbl_like_thoughts`
						  WHERE `bud_thought_id` = '".$budThoughtId."' order by like_id desc
						  ";
						  
	$resUserList = mysql_query($strGetUserLIstSQL) or die($strGetUserLIstSQL." : ".mysql_error());
	
	$likeBTUserList = array();					  
	while($rowUser = mysql_fetch_assoc($resUserList))
	{
		
			$userImg =getUserImagePath($rowUser['user_id']);
			if($userImg!='')
			{
				$path=BASEURL."images/profileimages/thumbnail/".$userImg;
	
			}else
			{
				$path='';
			}
			$likeBTUserList[]=array(
								'id'=> $rowUser['user_id'],
								'user_name'=>getUserName($rowUser['user_id'])!=''?getUserName($rowUser['user_id']):'',
								'thumb_url'=>$path
							  );
	}
	
	return array_reverse($likeBTUserList);
						  
}


//function - getCommentUserList
//date - 17th aug
function getCommentList($budThoughtId)
{
	$strGetUserLIstSQL = "SELECT comment_id, bud_thought,user_id
						  FROM tbl_comments
						  WHERE `bud_thought_id` = '".$budThoughtId."'
						  ";
						  
	$resUserList = mysql_query($strGetUserLIstSQL) or die($strGetUserLIstSQL." : ".mysql_error());
	
	$commentsList = array();					  
	while($rowUser = mysql_fetch_assoc($resUserList))
	{ 
		$atRate = getAtRateUsers($rowUser['bud_thought']);
		$username =getUserName($rowUser['user_id']);
		$commentsList[]=array(
								'bud_thought'=> base64_encode(stripslashes($rowUser['bud_thought'])),
								'user_name'=>  $username!=''?$username:'',
								'user_id'=>$rowUser['user_id'],
								'atRateUsersList'=>$atRate
							  );
	}
	
	return $commentsList;
						  
}



function getBudthoughtOwner($budthoughtid)
{
	$strGetOwnerNameSQL ="SELECT user_id FROM tbl_bud_thought WHERE 
						  bud_thought_id='".$budthoughtid."'";
	$resOwnerId = mysql_query($strGetOwnerNameSQL) or die($strGetOwnerNameSQL." : ".mysql_error());
	
	$OwneridArray = mysql_fetch_assoc($resOwnerId);
	
	return $OwneridArray['user_id'];
	
}


function getStrainOwner($strainId)
{
	$strGetOwnerNameSQL ="SELECT user_id FROM strains WHERE 
						  strain_id='".$strainId."'";
	$resOwnerId = mysql_query($strGetOwnerNameSQL) or die($strGetOwnerNameSQL." : ".mysql_error());
	
	$OwneridArray = mysql_fetch_assoc($resOwnerId);
	
	return $OwneridArray['user_id'];
	
}



function getDeviceType($userId)
{
	$strGetDeviceTYpeSQL ="SELECT device_type FROM users WHERE user_id='".$userId."'";
	$resDeviceType = mysql_query($strGetDeviceTYpeSQL) or die($strGetDeviceTYpeSQL." : ".mysql_error());
	$res =mysql_fetch_assoc($resDeviceType);
	return $res['device_type'];
}


?>