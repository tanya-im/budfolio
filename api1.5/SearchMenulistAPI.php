<?php
/*
	File Description - Search Menu List
	File Name - SearchMenulistAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		$this->response('Invalid request',406);
	}
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	$strSearchtext = $inputArray['Search'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];	
	$intLat = $inputArray['latitude'];
	$intLong =$inputArray['longitude'];
	// Input validations
	if(!empty($strSearchtext) && !empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		$strValidSearchtext = mysql_real_escape_string($strSearchtext);
		
		$strDataInRangeSQL = "SELECT DISTINCT (
							dispensary_id
							), dispensary_name, city, state, zip, image, phone_number, added_by,latitude,longitude,bio,street_address
							FROM (
							
							SELECT dispensary_id, dispensary_name, city, state, zip, image, phone_number, added_by,latitude,longitude,bio,street_address
							FROM dispensaries
							WHERE flag = 'Active'
							AND dispensary_name LIKE '".$strValidSearchtext."%'
							UNION
							SELECT dispensary_id, dispensary_name, city, state, zip, image, phone_number, added_by,latitude,longitude,bio,street_address
							FROM dispensaries
							WHERE flag = 'Active'
							AND city = '".$strValidSearchtext."' || state = '".$strValidSearchtext."' || zip = '".$strValidSearchtext."'
							) AS temp_table";
		
			$SearchResultNoSql = mysql_query($strDataInRangeSQL)or die($strDataInRangeSQL." : ".mysql_error());
			
			$SearchData = mysql_num_rows($SearchResultNoSql);
			$totalPages = ceil($SearchData / $intPageSize);
		
			$perpage = $intPageSize;
			$page = $intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;
			
			$strDataInRangeSQL.=" Limit $start, $perpage";
			$SearchResultNoSql = mysql_query($strDataInRangeSQL)or die($strDataInRangeSQL." : ".mysql_error());
			$arraySearchMenuArray = array();
			if(mysql_num_rows($SearchResultNoSql) > 0)
			{
				while($MenuList=mysql_fetch_array($SearchResultNoSql))
				{
					
					
					$distance = ceil(get_distance($intLat,$intLong,$MenuList['latitude'],$MenuList['longitude'],'M'));
						
					$settingArray= array();
					if($MenuList['added_by']!=0){
					$getUserSettings ="SELECT * FROM dispensary_settings 
									   WHERE dispensary_id=
									   '".$MenuList['dispensary_id']."'";
				
				    $getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());
	  
				    $settingData = mysql_fetch_assoc($getSettingsResponse);
				    $settingArray=array(
'DeliveryService'=>$settingData['DeliveryService']!=''?$settingData['DeliveryService']:'0',
'StoreFront'=>$settingData['StoreFront']!=''?$settingData['StoreFront']:'0',
'AcceptCreditCard'=>$settingData['AcceptCreditCard']!=''?$settingData['AcceptCreditCard']:'0',
'AcceptATMonSite'=>$settingData['AcceptATMonSite']!=''?$settingData['AcceptATMonSite']:'0',
'18YearsOld'=>$settingData['18YearsOld']!=''?$settingData['18YearsOld']:'0',
'21YearsOld'=>$settingData['21YearsOld']!=''?$settingData['21YearsOld']:'0',
'HandicapAssesseble'=>$settingData['HandicapAssesseble']!=''?$settingData['HandicapAssesseble']:'0',
'Security'=>$settingData['Security']!=''?$settingData['Security']:'0'	
										  );
					
					}else
					{
						$settingArray=array(
											  'DeliveryService'=>'0',
											  'StoreFront'=>'0',
											  'AcceptCreditCard'=>'0',
											  'AcceptATMonSite'=>'0',
											  '18YearsOld'=>'0',
											  '21YearsOld'=>'0',
											  'HandicapAssesseble'=>'0',
											  'Security'=>'0'
										  );
					}


						//Set image url
						if(!empty($MenuList['image']))
						{
			$MenuImageUrl=BASEURL.'images/dispensary_images/original/'.$MenuList['image'];
			$MenuThumbImageUrl=BASEURL.'images/dispensary_images/thumbnail/'.$MenuList['image'];
						}
						else
						{
							$MenuImageUrl='';
							$MenuThumbImageUrl='';
						}
						
						// Fet followup flag
						$FWPflag=getFollowUpList($MenuList['dispensary_id'],$intValidUserId);
						
						
						$atRateUsersArray = getAtRateUsers($MenuList['bio']);
						
						$arraySearchMenuArray[] =  array(
						   'DispensaryId'=> $MenuList['dispensary_id'],
						   'DispensaryName'=> base64_encode(str_replace("\\", "",$MenuList['dispensary_name'])), 
						   'City'=> base64_encode(str_replace("\\", "",$MenuList['city'])),
						   'State'=> base64_encode(str_replace("\\", "",$MenuList['state'])),
						   'ZipCode'=> $MenuList['zip'],
						   'flag'=>$FWPflag,
						   'DispensaryImageUrl'=> $MenuImageUrl,
						   'DispensaryThumbImageUrl'=>$MenuThumbImageUrl,
						   'PhoneNumber'=>$MenuList['phone_number'],
						   'settings'=>$settingArray,
						   'distance'=>$distance,
						   'lat'=>$MenuList['latitude'],
						   'long'=>$MenuList['longitude'],
						   'StreetAddress'=>base64_encode(str_replace("\\", "",$MenuList['street_address'])),
						   'Bio'=>base64_encode(str_replace("\\", "",$MenuList['bio'])),
						   'atRateUsersList'=>$atRateUsersArray
													  );// end of array
					}
					// If success everythig is good send header as "OK" and MenuDispensaryList list in reponse
					$result = array(
								'success' => '1',
								'TotalPageNo'=>$totalPages,
								'MenuDispensaryList' => $arraySearchMenuArray
								);
					$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
					// If Empty Menu List
					$error = array('success' => "0", "msg" => "No match found with this criteria");
					$this->response($this->toJson($error), 400,"application/json");
			}
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or Search Text or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>