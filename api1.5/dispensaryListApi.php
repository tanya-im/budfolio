<?php
/*
	File Description - dISPENSRY LISTING
	File Name - dispensaryListAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}

	//Getting the request JSON string
	$dataJson = file_get_contents("php://input");//getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	 $intUnixTime = $inputArray['dispensary_unix_time'];
	
	//SQL query to get Strain library
	$strDispensaryListSQL = "SELECT dispensary_name, dispensary_id,street_address,zip,
							 flag,city,UNIX_TIMESTAMP(date_time) as unixTime
							 FROM dispensaries 
							 WHERE UNIX_TIMESTAMP(date_time) > '".$intUnixTime."'
							 GROUP BY dispensary_name
							 ORDER BY dispensary_name";
		
	$DispensarySQL = mysql_query($strDispensaryListSQL) or die($strDispensaryListSQL." : ".mysql_error());
		
		$DisArray=array();
		
		$unix_time =0;
		// max unix time 
		$strDispensaryMaxTimeSQL = "SELECT 
									max( UNIX_TIMESTAMP( date_time ) ) 			                                                AS unixTime
									FROM dispensaries";
		
		$DispensaryMaxSQL = mysql_query($strDispensaryMaxTimeSQL) or die($strDispensarySQL." : ".mysql_error());
				
				$maxtime= mysql_fetch_assoc($DispensaryMaxSQL);
				$unix_time = $maxtime['unixTime'];
		// max unix time end 
		
		if(mysql_num_rows($DispensarySQL) > 0)
		{
			while($disList = mysql_fetch_array($DispensarySQL))
			{
				
				if($disList['flag']=='deactive')
				{
					$status='D';
				}
				else
				{
					$status='N';
				}
				
				
				
				$DisArray[] =  array(
		'DispensaryId'=> trim($disList['dispensary_id']),
		'DispensaryName'=> str_replace("\\", "",$disList['dispensary_name']),
		"ZipCode"=>$disList['zip'],
		"StreetAddress"=>str_replace("\\", "",$disList['street_address']),
		"City"=>str_replace("\\", "",$disList['city']),
		'status'=>$status
					);// end of array
			}
			// If success everythig is good send header as "OK" and sending Dispensary details in reponse
			$result = array('success' => '1',
							'Dispensary' => $DisArray,
							'dispensary_unix_time'=> $unix_time
				);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => '1',
						   'Dispensary' => array(),
						   'dispensary_unix_time'=> $unix_time);
			$this->response($this->toJson($error), 400,"application/json");
		}	

?>