<?php
/*
	File Description - Delete Strain
	File Name - removestrainAPI.php
*/

	// Cross validation if the request method is POST 
	//else it will return "Invalid request" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intStrainId = $inputArray['StrainId'];
	$intUserId = $inputArray['UserId'];
	
	// Input validations
	if(!empty($intStrainId) && !empty($intUserId))
	{
		$intValidintStrainId = mysql_real_escape_string($intStrainId);
		$intValidUserId = mysql_real_escape_string($intUserId);
		
		//SQL query to get Strain detail
		$strStrainDetailQuery = "SELECT * FROM strains 
								 WHERE strain_id ='".$intValidintStrainId."'
								 and user_id='".$intValidUserId."' Limit 0,1";
		$StrainDetailSQL = mysql_query($strStrainDetailQuery) or die(mysql_error());//executing strStrainDetailQuery
		if(mysql_num_rows($StrainDetailSQL) > 0)
		{
			$RowStrain= mysql_fetch_assoc($StrainDetailSQL);
			
			
			//deleite all the likes on the strain 
			$deleteStrainLikeSQL ="DELETE FROM tbl_strain_like 
								   WHERE strain_id='".$intValidintStrainId."'";
			$resStrainLike = mysql_query($deleteStrainLikeSQL) or die($deleteStrainLikeSQL." : ".mysql_error());
	
	
	// delete all the activity done on ste strain thta have to delete  
	$deleteActivitySQL ="UPDATE tbl_push_notification 
						 SET 
						 delete_status='1'		
	 					 WHERE 
						 	(type=1) 
						 	AND type_id='".$intValidintStrainId."'";
	$resActivitySQL = mysql_query($deleteActivitySQL) or die($deleteActivitySQL." : ".mysql_error());
	
	
			
			
			$strBlockStrainQuery = "UPDATE strains 
									SET flag='deactive' 
									WHERE strain_id ='".$intValidintStrainId."'";
			$strBlockStrainSql= mysql_query($strBlockStrainQuery)or(die(mysql_error()));
			// If success everythig is good send header as "OK" and user details
			// sending StrainId in reponse
			if($strBlockStrainSql)
			{
			$date = date('Y-m-d H:i:s');	
			//update time of strain in strain liberary when strain is from strain liberary
			$getStrainLIbListSQL ="SELECT strain_lib_id,strain_name 
							       FROM strain_library";
			$resStrainLibrary= mysql_query($getStrainLIbListSQL);
			while($rowSL=mysql_fetch_assoc($resStrainLibrary))
			{
				if($rowSL['strain_name']==$RowStrain['strain_name'])
				{
					// update time of strain liberary
					$updateTimeSQL ="UPDATE strain_library
									SET date_time='".$date."'
									WHERE strain_lib_id='".$rowSL['strain_lib_id']."'";
					$resUpdateTimeSQL = mysql_query($updateTimeSQL);				
					break;				
				}
			}
				
				
				
				
				
				
				
				
				$result = array(
								'success' => '1',
								'StrainId' =>$intValidintStrainId,
								"msg"=>"Strain Block");
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}
			else
			{
				// If Strain not block
				$error = array('success' => "0", "msg" => "Strain not delete. Please try again.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid StrainId
			$error = array('success' => "0", "msg" => "Invalid StrainId.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "StrainId or UserId empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>