<?php
/* 
	File Description - file to add strain images
	File Name - addstrainimagesAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	
	//Taking all values into local variable		
	$intStrainId= $_REQUEST['StrainId'];
	// Sanitize the values 	
	$intValidStrainId = mysql_real_escape_string($intStrainId);
	
	if(empty($intStrainId))
	{
		$error = array('success' => "0", "msg" => "Empty strain id.");
		$this->response($this->toJson($error), 406, "application/json");
	}
	
	
	// Check strain id is valid or not.	
	$strSQLChkStrainId ="SELECT strain_name 
						 FROM strains WHERE strain_id='".$intStrainId."' 
						 and flag='active'";
						
	$SQLResChkStrainId = mysql_query($strSQLChkStrainId);// execution of query intValidStrainId
	if(mysql_num_rows($SQLResChkStrainId)< 1)
	{
		$error = array('success' => "0", "msg" => "Invalid strain id.");
		$this->response($this->toJson($error), 406, "application/json");
	}
	
	//for image one
	$img_1_status = $_REQUEST['StrainsImageStatus_1'];// 0-add,1-update,2-delete,3 - no change
	$old_img_url_1= $_REQUEST['StrainsImageOldUrl_1'];
	
	
	//for image two
	$img_2_status = $_REQUEST['StrainsImageStatus_2'];// 0-add,1-update,2-delete,3 - no change
	$old_img_url_2= $_REQUEST['StrainsImageOldUrl_2'];
	
	
	//for image two
	$img_3_status = $_REQUEST['StrainsImageStatus_3'];// 0-add,1-update,2-delete,3 - no change
	$old_img_url_3= $_REQUEST['StrainsImageOldUrl_3'];
	
	
	switch($img_1_status)
	{
		
		case 3:// no change
			
			//extracting old imgaename only
			$oldUrlImg = explode('/',$old_img_url_1);
			$len = count($oldUrlImg);
			$checkExistSQL ="SELECT * FROM strainimages 
							 WHERE strain_id='".$intStrainId."'
							 AND image_name ='".$oldUrlImg[$len-1]."' LIMIT 0,1";		
			$strRES= mysql_query($checkExistSQL);
		
			if(mysql_num_rows($strRES)>0){
				$ImageStatus[1]['msg']="No change";
			}else
			{
				$strReinsertSQL = "insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$oldUrlImg[$len-1]."',
												 '".$intStrainId."',
												 '0')";
				$strResReInsert = mysql_query($strReinsertSQL) or die($strReinsertSQL." : ".mysql_error());		$ImageStatus[1]['msg']="No img change";
			}
			break;
		
		case 2:// delete
			
			//extracting old imgaename only
			$oldUrlImg = explode('/',$old_img_url_1);
			$len = count($oldUrlImg);
			
			//echo $oldUrlImg[$len-1]." Image one delete <br>";
			$deleteOldImgSQL = "DELETE FROM strainimages 
							    WHERE strain_id='".$intStrainId."'
							    AND image_name ='".$oldUrlImg[$len-1]."'";
		   
		   $resDeleteOldImgSQL	= mysql_query($deleteOldImgSQL) or die($deleteOldImgSQL." : ".mysql_error());
					   
		   $ImageStatus[1]['msg']="Image deleted successfully."; 
			
		   break;		
		   
		   
	    case 1:// edit
		
			//extracting old imgaename only
			$oldUrlImg = explode('/',$old_img_url_1);
			$len = count($oldUrlImg);
			
			if(isset($_FILES['StrainsImageData_1']))
			{
				$name = $_FILES['StrainsImageData_1']['name'];
				$size = $_FILES['StrainsImageData_1']['size'];
				
					list($txt, $ext) = explode(".", $name);
					$extention = explode(".", $name);
					$arrayconunt=(count($extention)-1);
					if(!empty($extention[$arrayconunt]))
					{
						$ext=$extention[$arrayconunt];
					}
			
					//fetch the dimensions of image 
					//with getimagesize method
					list($width,$height,$type,$attr)=getimagesize($_FILES['StrainsImageData_1']['tmp_name']);
					
					$upload_path ='../images/uploads/original/';	
					if($width >1000)
					{
						$upload_path ='../images/uploads/xlarge/';
							$newHeight= 480*($height/$width);
					}else
					{
						//upload path wil reamin 
						//$upload_path ='../images/uploads/original/';	
						//and height wil reamin as it is
					}
			
					$actual_image_name = time().rand()."_".$intStrainId.".".$ext;
					$tmp = $_FILES['StrainsImageData_1']['tmp_name'];
					if(move_uploaded_file($tmp,$upload_path.$actual_image_name))
					{
						$deleteOldImgQuery="DELETE FROM strainimages
											WHERE strain_id='".$intStrainId."'
											AND image_name='".$oldUrlImg[$len-1]."'
											";
							
						$SQLAddImages=mysql_query($deleteOldImgQuery)or die($deleteOldImgQuery." : ".mysql_error());

						$strImgSQL ="insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$actual_image_name."',
												 '".$intStrainId."',
												 '0')";
						
						$resStrImg = mysql_query($strImgSQL);
						
						$aftrUploadingPath = $upload_path.$actual_image_name;
						
						$src_o = '../images/uploads/original/'.$actual_image_name;
						$dest_m = '../images/uploads/medium/'.$actual_image_name;
						$dest_t = '../images/uploads/thumbnail/'.$actual_image_name;						
						
						resize_image($aftrUploadingPath,$src_o,480,$newHeight);
						resize_image($aftrUploadingPath,$dest_m,300,300);
						resize_image($aftrUploadingPath,$dest_t,115,115);
						$ImageStatus[1]['msg']="Image one updated successfully."; 
				   }
				    else	
				    { 
						$ImageStatus[1]['msg']="Image one upload failed. Please try agian."; 
				    }
					
		   }
		    else
		    {
				$ImageStatus[1]['msg']="Please select image one to update."; 
		    }		
		
			break;
			
	   case 0: 
		
			if(isset($_FILES['StrainsImageData_1']))
			{
				$name = $_FILES['StrainsImageData_1']['name'];
				$size = $_FILES['StrainsImageData_1']['size'];
			
				list($txt, $ext) = explode(".", $name);
				$extention = explode(".", $name);
				$arrayconunt=(count($extention)-1);
				if(!empty($extention[$arrayconunt]))
				{
					$ext=$extention[$arrayconunt];
				}
				
					//fetch the dimensions of image 
					//with getimagesize method
					list($width,$height,$type,$attr)=getimagesize($_FILES['StrainsImageData_1']['tmp_name']);
					
					
					$upload_path ='../images/uploads/original/';	
					if($width >1000)
					{
						$upload_path ='../images/uploads/xlarge/';
							$newHeightforOrignal= 480*($height/$width);
							$newHeightforMedium= 300*($height/$width);
					}else
					{
						//upload path wil reamin 
						//$upload_path ='../images/uploads/original/';	
						//and height wil reamin as it is
					}
				
					 $actual_image_name = time().rand()."_".$intStrainId.".".$ext;
					 $tmp = $_FILES['StrainsImageData_1']['tmp_name'];
					 if(move_uploaded_file($tmp,$upload_path.$actual_image_name))
					 {
							
							$strAddImagesQuery="insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$actual_image_name."',
												 '".$intStrainId."',
												 '0')";
						
							$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());
							$ImageId = mysql_insert_id();
							
					if($width >1000)
					{
						$src_o = '../images/uploads/original/'.$actual_image_name;
					}
					$dest_m = '../images/uploads/medium/'.$actual_image_name;
					$dest_t = '../images/uploads/thumbnail/'.$actual_image_name;						
					
					$aftrUploadingPath=$upload_path.$actual_image_name;
					
						
					resize_image($aftrUploadingPath,$src_o,480,$newHeightforOrignal);
					resize_image($aftrUploadingPath,$dest_m,300,$newHeightforMedium);
					resize_image($aftrUploadingPath,$dest_t,115,115);
					$ImageStatus[1]['msg']="Image one upload successfully."; 
					$flag=true; 
						}
					 else	
					 {
							$ImageStatus[1]['msg']="Image one upload failed. Please try agian."; 
					 }
			}
			 else
		    {
				$ImageStatus[1]['msg']="Please select image one to add."; 
		    }		
		break;
		
		default:
				$ImageStatus[1]['msg']='in default case';
	}//end of switch case
	
	
	

	switch($img_2_status)
	{
		
		case 3:// no change
		
			//extracting old imgaename only
			$oldUrlImg = explode('/',$old_img_url_2);
			$len = count($oldUrlImg);
			$checkExistSQL ="SELECT * FROM strainimages 
							 WHERE strain_id='".$intStrainId."'
							 AND image_name ='".$oldUrlImg[$len-1]."' LIMIT 0,1";		
			$strRES= mysql_query($checkExistSQL);
			
			if(mysql_num_rows($strRES)>0){
				$ImageStatus[2]['msg']="No change";
			}else
			{
				$strReinsertSQL = "insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$oldUrlImg[$len-1]."',
												 '".$intStrainId."',
												 '0')";
				$strResReInsert = mysql_query($strReinsertSQL) or die($strReinsertSQL." : ".mysql_error());											 				$ImageStatus[2]['msg']="No img change";
			}
			break;
		
		
		case 2:// delete
			
			//extracting old imgaename only
			$oldUrlImg2 = explode('/',$old_img_url_2);
			$len = count($oldUrlImg2);
			
			$deleteOldImgSQL = "DELETE FROM strainimages 
							    WHERE strain_id='".$intStrainId."'
							    AND image_name ='".$oldUrlImg2[$len-1]."'";
		  	$resDeleteOldImgSQL	= mysql_query($deleteOldImgSQL) or die($deleteOldImgSQL." : ".mysql_error());
					   
		   $ImageStatus[2]['msg']="Image deleted successfully."; 
			
		   break;		
		   
	    case 1:// edit
		
			//extracting old imgaename only
			$oldUrlImg = explode('/',$old_img_url_2);
			$len = count($oldUrlImg);
			
			if(isset($_FILES['StrainsImageData_2']))
			{
				$name = $_FILES['StrainsImageData_2']['name'];
				$size = $_FILES['StrainsImageData_2']['size'];
				
					list($txt, $ext) = explode(".", $name);
					$extention = explode(".", $name);
					$arrayconunt=(count($extention)-1);
					if(!empty($extention[$arrayconunt]))
					{
						$ext=$extention[$arrayconunt];
					}
					
					
					//fetch the dimensions of image 
					//with getimagesize method
					list($width,$height,$type,$attr)=getimagesize($_FILES['StrainsImageData_2']['tmp_name']);
					
					$upload_path ='../images/uploads/original/';	
					if($width >1000)
					{
						$upload_path ='../images/uploads/xlarge/';
						$newHeightforOrignal= 480*($height/$width);
						$newHeightforMedium= 300*($height/$width);
					}else
					{
						//upload path wil reamin 
						//$upload_path ='../images/uploads/original/';	
						//and height wil reamin as it is
					}
				
					$actual_image_name = time().rand()."_".$intStrainId.".".$ext;
					$tmp = $_FILES['StrainsImageData_2']['tmp_name'];
					if(move_uploaded_file($tmp,$upload_path.$actual_image_name))
					{
						
						$deleteOldImgQuery="DELETE FROM strainimages
											WHERE strain_id='".$intStrainId."'
											AND image_name='".$oldUrlImg[$len-1]."'
											";
						
						$SQLAddImages=mysql_query($deleteOldImgQuery)or die($deleteOldImgQuery." : ".mysql_error());

						$strImgSQL ="insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$actual_image_name."',
												 '".$intStrainId."',
												 '0')";
												
						$resStrImg = mysql_query($strImgSQL);

					if($width >1000)
					{
					   $src_o = '../images/uploads/original/'.$actual_image_name;
					}
					$dest_m = '../images/uploads/medium/'.$actual_image_name;
					$dest_t = '../images/uploads/thumbnail/'.$actual_image_name;						
					
					$aftrUploadingPath=$upload_path.$actual_image_name;
					
						
				resize_image($aftrUploadingPath,$src_o,480,$newHeightforOrignal);
				resize_image($aftrUploadingPath,$dest_m,300,$newHeightforMedium);
				resize_image($aftrUploadingPath,$dest_t,115,115);
						$ImageStatus[2]['msg']="Image two updated successfully."; 
				   }
				    else	
				    { 
						$ImageStatus[2]['msg']="Image two upload failed. Please try agian."; 
				    }
					
		   }
		    else
		    {
				$ImageStatus[2]['msg']="Please select image two to update."; 
		    }		
		
			break;
			
	   case 0: 
		
			if(isset($_FILES['StrainsImageData_2']))
			{
				$name = $_FILES['StrainsImageData_2']['name'];
				$size = $_FILES['StrainsImageData_2']['size'];
			
				list($txt, $ext) = explode(".", $name);
				$extention = explode(".", $name);
				$arrayconunt=(count($extention)-1);
				if(!empty($extention[$arrayconunt]))
				{
					$ext=$extention[$arrayconunt];
				}
				
				//fetch the dimensions of image 
				//with getimagesize method
				list($width,$height,$type,$attr)=getimagesize($_FILES['StrainsImageData_2']['tmp_name']);
					
					$upload_path ='../images/uploads/original/';	
					if($width >1000)
					{
						$upload_path ='../images/uploads/xlarge/';
						$newHeightforOrignal= 480*($height/$width);
						$newHeightforMedium= 300*($height/$width);
					}else
					{
						//upload path wil reamin 
						//$upload_path ='../images/uploads/original/';	
						//and height wil reamin as it is
					}
				
					 $actual_image_name = time().rand()."_".$intStrainId.".".$ext;
					 $tmp = $_FILES['StrainsImageData_2']['tmp_name'];
					 if(move_uploaded_file($tmp,$upload_path.$actual_image_name))
					 {
							$strAddImagesQuery="insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$actual_image_name."',
												 '".$intStrainId."',
												 '0')";
							//echo $strAddImagesQuery."<br>";
							$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());

							$ImageId = mysql_insert_id();
							
					if($width >1000)
					{
					   $src_o = '../images/uploads/original/'.$actual_image_name;
					}
					$dest_m = '../images/uploads/medium/'.$actual_image_name;
					$dest_t = '../images/uploads/thumbnail/'.$actual_image_name;						
					
					$aftrUploadingPath=$upload_path.$actual_image_name;
					
						
				resize_image($aftrUploadingPath,$src_o,480,$newHeightforOrignal);
				resize_image($aftrUploadingPath,$dest_m,300,$newHeightforMedium);
				resize_image($aftrUploadingPath,$dest_t,115,115);
							$ImageStatus[2]['msg']="Image two upload successfully."; 
							$flag=true; 
						}
					 else	
					 {
							$ImageStatus[2]['msg']="Image two upload failed. Please try agian."; 
					 }
			}
			 else
		    {
				$ImageStatus[2]['msg']="Please select image two to add."; 
		    }		
		break;
		
		default:
				$ImageStatus[2]['msg']='in default case';
	}//end of switch case
	
	
	
	
	

	switch($img_3_status)
	{
		case 3:// no change
	
			//extracting old imgaename only
			$oldUrlImg3 = explode('/',$old_img_url_3);
			$len = count($oldUrlImg3);
			$checkExistSQL ="SELECT * FROM strainimages 
							 WHERE strain_id='".$intStrainId."'
							 AND image_name ='".$oldUrlImg3[$len-1]."'";		
			$strRES= mysql_query($checkExistSQL);
			
			if(mysql_num_rows($strRES)>0){
				$ImageStatus[1]['msg']="No change";
			}else
			{
				$strReinsertSQL = "insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$oldUrlImg3[$len-1]."',
												 '".$intStrainId."',
												 '0')";
				$strResReInsert = mysql_query($strReinsertSQL) or die($strReinsertSQL." : ".mysql_error());											 				$ImageStatus[3]['msg']="No img change";
			}
			break;
		
		
		case 2:// delete
		
			//extracting old imgaename only
			$oldUrlImg3 = explode('/',$old_img_url_3);
			$len = count($oldUrlImg3);
			
			//echo $oldUrlImg[$len-1]." Image one delete <br>";
			$deleteOldImgSQL = "DELETE FROM strainimages 
							    WHERE strain_id='".$intStrainId."'
							    AND image_name ='".$oldUrlImg3[$len-1]."'";
		   
		   $resDeleteOldImgSQL	= mysql_query($deleteOldImgSQL) or die($deleteOldImgSQL." : ".mysql_error());
					   
		   $ImageStatus[3]['msg']="Image deleted successfully."; 
			
		   break;		
		   
	    case 1:// edit
		
			//extracting old imgaename only
			$oldUrlImg3 = explode('/',$old_img_url_3);
			$len = count($oldUrlImg3);
			
			if(isset($_FILES['StrainsImageData_3']))
			{
				$name = $_FILES['StrainsImageData_3']['name'];
				$size = $_FILES['StrainsImageData_3']['size'];
				
					list($txt, $ext) = explode(".", $name);
					$extention = explode(".", $name);
					$arrayconunt=(count($extention)-1);
					if(!empty($extention[$arrayconunt]))
					{
						$ext=$extention[$arrayconunt];
					}
					
					//fetch the dimensions of image 
					//with getimagesize method
					list($width,$height,$type,$attr)=getimagesize($_FILES['StrainsImageData_3']['tmp_name']);
					
					
					$upload_path ='../images/uploads/original/';	
					if($width >1000)
					{
						$upload_path ='../images/uploads/xlarge/';
							$newHeightforOrignal= 480*($height/$width);
							$newHeightforMedium= 300*($height/$width);
					}else
					{
						//upload path wil reamin 
						//$upload_path ='../images/uploads/original/';	
						//and height wil reamin as it is
					}
					
					$actual_image_name = time().rand()."_".$intStrainId.".".$ext;
					$tmp = $_FILES['StrainsImageData_3']['tmp_name'];
					if(move_uploaded_file($tmp,$upload_path.$actual_image_name))
					{
						
						$deleteOldImgQuery="DELETE FROM strainimages
											WHERE strain_id='".$intStrainId."'
											AND image_name='".$oldUrlImg3[$len-1]."'
											";
						
						$SQLAddImages=mysql_query($deleteOldImgQuery)or die($deleteOldImgQuery." : ".mysql_error());

						$strImgSQL ="insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$actual_image_name."',
												 '".$intStrainId."',
												 '0')";
												 echo $strImgSQL."<br>";
						$resStrImg = mysql_query($strImgSQL);


					if($width >1000)
					{
						$src_o = '../images/uploads/original/'.$actual_image_name;
					}
					$dest_m = '../images/uploads/medium/'.$actual_image_name;
					$dest_t = '../images/uploads/thumbnail/'.$actual_image_name;						
					
					$aftrUploadingPath=$upload_path.$actual_image_name;
					
						
					resize_image($aftrUploadingPath,$src_o,480,$newHeightforOrignal);
					resize_image($aftrUploadingPath,$dest_m,300,$newHeightforMedium);
					resize_image($aftrUploadingPath,$dest_t,115,115);
						$ImageStatus[3]['msg']="Image one updated successfully."; 
				   }
				    else	
				    { 
						$ImageStatus[3]['msg']="Image one upload failed. Please try agian."; 
				    }
					
		   }
		    else
		    {
				$ImageStatus[3]['msg']="Please select image one to update."; 
		    }		
		
			break;
			
	   case 0: 
			
			if(isset($_FILES['StrainsImageData_3']))
			{
				$name = $_FILES['StrainsImageData_3']['name'];
				$size = $_FILES['StrainsImageData_3']['size'];
			
				list($txt, $ext) = explode(".", $name);
				$extention = explode(".", $name);
				$arrayconunt=(count($extention)-1);
				if(!empty($extention[$arrayconunt]))
				{
					$ext=$extention[$arrayconunt];
				}
				
				
				//fetch the dimensions of image 
					//with getimagesize method
					list($width,$height,$type,$attr)=getimagesize($_FILES['StrainsImageData_1']['tmp_name']);
					
					
					$upload_path ='../images/uploads/original/';	
					if($width >1000)
					{
						$upload_path ='../images/uploads/xlarge/';
							$newHeightforOrignal= 480*($height/$width);
							$newHeightforMedium= 300*($height/$width);
					}else
					{
						//upload path wil reamin 
						//$upload_path ='../images/uploads/original/';	
						//and height wil reamin as it is
					}
				
				
					 $actual_image_name = time().rand()."_".$intStrainId.".".$ext;
					 $tmp = $_FILES['StrainsImageData_3']['tmp_name'];
					 if(move_uploaded_file($tmp,$upload_path.$actual_image_name))
					 {
							
							$strAddImagesQuery="insert into strainimages
												(image_name,
												 strain_id,
												 primary_image) 
												 values
												 (
												 '".$actual_image_name."',
												 '".$intStrainId."',
												 '0')";
						
							$SQLAddImages=mysql_query($strAddImagesQuery)or die(mysql_error());

						


							$ImageId = mysql_insert_id();
							if($width >1000)
					{
						$src_o = '../images/uploads/original/'.$actual_image_name;
					}
					$dest_m = '../images/uploads/medium/'.$actual_image_name;
					$dest_t = '../images/uploads/thumbnail/'.$actual_image_name;						
					
					$aftrUploadingPath=$upload_path.$actual_image_name;
					
						
					resize_image($aftrUploadingPath,$src_o,480,$newHeightforOrignal);
					resize_image($aftrUploadingPath,$dest_m,300,$newHeightforMedium);
					resize_image($aftrUploadingPath,$dest_t,115,115);
							$ImageStatus[3]['msg']="Image one upload successfully."; 
							$flag=true; 
						}
					 else	
					 {
							$ImageStatus[3]['msg']="Image one upload failed. Please try agian."; 
					 }
			}
			 else
		    {
				$ImageStatus[3]['msg']="Please select image one to add."; 
		    }		
		break;
		
		default:
				$ImageStatus[3]['msg']='in default case';
	}//end of switch 3
	
	$strGetImagePathSQL = "SELECT image_name FROM strainimages 
						   WHERE strain_id='".$intStrainId."'
						   ORDER BY image_id desc limit 0,1";

	$resImagePath = mysql_query($strGetImagePathSQL) or die($strGetImagePathSQL." : ".mysql_error());
	
	$resImage = mysql_fetch_assoc($resImagePath);

	$image = BASEURL."images/uploads/thumbnail/".$resImage['image_name'];


		$result = array( "success" => "1", 'ImageStatus' => $ImageStatus,'Image'=>$image);
		$this->response($this->toJson($result),200,"application/json");
	

	?>