<?php
/*
	File Description - file to add a strain
	File Name - addstrainAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	
//Taking all values into local variable	
	$intUserId = $inputArray['UserId'];
 	$strStrainName = $inputArray['StrainName'];		
	$strDispensaryName = $inputArray['DispensaryName'];
	$strSpecies	= $inputArray['Species'];
	$strLinage	= $inputArray['Linage'];
	$strProfileType =$inputArray['profileType'];
	$strSmellRating = $inputArray['SmellRating'];
	$strTasteRate	= $inputArray['TasteRate'];
	$strStrengthRate = $inputArray['StrengthRate'];
	$strOverallRating	= $inputArray['OverallRating'];
	$strExperienceArray	= $inputArray['ExperienceArray'];
	$strConsumption	= $inputArray['Consumption'];
	$strMedicinalUseArray	= $inputArray['MedicinalUseArray'];
	$strTestedBy	= $inputArray['TestedBy'];
	$strTHC	= $inputArray['THC'];
	$strCBD	= $inputArray['CBD'];
	$strCBN	= $inputArray['CBN'];
	$strTHCa	= $inputArray['THCa'];
	$strCBHa	= $inputArray['CBHa'];
	$strMoisture	= $inputArray['Moisture'];
	$strDescription	= $inputArray['Description'];
	$intDispensaryId = $inputArray['DispensaryId'];
	 
	
// Input validations
	if(!empty($intUserId) && !empty($strStrainName))
	{
// sanitization of values to insert in db
		$strValidStrainName = mysql_real_escape_string($strStrainName);
		$strValidDispensaryName = mysql_real_escape_string($strDispensaryName);
		$strValidSpecies	= mysql_real_escape_string($strSpecies);
		$strValidLinage	=mysql_real_escape_string($strLinage);
		$strValidSmellRating = mysql_real_escape_string($strSmellRating);
		$strValidTasteRate	= mysql_real_escape_string($strTasteRate);
		$strValidStrengthRate = mysql_real_escape_string($strStrengthRate);
		$strValidOverallRating	= mysql_real_escape_string($strOverallRating);
		$strValidExperienceArray	=$strExperienceArray;
		$strValidConsumption	= mysql_real_escape_string($strConsumption);
		$strValidMedicinalUseArray	= $strMedicinalUseArray;
		$strValidTestedBy	= mysql_real_escape_string($strTestedBy);
		$strValidTHC	= mysql_real_escape_string($strTHC);
		$strValidCBD	= mysql_real_escape_string($strCBD);
		$strValidCBN	= mysql_real_escape_string($strCBN);
		$strValidTHCa	= mysql_real_escape_string($strTHCa);
		$strValidCBHa	= mysql_real_escape_string($strCBHa);
		$strValidMoisture	= mysql_real_escape_string($strMoisture);
		$strValidDescription	= mysql_real_escape_string($strDescription);
		
		// to check whether the user id is exist.
		$uservalidation=CheckValidUser($intValidUserId);
		if($uservalidation)
		{
			$error = array('success' => "0", "msg" => "Invalid user id.");
			$this->response($this->toJson($error), 406, "application/json");
		}
		$date = date('Y-m-d H:i:s');
		
		// Sql Insert all the values in strains table. 		
		$strAddStrainQuery ="insert into strains
							(user_id,
							strain_name,
							dispensary_name,
							species,
							linage, 
							profile_type,
							smell_rating, 
							taste_rate, 
							strength_rate,
							overall_rating,
							consumption_name,
							tested_by,
							thc,
							cbd,
							cbn,
							thca,
							cbha,
							moisture,
							description,
							dispensary_id,
							post_date)
					values('".$intUserId."',
					'".$strValidStrainName."',
					'".$strValidDispensaryName."',
					'".$strValidSpecies."',
					'".$strValidLinage."',
					'".$strProfileType."',
					'".$strValidSmellRating."',
					'".$strValidTasteRate."',
					'".$strValidStrengthRate."',
					'".$strValidOverallRating."',
					'".$strValidConsumption."',
					'".$strValidTestedBy."',
					'".$strValidTHC."',
					'".$strValidCBD."',
					'".$strValidCBN."',
					'".$strValidTHCa."',
					'".$strValidCBHa."',
					'".$strValidMoisture."',
					'".$strValidDescription."',
					'".$intDispensaryId."',
					'".$date."')";
					
			
					
		$SQLAddStrain = mysql_query($strAddStrainQuery)or die($strAddStrainQuery." : ".mysql_error());
		
		
		
		$StrainId = mysql_insert_id(); //fetch last inserted strain id
		if($SQLAddStrain == 1)
		{ 
			if(is_array($strValidExperienceArray))
			{
				for($i=0;$i<count($strValidExperienceArray);$i++)
				{
					
					// Adding  Experience in experiance table with strain id as foreign key.
					$strAddExperienceQuery="insert into experience
											(
											experience_name,
											strain_id
											)
											values(
											'".mysql_real_escape_string($strValidExperienceArray[$i])."',
											'".$StrainId."')";
					$SQLAddExperience=mysql_query($strAddExperienceQuery)or die(mysql_error());
				}
			}
			if(is_array($strValidMedicinalUseArray))
			{
				for($i=0;$i<count($strValidMedicinalUseArray);$i++)
				{
				// Adding  Medicinal use in Medicinal_use table with strain id as foreign key.  
					$strAddMedicinalUseQuery="insert into medicinal_use
											  ( 
											  	medicinal_type,
					                            strain_id
											  )values(
											  '".mysql_real_escape_string($strValidMedicinalUseArray[$i])."',
											  '".$StrainId."'
											  )";
					$SQLAddMedicinalUse=mysql_query($strAddMedicinalUseQuery)or die(mysql_error());
				}
			}
			
			//update time of strain in strain liberary when strain is from strain liberary
			$getStrainLIbListSQL ="SELECT strain_lib_id,strain_name 
							       FROM strain_library";
			$resStrainLibrary= mysql_query($getStrainLIbListSQL);
			while($rowSL=mysql_fetch_assoc($resStrainLibrary))
			{
				if($rowSL['strain_name']==$strValidStrainName)
				{
					// update time of strain liberary
					$updateTimeSQL ="UPDATE strain_library
									SET date_time='".$date."'
									WHERE strain_lib_id='".$rowSL['strain_lib_id']."'";
					$resUpdateTimeSQL = mysql_query($updateTimeSQL);				
					break;				
				}
			}
			// if dispensary name is entered manually then update dispensaryId in strain date -19th feb
			 
			 $dispancry_link_query="select dispensary_id 
                                    from dispensaries 
                                    where LOWER(dispensary_name)='".$strValidDispensaryName."'
                                    Limit 0,1";
             $dispancry_link_sql = mysql_query($dispancry_link_query);
             if(mysql_num_rows($dispancry_link_sql)==1){
				 $dispancry_link_result = mysql_fetch_assoc($dispancry_link_sql);
				 $disId = $dispancry_link_result['dispensary_id'];
				 
				 //update dispensary id in starin 
				 $updateDispIDSQL= "UPDATE strains
				 					SET dispensary_id='".$disId."'
									WHERE strain_id='".$StrainId."'";
				$updateDispensaryId=mysql_query($updateDispIDSQL) or die($updateDispIDSQL." : ".mysql_error());
									
			 }else
			 {
				 // dispensary id will saved as "0"
			 }
			 //end of 19th feb 
			
			//if every thing is ok then send success=1 and strain  id in response.
			$result = array( "success" => "1", 'StrainId' => $StrainId);
			$this->response($this->toJson($result),200,"application/json");
		}
		else
		{
			$error = array('success' => "0", "msg" => "Strain not added. Please try agian later.");
			$this->response($this->toJson($error), 406, "application/json");
		}
	}
	else
	{
		$error = array('success' => "0", "msg" => "Strain name or User Id.");
		// If no records "Error msg" status
		$this->response($this->toJson($error), 406, "application/json");
	}	
		
?>