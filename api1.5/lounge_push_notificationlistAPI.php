<?php
/*
	File Description - Lounge list (for all users)
	File Name - loungeListAPI.php
*/

	// Cross validation if the request method is POST 
	//else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array

	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	
	// Input validations
	if(!empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		// Check user id is valid or not		
		if($intValidUserId!=0){
			$uservalidation = CheckValidUser($intValidUserId);
		}
			
		 $strstrainnoQuery="SELECT * FROM tbl_push_notification
						    WHERE user_id ='".$intUserId."'
							AND delete_status='0'";
		  $strstrainnoSql = mysql_query($strstrainnoQuery);

		  $noOfRows = mysql_num_rows($strstrainnoSql);
		 
		  $totalPages = ceil($noOfRows / $intPageSize);
			
		  $perpage = $intPageSize;
		  $page = $intPageNo;
		  $calc = $perpage * $page;
		  $start = $calc - $perpage;
		  //SQL query to get Lounge list(strains and bud thought together date wise)
		  $strLoungeListQuery = "SELECT pn_id,user_id,sender_user_id,type,
		  						 type_id,notification_msg,date_time
		  						 FROM tbl_push_notification
						   		 WHERE 
								 	user_id ='".$intUserId."'
								 	AND delete_status='0'
								 ORDER BY date_time DESC Limit $start, $perpage";
						   
		  $LoungeListSQL = mysql_query($strLoungeListQuery) or die($strLoungeListQuery." : ".mysql_error());			          
		  $arrayLoungeListArray=array();
			
			if(mysql_num_rows($LoungeListSQL) > 0)
			{
				while($LoungeList = mysql_fetch_array($LoungeListSQL))
				{	
					// Get user detail
					$userdetail = GetUserDetail($LoungeList['sender_user_id']);
					
					//for user profile image path
					$getPathSQL = "SELECT photo_url 
								   FROM users 
								   WHERE user_id='".$LoungeList['sender_user_id']."' ";
					$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());
					$row = mysql_fetch_assoc($response);
 					if(!empty($row['photo_url']))
					{
						$senderImgPath = BASEURL."images/profileimages/thumbnail/".$row['photo_url'];
						$senderImgOriginal=BASEURL."images/profileimages/original/".$row['photo_url'];
					}else
					{
						$senderImgPath = '';
						$senderImgOriginal='';
					}
					//end of user image
					
					$date = funTimeAgo(strtotime($LoungeList['date_time']));
					
					$budThoughtArray= array();
					$strainArray=array();
					
					// if bud thought realted activity is done 
					// 0 for like bud thougt
					// 2 mention in bud thought
					// 3 mention in comment
					// 4 commented on your bud thought
					if($LoungeList['type']==0 || $LoungeList['type']==2 || $LoungeList['type']==3 || $LoungeList['type']==4 )
					{
							$type='0'; // for budthought
							//get budthought Details 
							$strBUdThoughtInfoSQL ="SELECT * 
													FROM tbl_bud_thought
													WHERE 
											bud_thought_id='".$LoungeList['type_id']."' 
											AND 
											delete_status='0'											
											limit 0,1";
							$resBUdThoughtInfo = mysql_query($strBUdThoughtInfoSQL) or die($strBUdThoughtInfoSQL." : ".mysql_error());			
							
							$budThoughtInfo = mysql_fetch_assoc($resBUdThoughtInfo);
								
							$budThoughtPOstDate = 	funTimeAgo(strtotime($budThoughtInfo['post_date']));
						
							 if(!empty($budThoughtInfo['picture_url']))
						 	 {
							 		$budThoughtURL = BASEURL."images/budthought/".$budThoughtInfo['picture_url'];
					  		 		$isImage ='1';
									
					  
						 	}else
						 	{
									$budThoughtURL= "";
							 		$isImage ='0';
						 	}

							if($intUserId!=0)
							{
								
									$likeFlag = isUserLiked($LoungeList['type_id'],$intUserId);
							}else
							{
									$likeFlag = 0;
							}
							
							$likeArray = getLikeBudThoughtUserList($budThoughtInfo['bud_thought_id']);
							$likeCount = getLikeCount($LoungeList['type_id']);
							$commentCount = getCommentCount($LoungeList['type_id']);
							$atRateUsersArray = getAtRateUsers($LoungeList['type_id']);
							
							// Get user detail who has posted the bud thought
							$ownerdetail = GetUserDetail($budThoughtInfo['user_id']);
							
							$ownerImage= getUserImagePath($budThoughtInfo['user_id']);
							if($ownerImage!='')
							{
								$ownerImgPath = BASEURL."images/profileimages/thumbnail/".$ownerImage;
								$ownerImgOriginal=BASEURL."images/profileimages/original/".$ownerImage;
							}
							
							
							$arrayLoungeListArray[]=array(
							"sender_id"=>$LoungeList['sender_user_id'],
							"sender_name"=>str_replace("\\", "",$userdetail['UserName']),
							"sender_profile_pic"=>$senderImgPath,
							"date" => $date,
							"msg_text" =>str_replace("\\", "",$LoungeList['notification_msg']),
							'bud_thought_id' => $LoungeList['type_id'],
							'BudThought' =>base64_encode(str_replace("\\", "",$budThoughtInfo['bud_thought'])),
							'UserId' => $budThoughtInfo['user_id'],
							'UserName' => str_replace("\\", "",$ownerdetail['UserName']),
							'pictureUrl' => $ownerImgPath!=''?$ownerImgPath:'',
							'thumbnailUrl'=> $ownerImgOriginal!=''?$ownerImgOriginal:'',
							'LikeCount' => $likeCount,
							'CommentCount'=> $commentCount,
							'likeFlag' =>$likeFlag,
							'budThoughtUrl'=> $budThoughtURL,
							'LikeArray'=>$likeArray,
							'CommentList'=>getCommentList($LoungeList['type_id']),
							'isImage'=>$isImage,
							'bud_thought_post_date'=>$budThoughtPOstDate,
							'atRateUsersList'=>$atRateUsersArray,
							'notf_type'=>$LoungeList['type'],
							'type'=>'0'
			  );// end of array 
						
					}// end of if (type==0|2|3|4)
					else if ($LoungeList['type']==1) // type==1 -> like strain 
					{
						//get strain details
						$strGetStrainDetailsSQL = "SELECT * FROM strains 
												   WHERE 
												   strain_id='".$LoungeList['type_id']."' 
												   LIMIT 0,1";
							$resGetStrainDetails =mysql_query($strGetStrainDetailsSQL) or die ($strGetStrainDetailsSQL." : ".mysql_error());						
							$strainInfo = mysql_fetch_assoc($resGetStrainDetails);				
											
							$ownerImage= getUserImagePath($strainInfo['user_id']);
							if($ownerImage!='')
							{
								$ownerImgPath = BASEURL."images/profileimages/thumbnail/".$ownerImage;
								$ownerImgOriginal=BASEURL."images/profileimages/original/".$ownerImage;
							}
							$primaryimages = GetPrimaryImages($LoungeList['type_id']);
						
							if(!empty($primaryimages[0])){
									$img = $primaryimages[0];
									$imgThumb =  $primaryimages[1];
									$isImage ='1';
							}else
							{
								$img="";
								$imgThumb = "";
								$isImage='0';
									
							}

							if($strainInfo['dispensary_id']=='0')
							{
								$dispensaryName = $strainInfo['dispensary_name'];
							}else
							{
								$dispensaryName = getDispensaryName($strainInfo['dispensary_id']);
							}
						
							$strainPostdate = funTimeAgo(strtotime($strainInfo['post_date']));
						
							$likeCount = getLikeCountStrain($strainInfo['strain_id']);

							$likeFlag = isUserLikeStrain($strainInfo['strain_id'],$intUserId);
							$encodedStarinNAme =base64_encode(str_replace("\\", "",$strainInfo['strain_name']));
							$encodedDispensaryName =base64_encode(str_replace("\\", "",$dispensaryName));
						
							// Get user detail who has posted the bud thought
							$ownerdetail = GetUserDetail($strainInfo['user_id']);
							
							$arrayLoungeListArray[]=array(
							'sender_id'=>$LoungeList['sender_user_id'],
							'sender_name'=>str_replace("\\", "",$userdetail['UserName']),
							'sender_profile_pic'=>$senderImgPath,
							'date' => $date,
							'msg_text' =>$LoungeList['notification_msg'],
							'StrainId'=> $strainInfo['strain_id'],
							'StrainName'=> $encodedStarinNAme, 
							'Dispensory'=> $encodedDispensaryName,
							'Species'=> stripslashes($strainInfo['species']),
							'OverallRating'=> $strainInfo['overall_rating'],
							'StrainsImageUrl'=>$img!=''?$img:'',
							'StrainsThumbImageUrl'=> $imgThumb!=''?$imgThumb:'',
							'UserId'=>$strainInfo['user_id'],
							'UserName'=>str_replace("\\", "",$ownerdetail['UserName']),
							'ZipCode'=>$ownerdetail['ZipCode'],
							'DispensaryId'=> ($strainInfo['dispensary_id']!=''?(string)$strainInfo['dispensary_id']:'0'),
							'LikeCount' => $likeCount,
							'LikeArray'=>  getLikeStrainUserList($strainInfo['strain_id']),
							'likeFlag' =>$likeFlag,
							'date'=> $date,
							'pictureUrl'=>$ownerImgPath,
							'strain_post_date'=>$strainPostdate,
							'isImage'=>$isImage,
							'notf_type'=>$LoungeList['type'],
							'type'=>'1'
					);// end of array
					
				   }
				    else//type=5 -> follow user
 				    {
				   			$arrayLoungeListArray[]=array(
							'sender_id'=>$LoungeList['sender_user_id'],
							'sender_name'=>str_replace("\\", "",$userdetail['UserName']),
							'sender_profile_pic'=>$senderImgPath,
							'date' => $date,
							'msg_text' =>$LoungeList['notification_msg'],
							'followerId'=> $LoungeList['type_id'],
							'notf_type'=>$LoungeList['type']
					  );// end of array
					}
				}//end of while
				
				
				// If success everythig is good send header as "OK" and LoungeList in reponse
				$result = array('success' => '1',
								'LoungeList' => $arrayLoungeListArray,
								'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// Empty Lounge List
				$error = array("success" => "0", 
							   "msg" => "Empty Lounge List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>