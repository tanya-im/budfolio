<?php
/*
	File Description - Lounge list (for all users)
	File Name - loungeListAPI.php
*/

	// Cross validation if the request method is POST 
	//else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array

	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	
	// Input validations
	if(!empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		// Check user id is valid or not		
		if($intValidUserId!=0){
			$uservalidation = CheckValidUser($intValidUserId);
		}
			
		$strstrainnoQuery=" SELECT 
									bud_thought_id
							FROM tbl_bud_thought tbt
							INNER JOIN users ON users.user_id = tbt.user_id
							WHERE users.flag='active'
							
							AND delete_status='0'
							AND tbt.user_id='".$intUserId."'";
		  $strstrainnoSql = mysql_query($strstrainnoQuery);

		  $strstrainnoData = mysql_num_rows($strstrainnoSql);
		  $totalPages = ceil($strstrainnoData / $intPageSize);
			
		  $perpage = $intPageSize;
		  $page = $intPageNo;
		  $calc = $perpage * $page;
		  $start = $calc - $perpage;
		  //SQL query to get Lounge list(strains and bud thought together date wise)
		  $strLoungeListQuery = "SELECT 
		  								bud_thought_id,
		  								bud_thought,
								 		post_date,
								 		tbt.user_id as userId, 
										picture_url
								 FROM tbl_bud_thought tbt
								 INNER JOIN users ON users.user_id = tbt.user_id
								 WHERE users.flag = 'active'
								 
								 AND tbt.user_id='".$intUserId."'
								 AND delete_status='0'
								 ORDER BY post_date DESC Limit $start, $perpage";
						   

			$LoungeListSQL = mysql_query($strLoungeListQuery) or die($strLoungeListQuery." :".mysql_error());			            //executing strLoginSQL
			
			$arrayLoungeListArray=array();
			
			if(mysql_num_rows($LoungeListSQL) > 0)
			{
				while($LoungeList = mysql_fetch_array($LoungeListSQL))
				{	
					// Get user detail
					$userdetail = GetUserDetail($LoungeList['userId']);
					
					//for user profile image path
					$getPathSQL = "SELECT photo_url 
								   FROM users 
								   WHERE user_id='".$LoungeList['userId']."' ";
					$response = mysql_query($getPathSQL) 
								or die($getPathSQL." : ".mysql_error());
					$row = mysql_fetch_assoc($response);
 					if(!empty($row['photo_url']))
					{
						$userImgPath = BASEURL."images/profileimages/thumbnail/".$row['photo_url'];
						$userImgOriginal=BASEURL."images/profileimages/original/".$row['photo_url'];
					}else
					{
						$userImgPath = '';
						$userImgOriginal='';
					}
					//end of user image


						 if(!empty($LoungeList['picture_url']))
						 {
							 $budThoughtURL = BASEURL."images/budthought/".$LoungeList['picture_url'];
					  		 $isImage ='1';
					  
						 }else
						 {
							 $budThoughtURL= "";
							 $isImage ='0';
						 }

							$likeCount = getLikeCount($LoungeList['bud_thought_id']);
							$commentCount = getCommentCount($LoungeList['bud_thought_id']);
							$date = funTimeAgo(strtotime($LoungeList['post_date']));
							if($intUserId!=0){
								$likeFlag = isUserLiked($LoungeList['bud_thought_id'],$intUserId);
							}else
							{
								$likeFlag = 0;
							}
							// start of at rate user
				
							$atRateUsersArray = getAtRateUsers($LoungeList['bud_thought']);
						$arrayLoungeListArray[] =  array(
				'type'=>'0',
				'bud_thought_id' => $LoungeList['bud_thought_id'],
				'BudThought' =>base64_encode(str_replace("\\", "",$LoungeList['bud_thought'])),
				'pictureUrl' => $userImgOriginal!=''?$userImgOriginal:'',
				'thumbnailUrl'=> $userImgPath!=''?$userImgPath:'',
				'UserId' => $LoungeList['userId'],
				'UserName' => str_replace("\\", "",$userdetail['UserName']),
				'LikeCount' => $likeCount,
				'CommentCount'=> $commentCount,
				'likeFlag' =>$likeFlag,
				'bud_thought_post_date'=> $date,
				'budThoughtUrl'=> $budThoughtURL,
				'LikeArray'=>getLikeBudThoughtUserList($LoungeList['bud_thought_id']),
				'CommentList'=>getCommentList($LoungeList['bud_thought_id']),
				'isImage'=>$isImage,
				'atRateUsersList'=>$atRateUsersArray
							);// end of array
					
				  // }//end of else
				}//end of while
				
				
				// If success everythig is good send header as "OK" and LoungeList in reponse
$result = array('success' => '1','LoungeList' => $arrayLoungeListArray,'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// Empty Lounge List
				$error = array('success' => "0", "msg" => "Empty Lounge List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>