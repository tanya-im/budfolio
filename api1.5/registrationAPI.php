<?php
/*
	File Description - file to register a user
	File Name - registrationAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
    //Getting the request JSOn string
	$dataJson = file_get_contents("php://input"); //getting json input 
	$inputArray = json_decode($dataJson,true); //converting that json into array
    
	//storing all values into local variable
	$strUserName = trim($inputArray['UserName']);		
	$strEmailAddress = trim($inputArray['Email']);
	$strPassword	= trim($inputArray['Password']);
	$intZipCode	= trim($inputArray['ZipCode']);
	$strAddress = trim($inputArray['Address']);
	$strState	= trim($inputArray['State']);
	$strBio	= trim($inputArray['Bio']);
	$strCountry	= trim($inputArray['Country']);
	$strTermAndCondition	= trim($inputArray['TermAndConditions']);
	$str18YearOfAge = trim($inputArray['age18']);
	$strStayUpToDate =trim($inputArray['stayUpToDate']);
	$strTwitterId = $inputArray['TwitterId'];
	$strFaceBookId = $inputArray['FaceBookId'];
	$strReciveEmailFromBudFolio= $inputArray['ReceiveEmailFromBudFolio'];
	$strDeviceToken = $inputArray['DeviceToken'];
	$strDeviceType = $inputArray['DeviceType'];
	$strPrivacy	= $inputArray['Privacy'];

	// Input validations
	if(!empty($strUserName)&& !empty($strEmailAddress) && !empty($strPassword) &&  !empty($strTermAndCondition))
	{
		// sanitization of values to insert in db
		$strValidUserName = mysql_real_escape_string($strUserName);
		$strValidEmailAddress = mysql_real_escape_string($strEmailAddress);
		$strValidPassword = mysql_real_escape_string($strPassword);
		$md5password= md5($strValidPassword);
		
		//$confirmcode=hash('sha256',$md5password.$strValidEmailAddress); date 15th nov
		$confirmcode='';
		
		$date = date('Y-m-d H:i:s');
		$strValidAddress = mysql_real_escape_string($strAddress);
		$intValidZipcode = mysql_real_escape_string($intZipCode);
		$strValidState = mysql_real_escape_string($strState);
		$strValidBio = mysql_real_escape_string($strBio);
		$strValidCountry = mysql_real_escape_string($strCountry);
		$strValidTermAndCondition = mysql_real_escape_string($strTermAndCondition);
		$strValidDeviceToken = mysql_real_escape_string($strDeviceToken);
		
		
		
		if($str18YearOfAge=='NO')
		{
			$error = array('success' => "0", 
							'msg' => "You must be atleast of 18 years. "
						  );
			// If records match "Error msg" status
			$this->response($this->toJson($error), 406, "application/json");
		}
		if ($strTermAndCondition=='NO')
		{	
				$error = array('success' => "0", 
							'msg' => "You must agree to budfolio terms and conditions"
						  );
			// If records match "Error msg" status
			$this->response($this->toJson($error), 406, "application/json");
		}
		
		// to check whether the username/email is already in records.
		$strSQLChkUserName ="SELECT user_id 
							 FROM users 
							 WHERE user_name='".$strValidUserName."'";
		$SQLResChkUserName = mysql_query($strSQLChkUserName)or die(mysql_error());// execution of query 
		$flag=true;
		if(mysql_num_rows($SQLResChkUserName)>0)
		{
			$flag=false;
			$error = array('success' => "0", 
							'msg' => "Username already in use. Please try with another user name"
						  );
			// If records match "Error msg" status
			$this->response($this->toJson($error), 406, "application/json");
		}
		
		$strSQLChkEmailAddress = "SELECT user_id 
								  FROM users 
								  WHERE email_address='".$strValidEmailAddress."'";
		$SQLResChkEmailAddress = mysql_query($strSQLChkEmailAddress);// execution of query strSQLChkEmailAddress
		if(mysql_num_rows($SQLResChkEmailAddress)>0)
		{
			$flag=false;
			$error = array('success' => "0", 
			                'msg' => "Whoops! This email is already in use. Did you forget your username/password? Please try a new email to complete registration"
						  );
			// If records match "Error msg" status
			$this->response($this->toJson($error), 406, "application/json");
		}
		// if username/email not in DB
		if($flag) 
		{
			$latlongdata=getlatlong($intZipCode,$strState);

			$lat='';
			$lng='';
			
			// search if that device token already exist 8th jan
			$searchDEviceTokenSQL ="SELECT user_id FROM users WHERE ";
			if($strDeviceType==1)
			{
				$searchDEviceTokenSQL.="device_token_for_iphone='".$strDeviceToken."'";
			}else
			{
				$searchDEviceTokenSQL.="device_token_for_android='".$strDeviceToken."'";
			}
			//echo $searchDEviceTokenSQL."<br>";
			$exeSearchDeviceToken = mysql_query($searchDEviceTokenSQL) or die($searchDEviceTokenSQL." : ".mysql_query());
			
			if(mysql_num_rows($exeSearchDeviceToken)>0)
			{
				while($rowDT= mysql_fetch_assoc($exeSearchDeviceToken))
				{
					//if any device with that device token is found
					// then update device token with empty string
					$updateDTWidEmptyStrSQL ="UPDATE users SET ";
					if($strDeviceType==1)
					{
						$updateDTWidEmptyStrSQL.="device_token_for_iphone=''";
					}else
					{
						$updateDTWidEmptyStrSQL.="device_token_for_android=''";
					}
					$updateDTWidEmptyStrSQL.=" WHERE user_id='".$rowDT['user_id']."'";
			//		echo $updateDTWidEmptyStrSQL."<br>";
					$exeUpdateDTWidEmptySQL = mysql_query($updateDTWidEmptyStrSQL) or die(mysql_error()." : ".$updateDTWidEmptyStrSQL);
					
				}
			}// 8th jan 
			
			//SQL query to insert he data in the db					
			$strRegisterQuery ="INSERT INTO users
			set user_name ='".$strValidUserName."',
			password = '".$md5password."',
			email_address = '".$strValidEmailAddress."',
			confirm_code = '".$confirmcode."',
			address = '".$strValidAddress."',
			zip_code = '".$intValidZipcode."',
			state = '".$strValidState."',
			term_and_conditions = '".$strValidTermAndCondition."',
			atleast_18_years_old='".$str18YearOfAge."',
			stay_up_to_date='".$strStayUpToDate."',
			privacy = '".$strPrivacy."',
			registration_date = '".$date."',
			latitude = '".$lat."',
			longitude = '".$lng."',
			bio ='".$strValidBio."',
			country = '".$strValidCountry."',
			receive_email_from_budfolio='".$strReciveEmailFromBudFolio."',
			device_type='".$strDeviceType."',";
			
			if($strDeviceType==1){
				$strRegisterQuery .="device_token_for_iphone='".$strValidDeviceToken."'";
			}else
			{
				$strRegisterQuery .="device_token_for_android='".$strValidDeviceToken."'";
			}
			if(!empty($strTwitterId))
			{
				$strRegisterQuery .=" ,twitter_id ='".$strTwitterId."'";
			}
			if(!empty($strFaceBookId)){
				
				$strRegisterQuery .=" ,faceBook_id ='".$strFaceBookId."'";
			}

			//echo $strRegisterQuery;
			// execute query strRegisterQuery
			$sql = mysql_query($strRegisterQuery)or die(mysql_error());
			$userId = mysql_insert_id(); //fetch last inserted user id
			if($sql == 1)
			{
				if(!empty($userId))
				{
					$budThoughtToInsert= "Welcome new Budfolio member @".$strValidUserName." #newbud";
			
			$date = date('Y-m-d H:i:s');
			//make an entry for budthought autoPost
			$AutoPostBudThoughtSQL = "INSERT INTO tbl_bud_thought
									  SET bud_thought='".$budThoughtToInsert."',
									  user_id='1230',
									  post_date='".$date."'";
			$exeBudThoughtToInsert= mysql_query($AutoPostBudThoughtSQL) or die($AutoPostBudThoughtSQL." : ".mysql_error());						  			
			// end of auto post enetry
					//$url='http://budfolio.com/qa/emailconfirmation.php?cd='.$confirmcode.'&id='.$userId;
				$subject="Welcome to Budfolio";
					
				$msg ="Your username is :<b>".$strUserName."<b/><br>
						Personalize your profile now by adding a profile photo and updating your bio in the settings tab.<br>Congratulations! You now have a chance to win FREE prizes and give-a-ways every Friday. Watch for a lounge post at 4:20PM PST to announce the winner. The winner also be notified via email.<br>
Start your strain journal and capture photos and rate your buds on smell, taste, strength, medicinal use and more!<br>Share your Budfolio in the lounge! Find and follow other members, friends and dispensaries.<br>
If you have any questions or need help with your account, email Budfolio at help.budfolio@gmail.com<br>Follow @budfolio on Twitter, @strainrater on instragram and like Budfolio on Facebook.<br>Stay Lifted,<br>Brian<br>
Budfolio CEO / Founder";
					$sent_unsent=email(SenderEmail,strip_tags($strValidEmailAddress),$subject,$msg);
	
						$emailstatus="sent";
						$msg="Thank you for registering! Now you can login and access Budfolio features.";
				
				//if registration is successfull then success=1 and user id
				$result = array( "success" => "1", 
								 'UserId' => $userId,
								 'UserName'=>str_replace("\\", "",$strValidUserName),
								 "EmailStatus"=>$emailstatus,
								 "msg"=>$msg
								 );
					$this->response($this->toJson($result),200,"application/json");
				}
				else
				{
// If values are not inserted								
					$error = array('success' => "0", "msg" => "Failed to register, please try again later.");
					$this->response($this->toJson($error), 400, "application/json");
				}// end of else
									
			}// end of if($sql==1)
			else
			{
// If values are not inserted								
				$error = array('success' => "0", "msg" => "Failed to register, please try again later.");
				$this->response($this->toJson($error), 400, "application/json");
			}// end of else ($sql==1)
							
		} // closing mysql_num_rows
		else
		{
			$error = array('success' => "0", "msg" => "Username or email already in use. Please try using another username/email");
			// If no records "Error msg" status
			$this->response($this->toJson($error), 406, "application/json");
		}
	} 
	else
	{
		// If invalid inputs "Bad Request" status message and reason
		$error = array('success' => "0", "msg" => "Please accept all terms & conditions");
		$this->response($this->toJson($error), 400,"application/json");
	}
?>