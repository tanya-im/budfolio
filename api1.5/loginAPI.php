<?php
/*
	File Description - File to check credentials of user and allow to login
	File Name - loginAPI.php
*/

// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true);

	//converting that json into array
	$strUserName = $inputArray['UserName'];		
	$strPassword = $inputArray['Password'];
	$strDeviceToken = $inputArray['DeviceToken'];
	$strDeviceType =$inputArray['DeviceType'];//wheather its android device or iphone
	
	// Input validations
	if(!empty($strUserName) and !empty($strPassword))
	{
		$strValidUserName = mysql_real_escape_string($strUserName);
		$strValidPassword = mysql_real_escape_string($strPassword);
		

		//SQL query to check the  USER exists or not
		$strLoginSQL = "SELECT user_id,user_name,confirm_code,user_type ,photo_url
					    FROM users 
						WHERE user_name ='".$strValidUserName."' 
						AND password = '".md5($strValidPassword)."'";
		$sql = mysql_query($strLoginSQL)or die(mysql_error());//executing strLoginSQL
		if(mysql_num_rows($sql) > 0)
		{
			$resultSet = mysql_fetch_assoc($sql);
			
			//check email is confirm or not
			if(!empty($resultSet['confirm_code']))
			{
				
				// If email is not cofirm.
				$error = array('success' => "0", "msg" => "Please confirm your email address before login.");
				$this->response($this->toJson($error), 400,"application/json");
			}
			//thumb url 
			
			if(!empty($resultSet['photo_url']))
			{
						$userImgPath = BASEURL."images/profileimages/thumbnail/".$resultSet['photo_url'];
						$userImgOriginal=BASEURL."images/profileimages/original/".$resultSet['photo_url'];
			}else
			{
						$userImgPath = '';
						$userImgOriginal='';
			}
			
			
			// search if that device token already exist 7th jan
			$searchDEviceTokenSQL ="SELECT user_id FROM users WHERE ";
			if($strDeviceType==1)
			{
				$searchDEviceTokenSQL.="device_token_for_iphone='".$strDeviceToken."'";
			}else
			{
				$searchDEviceTokenSQL.="device_token_for_android='".$strDeviceToken."'";
			}
			//echo $searchDEviceTokenSQL."<br>";
			$exeSearchDeviceToken = mysql_query($searchDEviceTokenSQL) or die($searchDEviceTokenSQL." : ".mysql_query());
			
			if(mysql_num_rows($exeSearchDeviceToken)>0)
			{
				while($rowDT= mysql_fetch_assoc($exeSearchDeviceToken))
				{
					//if any device with that device token is found
					// then update device token with empty string
					$updateDTWidEmptyStrSQL ="UPDATE users SET ";
					if($strDeviceType==1)
					{
						$updateDTWidEmptyStrSQL.="device_token_for_iphone=''";
					}else
					{
						$updateDTWidEmptyStrSQL.="device_token_for_android=''";
					}
					$updateDTWidEmptyStrSQL.=" WHERE user_id='".$rowDT['user_id']."'";
			//		echo $updateDTWidEmptyStrSQL."<br>";
					$exeUpdateDTWidEmptySQL = mysql_query($updateDTWidEmptyStrSQL) or die(mysql_error()." : ".$updateDTWidEmptyStrSQL);
					
				}
			}// 7th jan 
			
			
			
			//thumb url end
			$date = date('Y-m-d H:i:s');
			$updatelogindate="update users set 
							  last_login_date='".$date."' ,
							  device_type='".$strDeviceType."',";
			if($strDeviceType==1)
			{
				$updatelogindate.="device_token_for_iphone='".$strDeviceToken."'";
			}else
			{
				$updatelogindate.="device_token_for_android='".$strDeviceToken."'";
			}
			$updatelogindate.="where user_id='".$resultSet['user_id']."'";
			//echo $updatelogindate;
			$datestatus=mysql_query($updatelogindate) or die($updatelogindate." : ".mysql_error());
			
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1",
							'UserId' => $resultSet['user_id'],
							'UserName'=>str_replace("\\", "",$resultSet['user_name']),
							'userType'=>$resultSet['user_type'],
				            'pictureUrlThumb'=>$userImgPath

				);
			$this->response($this->toJson($result),200,"application/json");

		} // closing mysql_num_rows
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "Invalid Username or password.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	} //end of input validations
	else
	{
		// If either username or passowrd is empty
		$error = array('success' => "0", "msg" => "Username or password is empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}
?>