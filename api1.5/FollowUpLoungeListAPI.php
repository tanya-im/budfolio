<?php
/*
	File Description - Lounge Follow Up List
	File Name - FollowUpLoungeListAPI.php
*/
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array
		
	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];		
	if(!empty($intUserId) && !empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		// Check user id is valid or not		
		$uservalidation=CheckValidUser($intValidUserId);
		if($uservalidation)
		{
			// Sql to get no followers			
			$strstrainnoQuery ="SELECT distinct(strain_id), strain_name,dispensary_name,     dispensary_id,post_date,species,overall_rating,
				 strains.user_id as userId,NULL AS picture_url, 1 AS TYPE
				 FROM strains
				 INNER JOIN user_followup on user_followup.follower_id = strains.user_id
				 INNER JOIN users ON users.user_id = user_followup.follower_id 
				 where strains.flag='active'
				 and user_followup.user_id='".$intUserId."'
				 AND privacy = 'public'
				 and users.flag='active'
				 UNION ALL
				 SELECT distinct(bud_thought_id), bud_thought, NULL AS dispensary_name,NULL AS dispensary_id, post_date, NULL AS species,
				 NULL AS overall_rating,tbt.user_id as userId, picture_url, 0 AS TYPE
				 FROM tbl_bud_thought tbt
				 INNER JOIN users ON users.user_id = tbt.user_id
			  	 INNER JOIN user_followup on user_followup.follower_id=tbt.user_id 
				 WHERE user_followup.user_id='".$intUserId."'
				 AND  users.flag = 'active'
				 AND delete_status='0'
				 ORDER BY post_date DESC";
				//echo 	$strstrainnoQuery;		
				//die;	
			$strstrainnoSql = mysql_query($strstrainnoQuery);
		    $strstrainnoData = mysql_num_rows($strstrainnoSql);
		 	$totalPages = ceil($strstrainnoData / $intPageSize);
		
			$perpage=$intPageSize;
			$page=$intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;

			 //SQL query to get Followers listing
			 $strstrainnoQuery .=  " Limit $start, $perpage";
					
			$arrayLoungeListArray=array();
			$LoungeListSQL = mysql_query($strstrainnoQuery) or die(mysql_error());
			if(mysql_num_rows($LoungeListSQL) > 0)
			{
				
				while($LoungeList = mysql_fetch_array($LoungeListSQL))
				{
					$userdetail = GetUserDetail($LoungeList['userId']);// Get user detail
					
					//for user profile image path
					$getPathSQL = "SELECT photo_url FROM users WHERE user_id='".$LoungeList['userId']."'";
					$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());
					$row = mysql_fetch_assoc($response);
 					if(!empty($row['photo_url']))
					{
						$userImgPath = BASEURL."images/profileimages/thumbnail/".$row['photo_url'];
						$userImgOriginal=BASEURL."images/profileimages/original/".$row['photo_url'];
					}else
					{
						$userImgPath = '';
						$userImgOriginal='';
					}
					//end of user image

					if($LoungeList['TYPE']==1)//start of strain
					{
						$primaryimages = GetPrimaryImages($LoungeList['strain_id']);//Get primary image.
						
						if(!empty($primaryimages[0])){
							$img = $primaryimages[0];
							$imgThumb =  $primaryimages[1];
							$isImage ='1';
						}else
						{
							$img="";
							$imgThumb = "";
							$isImage='0';
								
						}

					
						
						if($LoungeList['dispensary_id']=='0')
						{
							 $dispensaryName=$LoungeList['dispensary_name'];
					 		 $disNameArray = explode('-',$dispensaryName);
							 if(count($disNameArray)>0)
							 {
								$disName= $disNameArray[0];
								$disId =getDispensaryId($disName);
							 }	
						   $dispancry_link_query='select dispensary_id 
                                    from dispensaries 
                                    where LOWER(dispensary_name)="'.$disName.'"
                                    Limit 0,1';
                  			 $dispancry_link_sql = mysql_query($dispancry_link_query);
                  			 $dispancry_link_result = mysql_fetch_array($dispancry_link_sql);
                  			
								 $disId=$dispancry_link_result['dispensary_id'];
								$dispensaryName = $LoungeList['dispensary_name']!=''?$LoungeList['dispensary_name']:'';
							
						}else
						{
							$disId=$LoungeList['dispensary_id'];
							$dispensaryName = $LoungeList['dispensary_name']!=''?$LoungeList['dispensary_name']:'';
						}
						//10th augest
						//$date = date('d|m|Y | h:i A',strtotime($LoungeList['post_date']));//conevrting date time into string format
						
						$date = funTimeAgo(strtotime($LoungeList['post_date']));
						
						
						$likeCount = getLikeCountStrain($LoungeList['strain_id']);

						$likeFlag = isUserLikeStrain($LoungeList['strain_id'],$intUserId);

						$encodedStarinNAme =base64_encode(str_replace("\\", "",$LoungeList['strain_name']));
						$encodedDispensaryName =base64_encode(str_replace("\\", "",$dispensaryName));


						$arrayLoungeListArray[] =  array(
							  'type'=>$LoungeList['TYPE'],
							  'StrainId'=> $LoungeList['strain_id'],
							  'StrainName'=> $encodedStarinNAme,  
							  'Dispensory'=> $encodedDispensaryName,
							  'Species'=> str_replace("\\", "",$LoungeList['species']),
							  'OverallRating'=> $LoungeList['overall_rating'],
							  'StrainsImageUrl'=>$img!=''?$img:'',
							  'StrainsThumbImageUrl'=> $imgThumb!=''?$imgThumb:'',
							  'UserId'=>$LoungeList['userId'],
							  'UserName'=>str_replace("\\", "",$userdetail['UserName']),
							  'ZipCode'=>$userdetail['ZipCode'],
							  'DispensaryId'=> ($LoungeList['dispensary_id']!=''?(string)$LoungeList['dispensary_id']:'0'),
							  'LikeCount' => $likeCount,
							  'LikeArray'=>  getLikeStrainUserList($LoungeList['strain_id']),
							  'likeFlag' =>$likeFlag,
							  'date'=> $date,
							  'pictureUrl'=>$userImgPath,
							  'pictureUrlThumb'=>$userImgOriginal,
							  'isImage'=>$isImage
						);// end of array
					
				   }//end of ($LoungeList['TYPE']==1)
				  	else{//start of bud thought
					 
						 if(!empty($LoungeList['picture_url']))
						 {
							 $budThoughtURL = BASEURL."images/budthought/".$LoungeList['picture_url'];
					  		 $isImage ='1';
					  
						 }else
						 {
							 $budThoughtURL= "";
							  $isImage ='0';
						 }

							$likeCount = getLikeCount($LoungeList['strain_id']);
							$commentCount = getCommentCount($LoungeList['strain_id']);
							//$date = date('d|m|Y | h:i A',strtotime($LoungeList['post_date']));//conevrting date time into string format

							$date = funTimeAgo(strtotime($LoungeList['post_date']));

							if($intUserId!=0){
								$likeFlag = isUserLiked($LoungeList['strain_id'],$intUserId);
							}else
							{
								$likeFlag = 0;
							}
							
							
							$atRateUsersArray = getAtRateUsers($LoungeList['strain_name']);
							
							$arrayLoungeListArray[] =  array(
			'type'=>$LoungeList['TYPE'],
			'bud_thought_id' => $LoungeList['strain_id'],
			'BudThought' =>  base64_encode(str_replace("\\", "",$LoungeList['strain_name'])), 
			'pictureUrl' => $userImgOriginal!=''?$userImgOriginal:'',
			'thumbnailUrl'=> $userImgPath!=''?$userImgPath:'',//for thumbnail url on 21st june
			'UserId' => $LoungeList['userId'],
			'UserName' =>str_replace("\\", "",$userdetail['UserName']),
			'LikeCount' => $likeCount,
			'CommentCount'=> $commentCount,
			'likeFlag' =>$likeFlag,
			'date'=> $date,
			'budThoughtUrl'=> $budThoughtURL,
			'LikeArray'=>getLikeBudThoughtUserList($LoungeList['strain_id']),
			'CommentList'=>getCommentList($LoungeList['strain_id']),
			'isImage'=>$isImage,
			'atRateUsersList'=>$atRateUsersArray
							);// end of array
					
				   }//end of else
				}//end of while
				
				// If success everythig is good send header as "OK" and Follwers lounge list in reponse
				$result = array('success' => '1','LoungeList' => $arrayLoungeListArray,'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");

			}	
			else
			{
				// Empty Lounge List
				$error = array('success' => "1", 'LoungeList' => array());
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>