<?php
/*
	File Description - Profile TYpe 
	File Name - profileTypeAPI.php
*/
    // Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
    $dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	$intUserId = $inputArray['UserId'];	
    
	$profileTYpe = array("Flower","Concentrate","Edible");

    $intValidUserId = mysql_real_escape_string($intUserId);
			if(is_array($profileTYpe) && !empty($profileTYpe))
			{
				$arrayProfileType=array();
				for($i=0;count($profileTYpe)>$i;$i++)
				{   
					$arrayProfileType[$i]=$profileTYpe[$i];
				}
				sort($arrayProfileType,SORT_STRING);
				// If success everythig is good send header as "OK" and medicinal use list in reponse
				$result = array('success' => '1','ProfileTypeList' =>$arrayProfileType);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// If Empty Medicinal Use List
				$error = array('success' => "0", "msg" => "Empty profile TYpe List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		
?>