<?php
/*
	File Description - search strain ,lieange,speiecs in strain liberary
	File Name - searchStrainLiberaryAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable				
	 $strSearch = $inputArray['Search'];
	
	// Input validations
	if(isset($strSearch))
	{
		//SQL query to get Strain library
		$strStrainLiberarySQL = "SELECT *,UNIX_TIMESTAMP(date_time) as unixTime
								 FROM strain_library 
								 WHERE (strain_name like '%".$strSearch."%' 
								 or species like '%".$strSearch."%'
								 or lineage like '%".$strSearch."%')
								 order by strain_name
								 ";
		$StrainLiberarySQL = mysql_query($strStrainLiberarySQL) or die($strStrainLiberarySQL." : ".mysql_error());
		
		$StrainLibArray=array();
		$unix_time = 0;
		if(mysql_num_rows($StrainLiberarySQL) > 0)
		{
			while($strainLiberary = mysql_fetch_array($StrainLiberarySQL))
			{
				//Strain Liberary array
				$unix_time = $strainLiberary['unix_time'];
				$StrainLibArray[] =  array(
											'starin_lib_id'=> $strainLiberary['strain_lib_id'],
											'strain_name'=>str_replace("\\", "",$strainLiberary['strain_name']),
											'species'=> str_replace("\\", "",$strainLiberary['species']), 
											'lineage'=> str_replace("\\", "",$strainLiberary['lineage'])
										);// end of array
			}
			// If success everythig is good send header as "OK" and sending Dispensary details in reponse
			$result = array('success' => '1',
							'strain_liberary' => $StrainLibArray,
							'unix_time'=> $unix_time
				);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "0", "msg" => "No strain found for your search");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Please enter string to be search.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>