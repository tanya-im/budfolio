<?php
/*
	File Description - Inappropiate functionality Detail
	File Name - markInappropiateAPI.php
*/

// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable	
	$intPostType = $inputArray['PostType'];	
	$postID='';
	if($intPostType==1){
		$postID=$inputArray['BudthoughtId'];
		$table=	"tbl_bud_thought";
	}else{
		$postID=$inputArray['StrainId'];
		$table=	"strains";
	}	
		
	$intBudthoughtId = $inputArray['BudthoughtId'];
	$intStrainId = $inputArray['StrainId'];
	$intUserId = $inputArray['UserId'];
	
	if($intPostType==1)
	{
		$ownerId=getBudthoughtOwner($intBudthoughtId);
	}
	else
	{
		$ownerId=getStrainOwner($intStrainId);
	}
	
	$getCountRES='';
	
	/********sending mail to report Admin about inappropriate post*********/
	
	//user detail who report about post
	$getreportinguserdetailSQL="SELECT * FROM `users` WHERE user_id='".$intUserId."' ";
	$getreportinguserdetailRES=mysql_query($getreportinguserdetailSQL)or die(mysql_error());
	$getreportinguserdetail=mysql_fetch_array($getreportinguserdetailRES);
	
	//user detail who post budthought or strain
	$getpostinguserdetailSQL="SELECT * FROM `users` WHERE user_id='".$ownerId."' ";
	$getpostinguserdetailRES=mysql_query($getpostinguserdetailSQL)or die(mysql_error());
	$getpostinguserdetail=mysql_fetch_array($getpostinguserdetailRES);
	
	//detail of reporting budthought
	$getbudthoughtdetailSQL="SELECT * FROM `tbl_bud_thought` WHERE `bud_thought_id` = '".$intBudthoughtId ."' ";
	$getbudthoughtdetailRES=mysql_query($getbudthoughtdetailSQL)or die(mysql_error());
	$getbudthoughtdetail=mysql_fetch_array($getbudthoughtdetailRES);
	
	//detail of reporting strain
	$getstraindetailSQL="SELECT * FROM `strains` WHERE `strain_id` ='".$intStrainId ."' ";
	$getstraindetailRES=mysql_query($getstraindetailSQL)or die(mysql_error());
	$getstraindetail=mysql_fetch_array($getstraindetailRES);
	
	//detail of reporting strain's image
	$getstrainImageSQL="SELECT * FROM `strainimages` WHERE `strain_id` ='".$getstraindetail['strain_id'] ."' AND primary_image='1' ";
	$getstrainImageRES=mysql_query($getstrainImageSQL)or die(mysql_error());
	$getstrainImage=mysql_fetch_array($getstrainImageRES);
	
	
	//contact.budfolio@gmail.com 
	$to="rosshenry990@gmail.com";
	//$to='contact.budfolio@gmail.com';
	$subject = 'Inappropriate Post Reporting';
	$headers = "From: " .$getreportinguserdetail['email_address']. "\r\n";
	//$headers = "From:dev.maran07@gmail.com \r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$message = '<html><body>';
	
	
	
	$getUserIdSQL="SELECT user_id
					FROM tbl_strain_or_budthought_markinappropriate
					WHERE user_id='".$intUserId."'
					AND post_id='".$postID."'
					AND post_type='".$intPostType."'";
	$getUserIdRES=mysql_query($getUserIdSQL)or die(mysql_error());
	if(mysql_num_rows($getUserIdRES)>0) {
		
		$error = array('success' => "0", "msg" => "You already marked as report inappropriate.");
		$this->response($this->toJson($error), 400,"application/json");
		
	}else{
		
	//Input validations
	if(!empty($intUserId) && (!empty($intBudthoughtId) || !empty($intStrainId)) ){
		
			$insertBudthoughtdetail="INSERT INTO 															tbl_strain_or_budthought_markinappropriate(user_id,post_id,post_type)VALUES";
				
			if($intPostType==1){
					$insertBudthoughtdetail.="('".$intUserId ."','".$intBudthoughtId ."','1')";
			}elseif($intPostType==2){
					$insertBudthoughtdetail.="('".$intUserId ."','".$intStrainId ."','2')";	
			}
			$sql = mysql_query($insertBudthoughtdetail)or die(mysql_error());
			
			$getCountSQL="SELECT count(post_id) as countRES 
							FROM tbl_strain_or_budthought_markinappropriate";
							
			if($intPostType==1){
				$getCountSQL.=" WHERE post_id='".$intBudthoughtId ."' and post_type='1'";
			}elseif($intPostType==2){
				$getCountSQL.=" WHERE post_id='".$intStrainId ."' and post_type='2'";
			}
			$getBudthoughtCount=mysql_query($getCountSQL)or die(mysql_error());
			$getCountRES=mysql_fetch_array($getBudthoughtCount);
			
			/*if($getCountRES['countRES']){			
				// If success everythig is good send header as "OK" and sending success msg in reponse
				$result = array('success' => '1','count' => $getCountRES['countRES']);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}*/
			
			if($getCountRES['countRES'] <= 5){
				
		if($intPostType==1){
		
		//$imagepath=$_SERVER['DOCUMENT_ROOT'].'/qa/images/budthought/'.$getbudthoughtdetail['picture_url'];
	
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		
		$message .= "<tr style='background: #eee;'><td><strong>Inappropriate Marked By:</strong> </td><td>" . str_replace("\\"," " ,$getreportinguserdetail['user_name'] ). "</td></tr>";
		
		
		//$message .= "<tr style='background: #eee;'>Budthoght Image:<img src='".$_SERVER['DOCUMENT_ROOT'].'/qa/images/budthought/'.$getbudthoughtdetail['picture_url']."' alt='loading.....' /> </tr>";
		$message .= "<tr style='background: #eee;'><td><strong>Post Type:</strong> </td><td>Budthought</td></tr>";	
		$message .= "<tr style='background: #eee;'><td><strong>Budthought:</strong> </td><td>" . str_replace("\\"," " ,$getbudthoughtdetail['bud_thought'] ). "</td></tr>";
		
		

		
		$message .= "<tr style='background: #eee;'><td><strong>User Who has Posted Budthoght:</strong> </td><td>" . str_replace("\\"," " ,$getpostinguserdetail['user_name']) . "</td></tr>";
		
		
		//$message .= "<tr style='background: #eee;'><strong>Message:</strong> </tr>";
		
		$message .= "</table>";
	
		//mail($to, $subject, $message, $headers);
		
	}else{
	
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		
		$message .= "<tr style='background: #eee;'><td><strong>Inappropriate Marked By:</strong> </td><td>" . str_replace("\\"," " ,$getreportinguserdetail['user_name']) . "</td></tr>";
		
		
		//$message .= "<tr style='background: #eee;'>Budthoght Image:<img src='".$_SERVER['DOCUMENT_ROOT'].'/qa/images/uploads/original/'.$getstrainImage['image_name']."' alt='loading.....' /> </tr>";
		$message .= "<tr style='background: #eee;'><td><strong>Post Type:</strong> </td><td>Strain</td></tr>";	
		$message .= "<tr style='background: #eee;'><td><strong>Strain Name:</strong> </td><td>" . str_replace("\\"," " ,$getstraindetail['strain_name']) . "</td></tr>";
		if($getstraindetail['dispensary_name']!=""){
		$message .= "<tr style='background: #eee;'><td><strong>Dispensary Name:</strong> </td><td>" .str_replace("\\"," " ,$getstraindetail['dispensary_name']) . "</td></tr>";
		}
		$message .= "<tr style='background: #eee;'><td><strong>User Who has Posted Strain:</strong> </td><td>" . str_replace("\\"," " ,$getpostinguserdetail['user_name'] ). "</td></tr>";
				
		//$message .= "<tr style='background: #eee;'><strong>Message:</strong> </tr>";
		
		$message .= "</table>";
		
	}
	$message .= "</body></html>";
	mail($to, $subject, $message, $headers);
		
				
				
				
				// If success everythig is good send header as "OK" and sending success msg in reponse
				$result = array('success' => '1','count' => $getCountRES['countRES']);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
				
				
				
				
					
			}else{
				
				if($intPostType==1){
					$setInactiveSQL="UPDATE ".$table;
				}elseif($intPostType==2){
					$setInactiveSQL="UPDATE ".$table;
				}

				if($intPostType==1){
					$setInactiveSQL.=" SET delete_status='1' WHERE bud_thought_id='".$intBudthoughtId ."'";
				}elseif($intPostType==2){
					$setInactiveSQL.=" SET flag='deactive' WHERE strain_id='".$intStrainId ."'";
				}
				//echo $setInactiveSQL;							
				$setInactiveBudthoughtRES = mysql_query($setInactiveSQL)or die(mysql_error());
				
				// If success everythig is good send header as "OK" and sending success msg in reponse
				$result = array('success' => '1','msg' => "Post Removed");
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}
		
		
		
	}else{
		
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Invalid BudthoughtId or StrainId .");
		$this->response($this->toJson($error), 400,"application/json");
	}
	
	}
?>