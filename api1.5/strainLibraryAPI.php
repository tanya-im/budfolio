<?php
/*
	File Description - default strain library
	File Name - strainLiberaryAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable				
	 $intUnixTime = $inputArray['unix_time'];
	
	// Input validations
	if(isset($intUnixTime))
	{
		//SQL query to get Strain library
		$strStrainLiberarySQL = "SELECT *,UNIX_TIMESTAMP(date_time) as unixTime
								 FROM strain_library 
								 WHERE UNIX_TIMESTAMP(date_time) > '".$intUnixTime."'
								 ORDER BY strain_name";
		
		$StrainLiberarySQL = mysql_query($strStrainLiberarySQL) or die($strStrainLiberarySQL." : ".mysql_error());
		
		$StrainLibArray=array();
		
		$unix_time =0;
		
		
		//SQL query to get Strain library
		$strStrainLiberaryMaxTimeSQL = "SELECT 
												max( UNIX_TIMESTAMP( date_time ) ) 			                                                AS unixTime
                                                FROM strain_library";
		
		$StrainLiberaryMaxSQL = mysql_query($strStrainLiberaryMaxTimeSQL) or die($strStrainLiberarySQL." : ".mysql_error());
				
				$maxtime= mysql_fetch_assoc($StrainLiberaryMaxSQL);
				$unix_time = $maxtime['unixTime'];
		
		if(mysql_num_rows($StrainLiberarySQL) > 0)
		{
			
			
			while($strainLiberary = mysql_fetch_array($StrainLiberarySQL))
			{
				if($strainLiberary['status']=='1')
				{
					$status='D';
				}
				else
				{
					$status='N';
				}
				//Strain Liberary array
				$StrainLibArray[] =  array(
							  'starin_lib_id'=> $strainLiberary['strain_lib_id'],
							  'strain_name'=>str_replace("\\", "",$strainLiberary['strain_name']),
							  'species'=> str_replace("\\", "",$strainLiberary['species']), 
							  'lineage'=> str_replace("\\", "",$strainLiberary['lineage']),
							  'description'=>base64_encode(str_replace("\\", "",$strainLiberary['apearance'])),
							  'smell'=>str_replace("\\", "",$strainLiberary['smell']),
							  'taste'=>str_replace("\\", "",$strainLiberary['taste']),
							  'reviewCount' => str_replace("\\", "",strainLibReviewsCount(mysql_real_escape_string($strainLiberary['strain_name']))),
							  'status'=>$status
										);// end of array
			}
			// If success everythig is good send header as "OK" and sending Dispensary details in reponse
			$result = array('success' => '1',
							'strain_liberary' => $StrainLibArray,
							'unix_time'=> $unix_time
				);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "1",'strain_liberary' =>array(),'unix_time'=> $unix_time);
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Time is empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>