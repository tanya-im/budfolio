<?php
/*QA
	File Description - File to add bud thought for a user 
	File Name - addBudThoughtAPI.php
	creation date - 7th may
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//converting that json into array
 	$intUserId = $_REQUEST['UserId'];		
	$strBudThought = $_REQUEST['bud_thought'];
 	$intBudThoughtId = $_REQUEST['bud_thought_id'];

	// Input validations
	if(!empty($intUserId))
	{
		$date = date('Y-m-d H:i:s');
		$strValidBudThought = mysql_real_escape_string($strBudThought);
		
		// for Uploading image
		$strValidPictureURL ='';
		
		// for insertiing/updating profile pic
		//if profile picture is sent
		if(isset($_FILES['budThoughtPicture']['name']))
		{
			//taking only the base file name  
			$imagefile = $_FILES['budThoughtPicture']['name'];
			
			//fetch the dimensions of image 
			//with getimagesize method
			list($width,$height,$type,$attr)=getimagesize($_FILES['budThoughtPicture']['tmp_name']);
			
			$upload_path ='../images/budthought/';	
			if($width >1000)
			{
				$upload_path ='../images/budthought_xlarge/';
				$newHeightforOrignal= 480*($height/$width);
				
			}else
			{
						//upload path wil reamin 
						//$upload_path ='../images/uploads/original/';	
						//and height wil reamin as it is
			}
			
			
			// for extracting the extension of the doc/image	  
			$strGetImgName = explode('.',$imagefile);
			
			$fileName = time().$imagefile; //appending file with current time
				  
			$fileTempName_one = $_FILES['budThoughtPicture']['tmp_name'];
			
			$bpath=$upload_path.$fileName;
			
			if(move_uploaded_file($fileTempName_one,$bpath)){
				
				$strValidPictureURL = $fileName;
				
				if($width >1000)
				{
					
					$src_o = '../images/budthought/'.$fileName;
					resize_image($bpath,$src_o,480,$newHeightforOrignal);
				}
				//creating thumbnail of image
				$dest_t='../images/budthought/thumbnail/'.$fileName;
				resize_image($bpath,$dest_t,115,115);
		
			}else
			{
				// If no records such records available 
				$strError = array("success" => "0" ,
				"msg" => "Bud thought image does not uploaded.",
				"img_size"=>$_FILES['budThoughtPicture']['size']);
				$this->response($this->toJson($strError), 204,"application/json");
			}
		 }
		
		if ($intBudThoughtId==0){
			$table_name = "tbl_bud_thought";
			$appendFieldInQuery = "post_date='".$date."'";
		
			$msgPart ="budthought";
			$activityType=2;
			
		}else{
			$table_name = "tbl_comments";
			$appendFieldInQuery = "create_date_time='".$date."', bud_thought_id ='".$intBudThoughtId."'";
			$msgPart ="comment";
			$activityType=3;
			$typeId=$intBudThoughtId;
		}
		
		//SQL query to insert budThought into the Db
		$strAddBudThoughtSQL = " INSERT INTO $table_name 
								 set user_id = '".$intUserId."',
							     bud_thought = '".$strValidBudThought."',
							     picture_url=  '".$strValidPictureURL."',
								 $appendFieldInQuery
								";
								  
		$sql = mysql_query($strAddBudThoughtSQL)or die(mysql_error());//executing strLoginSQL
		
		$budThoughtId = mysql_insert_id();// inserted bud thought id
	
		if($budThoughtId !='')
		{
			$whoMentionUserInComment = getUserName($intUserId);
			$msg = $whoMentionUserInComment." mentioned you in ".$msgPart;
			//@username
			
			$arrayCommentParts = explode('@',$strValidBudThought); 
					
				if(count($arrayCommentParts)>0)
				{
					$arrayUserNames= array();
					// to get the users names			
					foreach($arrayCommentParts as $key=>$val){
						$userName = explode(' ',$val);
						if($userName[0]!=''){
							$arrayUserNames[] = strtolower($userName[0]);
						}
					}
						
					for ($i=0; $i<count($arrayUserNames);$i++)
					{	
					// search all users in DB
					 $strSearchUserIdsSQL ="SELECT user_id  
					 						FROM `users`
										    WHERE 
											user_name = '".$arrayUserNames[$i]."'";

					$resultUserIds = mysql_query($strSearchUserIdsSQL) 
					or die(mysql_error()." ".$strSearchUserIdsSQL);					   					     
					   while($rowUserS= mysql_fetch_assoc($resultUserIds))
					   {
							 $devicetype = getDeviceType($rowUserS['user_id']);
							 $UserWhoLiked = getUserName($intUserId);
                 			
							 $getUserInfoForNotiSQL="SELECT 	
													device_token_for_iphone,
													device_token_for_android 
													FROM users 
													WHERE 
													user_id='".$rowUserS['user_id']."'";
		
							$resNote= mysql_query($getUserInfoForNotiSQL) 
							or die($getUserInfoForNotiSQL." : ".mysql_error());
			
							$NoteUser = mysql_fetch_assoc($resNote);
							
							$checkNotificationSentFlag =0;
							
							if ($intBudThoughtId==0){
								$typeId=$budThoughtId;
							}
							else
							{
								$typeId=$intBudThoughtId;
							}
							
							
							
							
							if($NoteUser['device_token_for_iphone']!='')
							{
								
								sendNotificationToiPhone($rowUserS['user_id'],$msg,'mention in comment',$intUserId,'0',$typeId);
								$checkNotificationSentFlag =1;
							}
							if($NoteUser['device_token_for_android']!='')
							{	
								gcm_send($rowUserS['user_id'],$msg,'mention in comment',$intUserId,'0',$typeId);
								$checkNotificationSentFlag =1;
							}
							
							// store push notification information into DB
							if($checkNotificationSentFlag ==1)
							{
								$date = date('Y-m-d H:i:s');
								$insertPushActivitySQL= "INSERT INTO tbl_push_notification SET
														 user_id='".$rowUserS['user_id']."',
														 sender_user_id='".$intUserId."',
														 type='".$activityType."',
														 type_id='".$typeId."',
														 notification_msg='".$msg."',
														 date_time='".$date."'";
								$resPushActivity = mysql_query($insertPushActivitySQL) or die($insertPushActivitySQL." : ".mysql_error());				
				}
							
							
							
								  
					   }
			 		}// end of for loop	 
				
				}
			    if($msgPart=='comment')
				{
					$devicetype = getDeviceType($intUserId);
					
					$ownerOfBudthought = getBudthoughtOwner($intBudThoughtId);
					
					$whoHasCommented = getUserName($intUserId);
					
					$msg = $whoHasCommented." has commented on your budthought";
					
					$getUserInfoForNotiSQL="SELECT device_token_for_iphone,device_token_for_android 
									        FROM users 
											WHERE user_id='".$ownerOfBudthought."'";
			
					$resNote= mysql_query($getUserInfoForNotiSQL) or die($getUserInfoForNotiSQL." : ".mysql_error());
			
					$NoteUser = mysql_fetch_assoc($resNote);
					
					$checkNotificationSentFlag =0;
					
					if($NoteUser['device_token_for_iphone']!='')
					{
						sendNotificationToiPhone($ownerOfBudthought,$msg,'comment',$intUserId,'0',$intBudThoughtId);
						$checkNotificationSentFlag =1;
						
					}if($NoteUser['device_token_for_android']!='')
					{	
						gcm_send($ownerOfBudthought,$msg,'comment',$intUserId,'0',$intBudThoughtId);
						$checkNotificationSentFlag =1;
					}
					
					// store push notification information into DB
					if($checkNotificationSentFlag ==1)
					{
						$date = date('Y-m-d H:i:s');
						$insertPushActivitySQL= "INSERT INTO tbl_push_notification SET
												 user_id='".$ownerOfBudthought."',
												 sender_user_id='".$intUserId."',
												 type='4',
												 type_id='".$intBudThoughtId."',
												 notification_msg='".$msg."',
												 date_time='".$date."'";
						$resPushActivity = mysql_query($insertPushActivitySQL) or die($insertPushActivitySQL." : ".mysql_error());				
					}

				  }// end of else if 
				  
				  $getRecentBudthoughtIdSql="SELECT bud_thought_id FROM `tbl_bud_thought` WHERE `user_id`='".$intUserId."'order by bud_thought_id desc limit 1 ";
				  $getRecentBudthoughtIdRes=mysql_query($getRecentBudthoughtIdSql) or die($getRecentBudthoughtIdSql." : ".mysql_error());
				  $getRecentBudthoughtId=mysql_fetch_assoc($getRecentBudthoughtIdRes);
			
			//@username
			// start of at rate user
			$atRateUsersArray = getAtRateUsers($strBudThought);
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1","picture_url" => BASEURL.BTIMGPATH.$strValidPictureURL,"atRateUsersList"=>$atRateUsersArray,"userId" =>$intUserId,"budthoughtId" =>$getRecentBudthoughtId['bud_thought_id']);
			$this->response($this->toJson($result),200,"application/json");

		} // closing mysql_num_rows
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "Bud thought not inserted.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	} //end of input validations
	else
	{
		// If either username or passowrd is empty
		$error = array('success' => "0", "msg" => "Please enter user id.");
		$this->response($this->toJson($error), 400,"application/json");
	}
?>