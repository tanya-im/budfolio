<?php
/*
	File Description - Lounge list (for all users)
	File Name - loungeListAPI.php
*/

	// Cross validation if the request method is POST 
	//else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array

	$intId = $inputArray['id'];// it can be strain id or bud thught id depends on type value if type==1 ten strain else budthought
	$intType = $inputArray['type'];
	// Input validations
	if(!empty($intId))
	{
			if($intType==1)// if type ==1 then give strain details
			{
					//get strain details
					$strGetStrainDetailsSQL = "SELECT * 
													   FROM strains 
													   where 
													   strain_id='".$intId."' 
													   LIMIT 0,1";
					$resGetStrainDetails =mysql_query($strGetStrainDetailsSQL) or die ($strGetStrainDetailsSQL." : ".mysql_error());						
					$strainInfo = mysql_fetch_assoc($resGetStrainDetails);				
											
					$ownerImage= getUserImagePath($strainInfo['user_id']);
					if($ownerImage!='')
					{
								$ownerImgPath = BASEURL."images/profileimages/thumbnail/".$ownerImage;
								$ownerImgOriginal=BASEURL."images/profileimages/original/".$ownerImage;
					}
					else
					{
							$ownerImgPath ='';
							$ownerImgOriginal='';
					}
					$primaryimages = GetPrimaryImages($intId);
						
					if(!empty($primaryimages[0]))
					{
									$img = $primaryimages[0];
									$imgThumb =  $primaryimages[1];
									$isImage ='1';
					}else
					{
								$img="";
								$imgThumb = "";
								$isImage='0';
									
					}

					if($strainInfo['dispensary_id']=='0')
					{
						$dispensaryName = $strainInfo['dispensary_name'];
					}else
					{
						$dispensaryName = getDispensaryName($strainInfo['dispensary_id']);
					}
						
					$strainPostdate = funTimeAgo(strtotime($strainInfo['post_date']));
						
					$likeCount = getLikeCountStrain($strainInfo['strain_id']);

					$likeFlag = isUserLikeStrain($strainInfo['strain_id'],$intUserId);
							
					$encodedStarinNAme =base64_encode(str_replace("\\", "",$strainInfo['strain_name']));
					$encodedDispensaryName =base64_encode(str_replace("\\", "",$dispensaryName));
						
					// Get user detail who has posted the bud thought
					$ownerdetail = GetUserDetail($strainInfo['user_id']);
							
					$arrayLoungeListArray[]=array(
									'StrainId'=> $strainInfo['strain_id'],
									'StrainName'=> $encodedStarinNAme, 
									'Dispensory'=> $encodedDispensaryName,
									'Species'=> str_replace("\\", "",$strainInfo['species']),
									'OverallRating'=> $strainInfo['overall_rating'],
									'StrainsImageUrl'=>$img!=''?$img:'',
									'StrainsThumbImageUrl'=> $imgThumb!=''?$imgThumb:'',
									'UserId'=>$strainInfo['user_id'],
									'UserName'=>str_replace("\\", "",$ownerdetail['UserName']),
									'ZipCode'=>$ownerdetail['ZipCode'],
									'DispensaryId'=> ($strainInfo['dispensary_id']!=''?(string)$strainInfo['dispensary_id']:'0'),
									'LikeCount' => $likeCount,
									'LikeArray'=>  getLikeStrainUserList($strainInfo['strain_id']),
									'likeFlag' =>$likeFlag,
									'pictureUrl'=>$ownerImgPath,
									'strain_post_date'=>$strainPostdate,
									'isImage'=>$isImage,
									'type'=>'1'
									);// end of array
			}
			else//type=0 -> then bud thought deatils
 			{
					//get budthought Details 
					$strBUdThoughtInfoSQL ="SELECT * FROM tbl_bud_thought
											WHERE bud_thought_id='".$intId."'
											limit 0,1";
					$resBUdThoughtInfo = mysql_query($strBUdThoughtInfoSQL) or die($strBUdThoughtInfoSQL." : ".mysql_error());			
							
					$budThoughtInfo = mysql_fetch_assoc($resBUdThoughtInfo);
								
					$budThoughtPOstDate = 	funTimeAgo(strtotime($budThoughtInfo['post_date']));
						
					if(!empty($budThoughtInfo['picture_url']))
					{
							$budThoughtURL = BASEURL."images/budthought/".$budThoughtInfo['picture_url'];
					  		$isImage ='1';
					}else
					{
							$budThoughtURL= "";
							$isImage ='0';
					}

					if($intUserId!=0)
					{
							$likeFlag = isUserLiked($intId,$intUserId);
					}else
					{
							$likeFlag = 0;
					}
							
					$likeArray = getLikeBudThoughtUserList($budThoughtInfo['bud_thought_id']);
					$likeCount = getLikeCount($intId);
					$commentCount = getCommentCount($intId);
					$atRateUsersArray = getAtRateUsers($budThoughtInfo['bud_thought']);
							
					// Get user detail who has posted the bud thought
					$ownerdetail = GetUserDetail($budThoughtInfo['user_id']);
					$ownerImage= getUserImagePath($budThoughtInfo['user_id']);
					if($ownerImage!='')
					{
							$ownerImgPath = BASEURL."images/profileimages/thumbnail/".$ownerImage;
							$ownerImgOriginal=BASEURL."images/profileimages/original/".$ownerImage;
					}
							
					$arrayLoungeListArray[]=array(
												  'bud_thought_id' => $intId,
												  'BudThought' =>base64_encode(str_replace("\\", "",$budThoughtInfo['bud_thought'])),
												  'UserId' => $budThoughtInfo['user_id'],
												  'UserName' => str_replace("\\", "",$ownerdetail['UserName']),
												  'pictureUrl' => $ownerImgPath!=''?$ownerImgPath:'',
												  'thumbnailUrl'=> $ownerImgOriginal!=''?$ownerImgOriginal:'',
												  'LikeCount' => $likeCount,
												  'CommentCount'=> $commentCount,
												  'likeFlag' =>$likeFlag,
												  'budThoughtUrl'=> $budThoughtURL,
												  'LikeArray'=>$likeArray,
												  'CommentList'=>getCommentList($intId),
												  'isImage'=>$isImage,
												  'bud_thought_post_date'=>$budThoughtPOstDate,
												  'atRateUsersList'=>$atRateUsersArray,
												  'type'=>'0'
											);// end of array 
						
			}// end of else i.e. budhtought
			
			// If success everythig is good send header as "OK" and LoungeList in reponse
			$result = array('success' => '1','detail' => $arrayLoungeListArray);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
	}//end of if(!empty($intStrainId))
	else
	{
			// Empty Lounge List
			$error = array('success' => "0", "msg" => "Empty ID.");
			$this->response($this->toJson($error), 400,"application/json");
	}

?>