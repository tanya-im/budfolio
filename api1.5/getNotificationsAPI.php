<?php
/*
	File Description - budthought list and comment list if user is mentioned in it 
	File Name - notificationsAPI.php
	creation date - 25th june
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	$dataJson = file_get_contents("php://input"); //getting json input 
	$inputArray = json_decode($dataJson,true); //converting that json into array
	//converting that json into array
 	$intUserId = $inputArray['UserId'];		
	
	// Input validations
	if(!empty($intUserId))
	{
		$userName = getUserName($intUserId);
		// search bud thought if user is mentioned in it 
		$strBudThoughtSQL ="SELECT * FROM `tbl_bud_thought`
							WHERE bud_thought LIKE '%".$userName."%' 
							AND delete_status='0'
							order by post_date desc";

		//execute query
		$resultBudThoughts = mysql_query($strBudThoughtSQL) or die(mysql_error()." ".$strBudThoughtSQL);					   					     
		while($bt = mysql_fetch_assoc($resultBudThoughts))
		{
			   $date = funTimeAgo(strtotime($bt['post_date']));
			   $likeCount = getLikeCount($bt['bud_thought_id']);
			   $commentCount = getCommentCount($bt['bud_thought_id']);
			   $likeFlag = isUserLiked($bt['bud_thought_id'],$intUserId);
			   // start of at rate user
			   $atRateUsersArray = getAtRateUsers($bt['bud_thought']);
			   
			   if($bt['picture_url']!='')
			   {
					 $imgURL = BASEURL.BTIMGPATH.$bt['picture_url'];
					 $thumbURL = BASEURL.BTTIMGPATH.$bt['picture_url'];
					 $isImage ='1';
			   }
			   else
			   {
					$imgURL = '';
					$thumbURL ='';
					$isImage ='0';
			   }


				$getPathSQL = "SELECT photo_url 
							   FROM users 
							   WHERE user_id='".$bt['user_id']."'";
				$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());

				$row = mysql_fetch_assoc($response);
 
				if(!empty($row['photo_url'])){
	
							$userImgPath = $row['photo_url'];
				}else
				{
						 $userImgPath = '';
				}

				 
				$arrayList[]= array(
								'type'=>'0',
								'bud_thought_id' => $bt['bud_thought_id'],
								'BudThought' 	 => base64_encode(str_replace("\\", "",$bt['bud_thought'])), 
								'pictureUrl'     => $userImgPath!=''?BASEURL."images/profileimages/original/".$userImgPath:'',
								'thumbnailUrl'   =>$userImgPath!=''?BASEURL."images/profileimages/thumbnail/".$userImgPath:'',//for thumbnail url on 21st june
								'UserId'         => $bt['user_id'],
								'UserName'       => str_replace("\\", "",getUserName($bt['user_id'])),
								'LikeCount'      => $likeCount,
								'CommentCount'   => $commentCount,
								'likeFlag'       => $likeFlag,
							    'date'           => $date,
								'budThoughtUrl'  => $imgURL,
								'LikeArray'=>getLikeBudThoughtUserList( $bt['bud_thought_id']),
								'CommentList'=>getCommentList( $bt['bud_thought_id']),
								'isImage'=>$isImage,
								'atRateUsersList'=>str_replace("\\", "",$atRateUsersArray)
				); 
		}//end of while
						
		//search username in comments of bud thought
		$strCommentSQL = "SELECT *
						  FROM `tbl_bud_thought`
						  WHERE bud_thought_id
						  IN (
						  
						  SELECT bud_thought_id
						  FROM tbl_comments
						  WHERE `bud_thought` LIKE '%".$userName."%'
						  )
						  and delete_status='0'";

		$resComment = mysql_query($strCommentSQL) or die($strCommentSQL." : ".mysql_error());
					 
		while($cmnt = mysql_fetch_assoc($resComment))
		{
			$date = funTimeAgo(strtotime($cmnt['post_date']));//conevrting date time into string format
			$likeCount = getLikeCount($cmnt['bud_thought_id']);
			$commentCount = getCommentCount($cmnt['bud_thought_id']);
			$likeFlag = isUserLiked($cmnt['bud_thought_id'],$intUserId);
			
			// start of at rate user
			$atRateUsersArray = getAtRateUsers($cmnt['bud_thought']);
			
				  
				   if($cmnt['picture_url']!='')
				   {
					   $imgURL = BASEURL.$budThoughtImgPath.$cmnt['picture_url'];
					   $isImage='1';
				   }
				   else
				   {
					  $imgURL = '';
					   $isImage='0';
				   }
				   
				$getPathSQL = "SELECT photo_url 
							   FROM users 
							   WHERE user_id='".$cmnt['user_id']."'";
				$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());

				$row = mysql_fetch_assoc($response);
 
				if(!empty($row['photo_url'])){
	
							$userImgPath = $row['photo_url'];
				}else
				{
						 $userImgPath = '';
				}
				   
				  $arrayList[]= array(
					  		 	'type'=>'0',
				  				'bud_thought_id' => $cmnt['bud_thought_id'],
								'BudThought' 	 => base64_encode(str_replace("\\", "",$cmnt['bud_thought'])), 
							    'pictureUrl'     => $userImgPath!=''?BASEURL."images/profileimages/original/".$userImgPath:'',
								'thumbnailUrl'   =>$userImgPath!=''?BASEURL."images/profileimages/thumbnail/".$userImgPath:'',//for thumbnail url on 21st june
								'UserId'         => $cmnt['user_id'],
								'UserName'       => str_replace("\\", "",getUserName($cmnt['user_id'])),
								'LikeCount'      => $likeCount,
								'CommentCount'   => $commentCount,
								'likeFlag'       => $likeFlag,
							    'date'           => $date,//getUserName($cmnt['user_id']).' / '.
								'budThoughtUrl'	 => $imgURL,
								'LikeArray'=>getLikeBudThoughtUserList( $cmnt['bud_thought_id']),
								'CommentList'=>getCommentList( $cmnt['bud_thought_id']),
								'isImage'=>$isImage,
								'atRateUsersList'=>str_replace("\\", "",$atRateUsersArray)
								
				  ); 
		}//end of while comment
		if(count($arrayList)>0)
		{
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1",'LoungeList'=>$arrayList);
			$this->response($this->toJson($result),200,"application/json");
				 
		}// end of if(count($arrayUserNames) >0)	 
		else
		{
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1",'LoungeList'=>array());
			$this->response($this->toJson($result),200,"application/json");
		}
	} // closing mysql_num_rows
	else
	{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "User id is empty.");
			$this->response($this->toJson($strError), 204,"application/json");
	}	
	
?>