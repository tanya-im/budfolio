<?php
/*
	File Description - Menu Follow Up List
	File Name - FollowupMenuListAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
			
	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];
	$intLat = $inputArray['latitude'];
	$intLong =$inputArray['longitude'];
			
	// Input validations
	if(!empty($intUserId) && !empty($intPageSize) && !empty($intPageNo))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		
		// Check user id is valid or not		
		$uservalidation=CheckValidUser($intValidUserId);
		if($uservalidation)
		{
			// Get no of records			
			$strstrainnoQuery="select Count(*) As Total from menu_followup where user_id='".$intValidUserId."' ";
			$strstrainnoSql = mysql_query($strstrainnoQuery);
			$strstrainnoResult=mysql_fetch_array($strstrainnoSql);
			$strstrainnoData=$strstrainnoResult[0];
			$totalPages = ceil($strstrainnoData / $intPageSize);
			
			// Set start and end limit			
			$perpage=$intPageSize;
			$page=$intPageNo;
			$calc = $perpage * $page;
			$start = $calc - $perpage;
				
			//SQL query to get menu followup list
			$arrayDispensaryListArray=array();
			$strFollowupListQuery = "SELECT * FROM menu_followup 
									 WHERE user_id='".$intValidUserId."' 
									 order by id desc Limit $start, $perpage";
			$FollowupListSQL = mysql_query($strFollowupListQuery) or die(mysql_error());
			if(mysql_num_rows($FollowupListSQL) > 0)
			{
				while($FollowupListData=mysql_fetch_array($FollowupListSQL))
				{
					// Get dispensary detail					
					$strDispensaryListQuery = "SELECT 
													dispensary_id,
													dispensary_name,
												    city,
													state,
													zip,
													image,
													phone_number,
													added_by ,
													latitude,
													longitude,
													bio,
													street_address
					FROM dispensaries WHERE flag='Active' 
					and dispensary_id='".$FollowupListData['dispensary_id']."' 
					group by dispensary_name
					order by dispensary_name ";
					$DispensaryListSQL = mysql_query($strDispensaryListQuery) or die(mysql_error());
					if(mysql_num_rows($DispensaryListSQL) > 0)
					{
						while($DispensaryList=mysql_fetch_array($DispensaryListSQL))
						{
							
							$distance = ceil(get_distance($intLat,$intLong,$DispensaryList['latitude'],$DispensaryList['longitude'],'M'));
							
							if($DispensaryList['added_by']!=0){
					$getUserSettings ="SELECT * FROM dispensary_settings 
									   WHERE dispensary_id=
									   '".$DispensaryList['dispensary_id']."'";

				  $getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());
	  
				  $settingData = mysql_fetch_assoc($getSettingsResponse);
				   $settingArray=array(
'DeliveryService'=>$settingData['DeliveryService']!=''?$settingData['DeliveryService']:'0',
'StoreFront'=>$settingData['StoreFront']!=''?$settingData['StoreFront']:'0',
'AcceptCreditCard'=>$settingData['AcceptCreditCard']!=''?$settingData['AcceptCreditCard']:'0',
'AcceptATMonSite'=>$settingData['AcceptATMonSite']!=''?$settingData['AcceptATMonSite']:'0',
'18YearsOld'=>$settingData['18YearsOld']!=''?$settingData['18YearsOld']:'0',
'21YearsOld'=>$settingData['21YearsOld']!=''?$settingData['21YearsOld']:'0',
'HandicapAssesseble'=>$settingData['HandicapAssesseble']!=''?$settingData['HandicapAssesseble']:'0',
'Security'=>$settingData['Security']!=''?$settingData['Security']:'0'	
										  );
					
					}else
					{
						$settingArray=array(
											  'DeliveryService'=>'0',
											  'StoreFront'=>'0',
											  'AcceptCreditCard'=>'0',
											  'AcceptATMonSite'=>'0',
											  '18YearsOld'=>'0',
											  '21YearsOld'=>'0',
											  'HandicapAssesseble'=>'0',
											  'Security'=>'0'
										  );
					}
							
							
					// Set image url 
				if(!empty($DispensaryList['image']))
				{
				$DispensaryImageUrl=BASEURL.$dispensaryimagefoldername_original.$DispensaryList['image'];
		$DispensaryThumbImageUrl=BASEURL.$dispensaryimagefoldername_thumbnail.$DispensaryList['image'];	
				}
				else
				{
					$DispensaryImageUrl='';
					$DispensaryThumbImageUrl='';
				}
				
				$atRateUsersArray = getAtRateUsers($DispensaryList['bio']);
					
				$arrayDispensaryListArray[] = array(
									'DispensaryId'=> $DispensaryList['dispensary_id'],	
									'DispensaryName'=> base64_encode(str_replace("\\", "",$DispensaryList['dispensary_name'])), 													
									'City'=> base64_encode(str_replace("\\", "",$DispensaryList['city'])),
									'State'=> base64_encode(str_replace("\\", "",$DispensaryList['state'])),
									'ZipCode'=> $DispensaryList['zip'],
									'flag'=>1,
									'DispensaryImageUrl'=> $DispensaryImageUrl,
									'DispensaryThumbImageUrl'=>$DispensaryThumbImageUrl,
									'PhoneNumber'=>$DispensaryList['phone_number'],
									'settings'=>$settingArray,
									'distance'=>$distance,
									'lat'=>$DispensaryList['latitude'],
									'long'=>$DispensaryList['longitude'],
									'StreetAddress'=>base64_encode(str_replace("\\", "",$DispensaryList['street_address'])),
									
									'Bio'=>base64_encode(str_replace("\\", "",$DispensaryList['bio'])),
		 							'atRateUsersList'=>$atRateUsersArray
		);// end of array
						}
					}
				}
				// If success everythig is good send header as "OK" and  MenuList in reponse
				$result = array('success' => '1',
								'MenuDispensaryList' => $arrayDispensaryListArray,
								'TotalPageNo'=>$totalPages);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// Empty Menu List
				$error = array('success' => "0", "msg" => "Empty Menu List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>