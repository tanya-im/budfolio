<?php
/*
	File Description - Follow UnFollow Strain
	File Name - FollowUpLoungeAPI.php
*/

    // Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST"){
		$this->response('Invalid request',406);
	}
    
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	//print_r($inputArray);		
	$intUserId = $inputArray['UserId'];
	$intFollowerId = $inputArray['FollowerId'];
	$intFlag = $inputArray['Flag'];		
	
	// Input validations
	if(!empty($intUserId) && !empty($intFollowerId))
	{
		$intValidUserId = mysql_real_escape_string($intUserId);
		$intFollowerId = mysql_real_escape_string($intFollowerId);
		$intValidFlag = mysql_real_escape_string($intFlag);
		
		//SQL query to check the  USER exists or not
		$strUserNameSQL = "SELECT user_name 
						   FROM users 
						   WHERE user_id ='".$intValidUserId."'";
		$sql = mysql_query($strUserNameSQL)or die(mysql_error());//executing strUserNameSQL
		if(mysql_num_rows($sql) > 0)
		{
			$CheckStrainIdQuery = "SELECT user_name 
								   FROM users 
								   WHERE user_id ='".$intFollowerId."'";
			$CheckStrainIdSql = mysql_query($CheckStrainIdQuery)or die(mysql_error());
			if(mysql_num_rows($CheckStrainIdSql) > 0)
			{
				if($intFlag==1)
				{
					$CheckFollowupQuery = "select * from user_followup 
										   where follower_id ='".$intFollowerId."' 
										   and user_id ='".$intValidUserId."'";
					$CheckFollowupSql = mysql_query($CheckFollowupQuery) or die(mysql_error());
					$RecordStatus = mysql_num_rows($CheckFollowupSql);
					if($RecordStatus==0)
					{
						$FollowupQuery = "insert into user_followup
										  (
										  user_id,
										  follower_id
										  )
										  values
										  (
										  '".$intValidUserId."',
										  '".$intFollowerId."'
										  )";
										  
						$FollowupSql = mysql_query($FollowupQuery)or die(mysql_error());
						
						$followerName = getUserName($intValidUserId);
						
						$msg = $followerName." has started following you";
						
						$getUserInfoForNotiSQL="SELECT device_token_for_iphone,device_token_for_android 
												FROM users 
												WHERE user_id='".$intFollowerId."'";
			
						$resNote= mysql_query($getUserInfoForNotiSQL) or die($getUserInfoForNotiSQL." : ".mysql_error());
			
						$NoteUser = mysql_fetch_assoc($resNote);
						
						$checkNotificationSentFlag =0;
						
						if($NoteUser['device_token_for_iphone']!='')
						{
								sendNotificationToiPhone($intFollowerId,$msg,'follow',$intValidUserId,'2','');
								$checkNotificationSentFlag =1;
						}
						if($NoteUser['device_token_for_android']!='')
						{	
							gcm_send($intFollowerId,$msg,'follow',$intValidUserId,'2','');
							$checkNotificationSentFlag =1;
						}
						
						// store push notification information into DB
						if($checkNotificationSentFlag ==1)
						{
							$date = date('Y-m-d H:i:s');
							$insertPushActivitySQL= "INSERT INTO tbl_push_notification SET
													 user_id='".$intFollowerId."',
													 sender_user_id='".$intValidUserId."',
													 type='5',
													 type_id='".$intValidUserId."',
													 notification_msg='".$msg."',
													 date_time='".$date."'";
							$resPushActivity = mysql_query($insertPushActivitySQL) or die($insertPushActivitySQL." : ".mysql_error());				
						}
						
						
						
						
					}
					//if every thing looks good then success=1 and user id
					$result = array( "success" => "1", 'UserId' => $intValidUserId,"Status"=>"Follow");
					$this->response($this->toJson($result),200,"application/json");
				}
				else
				{
					$FollowupQuery = "delete FROM user_followup 
									  WHERE user_id ='".$intValidUserId."' 
									  and follower_id='".$intFollowerId."'";
					$FollowupSql = mysql_query($FollowupQuery)or die(mysql_error());
					
					// if reverting like from bud thought then 
					//it should get delete from push notification table as well
					$removePushActivitySQL= "DELETE FROM tbl_push_notification WHERE
											 user_id='".$intFollowerId."' AND
											 sender_user_id='".$intValidUserId."' AND
											 type='5' AND
											 type_id='".$intValidUserId."' AND
											 notification_msg='".$msg."'";
					$resPushActivity = mysql_query($removePushActivitySQL) or die($removePushActivitySQL." : ".mysql_error());
					
					
					
					
					//if every thing looks good then success=1 and user id
					$result = array( "success" => "1", 'UserId' => $intValidUserId,"Status"=>"UnFollow");
					$this->response($this->toJson($result),200,"application/json");
				}
			}
			else
			{
				// If Invalid strain id
				$error = array('success' => "0", "msg" => "Invalid Follower id.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		}
		else
		{
			// If Invalid uesr id
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If UserId or pageSize or PageNo empty
		$error = array('success' => "0", "msg" => "UserId or FollowerId  empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>