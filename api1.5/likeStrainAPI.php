<?php
/*
	File Description - File to like a Strain
	File Name - likeStrainAPI.php
	creation date - 14th june
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true);
	

	//converting that json into array
	 $intUserId = $inputArray['UserId'];
	 $intStrainId = $inputArray['StrainId'];
	$intFlag = $inputArray['Flag'];//1 for liking and 2 for revert liking

	$alreadyLikeByUser = isUserLikeStrain($intStrainId,$intUserId);
	
	if($alreadyLikeByUser == 0)//if user is liking strain first time
	{	
	  //SQL query to like budThought  if flag ==1
	  $strLikeStrainSQL = " INSERT INTO tbl_strain_like
							set user_id = '".$intUserId."',
							strain_id = '".$intStrainId."'";
		//echo $strLikeStrainSQL;
				 $ownerUserId =getStrainOwner($intStrainId);
                 $devicetype = getDeviceType($ownerUserId);
				 $UserWhoLiked = getUserName($intUserId);
                 $msg = $UserWhoLiked." liked your strain";

			 $getUserInfoForNotiSQL="SELECT device_token_for_iphone,device_token_for_android 
									FROM users WHERE user_id='".$ownerUserId."'";
			$resNote= mysql_query($getUserInfoForNotiSQL) or die($getUserInfoForNotiSQL." : ".mysql_error());
			
			$NoteUser = mysql_fetch_assoc($resNote);
			
			$checkNotificationSentFlag =0;
			
			if($NoteUser['device_token_for_iphone']!='')
			{
					sendNotificationToiPhone($ownerUserId,$msg,'like strain',$intUserId,'1',$intStrainId);
					$checkNotificationSentFlag =1;
			}
			if($NoteUser['device_token_for_android']!='')
			{	
				gcm_send($ownerUserId,$msg,'like strain',$intUserId,'1',$intStrainId);
				$checkNotificationSentFlag =1;
			}
			
			// store push notification information into DB
			if($checkNotificationSentFlag ==1)
			{
				$date = date('Y-m-d H:i:s');
				$insertPushActivitySQL= "INSERT INTO tbl_push_notification SET
										 user_id='".$ownerUserId."',
										 sender_user_id='".$intUserId."',
										 type='1',
										 type_id='".$intStrainId."',
										 notification_msg='".$msg."',
										 date_time='".$date."'";
				$resPushActivity = mysql_query($insertPushActivitySQL) or die($insertPushActivitySQL." : ".mysql_error());				
			}

	}else
	{
		// removing like from the bud thought and like entry from the table will get delete 
		$strLikeStrainSQL = "DELETE FROM tbl_strain_like WHERE 
							 strain_id = '".$intStrainId."' 
							 AND user_id = '".$intUserId."'";
							 
							 
		// if reverting like from bud thought then 
		//it should get delete from push notification table as well
		$removePushActivitySQL= "DELETE FROM tbl_push_notification WHERE
											 user_id='".$ownerUserId."' AND
											 sender_user_id='".$intUserId."' AND
											 type='1' AND
											 type_id='".$intStrainId."' AND
											 notification_msg='".$msg."'";
		$resPushActivity = mysql_query($removePushActivitySQL) or die($removePushActivitySQL." : ".mysql_error());					 
							 
		
	}
	
	 	$sql = mysql_query($strLikeStrainSQL) or die($strLikeStrainSQL." : ".mysql_error());//executing $strLikeBudThoughtSQL

		if($sql == 1)
		{
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1");
			$this->response($this->toJson($result),200,"application/json");

		} // end of if ($sql == 1)
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "Unable to like strain.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	
?>