<?php
/*
	File Description - get the details of starinLib Details and user list
	File Name - starinLibDetailsAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable				
	 $intStarinLibId = $inputArray['starin_lib_id'];
	
	// Input validations
	if(isset($intStarinLibId))
	{
		$strainInfoArray = getStrainLibName($intStarinLibId);
		
		//SQL query to get Strain name used by users 
		$strStrainLiberarySQL = "SELECT strain_id, 
								  		strain_name,
										user_id,
										dispensary_name,
										species,
										linage,
										avg(overall_rating ) as avg_ratting
								  FROM strains WHERE
								  strain_name ='".mysql_real_escape_string($strainInfoArray['strain_name'])."'
								  AND user_id !=  '0'
								  AND user_id
								  IN (
								  	SELECT user_id
								  	FROM users
								  )
								  and flag='active'
								  GROUP BY user_id";

		$resStrainLiberarySQL = mysql_query($strStrainLiberarySQL) or die($strStrainLiberarySQL." : ".mysql_error());
		
		$StrainLibArray=array();
		
		while($strainLiberary = mysql_fetch_array($resStrainLiberarySQL))
		{
				//Strain Liberary array
				$StrainLibArray[] =  array(
  'user_id'=> $strainLiberary['user_id'],
  'user_name'=>str_replace("\\", "", stripslashes(getUserName($strainLiberary['user_id']))),
  'dispensary_name'=>str_replace("\\", "", $strainLiberary['dispensary_name']),
  'ratting'=>$strainLiberary['avg_ratting']!=''?$strainLiberary['avg_ratting']:'0'
										);// end of array
		}
		// If success everythig is good send header as "OK" and sending Dispensary details in reponse
			$result = array('success' => '1',
							'strain_name' =>str_replace("\\", "",$strainInfoArray['strain_name']),
							'species' => str_replace("\\", "",$strainInfoArray['species']),
							'lineage' => base64_encode(str_replace("\\", "",$strainInfoArray['lineage'])),
							'apearance'=>$strainInfoArray['apearance']!=''?base64_encode(str_replace("\\", "",$strainInfoArray['apearance'])):'',
							'smell'=>$strainInfoArray['smell']!=''?str_replace("\\", "",$strainInfoArray['smell']):'',
							'taste'=>$strainInfoArray['taste']!=''?str_replace("\\", "",$strainInfoArray['taste']):'',
							'users_list'=>$StrainLibArray);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Time is empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>