<?php
/*
	File Description - file to add a strain
	File Name - addstrainAPI.php
*/
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//  Get all values in local varibale	
	$intUserId = trim($inputArray['UserId']);
	$intStrainId = trim($inputArray['StrainId']);
	$strStrainName = trim($inputArray['StrainName']);		
	$strDispensaryName = trim($inputArray['DispensaryName']);
	$strSpecies	= trim($inputArray['Species']);
	$strLinage	= trim($inputArray['Linage']);
	$strProfileType =trim($inputArray['profileType']);
	$strSmellRating = trim($inputArray['SmellRating']);
	$strTasteRate	= trim($inputArray['TasteRate']);
	$strStrengthRate = trim($inputArray['StrengthRate']);
	$strOverallRating	= trim($inputArray['OverallRating']);
	$strExperienceArray	= $inputArray['ExperienceArray'];
	$strConsumption	= trim($inputArray['Consumption']);
	$strMedicinalUseArray	= $inputArray['MedicinalUseArray'];
	$strTestedBy	= trim($inputArray['TestedBy']);
	$strTHC	= trim($inputArray['THC']);
	$strCBD	= trim($inputArray['CBD']);
	$strCBN	= trim($inputArray['CBN']);
	$strTHCa	= trim($inputArray['THCa']);
	$strCBHa	= trim($inputArray['CBHa']);
	$strMoisture	= trim($inputArray['Moisture']);
	$strDescription	= trim($inputArray['Description']);
	$arrayStrainOldImagesArray	=$inputArray['StrainOldImagesArray'];
	$intDispensaryId = $inputArray['DispensaryId'];
	
	// Input validations
	if(!empty($intUserId)&& !empty($intStrainId)&&  !empty($strStrainName))
	{
		// sanitization of values to insert in db
		$strValidStrainName = mysql_real_escape_string($strStrainName);
		$strValidDispensaryName =mysql_real_escape_string($strDispensaryName);
		$strValidSpecies	= mysql_real_escape_string($strSpecies);
		$strValidLinage	=mysql_real_escape_string($strLinage);
		$strValidSmellRating = mysql_real_escape_string($strSmellRating);
		$strValidTasteRate	= mysql_real_escape_string($strTasteRate);
		$strValidStrengthRate = mysql_real_escape_string($strStrengthRate);
		$strValidOverallRating	= mysql_real_escape_string($strOverallRating);
		$strValidExperienceArray	=$strExperienceArray;
		$strValidConsumption	= mysql_real_escape_string($strConsumption);
		$strValidMedicinalUseArray	= $strMedicinalUseArray;
		$strValidTestedBy	= mysql_real_escape_string($strTestedBy);
		$strValidTHC	= mysql_real_escape_string($strTHC);
		$strValidCBD	= mysql_real_escape_string($strCBD);
		$strValidCBN	= mysql_real_escape_string($strCBN);
		$strValidTHCa	= mysql_real_escape_string($strTHCa);
		$strValidCBHa	= mysql_real_escape_string($strCBHa);
		$strValidMoisture	= mysql_real_escape_string($strMoisture);
		$strValidDescription	= mysql_real_escape_string($strDescription);
		
		// to check whether the user id is exist.
		$strSQLChkUserId ="SELECT user_id 
						   FROM users 
						   WHERE user_id='".$intUserId."'";
		$SQLResChkUserId = mysql_query($strSQLChkUserId);// execution of query strSQLChkUserId
		if(mysql_num_rows($SQLResChkUserId)< 1)
		{
			// If no records "Error msg" status
			$error = array('success' => "0", "msg" => "Invalid user id.");
			$this->response($this->toJson($error), 406, "application/json");
		}
		
		// to check whether the strain id is exist.
		$strSQLChkStrainId ="SELECT strain_name FROM strains WHERE strain_id='".$intStrainId."'";
		$SQLResChkStrainId = mysql_query($strSQLChkStrainId);// execution of query strSQLChkStrainId
		if(mysql_num_rows($SQLResChkStrainId)< 1)
		{
			// If no records "Error msg" status
			$error = array('success' => "0", "msg" => "Invalid strain id.");
			$this->response($this->toJson($error), 406, "application/json");
		}
		// Sql to update strain detail		
		$strUpdateStrainQuery ="update strains set 
								strain_name='".$strValidStrainName."',
								dispensary_name='".$strValidDispensaryName."', 
								species='".$strValidSpecies."',
								linage='".$strValidLinage."',
								smell_rating='".$strValidSmellRating."',
								taste_rate='".$strValidTasteRate."',
								strength_rate='".$strValidStrengthRate."',
								overall_rating='".$strValidOverallRating."',
								consumption_name='".$strValidConsumption."',
								tested_by='".$strValidTestedBy."',
								thc='".$strValidTHC."',
								cbd='".$strValidCBD."',
								cbn='".$strValidCBN."',
								thca='".$strValidTHCa."',
								cbha='".$strValidCBHa."',
								moisture='".$strValidMoisture."',
								description='".$strValidDescription."',
								dispensary_id='".$intDispensaryId."'";
								if($strProfileType!='')
								{
									$strUpdateStrainQuery.=",profile_type='".$strProfileType."' ";
								}

					$strUpdateStrainQuery.=" where flag='Active' and 
								user_id='".$intUserId."' and
								strain_id='".$intStrainId."'";
		$SQLUpdateStrain = mysql_query($strUpdateStrainQuery)or die($strUpdateStrainQuery." :".mysql_error());
		$StrainId = $intStrainId; //fetch last inserted strain id
		if($SQLUpdateStrain == 1 && !empty($StrainId))
		{ 
			if(is_array($strValidExperienceArray))
			{ 
				// Deleting old  Experience values.
				$strDeleteExperienceQuery="delete from experience 
										   where strain_id='".$StrainId."'";
				$SQLDeleteExperience=mysql_query($strDeleteExperienceQuery)or die(mysql_error());
				
				for($i=0;$i<count($strValidExperienceArray);$i++)
				{
					// Adding  Experience
					$strAddExperienceQuery="insert into experience
											(
												experience_name,
												strain_id
											)
											values(
											'".mysql_real_escape_string($strValidExperienceArray[$i])."',
											'".$StrainId."'
											)";
					$SQLAddExperience=mysql_query($strAddExperienceQuery)or die(mysql_error());
				}
			}
			if(is_array($strValidMedicinalUseArray))
			{
				// Deleting old  Medicinal use values.
				$strDeleteMedicinalUseQuery="delete from medicinal_use 
											 where strain_id='".$StrainId."'";
				$SQLDeleteMedicinalUse=mysql_query($strDeleteMedicinalUseQuery)or die(mysql_error());
				for($i=0;$i<count($strValidMedicinalUseArray);$i++)
				{
					// Adding  Medicinal use
					$strAddMedicinalUseQuery="insert into medicinal_use
											(medicinal_type,
											strain_id
											)values(
											'".mysql_real_escape_string($strValidMedicinalUseArray[$i])."',
											'".$StrainId."')";
					$SQLAddMedicinalUse=mysql_query($strAddMedicinalUseQuery)or die(mysql_error());
				}
			}
			$strDeleteStrainImagesQuery="delete from strainimages 
			where strain_id='".$StrainId."'";
			$SQLDeleteStrainImages=mysql_query($strDeleteStrainImagesQuery)or die(mysql_error());
			
			//if strain add is successfull then success=1 and user id
			$result = array( "success" => "1", 
							 "StrainId" => $StrainId);
			$this->response($this->toJson($result),200,"application/json");
		}
		else
		{
			$error = array("success" => "0",
						   "msg" => "Strain not added. Please try agian later.");
			// If Strain not added
			$this->response($this->toJson($error), 406, "application/json");
		}
	}
	else
	{
		$error = array('success' => "0", "msg" => "Strain name or Dispensary or Lineage empty.");
		// If Strain not added
		$this->response($this->toJson($error), 406, "application/json");
	}	
		
?>