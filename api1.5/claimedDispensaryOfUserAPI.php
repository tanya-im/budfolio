<?php
/* qa
	File Description - Menu Dispensary list
	File Name - menudispensarylistAPI.php
*/
	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
    
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true);     //converting that json into array

	$intUserId = $inputArray['UserId'];
	$intPageSize = $inputArray['PageSize'];
	$intPageNo = $inputArray['PageNo'];
	$intLat = $inputArray['latitude'];
	$intLong =$inputArray['longitude'];	
	
	if(!empty($intPageSize) && !empty($intPageNo))
	{
	
		//SQL query to get Menu list
		$strDispensaryListQuery = "SELECT 
									dispensary_id,
									dispensary_name,
									city,
									state,
									zip,
									image,
									phone_number,
									added_by,
									latitude,
									longitude,
									bio,
									street_address
									FROM dispensaries 
									WHERE flag='Active'
									AND added_by='".$intUserId."' 
									group by dispensary_name
									order by dispensary_name ";
	
		$arrayDispensaryListArray=array();
		$DispensaryListSQL = mysql_query($strDispensaryListQuery) or die(mysql_error());
				
		$strdispensariesnoData = mysql_num_rows($DispensaryListSQL);
		$totalPages = ceil($strdispensariesnoData / $intPageSize);
				
		// Set strat and end limit			
		$perpage=$intPageSize;
		$page=$intPageNo;
		$calc = $perpage * $page;
		$start = $calc - $perpage;
				
		$strDispensaryListQuery .=" Limit $start, $perpage";
				
		$DispensaryListSQL = mysql_query($strDispensaryListQuery) or die(mysql_error());
		if(mysql_num_rows($DispensaryListSQL) > 0)
		{
				while($DispensaryList=mysql_fetch_array($DispensaryListSQL))
				{
						$settingArray= array();
						$distance = ceil(distance($intLat,$intLong,$DispensaryList['latitude'],$DispensaryList['longitude'],'M'));
						
					if($DispensaryList['added_by']!=0)
					{
							$getUserSettings ="SELECT * FROM dispensary_settings 
											   WHERE dispensary_id=
										       '".$DispensaryList['dispensary_id']."'";
							$getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());
		  
							$settingData = mysql_fetch_assoc($getSettingsResponse);
							$settingArray=array(
	'DeliveryService'=>$settingData['DeliveryService']!=''?$settingData['DeliveryService']:'0',
	'StoreFront'=>$settingData['StoreFront']!=''?$settingData['StoreFront']:'0',
	'AcceptCreditCard'=>$settingData['AcceptCreditCard']!=''?$settingData['AcceptCreditCard']:'0',
	'AcceptATMonSite'=>$settingData['AcceptATMonSite']!=''?$settingData['AcceptATMonSite']:'0',
	'18YearsOld'=>$settingData['18YearsOld']!=''?$settingData['18YearsOld']:'0',
	'21YearsOld'=>$settingData['21YearsOld']!=''?$settingData['21YearsOld']:'0',
	'HandicapAssesseble'=>$settingData['HandicapAssesseble']!=''?$settingData['HandicapAssesseble']:'0',
	'Security'=>$settingData['Security']!=''?$settingData['Security']:'0'	
											  );
						
						}else
						{
								$settingArray=array(
												  'DeliveryService'=>'0',
												  'StoreFront'=>'0',
												  'AcceptCreditCard'=>'0',
												  'AcceptATMonSite'=>'0',
												  '18YearsOld'=>'0',
												  '21YearsOld'=>'0',
												  'HandicapAssesseble'=>'0',
												  'Security'=>'0'
								);
						}
						
						//SQL query to get Followup flag list
						$getfollowflagQuery="SELECT * FROM menu_followup 
											 WHERE 
											 dispensary_id='".$DispensaryList['dispensary_id']."'	
											 AND user_id='".$intValidUserId."'";
						$faldSql=mysql_query($getfollowflagQuery);
						$flagdatacount=mysql_num_rows($faldSql);
						if($flagdatacount>0)
						{
							$followFlag=1;
						}
						else
						{
							$followFlag=0;
						}
						// Set image url.					
						if(!empty($DispensaryList['image']))
						{
						$DispensaryImageUrl=BASEURL.$dispensaryimagefoldername_original.$DispensaryList['image'];
						$DispensaryThumbImageUrl=BASEURL.$dispensaryimagefoldername_thumbnail.$DispensaryList['image'];
						}
						else
						{
							$DispensaryImageUrl='';	
							$DispensaryThumbImageUrl='';
						}
						$FWPflag=getFollowUpList($DispensaryList['dispensary_id'],$intValidUserId);
						
						$atRateUsersArray = getAtRateUsers($DispensaryList['bio']);
						
						// Prepare array for response 					
						$arrayDispensaryListArray[] = array(
						'DispensaryId'=> $DispensaryList['dispensary_id'],
						'DispensaryName'=> base64_encode(str_replace("\\", "",$DispensaryList['dispensary_name'])), 
						'City'=> base64_encode(str_replace("\\", "",$DispensaryList['city'])),
						'State'=> base64_encode(str_replace("\\", "",$DispensaryList['state'])),
						'ZipCode'=> $DispensaryList['zip'],
						'flag'=>$followFlag,
						'DispensaryImageUrl'=> $DispensaryImageUrl,
						'DispensaryThumbImageUrl'=>$DispensaryThumbImageUrl,
						'PhoneNumber'=>$DispensaryList['phone_number'],
						'settings'=>$settingArray,
						'distance'=>$distance,
						'lat'=>$DispensaryList['latitude'],
						'long'=>$DispensaryList['longitude'],
						'StreetAddress'=>base64_encode(str_replace("\\", "",$DispensaryList['street_address'])),
						'Bio'=>base64_encode(str_replace("\\", "",$DispensaryList['bio'])),
		 	            'atRateUsersList'=>$atRateUsersArray
						);
				}// end of while
				
				// If success everythig is good send header as "OK" and Menu list in reponse
				$result = array('success' => '1',
								'TotalPageNo'=>$totalPages,
								'MenuDispensaryList' => $arrayDispensaryListArray);
	
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
		}	
		else
		{
				// If Empty Menu List
				$error = array('success' => "0", "msg" => "No Claimed Dispensary ");
				$this->response($this->toJson($error), 400,"application/json");
	}
	}else
	{
		// UserId or DispensaryId or pageSize or PageNo empty
		$error = array("success" => "0",
		               "msg" => "UserId, pageSize or PageNo empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}
 
?>