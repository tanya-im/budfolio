<?php
/*
	File Description - write a review Api
	File Name - writeAReviewAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array

	//Taking all values into local variable				
	$intDispensaryId = $inputArray['DispensaryId'];
	$intUserid = $inputArray['UserId'];
	$intStrainQuality = $inputArray['strain_quality'];
	$intStrainVariety = $inputArray['strain_variety'];
	$intService = $inputArray['service'];
	$intAtmosphere = $inputArray['atmosphere'];
	$intPrivacy = $inputArray['Privacy'];
	$intPricing = $inputArray['Pricing'];
	
	$arrRadioComments = $inputArray['radio_review'];

	$shopHereAgain = $arrRadioComments[0];
	$recommendedClub = $arrRadioComments[1];
	$firstVisit = $arrRadioComments[2];


	$strReviewText = mysql_real_escape_string($inputArray['review_text']);

	$date = date('Y-m-d H:i:s');
	
	// Input validations
	if(!empty($intDispensaryId))
	{
		$strInsertReviewSQL ="INSERT INTO tbl_dispensary_review set
						 dispensary_id ='".$intDispensaryId."',
						 user_id='".$intUserid."',
						 strain_quality ='".$intStrainQuality."',
						 strain_variety='".$intStrainVariety."',
						 service='".$intService."',
						 atmosphere='".$intAtmosphere."',
						 privacy ='".$intPrivacy."',
						 pricing = '".$intPricing."',
						 rc_shop_here_again='".$shopHereAgain."',
						 rc_recommend_club 	='".$recommendedClub."',
						 rc_first_visit ='".$firstVisit."',
						 review_text='".$strReviewText."',
						 date_time='".$date."'
						";
		$resInsertReview = mysql_query($strInsertReviewSQL) or die($strInsertReviewSQL." : ".mysql_error());
		
		if($resInsertReview)
		{
			//if every thing is ok then send success=1 and strain  id in response.
			$result = array( "success" => "1",'msg'=>" Location review posted successfully");
			$this->response($this->toJson($result),200,"application/json");
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "0", "msg" => "review is not added.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Please select dispensary.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>