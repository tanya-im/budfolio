<?php
/* 
	FileName: api.php 
	Description : RESTFull Apis for Budfolio project
	Create Date : 12 March 2013
*/
ob_start();
date_default_timezone_set('GMT');		
error_reporting(E_ALL^E_WARNING^E_NOTICE);
ini_set("display_errors", 1);  // display errorn on the page set it 0 if wants off
include("apiconstantfile.php");  //include file for loading static constant
require_once("Rest.inc.php"); //including rest class for implementing services
include("function_api.php");//for external function
include('functions.php');//for external function
include('phplatlong.php');
include('../timeAgoUtil.php');
include('checkNotification.php');// for notification
include('budfolioAndriodNotification.php');
include("../values_array.php");//for get values from array	


class budfolio extends REST 
{ 
//create a class which extend basic methods of REST class and provide different methods for different URLs
	public $data = ""; // initialization
	private $db = NULL; //db initialization
	
	public function __construct()
	{
		parent::__construct();				// Init parent contructor
		$this->funDBConnect();				// Initiate Database connection
	}

/*  Private method for Database connection 
	It will use constant file for resources like DB_server etc. */
	private function funDBConnect()
	{
		$this->db = mysql_connect(DB_SERVER,DB_USER,DB_PASSWORD); //code for connection
		if($this->db)
		{
			mysql_select_db(DB,$this->db); //selection of db;
		}
	}//end of function funDBConnect

/* 	Public method for access api.
	This method dynmically call the method based on the query string */
	public function processApi()
	{
		$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
		if((int)method_exists($this,$func) > 0)
			$this->$func();
		else
			$this->response('Page Not Found.',404);				
			// If the method not exist with in this class, response would be "Page not found".
	}

/* 	Registration API
	Registration must be POST method */
	private function registration()
	{
		//including file registrationAPI.php
		include('registrationAPI.php');
	}//end of registration

/* 	login API
	*  Login must be POST method
 	*  username : <USER NAME>
 	*  pwd : <USER PASSWORD> */	
	private function login()
	{
		include("loginAPI.php")	;//including file loginAPI.php
	}//end of function login

/* 	Forgot Password API
 	*  Forgot password must be POST method
 	*  email : <Email Address>	*/	
	private function forgotpassword()
	{
		include("forgotpasswordAPI.php");//including file forgotpasswordAPI.php
	}//end of function forgotpassword	

/*	*  List of Medicinal Use 
 	*  medicinaluse must be POST method
 	*  userid : <UserId>	*/	
	private function medicinaluse()
	{
		global $medicinaluse; // Global variable   
		include("medicinaluseAPI.php")	;//including file medicinaluseAPI.php
	}//end of function medicinaluse		

/* 	*  List of Experience 
 	*  experience must be POST method
 	*  userid : <UserId>	*/	
	private function experience()
	{
		global $experience; // Global variable
		include("experienceAPI.php");//including file experienceAPI.php
	}//end of function experience	

/* 	*  Add Strain 
 	*  addstrain must be POST method
 	*  text or images	*/	
	private function addstrain()
	{
		global $valid_formats,$strainimagefoldername_original,$strainimagefoldername_medium,$strainimagefoldername_thumbnail; // Global variables
		include("addstrainAPI.php")	;//including file addstrainAPI.php
	}//end of function addstrain	

/* 	*  Add Strain Images 
 	*  addstrainimages must be POST method
 	*  text or images	*/	
	private function addstrainimages()
	{
		global $valid_formats,$strainimagefoldername_original,$strainimagefoldername_medium,$strainimagefoldername_thumbnail; // Global variables
		include("addstrainimagesAPI.php");//including file addstrainimagesAPI.php
	}//end of function addstrainimages		

/* 	*  MyBudFolioList 
 	*  mybudfoliolist must be POST method
 	*  text or image 	*/	
	private function mybudfoliolist()
	{
		global $valid_formats,$strainimagefoldername_original,$strainimagefoldername_medium,$strainimagefoldername_thumbnail; // Global variables
		include("mybudfoliolistAPI.php");//including file mybudfoliolistAPI.php
	}//end of function mybudfoliolist	

/* 	*  LoungeList 
 	*  loungelist must be POST method
 	*  text or image 	*/	
	private function loungelist()
	{
		global $valid_formats,$strainimagefoldername_original,$strainimagefoldername_medium,$strainimagefoldername_thumbnail; // Global variables
		include("loungelistAPI.php");//including file loungelistAPI.php
	}//end of function loungelist	
	
/*  *  Local LoungeList 
 	*  localloungelist must be POST method
 	*  text or image 	*/	
	private function localloungelist()
	{
		global $valid_formats,$strainimagefoldername_original,$strainimagefoldername_medium,$strainimagefoldername_thumbnail,$budThoughtImgPath;	// Global variables
		include("LocalLoungelistAPI.php");//including file loungelistAPI.php
	}//end of function loungelist		
	
/*
	*loungeAllUsersAPI.php
	*loungeAllUsers must be POST method
 	*text or image 		
*/	
	private function loungeAllUsers()
	{
		global $valid_formats,$strainimagefoldername_original,$strainimagefoldername_medium,$strainimagefoldername_thumbnail,$budThoughtImgPath; // Global variables
		include("loungeAllUsersAPI.php");//including file loungelistAPI.php
	}
	
/* 	*  Lounge FollowUp List   
 	*  followuploungelist must be POST method	*/	
	private function followuploungelist()
	{
		
		global $dispensaryimagefoldername_original,$dispensaryimagefoldername_medium,$dispensaryimagefoldername_thumbnail,$budThoughtImgPath;			// Global variables
		include("FollowUpLoungeListAPI.php");	//including file FollowUpLoungeListAPI.php
	}//end of function followuploungelist	
	
	
/*  *  StrainDetail 
 	*  straindetail must be POST method
 	*  text or image	*/	
	private function straindetail()
	{
		global $valid_formats,$strainimagefoldername_original,$strainimagefoldername_medium,$strainimagefoldername_thumbnail;	// Global variables
		include("straindetailAPI.php");//including file straindetailAPI.php
	}//end of function straindetail	

/*  *  Update Strain Detail 
 	*  updatestrain must be POST method
 	*  text or image	*/	
	private function updatestrain()
	{
		global $valid_formats,$strainimagefoldername_original,$strainimagefoldername_medium,$strainimagefoldername_thumbnail;	// Global variables
		include("updatestrainAPI.php")	;//including file updatestrainAPI.php
	}//end of function updatestrain	
	
/*  *  Remove Strain  
 	*  removestrain must be POST method
 	*  Strain Id : <strain_id>
	User Id	  : <user id> 		*/	
	private function removestrain()
	{
		include("removestrainAPI.php");//including file updatestrainAPI.php
	}//end of function updatestrain	
		
/*  *  Update Update setting 
 	*  updatesetting must be POST method
 	*  text or image	*/	
	private function showuserprofile()
	{
		global $valid_formats,$userprofileimagefoldername_original,$userprofileimagefoldername_medium,$userprofileimagefoldername_thumbnail; // Global variables
		include("showuserprofileAPI.php");//including file showuserprofileAPI.php
	}//end of function showuserprofile	
		
/*  *  Update setting 
 	*  updatesetting must be POST method
 	*  text or image	*/	
	private function updatesetting()
	{
		global $valid_formats,$userprofileimagefoldername_original,$userprofileimagefoldername_medium,$userprofileimagefoldername_thumbnail; // Global variables
		include("updatesettingAPI.php")	;//including file updatesettingAPI.php
	}//end of function updatesetting	
	

/* 	*  Show dispensary list in menu section  
 	*  menudispensarylist must be POST method
 	*  text	and images	*/	
	private function menudispensarylist()
	{
		global $dispensaryimagefoldername_original,$dispensaryimagefoldername_medium,$dispensaryimagefoldername_thumbnail; // Global variables
		include("menudispensarylistAPI.php");//including file menudispensarylistAPI.php
	}//end of function menudispensarylist	
	
/* 	*  Show Local Menu list in menu section( 20 or 15 miles by login users)  
 	*  localmenudispensarylist must be POST method
 	*  text */	
	private function localmenudispensarylist()
	{
		global $dispensaryimagefoldername_original,$dispensaryimagefoldername_medium,$dispensaryimagefoldername_thumbnail;	// Global variables
		include("LocalMenulistAPI.php");//including file LocalMenulistAPI.php
	}//end of function localmenudispensarylist	

/*	*  Show dispensary Detail in menu section  
 	*  menudispensarydetail must be POST method
 	*  text or images	*/	
	private function menudispensarydetail()
	{
		global $dispensaryimagefoldername_original,$dispensaryimagefoldername_medium,$dispensaryimagefoldername_thumbnail; 			// Global variables
		include("dispensarydetailAPI.php");		//including file dispensarydetailAPI.php
	}//end of function menudispensarydetail	
	
/* 	*  Show Search Lounge List  
 	*  searchloungelist must be POST method
 	*  text or images */	
	private function searchloungelist()
	{
		global $dispensaryimagefoldername_original,$dispensaryimagefoldername_medium,$dispensaryimagefoldername_thumbnail; 			// Global variables
		include("SearchLoungelistAPI.php");		//including file SearchLoungelistAPI.php
	}//end of function searchloungelist	
	
/*  *  Show Search Menu List  
 	*  searchmenulist must be POST method
 	*  text  or images 	*/	
	private function searchmenulist()
	{
		global $dispensaryimagefoldername_original,$dispensaryimagefoldername_medium,$dispensaryimagefoldername_thumbnail;			// Global variables
		include("SearchMenulistAPI.php");		//including file SearchMenulistAPI.php
	}//end of function searchmenulist
	
/* 	*  User FollowUp   
 	*  userfollowup must be POST method
 	*  text or images */	
	private function userfollowup()
	{
		include("FollowUpLoungeAPI.php");//including file FollowUpLoungeAPI.php
	}//end of function userfollowup
	
/* 	*  Menu FollowUp   
 	*  menufollowup must be POST method		*/	
	private function menufollowup()
	{
		include("FollowUpMenuAPI.php");//including file FollowUpMenuAPI.php
	}//end of function menufollowup	


	
/* 	*  Menu FollowUp List   
 	*  followupmenulist must be POST method		*/	
	private function followupmenulist()
	{
		global $dispensaryimagefoldername_original,$dispensaryimagefoldername_medium,$dispensaryimagefoldername_thumbnail; 				// Global variables
		include("FollowupMenuListAPI.php");			//including file SearchMenulistAPI.php
	}//end of function followupmenulist
	
/*  *  Show member profile 
 	*  memberprofile must be POST method
 	*  text or image	*/	
	private function memberprofile()
	{
		global $valid_formats,$userprofileimagefoldername_original,$userprofileimagefoldername_medium,$userprofileimagefoldername_thumbnail;			// Global variables
		include("MemeberProfileAPI.php");		//including file MemeberProfileAPI.php
	}//end of function memberprofile									

/*
 * Api name - addBudThought
 * Api description - to add bud thought by any user
 * Creation date - 7th may 
 */
 	private function addBudThought()
	{
		//include file (addBudThoughtAPI.php)
		include("addBudThoughtAPI.php");
	}
	
/*
 * Api name - likeBudThought
 * Api description - if any user like any bud thought then make that entry into DB 
 * Creation date - 8th may 
 */
 	private function likeBudThought()
	{
		//include file (likeBudFolioAPI.php)
		include("likeBudFolioAPI.php");
	}	
/*
 * Api name - writeAReview
 * Api description - if any user like any bud thought then make that entry into DB 
 * Creation date - 8th may 
 */
 	private function writeAReview()
	{
		//include file (likeBudFolioAPI.php)
		include("writeAReviewAPI.php");
	}
	
	/*
 * Api name - showReview
 * Api description - if any user like any bud thought then make that entry into DB 
 * Creation date - 8th may 
 */
 	private function showReview()
	{
		//include file (likeBudFolioAPI.php)
		include("showReviewsAPI.php");
	}	
	
/* Api name - dispensaryPhotos
 * Api description - if any user like any bud thought then make that entry into DB 
 * Creation date - 8th may 
 */
 	private function dispensaryPhotos()
	{
		//include file (likeBudFolioAPI.php)
		include("dispensaryPhotoAPI.php");
	}	

/* Api name - dispensaryPhotos
 * Api description - if any user like any bud thought then make that entry into DB 
 * Creation date - 8th may 
 */
 	private function dispensaryFollowers()
	{
		//include file (likeBudFolioAPI.php)
		include("DispensaryFollowersAPI.php");
	}

/*
 * Api name - getBudThought
 * Api description - get the complet details of bud thought alog with all the comments on that bud thought
 * Creation date - 8th may 
 */
 	private function getBudThought()
	{
		//include file (likeBudFolioAPI.php)
		include("getBudThoughtDetails.php");
	}	

/*
 * Api name - strainLib
 * Api description - get the list of strain library 6/11/2013
 * Creation date - 10th may 
 */
 	private function getStarinLib()
	{
		
		include("strainLibraryAPI.php");
	}	
/*
 * Api name - searchStrainLib
 * Api description - get the list of strain library that has been searched by the user
 * Creation date - 10th may 
 */
 	private function searchStarinLib()
	{
		
		include("searchStrainLiberaryAPI.php");
	}


	/*
 * Api name - strainLiberaryDetails
 * Api description - get the details of strain that has been selected by the user from the strain liberary
 * Creation date - 12th may 
 */
 	private function strainLiberaryDetails()
	{
		
		include("strainLibDetailsAPI.php");
	}


/*
 * Api name - strainLiberaryDetails
 * Api description - get the details of followinfs of the logged in users and also uses the strains from strain 	   liberary
 * Creation date - 13th may 
 */
 	private function followingStrainLibList()
	{
		
		include("followingStrainLibAPI.php");
	}				
		
/*
 * Api name - localstrainLibList
 * Api description - get the list of users in the range of  the logged in users and also uses the strains from strain liberary
 * Creation date - 13th may 
 */
 	private function localStrainLibList()
	{
		include("localStrainLibAPI.php");
	}		

/*
 * Api name - likeBudThought
 * Api description - if any user like any bud thought then make that entry into DB 
 * Creation date - 8th may 
 */
 	private function likeStrain()
	{
		
		//include file (likeBudFolioAPI.php)
		include("likeStrainAPI.php");
	}


/*
 * Api name - FollowersList
 * Creation date - 24thjune 
 */
 	private function FollowingList()
	{
		//include file (likeBudFolioAPI.php)
		include("FollowersListAPI.php");
	}

	/*
 * Api name - getNotifications
 * Api description - if any user like any bud thought then make that entry into DB 
 * Creation date - 8th may 
 */
 	private function getNotifications()
	{
		//include file (likeBudFolioAPI.php)
		include("getNotificationsAPI.php");
	}	




/* api to check emmail or twitter id already exist or not
*/
private function checkEmailExistence(){
	include('checkEmailExistenceAPI.php');
}

/*
Api name - geoLocationDisp
File Name -GeoLocationAPI.php
date -22nd july
*/
private function geoLocationDisp(){
include('GeoLocationAPI.php');
}



//Dispensary List Api
private function dispensaryList()
{
	include('dispensaryListApi.php');
}


private function addStrImg()
{
	include("newAddStrainImgAPI.php");
}



private function geoLocalLounge()
{
	include('newLocalLoungeAPI.php');
}

private function updateToken()
{
	include('updateDeviceToken.php');
}


private function addTestImg()
{
	include('add_image.php');
}


private function getProfileTypes()
{
	include('prodileTypeAPI.php');
}


private function hashTextList()
	{
		//include file (likeBudFolioAPI.php)
		include("hashTextListAPI.php");
	}	


private function addXlargeImg()
{
	include('addimgXlarge.php');
}
private function loungeAllXlarge()
{
	
	include('loungeAllForXlargeApi.php');
}

private function loungePushNotificationList()
{
	include('lounge_push_notificationlistAPI.php');
}
private function strainBudThoughtDetail()
{
	include('strainBudThoughtDetailAPI.php');
}

private function budThoughtList()
{
	include('budThoughtListAPI.php');
}
private function remove_bud_thought()
{
	include('remove_bud_thoughtAPI.php');
}
//date 11th feb 2014 
//list of users who are following current user
private function followingUsersList()
{
	include('followingUsersListAPI.php');
}
// date 11th feb 2014
private function claimedDispensaryOfUser()
{
	include('claimedDispensaryOfUserAPI.php');
}


/* 	Mark inappropiate api*/
	private function markInappropriate()
	{
		//including file markInappropiateAPI.php
		include('markInappropriateAPI.php');
	}//end of inappropiate functionality


/* 	Encode array into JSON	*/
	private function toJson($arrdata)
	{
		if(is_array($arrdata))
		{
			return json_encode($arrdata);
		}
	}
/*	Encode array into XML	*/
	private function toXML($arrData)
	{
		if(is_array($arrdata))
		{
			$xml = new SimpleXMLElement('<root/>');
			array_walk_recursive($arrdata, array ($xml, 'addChild'));
			$xml=$xml->asXML();
		    return $xml;
		}
	}
} 	// closing of class moodWireAPI
$BudFolio = new budfolio;
$BudFolio->processApi();
?>