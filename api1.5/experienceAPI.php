<?php
/*
	File Description - Experience list
	File Name - experienceAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array
	$intUserId = $inputArray['UserId'];		
	// Input validations
	
		$intValidUserId = mysql_real_escape_string($intUserId);
		// to check whether the user id is exist.
		if($intUserId!=0)
		{
			$uservalidation=CheckValidUser($intValidUserId);
		}
		/*if($uservalidation)
		{*/
			// Check if experience array is exist			
			if(is_array($experience) && !empty($experience))
			{
				$arrayExperience=array();
				for($i=0;count($experience)>$i;$i++)
				{   
					$arrayExperience[$i]=$experience[$i];
				}
				sort($arrayExperience,SORT_STRING);
				// If success everythig is good send header as "OK" and ExperienceList in reponse
				$result = array('success' => '1','ExperienceList' =>$arrayExperience );
				$this->response(stripslashes($this->toJson($result)),200,"application/json");
			}	
			else
			{
				// If Empty Experience List
				$error = array('success' => "0", "msg" => "Empty Experience List.");
				$this->response($this->toJson($error), 400,"application/json");
			}
		/*}
		else
		{
			// If user id empty
			$error = array('success' => "0", "msg" => "Invalid uesr id.");
			$this->response($this->toJson($error), 400,"application/json");
		}*/	
		
?>