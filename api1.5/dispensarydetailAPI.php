<?php
/*
	File Description - Dispensary Detail
	File Name - dispensarydetailAPI.php
*/

// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	
	//Taking all values into local variable				
	$intDispensaryId = $inputArray['DispensaryId'];
	$intUserId = $inputArray['UserId'];
	$intLat = $inputArray['latitude'];
	$intLong =$inputArray['longitude'];
	//Input validations
	if(!empty($intDispensaryId))
	{
		// sanitization of values to set in sql
		$ValidintDispensaryId = mysql_real_escape_string($intDispensaryId);
		
		//SQL query to get Strain detail
		$strDispensaryDetailQuery = "SELECT * 
									 FROM dispensaries 
									 WHERE flag='Active' 
									 and  dispensary_id ='".$ValidintDispensaryId."' 
									 Limit 0,1";
		$DispensaryDetailSQL = mysql_query($strDispensaryDetailQuery) or die(mysql_error());
		$DispensaryDetailArray=array();
		$menuheadingArray=array();
		$DispensaryDetail=array();
		$number=0;
		if(mysql_num_rows($DispensaryDetailSQL) > 0)
		{
			while($DispensaryDeatil=mysql_fetch_array($DispensaryDetailSQL))
			{
				//SQL query to get menu header detail
				$strDispensaryHeading = "SELECT * FROM menu_heading 
										 WHERE 
										 dispensary_id =
										 '".$DispensaryDeatil['dispensary_id']."' 
										 order by menu_heading_id asc ";
				$DispensaryHeadingsSQL = mysql_query($strDispensaryHeading);
				
				//23rd may
				if(mysql_num_rows($DispensaryHeadingsSQL)>0)
				{
					while($headingdata=mysql_fetch_array($DispensaryHeadingsSQL))
					{
						
					  // SQL query to get dispensary detail
					  $strDispensariesDetailQuery = "SELECT * 
					  FROM dispensaries_detail 
					  WHERE menu_heading_id ='".$headingdata['menu_heading_id']."' 	
					  order by dispensary_detail_id asc ";
					  
					  $SQLDispensariesDetail = mysql_query($strDispensariesDetailQuery);
					  //executing strDispensariesDetailQuery
					  $headingflag=0;
					  $mn=0;
					  while($DispensariesDetailData=mysql_fetch_array($SQLDispensariesDetail))
					  {
						  if($headingflag==0)
						  {
							  $mHeader=unserialize($DispensariesDetailData['dispensaries_detail_data']);
							  
							  /*for($i=0; $i<=count($mHeader);$i++){
								  
								  $mHeader[]=str_replace("\\", "",$mHeader[$i]);
								  echo $mHeader[$i];
							  }
							  die;*/
							  if($mHeader[6]!='hide')
							  {
									 $menuheadingArray[$number]['heading']=$mHeader;
							  }
							 
						  }
						  else
						  {
 							$menuRow=  unserialize($DispensariesDetailData['dispensaries_detail_data']);
							
							/*for($i=0; $i<=count($menuRow);$i++){
								
								  $menuRow[]=str_replace("\\", "",$menuRow[$i]);
							  }*/
							  
							if($menuRow[6]!='hide')
							{
									$menuheadingArray[$number]['detail'][$mn]['menudata']=$menuRow;
									// Unserialize data							
									$data=$menuRow;
									// Select all users id that added same strain with added by admin in menu despensary 							
								   $Queryuserno="select distinct user_id 
												  from strains 
												  where strain_name='".mysql_real_escape_string($data[0])."'
												  AND (dispensary_name= '".mysql_real_escape_string($DispensaryDeatil['dispensary_name'])."'
												  or 	dispensary_id='".$intDispensaryId."')
												  and flag='active'
												  ";	//change on 2nd dec											
									$Sqluserno=mysql_query($Queryuserno)or die(mysql_error()); 
									$Resuserno=mysql_num_rows($Sqluserno);
									$menuheadingArray[$number]['detail'][$mn]['userdata']['userrating']=$Resuserno;
									
								  //strength 
								   $strenghtSQL ="select strength_rate,count(strength_rate) as strengthRate 
												 from strains 
												 where strain_name='".mysql_real_escape_string($data[0])."'
												 AND (dispensary_name= '".mysql_real_escape_string($DispensaryDeatil['dispensary_name'])."'
												 or 	dispensary_id='".$intDispensaryId."')
												 AND FLAG='active'
												 group by strength_rate
												 ORDER BY strengthRate desc  LIMIT 0 , 1";
									
									  $SqlStrenght=mysql_query($strenghtSQL)or die(mysql_error()); 
									  $rows =mysql_num_rows($SqlStrenght);
									  if($rows==0)
									  {
										  $ResStrenght['strength_rate']='';
									  }
									  else{
										  $ResStrenght=mysql_fetch_array($SqlStrenght);
								   
										  $menuheadingArray[$number]['detail'][$mn]['userdata']['strength_rate']=$ResStrenght['strength_rate'];
									  }  
								  // Select strain detail							
									$Querysmell="select smell_rating,count(smell_rating) as rating 
												 from strains 
												 where strain_name='".mysql_real_escape_string($data[0])."' 
												 group by smell_rating 
												 ORDER BY rating desc 
												 LIMIT 0 , 1";
									$Sqlsmell=mysql_query($Querysmell)or die($Querysmell." : ".mysql_error()); 
									$Ressmell=mysql_fetch_array($Sqlsmell);
									$menuheadingArray[$number]['detail'][$mn]['userdata']['smell_rating']='';						//$Ressmell['smell_rating']; date 14 nov
								  // Get test rate rating sql 							
									$Querytaste="select taste_rate,count(taste_rate) as rating 
												 from strains 
												 where strain_name='".mysql_real_escape_string($data[0])."' 
												 group by taste_rate 
												 ORDER BY rating desc 
												 LIMIT 0 , 1";
									$Sqltaste=mysql_query($Querytaste)or die(mysql_error()); 
									$Restaste=mysql_fetch_array($Sqltaste);
									$menuheadingArray[$number]['detail'][$mn]['userdata']['taste_rate']='';//$Restaste['taste_rate']; date 14 nov
								  // Select over all rating sql 							
									$QueryOverallRating="select count(overall_rating) as rating 
														 from strains 
														 where strain_name='".mysql_real_escape_string($data[0])."'
														 AND (
														 dispensary_name= '".mysql_real_escape_string($DispensaryDeatil['dispensary_name'])."'
														 or dispensary_id='".$intDispensaryId."')	
														 AND FLAG='active'
														 group by overall_rating LIMIT 0 , 1";
														 
														 
									$SqlOverallRating=mysql_query($QueryOverallRating)or die(mysql_error()); 
									$ResOverallRating=mysql_fetch_array($SqlOverallRating);
								  // Get some of overall rating							
															 
									  $avrageRattingSQL ="SELECT avg( overall_rating ) AS avgsum
														  FROM strains
															where strain_name='".mysql_real_escape_string($data[0])."'
														 AND (
														 dispensary_name= '".mysql_real_escape_string($DispensaryDeatil['dispensary_name'])."'
														 or dispensary_id='".$intDispensaryId."')	
														  AND FLAG = 'active'";					   
															 
															 
									$SqlOverallRating_avg=mysql_query($avrageRattingSQL)or die(mysql_error()); 
									$ResOverallRating_avg=mysql_fetch_array($SqlOverallRating_avg);
									  // Calculate avg rating							
								  
									  $avg = $ResOverallRating_avg['avgsum'];
									  if($avg=="null" ||$avg='')
									{
										$avg='';
									}
									$menuheadingArray[$number]['detail'][$mn]['userdata']['avg_rating']=$avg;
									$mn++;
							}// end of if($menuRow[6]!='hide')
						  }
						  $headingflag++;
					  }
					  $number++;
				  }// end of while loop
				}//23rd may
				if(!empty($DispensaryDeatil['image']))
				{
					$imgurl=BASEURL.$dispensaryimagefoldername_original.$DispensaryDeatil['image'];
					$thumimageurl=BASEURL.$dispensaryimagefoldername_thumbnail.$DispensaryDeatil['image'];
				}
				else
				{
					$imgurl='';
					$thumimageurl='';
				}

				//if($DispensaryDeatil['added_by']!=0){
				$getUserSettings ="SELECT * FROM dispensary_settings 
								   WHERE dispensary_id='".$intDispensaryId."'";
				
				  $getSettingsResponse = mysql_query($getUserSettings) or die($getUserSettings." : ".mysql_error());
	  
				  $settingData = mysql_fetch_assoc($getSettingsResponse);
 $settingArray=array(
				  'DeliveryService'=>$settingData['DeliveryService']!=''?$settingData['DeliveryService']:'0',
				  'StoreFront'=>$settingData['StoreFront']!=''?$settingData['StoreFront']:'0',
				  'AcceptCreditCard'=>$settingData['AcceptCreditCard']!=''?$settingData['AcceptCreditCard']:'0',
				  'AcceptATMonSite'=>$settingData['AcceptATMonSite']!=''?$settingData['AcceptATMonSite']:'0',
				  '18YearsOld'=>$settingData['18YearsOld']!=''?$settingData['18YearsOld']:'0',
				  '21YearsOld'=>$settingData['21YearsOld']!=''?$settingData['21YearsOld']:'0',
				  'HandicapAssesseble'=>$settingData['HandicapAssesseble']!=''?$settingData['HandicapAssesseble']:'0',
				  'Security'=>$settingData['Security']!=''?$settingData['Security']:'0'	
						);
					
				/*	}
				else{
						$settingArray=array(
											  'DeliveryService'=>'0',
											  'StoreFront'=>'0',
											  'AcceptCreditCard'=>'0',
											  'AcceptATMonSite'=>'0',
											  '18YearsOld'=>'0',
											  '21YearsOld'=>'0',
											  'HandicapAssesseble'=>'0',
											  'Security'=>'0'
										  );
					}*/
					
			
				$FWPflag = getFollowUpList($intDispensaryId,$intUserId);
				$atRateUsersArray = getAtRateUsers($DispensaryDeatil['bio']);
				$distance = ceil(get_distance($intLat,$intLong,$DispensaryDeatil['latitude'],$DispensaryDeatil['longitude'],'M'));	
				
				$isSubscription = 'no';
				if($DispensaryDeatil['added_by']!='0')
				{
					$strainCount=getStrainsCount($DispensaryDeatil['added_by']);
					$budThoughtCount=getBudThoughtsCount($DispensaryDeatil['added_by']);
					$checkSubscription = checkSubscriptionForAUser($DispensaryDeatil['added_by'],$intDispensaryId);
					if ($checkSubscription > 0 )
					{
						$isSubscription = 'Yes';
					}
					else
					{
						$isSubscription = 'no';
					}
				}else
				{
					$strainCount='0';
					$budThoughtCount='0';
				}
				
			  //Deispensary detail array
			  $DeispensaryDetailArray[] =  array(
			  'DispensaryId'=> $DispensaryDeatil['dispensary_id'],
			  'DispensaryName'=>base64_encode(str_replace("\\", "",$DispensaryDeatil['dispensary_name'])),
			  'CustomerName'=> str_replace("\\", "",$DispensaryDeatil['customer_name']), 
			  'StreetAddress'=> base64_encode(str_replace("\\", "",$DispensaryDeatil['street_address'])),
			  'PhoneNumber'=> $DispensaryDeatil['phone_number'],
			  'Email'=>$DispensaryDeatil['email'],
			  'Website'=> $DispensaryDeatil['website'],
			  'City'=> base64_encode(str_replace("\\", "",$DispensaryDeatil['city'])),
			  'State'=> base64_encode(str_replace("\\", "",$DispensaryDeatil['state'])),
			  'ZipCode'=> $DispensaryDeatil['zip'],
			  'HoursOfOperation'=> str_replace("\\", "",$DispensaryDeatil['hours_of_operation']),
			  'Directions'=>$DispensaryDeatil['directions'],
			  'MenuHeadingArray'=> $menuheadingArray,
			  'DispensaryImageUrl'=> $imgurl,
			  'DispensaryThumbImageUrl'=> $thumimageurl,
			  'Bio'=>base64_encode(str_replace("\\", "",$DispensaryDeatil['bio'])),
			  'ReviewCount'=>getReviewsCount($DispensaryDeatil['dispensary_id']),
			  'PhotoCount'=>getPhotosCount($DispensaryDeatil['dispensary_id']),
			  'FollowersCount'=>getFollowersCount($DispensaryDeatil['dispensary_id']),
			  'StrainCount'=>$strainCount,
			  'BudThoughtCount'=>$budThoughtCount,
			  'settings'=>$settingArray,
			  'flag'=>$FWPflag,
		  	  'lat'=>$DispensaryDeatil['latitude'],
		      'long'=>$DispensaryDeatil['longitude'],
			  'atRateUsersList'=>$atRateUsersArray,
			  'Claimed_by'=>$DispensaryDeatil['added_by'],
			  'distance'=>$distance,
			  'isSubscription'=>$isSubscription
			 );// end of array//=>
			}
			

				// If success everythig is good send header as "OK" and sending Dispensary details in reponse
				$result = array('success' => '1','DispensaryDetail' => $DeispensaryDetailArray);
				$this->response(stripslashes($this->toJson($result)),200,"application/json");

		
		}
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "0", "msg" => "Invalid DispensaryId.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Dispensary Id empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>