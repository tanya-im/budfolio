<?php
/*
	File Description - File to get bud thought details of a user 
	File Name - budThoughtDetailsAPI.php
	creation date - 8th may
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}

	$dataJson = file_get_contents("php://input"); //getting json input

	$inputArray = json_decode($dataJson,true);
	
	//converting that json into array
	$intBudthoughtId = $inputArray['bud_thought_id'];
	$intUSerId = $inputArray['UserId'];
	
	// Input validations
	if(!empty($intBudthoughtId))
	{
		 //SQL query to get budThought 
		 $strGetBudThoughtSQL = "SELECT `user_id`,`bud_thought`,`picture_url`,`post_date` 								 			 					 FROM `tbl_bud_thought`
					 WHERE `bud_thought_id` ='".$intBudthoughtId."'";
								 
		$sql = mysql_query( $strGetBudThoughtSQL)or die( $strGetBudThoughtSQL." ".mysql_error());//executing strLoginSQL
		
		if(mysql_num_rows($sql)>0)
		{
			
			$budThgtDetails = mysql_fetch_assoc($sql);// getting all the bud thought detials
			//$date = date('d|m|Y | h:i A',strtotime($budThgtDetails['post_date']));//conevrting date time into string format
			
			$date = funTimeAgo(strtotime($budThgtDetails['post_date']));
			$likeCount = getLikeCount($intBudthoughtId);
			$commentCount = getCommentCount($intBudthoughtId);
			$likeFlag = isUserLiked($intBudthoughtId,$intUSerId);
			
			
			//USER iMAGE - for user profile image path
			$getPathSQL = "SELECT photo_url 
						   FROM users WHERE user_id='".$budThgtDetails['user_id']."'";
			$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());
			$row = mysql_fetch_assoc($response);
			if(!empty($row['photo_url']))
			{
				$cmntuserImgPath = BASEURL."images/profileimages/thumbnail/".$row['photo_url'];
			}else
			{
				$cmntuserImgPath = '';
			}
			
			// start of at rate user
			$atRateUsersArray = getAtRateUsers($budThgtDetails['bud_thought']);
			
			// get all the replies on the bud thought
			$strGetRepliesSQL = "SELECT * FROM tbl_comments WHERE bud_thought_id='".$intBudthoughtId."'";
			
			$resReply = mysql_query($strGetRepliesSQL) or die($strGetRepliesSQL." ".mysql_error());
			
			if($resReply){
				
				$arrayReplies = array();
				while($rowReply = mysql_fetch_assoc($resReply))
				{
					$getPathSQL = "SELECT photo_url FROM users WHERE user_id='".$rowReply['user_id']."'";
					$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());
					$row = mysql_fetch_assoc($response);
					if(!empty($row['photo_url']))
					{
						$userImgPath = BASEURL."images/profileimages/thumbnail/".$row['photo_url'];
					}else
					{
						$userImgPath = '';
					}		
				
					
				/*if($rowReply['picture_url']!=''){
							  $imgURL = BASEURL.BTIMGPATH.$rowReply['picture_url'];
							  
					}
					else
					{
								$imgURL = '';
					}*/
					$dateComment = funTimeAgo(strtotime($rowReply['create_date_time']));//conevrting date time into string format
					
					$atRateUsersArrayComment = getAtRateUsers($rowReply['bud_thought']);
					
					$arrayReplies[]= array(
								'comment_id'  => $rowReply['comment_id'],
								'comment'	  => base64_encode(str_replace("\\", "",$rowReply['bud_thought'])),
								'user_id'    => $rowReply['user_id'],
								'user_name'   => getUserName($rowReply['user_id']),
								'comment_time'=> $dateComment,
								'picture_url' => $cmntuserImgPath,
								'user_picture_url'=>$userImgPath,
								'atRateUsersList'=>str_replace("\\", "",$atRateUsersArrayComment)
					 );// end of the array
				}// end of while loop
			}// end of if($resreply==1)
			
			$arrayBuThought = array(
								"user_id" => $budThgtDetails['user_id'],
								'user_name' => getUserName($budThgtDetails['user_id']),
								"bud_thought" =>base64_encode(str_replace("\\", "",$budThgtDetails['bud_thought'])),
								"picture_url" => ($budThgtDetails['picture_url']!=''?  BASEURL.BTTIMGPATH.$budThgtDetails['picture_url']:''),
								"originalimage" => ($budThgtDetails['picture_url']!=''?  BASEURL.BTIMGPATH.$budThgtDetails['picture_url']:''),
								 "date"=> $date,
								 "LikeCount" => $likeCount,
								 "CommentCount" => $commentCount,
								 "likeFlag" =>$likeFlag,
								 "reply"=>$arrayReplies,
								'atRateUsersList'=>$atRateUsersArray 
							  );
				
			// If success everythig is good send header as "OK" and user details
			$result = array( "success" => "1",'bud_thought_details' => $arrayBuThought);
			$this->response($this->toJson($result),200,"application/json");

		} // closing ($sql == 1)
		else
		{
			// If no records such records available 
			$strError = array("success" => "0" ,"msg" => "No such bud thought found.");
			$this->response($this->toJson($strError), 204,"application/json");
		}	
				
	} //end of input validations
	else
	{
		// If either username or passowrd is empty
		$error = array('success' => "0", "msg" => "Please enter bud thought.");
		$this->response($this->toJson($error), 400,"application/json");
	}
?>