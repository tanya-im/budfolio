<?php
/* production
	File Description - Strain Detail
	File Name - straindetailAPI.php
*/
// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}

	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray=json_decode($dataJson,true); //converting that json into array
	$intStrainId = $inputArray['StrainId'];

	// Input validations
	if(!empty($intStrainId))
	{
		$intValidintStrainId = mysql_real_escape_string($intStrainId);

		//SQL query to check valid user getting strain detail 
		$strStrainDetailQuery = "SELECT * FROM strains WHERE flag='Active' 
								 and strain_id ='".$intValidintStrainId."' Limit 0,1";
		$StrainDetailSQL = mysql_query($strStrainDetailQuery) or die(mysql_error());
		$StrainDetailArray=array();
		if(mysql_num_rows($StrainDetailSQL) > 0)
		{
			while($StrainDeatil=mysql_fetch_array($StrainDetailSQL))
			{
                //SQL query to get Strain detail
				$strAllImages = "SELECT * FROM strainimages WHERE 
								strain_id ='".$StrainDeatil['strain_id']."' order by image_id asc limit 0,3";
				$AllImagesSQL = mysql_query($strAllImages);//executing strAllImages
				$imagesurl=array();
				$imgOrignals = array();
				$pimgno=3;
				if(mysql_num_rows($AllImagesSQL)>0)
				{
					$i=0;
					while($i<3){
						$AllImages = mysql_fetch_array($AllImagesSQL);
						$imagesurl['ImagesUrl'][] = $AllImages['image_name']!=''?(BASEURL.$strainimagefoldername_thumbnail.$AllImages['image_name']):'';
						$imgOrignals['ImagesUrl'][]=$AllImages['image_name']!=''? (BASEURL."images/uploads/original/".$AllImages['image_name']):'';
						$i++;
					}
					/*while($AllImages = mysql_fetch_array($AllImagesSQL))
					{
						// Set images url					
						$imagesurl['ImagesUrl'][] = $AllImages['image_name']!=''?(BASEURL.$strainimagefoldername_thumbnail.$AllImages['image_name']):'';
						$imgOrignals['ImagesUrl'][]=$AllImages['image_name']!=''? (BASEURL."images/uploads/original/".$AllImages['image_name']):'';	 
						// Check primary image and set it in array					
						if($AllImages['primary_image']==1)
						{
							$imagesurl['PrimaryImage']=$pimgno;
							$imgOrignals['PrimaryImage']=$pimgno;
						}
						$pimgno--;
					}*/
				}
				else
				{
					$imagesurl['ImagesUrl']=array();
					$imagesurl['PrimaryImage']=0;
					$imgOrignals['ImagesUrl']=array();
					$imgOrignals['PrimaryImage']=0;
				}	

				// Get experiance detail				
				$strExperienceQuery = "SELECT experience_name,experience_id FROM experience WHERE strain_id ='".$StrainDeatil['strain_id']."' order by experience_id asc ";
				$SQLExperience = mysql_query($strExperienceQuery);//executing strExperienceQuery
				$experience=array();
				while($allexperience=mysql_fetch_array($SQLExperience))
				{
					$experience[]=$allexperience['experience_name'];
				}

				// Get medicinal use detail				
				$strMedicinalUseQuery = "SELECT medicinal_id,medicinal_type FROM medicinal_use WHERE strain_id ='".$StrainDeatil['strain_id']."' order by medicinal_id asc ";
				$SQLMedicinalUse = mysql_query($strMedicinalUseQuery);//executing strMedicinalUseQuery
				$medicinaluse=array();
				while($AllMedicinalUse=mysql_fetch_array($SQLMedicinalUse))
				{
					$medicinaluse[]=$AllMedicinalUse['medicinal_type'];
				}
				
				
				
				$dispensaryName='';
				$disId= '';
				if($StrainDeatil['dispensary_id']=='0')
				{
							 $dispensaryName=$StrainDeatil['dispensary_name'];
					 		 $disNameArray = explode('-',$dispensaryName);
							 if(count($disNameArray)>0)
							 {
								$disName= $disNameArray[0];
								$disId =getDispensaryId($disName);
							 }	
						     $dispancry_link_query="select dispensary_id 
                                    from dispensaries 
                                    where LOWER(dispensary_name)='".$disName."'
                                    Limit 0,1";
                  			 $dispancry_link_sql = mysql_query($dispancry_link_query);
							 if(mysql_num_rows($dispancry_link_sql)>0)
							 {
                  			 	$dispancry_link_result = mysql_fetch_array($dispancry_link_sql);
                  				$disId= $dispancry_link_result['dispensary_id'];
							 	$dispensaryName = $StrainDeatil['dispensary_name']!=''?$StrainDeatil['dispensary_name']:'';
							 }
							 else
							 {
								 $disId= '';
							 	 $dispensaryName = $StrainDeatil['dispensary_name']!=''?$StrainDeatil['dispensary_name']:'';
							 }
							
				}else
				{
					 		$dispensaryName = $StrainDeatil['dispensary_name']!=''?$StrainDeatil['dispensary_name']:'';
							$disId=$StrainDeatil['dispensary_id'];
				}
				
				
				
				
				
				
				$encoded_strainName =base64_encode(str_replace("\\", "", $StrainDeatil['strain_name']));
				if($dispensaryName!='')
				{
					$encoded_dispensaryName =base64_encode(str_replace("\\", "",$dispensaryName));
				}else
				{
					$encoded_dispensaryName ='';
				}
				$encoded_discription =base64_encode(str_replace("\\", "",$StrainDeatil['description']));

				// Prepare array for strain detail				
				$StrainDetailArray[] =  array(
					'StrainId'=> $StrainDeatil['strain_id'],
					'UserId'=>$StrainDeatil['user_id'],
					'StrainName'=> $encoded_strainName , 
					'Dispensory'=> $encoded_dispensaryName,
					'DispensaryId'=> $disId,
					'profileType'=>$StrainDeatil['profile_type'],
					'Species'=> str_replace("\\", "",$StrainDeatil['species']),
					'Linage'=>str_replace("\\", "",$StrainDeatil['linage']),
					'SmellRating'=> $StrainDeatil['smell_rating'],
					'TasteRate'=>str_replace("\\", "",$StrainDeatil['taste_rate']),
					'StrengthRate'=>str_replace("\\", "",$StrainDeatil['strength_rate']),
					'OverallRating'=> $StrainDeatil['overall_rating'],
					'ExperienceArray'=>$experience,
					'Consumption'=> $StrainDeatil['consumption_name'],
					'MedicinalUseArray'=> $medicinaluse,
					'TestedBy'=>str_replace("\\", "",$StrainDeatil['tested_by']),
					'THC'=> $StrainDeatil['thc'],
					'CBD'=> $StrainDeatil['cbd'],
					'CBN'=> $StrainDeatil['cbn'],
					'THCa'=> $StrainDeatil['thca'],
					'CBHa'=> $StrainDeatil['cbha'],
					'Moisture'=> $StrainDeatil['moisture'],
					'Description'=> $encoded_discription,
					'StrainsImagesUrlArray'=> $imagesurl,
					'StrainOrignalImagesUrl'=> $imgOrignals
												);// end of array
			}
			// If success everythig is good send header as "OK" and StrainDetail in reponse
			$result = array('success' => '1','StrainDetail' => $StrainDetailArray);
			$this->response(stripslashes($this->toJson($result)),200,"application/json");
		}
		else
		{
			// If Invalid StrainId
			$error = array('success' => "0", "msg" => "Invalid StrainId.");
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "StrainId empty.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>