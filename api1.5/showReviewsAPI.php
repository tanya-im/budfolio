<?php
/*
	File Description - show a review Api on dispensary popup
	File Name - showReviewsAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input"); //getting json input
	$inputArray = json_decode($dataJson,true); //converting that json into array

	//Taking all values into local variable				
	 $intDispensaryId = $inputArray['DispensaryId'];
	 $intPageSize = $inputArray['PageSize'];
     $intPageNo = $inputArray['PageNo'];	
	 
	// Input validations
	if(!empty($intDispensaryId))
	{
		$dispensaryName = getDispensaryName($intDispensaryId);
		
		$strReviewCountSQL ="SELECT strain_id 
							 FROM strains
							 WHERE flag='active' 
							 and dispensary_name LIKE  '".addslashes($dispensaryName)."%'
							 OR dispensary_id = '".$intDispensaryId."'
							 union all 
							 SELECT distinct(user_id)
							 FROM tbl_dispensary_review 
							 WHERE dispensary_id='".$intDispensaryId."'";
		
		$resCount = mysql_query($strReviewCountSQL) or die($strReviewCountSQL." : ".mysql_error());

		$strReviewData = mysql_num_rows($resCount);
		$totalPages = ceil($strReviewData / $intPageSize);
			
		$perpage = $intPageSize;
		$page = $intPageNo;
		$calc = $perpage * $page;
		$start = $calc - $perpage;
			
		$strGetReviewSQL  = "SELECT distinct(user_id),
							   review_id,
							   strain_quality,
							   strain_variety,
							   service,
							   atmosphere,
							   privacy,
							   pricing,
							   review_text,
							   NUll as description,
							   date_time as ptime,
							   1 AS TYPE
							FROM tbl_dispensary_review 
							WHERE dispensary_id='".$intDispensaryId."'
							union all 
							SELECT user_id,
							   strain_id ,
							   strain_name,
							   species,
							   overall_rating,
							   smell_rating,
							   taste_rate,
							   strength_rate,
							   dispensary_name,
							   description,
							   post_date as ptime,
							   2 AS TYPE 
						  FROM strains
						  WHERE flag='active' and
						  (dispensary_name = '".addslashes($dispensaryName)."'
						  OR dispensary_id = '".$intDispensaryId."')
						  
						  ORDER BY ptime desc";
						  
		if(!empty($perpage) && !empty($page)){
			$strGetReviewSQL  .="  LIMIT $start,$perpage";
		}
		//echo $strGetReviewSQL."<br><br>";
		$resGetreviews = mysql_query($strGetReviewSQL) or die($strGetReviewSQL." : ".$strGetReviewSQL);
		
		if(mysql_num_rows($resGetreviews)>0)
		{
			$arrayReview = array();
			while($reviewRow = mysql_fetch_assoc($resGetreviews))
			{
				
				//for user profile image path
				$getPathSQL = "SELECT photo_url 
							   FROM users 
							   WHERE user_id='".$reviewRow['user_id']."' ";
				$response = mysql_query($getPathSQL) or die($getPathSQL." : ".mysql_error());
				$row = mysql_fetch_assoc($response);
 				if(!empty($row['photo_url']))
				{
					$userImgPath = BASEURL."images/profileimages/thumbnail/".$row['photo_url'];
					$userImgOriginal=BASEURL."images/profileimages/original/".$row['photo_url'];
				}else
				{
						$userImgPath = '';
						$userImgOriginal='';
				}
				
				if($reviewRow['TYPE']==1)// type==1 means location reviews
				{
					
					$atRateUsersArray = getAtRateUsers($reviewRow['review_text']);
					
					$arrayReview[] = array(
			'user_id'=> $reviewRow['user_id'],		
			'user_name'  => getUserName($reviewRow['user_id']),
			'pictureUrl' => $userImgPath,
			'strain_quality' => $reviewRow['strain_quality'],
			'strain_variety' =>  $reviewRow['strain_variety'],
			'service'		 => $reviewRow['service'],
			'atmosphere'     => $reviewRow['atmosphere'],
			'Privacy'		 => ($reviewRow['privacy']!=''?$reviewRow['privacy']:'0'),
			'Pricing'		 =>($reviewRow['pricing']!=''?$reviewRow['pricing']:'0'),
			'review_text'    => base64_encode(str_replace("\\", "",$reviewRow['review_text'])),
			'type'=>'1',
			'atRateUsersList'=>str_replace("\\", "",$atRateUsersArray)																				
					);// end of array
					
			}// end of if
			else// if(type==2 means strain reviews) 
				{
					// get strain images
						$thumbImg = GetThreeImages($reviewRow['review_id']);
							
					if(!empty($primaryimages[0])){
						$img = $primaryimages[0];
						$imgThumb =  $primaryimages[1];
						$isImage ='1';
					}else
					{
						$img="";
						$imgThumb = "";
						$isImage='0';
							
					}
	$encode_strain_name =base64_encode(str_replace("\\", "",$reviewRow['strain_quality']));
	$encoded_dispNAme =base64_encode(str_replace("\\", "",$reviewRow['review_text']));
	$encoded_discription =base64_encode(str_replace("\\", "",$reviewRow['description']));
	
	$atRateUsersArray = getAtRateUsers($reviewRow['description']);
	
					$arrayReview[] = array(
	  'user_id'=> $reviewRow['user_id'],		
	  'user_name'  => str_replace("\\", "",getUserName($reviewRow['user_id'])),
	  'pictureUrl' => $userImgPath,
	  'StrainId'=>$reviewRow['review_id'],
	  'StrainName' =>($encode_strain_name!=''?$encode_strain_name:''),
	  'Species' =>($reviewRow['strain_variety']!=''?$reviewRow['strain_variety']:''),
	  'Dispensory'=>($encoded_dispNAme!=''?$encoded_dispNAme:''),
	  'SmellRating' =>($reviewRow['atmosphere']!=''?$reviewRow['atmosphere']:''),
	  'TasteRate'=>($reviewRow['privacy']!=''?$reviewRow['privacy']:''),
	  'StrengthRate'=>($reviewRow['pricing']!=''?$reviewRow['pricing']:''),
	  'Description'=>($encoded_discription!=''?$encoded_discription:''),
	  'OverallRating'		 => $reviewRow['service'],
	  'Thumb1'=> (!empty($thumbImg[2]))? $thumbImg[2] :'',
	  'Thumb2'=> (!empty($thumbImg[1]))? $thumbImg[1] :'',
	  'Thumb3'=> (!empty($thumbImg[0]))? $thumbImg[0] :'',
	  'type'=>'2',												
	  'atRateUsersList'=>str_replace("\\", "",$atRateUsersArray)	);// end of array
			}// end of else
			
			}// end of while loop
			
			//if every thing is ok then send success=1 and strain  id in response.
			$result = array( "success" => "1","reviewList"=>$arrayReview);
			$this->response($this->toJson($result),200,"application/json");
		} // end of if(mysql_num_rows($resGetreviews)>0)
		else
		{
			// If Invalid DispensaryId
			$error = array('success' => "0","reviewList"=> array());
			$this->response($this->toJson($error), 400,"application/json");
		}	
	}
	else
	{
		// If StrainId empty
		$error = array('success' => "0", "msg" => "Please select dispensary.");
		$this->response($this->toJson($error), 400,"application/json");
	}	
?>