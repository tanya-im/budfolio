<?php
	ini_set("display_errors",1); //Error handling
    include_once("../config.php"); //send notification
	
	/*
		gcm_send
		Inputs:$device_type,$fields
		Summary:Sends gcm push
	*/
	function gcm_send($iUSERId,$strMessage,$pushType,$whoIsFollowing,$type,$typeId){
		//echo $iUSERId." ".$strMessage." :".$pushType;
		
		$inputs=array(
			'data'=>$strMessage,
			'UserId'=>$whoIsFollowing,
			"type"=>$type,
			"typeId"=>$typeId
		);
		
		//Query to fetch all the device tokens
	 $strGetDeviceTokensSQl = "SELECT device_token_for_android 
					 		   FROM users 
							   WHERE user_id='".$iUSERId."'";
		$resultGetDeviceTokens = mysql_query($strGetDeviceTokensSQl) or die($strGetDeviceTokensSQl." : ".mysql_error());
				
		$deviceRow = mysql_fetch_assoc($resultGetDeviceTokens);
		$deviceToken = $deviceRow['device_token_for_android']; //device token */
		/*$deviceToken='APA91bH7_qrUKGaFM1-0q9dPG-sO4yzntGx6_-0XL-42rO9Nh43deAFSK7Usw4rjDqg2-mZ6DXg5Lt4KCmDhrrHqKwXZmhl42uaA56913NtALTyJDRQvrBaN2UpjZINdoNh4XzHnDodXwfOImiL8R6O_ZHMxOlV6eQ';*/
		/*$deviceToken='APA91bFGbI_I9xsLH0InT0bzDWx9OGFg4GRy5QpQnL1WsFr1sQnkNwR8jQZFkrR9lTnw2w6L37amoLC1GqQkPxEM1krBBeM6NHfX_lzuhbnrqECPLwnaWT5skQ3dVce8r2VarhBlbGf0';*/
		$arrDeviceToken = array($deviceToken);
		
		$fields = array(
		'collapse_key' 		=> "time()",
			'data'          	=> $inputs,
			
			'registration_ids'  => $arrDeviceToken
	);
		
		//Url where we need to send push request
		
		//Header for request
		$headers = array( 
		
		//old - AIzaSyDMqz0a6bGitR5hCS5nTzLpgtjUj_bhXmU
		//AIzaSyC2gdBkkxVgrDlFsgYS-35TaU0ET2oSFzA
			'Authorization: key=AIzaSyDMqz0a6bGitR5hCS5nTzLpgtjUj_bhXmU',
			'Content-Type: application/json'
		);
		
		
		//curl request starts
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL,'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		 $pushResponse = curl_exec($ch);
		$resultInfo = curl_getinfo($ch);
		curl_close($ch);//close the curl
		//check the response
//return generate_push_response_message($pushResponse);
	}

	/*
		gcm_send
		Inputs:$response
		Summary:gencerate push message
	*/
	function generate_push_response_message($response){
	
		$response=json_decode($response);
		if(isset($response->success) && !empty($response->success)){
			if($response->success==1){//if response is success
				echo "Notification server send a message to the device.";
			}
			elseif($response->success==0){
				 $error=$response->results[0]->error;	
				switch($error){
					case "NotRegistered":
						echo "Notification server tries to send a message to the device and the device answers that the application is uninstalled.";
						break;
					default:
						echo "Notification server failed to send a message to the device 1";
						break;	
				}
			}
		}
		else{
			echo "Notification server failed to send a message to the device2";
		}
	}


	//echo $res= gcm_send(1,'test','request123');
?>