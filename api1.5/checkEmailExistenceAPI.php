<?php
/*
	File Description - File to check email and twitter id already exist or not
	File Name - emailCheckAPI.php
*/

	// Cross validation if the request method is POST else it will return "Not Acceptable" status
	if($this->get_request_method() != "POST")
	{
		$this->response('Invalid request',406);
	}
	
	//Getting the request JSON string
	$dataJson = file_get_contents("php://input");
	$inputArray=json_decode($dataJson,true);

	//converting that json into array
	$strEmail = $inputArray['Email'];		
	$strTwitterId = $inputArray['TwitterId'];
	$strFaceBookId =$inputArray['FaceBookId'];
	$strDeviceToken = $inputArray['DeviceToken'];
	$strDeviceType =$inputArray['DeviceType'];//wheather its android device or iphone// for android device its 2

	// Input validations
	$strValidEmail = mysql_real_escape_string($strEmail);
	$strValidFaceBookId = mysql_real_escape_string($strFaceBookId);
	$strValidTwitterId = mysql_real_escape_string($strTwitterId);
	
	//SQL query to check the  USER exists or notsss
	$strCheckEmailSQL = "SELECT user_id,user_name FROM users WHERE";
	if(!empty($strValidEmail))
	{
		 $strCheckEmailSQL .=" email_address ='".$strValidEmail."'";
	}
	if(!empty($strFaceBookId)){

		 $strCheckEmailSQL .=" faceBook_id ='".$strFaceBookId."'";
	}
	if(!empty($strValidTwitterId))
	{
		$strCheckEmailSQL .=" twitter_id='".$strValidTwitterId."'";
	}
	
	$sql = mysql_query($strCheckEmailSQL)or die($strCheckEmailSQL." : ".mysql_error());//executing strLoginSQL
	
	if(mysql_num_rows($sql) > 0)
	{		
		$resultSet = mysql_fetch_assoc($sql);
		
		
		if($strDeviceType==1)
		{
				$updatelogindate.="device_token_for_iphone='".$strDeviceToken."'";
		}else
		{
				$updatelogindate.="device_token_for_android='".$strDeviceToken."'";
		}
		
		$updatelogindate="update users set ";
			if($strDeviceType==1)
			{
				$updatelogindate.="device_token_for_iphone='".$strDeviceToken."'";
			}else
			{
				$updatelogindate.="device_token_for_android='".$strDeviceToken."'";
			}
			$updatelogindate.="where user_id='".$resultSet['user_id']."'";
		//echo $updatelogindate;
		$datestatus=mysql_query($updatelogindate) or die($updatelogindate." : ".mysql_error());
			
		

			$result = array( 
							"success" => "1",
					        'UserId' => $resultSet['user_id'],
							'UserName'=>str_replace("\\", "", $resultSet['user_name']));
			$this->response($this->toJson($result),200,"application/json");

	} // closing mysql_num_rows
	else
	{
			// If no records such records available 
			$strError = array("success" => "1" ,"UserId" => "");
			$this->response($this->toJson($strError), 204,"application/json");
	}	
				
	
?>