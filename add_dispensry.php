<?php 
include_once("config.php");
include_once("function.php");
?>
<div class="cell_con">
    <div class="gray_headingbar"> <h2>  Add Dispensary Details </h2> </div>
         <div class="content" id="content_1">
         	<div class="edit_dis_con">
         
              <div class="addleftcon"> 
              <form action="add_dispansary_ajaximage.php" enctype="multipart/form-data" method="post" id="dispansary_add_img_form" name="dispansary_add_img_form">
                  <input class="hideme" type="file" name="disImg" id="disImg" />
              </form>
            <div class="disphoto_con"> <img class="scale-with-grid" src="images/new_web/profile_pic.png"> </div>   
            <div> <a id="addDispensaryImg" href="javascript:void(0);"class="yellow_btn"> Upload </a> </div> 
            <div class="clear">&nbsp;</div>
            </div>
                        
              <form name="add_dispensaryform" id="add_dispensaryform" method="post" action="add_dispensary_on_menu.php"> 
              
              <div class="addritcon"> 
                    	 <dl>
                            <dt> Customer Name 
                            	<span class="adddisinput">
                            	 	<input type="text" name="customername" id="customername" maxlength="50" tabindex="1" > 
                                 </span> 
                            </dt>
                            <dd> Hours of operation 
                            	<span class="adddisinput"> 
	                              <input type="text" name="hoursofoperation" id="hoursofoperation" maxlength="50" tabindex="6"> 							   </span>
                            </dd>
                         </dl>
                         
                         <dl>
                            <dt> Street address 
                            	<span class="adddisinput"> 
                                	<input type="text" tabindex="2" name="streetaddress" id="streetaddress" maxlength="50" >
                                </span>
                            </dt>
                            <dd> Phone number 
                            	<span class="adddisinput"> 
                                  <input type="text"  name="phonenumber" id="phonenumber" maxlength="50" tabindex="7">
                                </span> 
                           </dd>
                         </dl>
                         
                         <dl>
                            <dt> City 
                            	<span class="adddisinput"> 
                                	<input type="text" tabindex="3" name="city" id="city" maxlength="50" > 
                                </span> 
                            </dt>
                            <dd> Email 
                            	<span class="adddisinput"> 
                                  <input type="text"  name="email" id="email" maxlength="50" tabindex="8"> 
                                </span> 
                            </dd>
                         </dl>
                         
                         <dl>
                            <dt> State 
                            	<span class="adddisinput"> 
                                	<input type="text" name="state" id="state" maxlength="50"  tabindex="4"> 
                                </span> 
                            </dt>
                            <dd> Website 
                            	<span class="adddisinput"> 
                                	<input type="text" name="website" id="website" maxlength="50" tabindex="9"> 
                                 </span> 
                            </dd>
                         </dl>
                         
                         <dl>
                            <dt> Zipcode 
                            	<span class="adddisinput"> 
                                	<input type="text" name="zipcode" id="zipcode" maxlength="6" tabindex="5"> 
                                </span> 
                           </dt>
                          
                          <dd>&nbsp; </dd>
                         </dl>
                         
                         <!--adding bio-->
						<dl>
                        <dt style="padding-top:1px;" > Bio 
                      	<span class="regbio" style="margin-left:0px;margin-top:2px;">
                    <textarea placeholder="Enter Bio"  id="bio_dis" name="bio_dis" col="200" maxlength="300" ></textarea>
                    </span>
                    </dt>
                    <dd>&nbsp;</dd>
                    </dl> 
						<!--end of bio-->
                    </div>
                    
    		  <div class="dividerline"> <img class="scale-with-grid" src="images/new_web/dividerline.png"> </div>
                        
             <!--<div class="adddiscon"> 
                  <div class="fllft"> <span> <div class="custom-checkbox">
                  <input type="checkbox" value="Remove" name="Remove"></div> &nbsp; &nbsp; Remove </span> </div>
                  <div class="lftmar40"> <span class="lftmar8"> <a href="javascript:void(0);" onclick="addRow('menudata_<?php echo $_POST['tabelno'];?>');"> <img width="96" height="25" src="images/new_web/add_row_btn.png"> </a> </span>  
                                  <span class="lftmar8"> <a href="javascript:void(0);" onclick="deleteRow('menudata_<?php echo $_POST['tabelno'];?>');"> <img width="113" height="25" src="images/new_web/delete_row_btn.png"> </a> </span>
                                  <span class="lftmar8"> <a href="javascript:void(0);" onclick="addColumn('menudata_<?php echo $_POST['tabelno'];?>');" > <img width="121" height="25" src="images/new_web/add_column_btn.png"> </a> </span>
                                  <span class="lftmar8"> <a href="javascript:void(0);"  onclick="deleteColumn('menudata_<?php echo $_POST['tabelno'];?>');"> <img width="138" height="25" src="images/new_web/delete_column_btn.png"> </a> </span>
                            </div>
                            
                            
                            <div class="row">
                                <span class="rowdiv"> <div class="custom-checkbox"><input type="checkbox" value="Remove" name="Remove"></div> </span>
                                <span class="typeinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                            </div>
                            
                             
                             <div class="row">
                                <span class="rowdiv"> <div class="custom-checkbox"><input type="checkbox" value="Remove" name="Remove"></div> </span>
                                <span class="typeinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                            </div>
                            
                             <div class="row">
                                <span class="rowdiv"> <div class="custom-checkbox"><input type="checkbox" value="Remove" name="Remove"></div> </span>
                                <span class="typeinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                            </div>
                            
                            <div class="row">
                                <span class="rowdiv"> <div class="custom-checkbox"><input type="checkbox" value="Remove" name="Remove"></div> </span>
                                <span class="typeinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                                <span class="valueinput"> <input type="text" size="20"> </span>
                            </div>
                            <div class="dividerline"> <img width="504" height="2" src="images/new_web/dividerline.png"> </div>
                            
                            
                            <div class="lftmar50"> 
                        <input type="hidden" name="moptionntabelon" id="moptionntabelon" value="0" />
                        
                        <span class="lftmar8"> 
                        	<a href="javascript:void(0);"  name="addmoreoption" id="addmoreoption" 
                            onclick="menuoption();"> 
                            	<img width="160" height="25" src="images/new_web/addmore_option_btn.png">
                             </a> 
                        </span>  
                        <span class="lftmar8"> 
                        	<a href="javascript:void(0);" name="removeoption" id="removeoption" onclick="remove_option();"> 
                            	<img width="146" height="25" src="images/new_web/remove_btn.png"> 
                            </a> 
                       </span>
                       <span class="lftmar8"> 
                       		<a href="javascript:void(0);" onclick="validate_dispensary();" >
                            	<img width="55" height="25" src="images/new_web/save_btn.png"> 			
                            </a> 
                       </span>
                       <span class="lftmar8"> 
                            <a href="javascript:void(0);" onclick="goOneStepBAck();"> 
                              <img width="69" height="25" src="images/new_web/cancel_btn.png">
                            </a> 
                       </span>
                       </div>
                        </div>-->
                        
              <div class="admindispmenu"></div>
                    
                    <div class="lftmar50"> 
                        <input type="hidden" name="moptionntabelon" id="moptionntabelon" value="0" />
                        <span class="lftmar8"> 
                        	<a href="javascript:void(0);"  name="addmoreoption" id="addmoreoption" 
                            onclick="menuoption();"> 
                            	<img width="160" height="25" src="images/new_web/addmore_option_btn.png">
                             </a> 
                        </span>  
                        <span class="lftmar8"> 
                        	<a href="javascript:void(0);" name="removeoption" id="removeoption" onclick="remove_option();"> 
                            	<img width="146" height="25" src="images/new_web/remove_btn.png"> 
                            </a> 
                       </span>
                        <span class="lftmar8"> 
                       		<a href="javascript:void(0);" onclick="validate_dispensary();" >
                            	<img width="55" height="25" src="images/new_web/save_btn.png"> 			
                            </a> 
                       </span>
                        <span class="lftmar8"> 
                            <a href="javascript:void(0);" onclick="goOneStepBAck();"> 
                              <img width="69" height="25" src="images/new_web/cancel_btn.png">
                            </a> 
                       </span>
                       </div>          
                  </form>      
               </div>         
                    </div>
    </div>
</div>