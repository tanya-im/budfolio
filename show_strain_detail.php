<?php 
// show strain detail in lightbox
session_start();
include_once("config.php");
if(isset($_POST['strain_id'])&& !empty($_POST['strain_id']))
{
	echo '<div id="light_box_wrap">';
	$getStrainQuery="select * from strains where flag='".Active."'and strain_id='".$_POST['strain_id']."'";
	$getStrainSql=mysql_query($getStrainQuery);
	while($getStrainResult=mysql_fetch_array($getStrainSql))
	{
		$strain_images_preview_query="select * from strainimages where strain_id='".$_POST['strain_id']."' order by image_id desc";
		$strain_preview_imgdata=mysql_query($strain_images_preview_query);
		$images_list_no=mysql_num_rows($strain_preview_imgdata);
		$exp_query="select * from experience where strain_id='".$straindetail['strain_id']."'";
		$exp_data=mysql_query($exp_query);
		$medicinaluse_query="select * from medicinal_use where strain_id='".$_POST['strain_id']."'";
		$medicinaluse_data=mysql_query($medicinaluse_query);
		?>
		<div class="strain_img">
        	<?php 	
			if($images_list_no<1)
			{
				echo '<img class="uingfordisp" src="images/uploads/original/User_default.JPG" />';
			}
			else
			{
				echo '<div id="mcts1">';
				while($strain_preview_images_list=mysql_fetch_array($strain_preview_imgdata)){?>
            		<img src="images/uploads/original/<?php echo $strain_preview_images_list['image_name'];?>" />
			<?php } ?>
                </div>
        	<?php } ?>
        </div>
		<div class="strain_detail left100">
			<div class="strain_area_one left100">
				<?php	if(!empty($getStrainResult['strain_name'])){?> 
					<div class="left25">
                    	<label class="stain_detail_label">Strain Name : </label>
                    	<label><?php echo $getStrainResult['strain_name'];?></label>
                    </div>
                <?php } if(!empty($getStrainResult['dispensary_name'])){ ?>
					<div class="left25">
                    	<label class="stain_detail_label">Dispensary : </label>
                    	<label><?php echo $getStrainResult['dispensary_name'];?></label>
                    </div>
					<?php } ?>
				<?php	if(!empty($getStrainResult['species'])){?> 
					<div class="left25">
                    	<label class="stain_detail_label">Species : </label>
                    	<label><?php echo $getStrainResult['species'];?></label>
                	</div>
				<?php } if(!empty($getStrainResult['linage'])){ ?>
					<div class="left25">
                    	<label class="stain_detail_label">Lineage : </label>
                    	<label><?php echo $getStrainResult['linage'];?></label>
                    </div>
				<?php } ?>
			</div>
			<div class="strain_area_two left100">
               	<div class="left25">	
					<h3>Ratings</h3>
					<?php if(!empty($getStrainResult['smell_rating'])){?> 
						<div>
                        	<label class="stain_detail_label_two">Smell : </label>
                        	<label><?php echo $getStrainResult['smell_rating'];?></label>
                        </div>    
					<?php } if(!empty($getStrainResult['taste_rate'])){ ?>
                    	<div>
							<label class="stain_detail_label_two">Taste : </label>
                        	<label><?php echo $getStrainResult['taste_rate'];?></label>
                    	</div>
					<?php } if(!empty($getStrainResult['strength_rate'])) {?>
						<div>
                        	<label class="stain_detail_label_two">Strength : </label>
                        	<label><?php echo $getStrainResult['strength_rate'];?></label>
                    	</div>
					<?php } if(!empty($getStrainResult['overall_rating'])){?>
						<div>
                        	<label class="stain_detail_label_two">Overall : </label>
                        	<label><?php echo $getStrainResult['overall_rating'];?></label> 
                        </div>
						<?php }?>      
				</div>
				<div class="left25 explbl">
					<h3>Experience</h3>	
					<?php while($exp_list=mysql_fetch_array($exp_data))
						{
							echo '<label>'.$exp_list['experience_name'].'</label>';
						}
					?>	
				</div>
                <div class="left25">
					<?php if(!empty($getStrainResult['consumption_name'])){?>
                        <h3>Consumption</h3> 
                        <label><?php echo $getStrainResult['consumption_name'];?></label>
                    <?php } ?>
                    </div>
				<div class="left25 mdcuse">
                   	<h3>Medicinal Use</h3>
                   	<?php	while($medicinaluse_list=mysql_fetch_array($medicinaluse_data))
							{
								echo '<div><label>'.$medicinaluse_list['medicinal_type'].'</label></div>';
							} 
					?>
				</div>
			</div>                
            <div class="left100 labtested">
                   	<h3>Lab Tested</h3>
					<?php if(!empty($getStrainResult['tested_by'])){?> 
                    	<div class="left20 padding3">
                        	<label class="stain_detail_label">Tested By : </label>
                            <label><?php echo $getStrainResult['tested_by'];?></label>
                       </div>
                    <?php }?>
                    <?php if(!empty($getStrainResult['thc'])){ ?>
                            <div class="left10 padding3">
                            	<label class="stain_detail_label_40">THC : </label>
                            	<label><?php echo $getStrainResult['thc'];?>%</label>
                        	</div>
					<?php } ?>
                    <?php if(!empty($getStrainResult['cbd'])) {?>
                            <div class="left10 padding3">
                            	<label class="stain_detail_label_40">CBD : </label>
                            	<label><?php echo $getStrainResult['cbd'];?>%</label>
                      		</div>
                    <?php } ?>
                   	<?php if(!empty($getStrainResult['cbn'])){?>
                            <div class="left10 padding3">
                            	<label class="stain_detail_label_40">CBN : </label>
                            	<label><?php echo $getStrainResult['cbn'];?>%</label> 
                    		</div>
					<?php }?>  
                    <?php if(!empty($getStrainResult['thca'])){?> 
                          	<div class="left10 padding3">
                          		<label class="stain_detail_label_40">THCa : </label>
                            	<label><?php echo $getStrainResult['thca'];?>%</label>
                        	</div>
					<?php } ?>
                    <?php if(!empty($getStrainResult['cbha'])){ ?>
                            <div class="left10 padding3">
                            	<label class="stain_detail_label_40">CBHa : </label>
                            	<label><?php echo $getStrainResult['cbha'];?>%</label>
                        	</div>
					<?php } ?>
                    <?php if(!empty($getStrainResult['moisture'])) {?>
                            <div class="left20 padding3">
                            	<label class="stain_detail_label">Moisture : </label>
                            	<label><?php echo $getStrainResult['moisture'];?></label>
                        	</div>
					<?php }?>  
                </div>
				<div class="left100">
					<h3>Description</h3>
                    <div class="left100"><?php echo $getStrainResult['description']; ?></div>
                    </div>		
				</div>
			</div>
<?php }
}
else
{
	echo 'Strain not exist';
}
die;?>	