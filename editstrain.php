<?php
// Include header,auth,transection and constant array file.  
include_once("newHeader.php");
require_once("auth.php");
$_SESSION['strain_id'] = $_GET['strainid'];
include_once("strain_transection.php");
include_once("values_array.php");
$_SESSION['images']=array();

?>
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel= "stylesheet" type="text/css" href="css/jquery.editable-select.css" />
<script type="text/javascript" src="js/customInput.jquery.js"></script>

<?php 
// Get strain detail 
$strain_query ="select * from strains 
				where strain_id ='".$_GET['strainid']."'
				and flag = 'Active'
				and user_id = ".$_SESSION['user_id']."";
$straindata = mysql_query($strain_query);
$numrows = mysql_num_rows($straindata);
$straindetail = mysql_fetch_array($straindata);


$smellArray = explode(',',$straindetail['smell_rating']);
$tasteArray =explode(',',$straindetail['taste_rate']);
// Get strain latest image 
$strain_images_preview_query_one="select * from strainimages 
								  where strain_id='".$_GET['strainid']."' 
								  order by image_id asc limit 0,1";
$strain_preview_imgdata_one = mysql_query($strain_images_preview_query_one);
$strain_preview_images_list_one = mysql_fetch_array($strain_preview_imgdata_one);

$strain_images_preview_query_two = "select * from strainimages 
									where strain_id='".$_GET['strainid']."'
									and image_id!='".$strain_preview_images_list_one['image_id']."'
									order by image_id asc limit 0,1";
$strain_preview_imgdata_two = mysql_query($strain_images_preview_query_two);
$strain_preview_images_list_two = mysql_fetch_array($strain_preview_imgdata_two);

$strain_images_preview_query_three="select * from strainimages 
									where strain_id='".$_GET['strainid']."' 
									and image_id!='".$strain_preview_images_list_one['image_id']."'
									and image_id!='".$strain_preview_images_list_two['image_id']."' 
									order by image_id asc limit 0,1";
$strain_preview_imgdata_three = mysql_query($strain_images_preview_query_three);
$strain_preview_images_list_three =mysql_fetch_array($strain_preview_imgdata_three);

?>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft">
 			<div class="cell_con">
              <?php if($numrows>0){ ?> 
            	<div class="gray_headingbar"><div class="add_dis_btn" style="float:left;position:relative;right:0px;"> 
                    	<a style="float:left; margin-left: 13px;margin-top: 2px;" name="back_lounge" onclick="goOneStepBAck();" class="edit_btn" href="javascript:void(0);"> Back </a>
                     </div> <h2 style="width:72%;"> Update Strain </h2> </div>
                <div id="content_1" class="content">
             
                	<div class="edit_dis_con">
                    <?php 
				// Status or error messages.
				if(isset($_GET['strainstatus']))
				{
					if($_GET['strainstatus']=='added')
					{
						echo '<div class="left100">
							<h4 class="left100 green center fadinmsg">Strain added successfully.</h4></div>';
					}
					if($_GET['strainstatus']=='notadded')
					{
						echo '<div class="left100">
							<h4 class="left100 red center fadinmsg">Strain not added. Please try again.</h4></div>';
					}
				}
				// End messages
					?>
                     <span id="strainameval" class="error_msg_all red"></span>
                        <div class="addstraincon martop_btm2">
                           <div class="addstrain_photocon">
                             <div id="previewwrapper_1" class="comman">
                        		<div id="preview_1" class="add_pic">
                             <?php if(empty($strain_preview_images_list_one['image_name'])){?>
                            <img src="images/css_images/ad_img.png" class="scale-with-grid"/>
                        <?php }else{ $_SESSION['images'][0]=$strain_preview_images_list_one['image_name'];?>
                            <img src="images/uploads/original/<?php echo $strain_preview_images_list_one['image_name'];?>" class="scale-with-grid" /><?php }?>
                            </div></div>
                            
                             <div id="previewwrapper_2" class="comman">    
                       			 <div id="preview_2" class="add_pic">
                                <?php if(empty($strain_preview_images_list_two['image_name'])){?>
                                    <img src="images/css_images/ad_img.png" />
                                <?php }else{ $_SESSION['images'][1]=$strain_preview_images_list_two['image_name'];?>
                                    <span id="strainimageid_2" class="removeupdateimage">&nbsp;</span><img src="images/uploads/original/<?php echo $strain_preview_images_list_two['image_name'];?>" /><?php }?>
                        		</div>
                    		</div>                            
<div id="previewwrapper_3" class="comman">    
                        <div id="preview_3" class="add_pic">
                                <?php if(empty($strain_preview_images_list_three['image_name'])){?>
                                    <img src="images/css_images/ad_img.png" />
                                <?php }else{ $_SESSION['images'][2]=$strain_preview_images_list_three['image_name'];?>							<span id="strainimageid_3" class="removeupdateimage">&nbsp;</span><img src="images/uploads/original/<?php echo $strain_preview_images_list_three['image_name'];?>" /><?php }?>
                        </div>
                    </div>                            
                            <span class="mar_rit2"> <a href="javascript:void(0);" id="updatestrainimagedilogbox" class="edit_btn"> Add Photo </a> </span>
                           <form id="updateimageform" method="post" enctype="multipart/form-data" action='updatestrainimg.php'><input class="hideme" type="file" name="updatephotoimg" id="updatephotoimg" /></form>
                    <form id="updateimageformtwo" method="post" enctype="multipart/form-data" action='updatestrainimgTWO.php'><input class="hideme" type="file" name="updatephotoimgtwo" id="updatephotoimgtwo" /></form>
                    <form id="updateimageformthree" method="post" enctype="multipart/form-data" action='updatestrainimgTHREE.php'><input class="hideme" type="file" name="updatephotoimgthree" id="updatephotoimgthree" /></form>
                            </div>
                            
                            
                            
                            
                            
                            
                          <form method="post" name="strainopration" id="strainopration" onsubmit="return add_strain_validation();">
	          	<div id="primaryradionwapper">
                    	<input type="hidden" value="<?php echo $strain_preview_images_list_one['image_id'];?>" name="imgformcountone" id="imgformcountone"/>
                        <input type="hidden" value="<?php echo $strain_preview_images_list_two['image_id'];?>" name="imgformcounttwo" id="imgformcounttwo"/>
                        <input type="hidden" value="<?php echo $strain_preview_images_list_three['image_id'];?>" name="imgformcountthree" id="imgformcountthree"/> 
                            <dl>
                                <dt> Strain Name <span class="adddisinput"> <select name="strainname" id="strainname" class="editable-select">
                                  <?php
								  $strStrainLibSQl = "SELECT * FROM strain_library";
								  $res = mysql_query($strStrainLibSQl);
                                  while($rowSl= mysql_fetch_assoc($res))
								  {	
								  	if($straindetail['strain_name']==$rowSl['strain_name']){
								  		 echo '<option selected="selected" value="'.str_replace("\\", "",stripslashes($rowSl['strain_name'])).'">'.str_replace("\\", "",stripslashes($rowSl['strain_name'])).'</option>';			}else
									{
										
										 echo '<option value="'.str_replace("\\", "",stripslashes($rowSl['strain_name'])).'">'.str_replace("\\", "",stripslashes($rowSl['strain_name'])).'</option>';
										 	
									}
								  }
								   echo '<option selected="selected" value="'.str_replace("\\", "",stripslashes($straindetail['strain_name'])).'">'.str_replace("\\", "",stripslashes($straindetail['strain_name'])).'</option>';
								?>	
                                </select> </span> </dt>
                                <dd> Dispensary <span class="adddisinput"> <select name="dispensensary" id="dispensensary" class="editable-select">
                                <?php 
								$strGetDispSQL = "SELECT dispensary_id,dispensary_name FROM `dispensaries`";
								$responseDis = mysql_query($strGetDispSQL);
								while($dis=mysql_fetch_assoc($responseDis))
								{ 
									if($straindetail['dispensary_name']==$dis['dispensary_name']){
										echo '<option selected="selected" value="'.str_replace("\\", "",stripslashes($dis['dispensary_name'])).'">'.str_replace("\\", "",stripslashes($dis['dispensary_name'])).'</option>';		}else
									{
										
										 echo '<option value="'.str_replace("\\", "",stripslashes($dis['dispensary_name'])).'">'.str_replace("\\", "",stripslashes($dis['dispensary_name'])).'</option>';
									}
								}echo '<option selected="selected" value="'.str_replace("\\", "",stripslashes($straindetail['dispensary_name'])).'">'.str_replace("\\", "",stripslashes($straindetail['dispensary_name'])).'</option>';
								?>
                                </select></span> </dd>
                            </dl>
                             
                            <dl>
                                <dt> Species  <span class="adddisinput"> <select name="species" id="species">
                            	<option value="">Please select species</option> 
                               <?php	for($i=0;count($species)>$i;$i++)
										{
											if($straindetail['species']==$species[$i]){
												echo '<option selected="selected" value="'.stripslashes($species[$i]).'">'.stripslashes($species[$i]).'</option>';					}
											else{
												echo '<option value="'.stripslashes($species[$i]).'">'.stripslashes($species[$i]).'</option>';											}
										}
								?>
							</select>  </span> </dt>
                                <dd> Lineage  <span class="adddisinput"> <input type="text" placeholder="Enter Lineage"  name="linage" id="linage"  maxlength="55" value="<?php echo str_replace("\\", "",stripslashes($straindetail['linage']));?>"> </span> </dd>
                            </dl>
                             
                            <dl>
                                <dt> Consumption Method  <span class="adddisinput"> <select name="consumption" id="consumption">
                                	<option value="">Please select consumption</option> 
                                	<?php	for($i=0;count($consumption)>$i;$i++){
												if($straindetail['consumption_name']==$consumption[$i])
												{
													echo '<option selected="selected" value="'.$consumption[$i].'">'.$consumption[$i].'</option>';				}	
												else{echo '<option value="'.$consumption[$i].'">'.$consumption[$i].'</option>';									}
											}
									?>
                                </select>  </span> </dt>
                                <dd>&nbsp;  </dd>
                             </dl>
                        </div>
                        
                        <div class="addstraincon martop_btm2">
                <div class="headtext marbtm10"> Notes</div>
                <div class="add-con"> 
                    <textarea placeholder="Enter Description" rows="3" cols="57" maxlength='1000' name="description" id="description"><?php echo stripslashes($straindetail['description']);?></textarea>
                        <div class="dbox_msg">Max 1000 characters.</div>  
                </div>
            </div>
                        
    
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        <div class="addstraincon martop_btm2">
                        	<div class="headtext"> Tell us about your Bud </div>
                             <dl>
                                <dt> Smell <br /> <span class="select_wrap"> <select name="smell[]" id="smell" multiple="multiple" class="select_field">
                               <?php for($i=0;count($smell)>$i;$i++){
											if(in_array($smell[$i],$smellArray)){
												echo '<option selected="selected" value="'.$smell[$i].'">'.$smell[$i].'</option>';							}else{
												echo '<option value="'.$smell[$i].'">'.$smell[$i].'</option>';							
											}
										}
								?>
                            </select>  </span> </dt>
                                    
                                <dd> Taste <br /> 
                                <span class="select_wrap"> 
                                	<select name="taste[]" id="taste" multiple="multiple" class="select_field">  
                                 <?php for($i=0;count($taste)>$i;$i++){
											if(in_array($taste[$i],$tasteArray))
											{
												echo '<option selected="selected" value="'.$taste[$i].'">'.$taste[$i].'</option>';							}else{
												echo '<option value="'.$taste[$i].'">'.$taste[$i].'</option>';
											}
										}
								?>
                            </select> </span> </dt>
                             </dl>
                             
                             <dl>
                                <dt> Strength  <span class="adddisinput"> <select name="strength" id="strength"> 
                        	<option value="">Please select strength</option> 
                          <?php	for($i=0;count($strength)>$i;$i++)
								{
										if($straindetail['strength_rate']==$strength[$i]){
											echo '<option selected="selected" value="'.$strength[$i].'">'.$strength[$i].'</option>';				}else{
											echo '<option value="'.$strength[$i].'">'.$strength[$i].'</option>';
										}
									}
							?>
                        </select>  </span> </dt>
                                    
                                <dd> Rating  <br> <span class="martop_btm2 floatlft"><div class="rating_wrapp">
                                <?php for($r=1;$r<6;$r++){ 
                            			if($straindetail['overall_rating']>=$r)
										{
											echo '<a href="javascript:void(0);" class="ratthis rated" id="ratting_'.$r.'">&nbsp;</a>';		}else{
											echo '<a href="javascript:void(0);" class="ratthis" id="ratting_'.$r.'">&nbsp;</a>';							}	
                            		}
								?>
                            </div>
                             <input type="hidden" name="overall" id="overall" value="<?php echo $straindetail['overall_rating'];?>"> </span> </dd>
                             </dl>
                        </div>
                        
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                         <!-- exprience-->
                        <div class="addstraincon martop_btm2">
                        <div class="headtext marbtm10"> Experiences </div>
                          <div class="select_wrap">
                        	<select name="experience[]" class="select_field" multiple="multiple" id="experience" >
                            	
                                <?php	
								$getExpSQL="SELECT * FROM experience 
								WHERE strain_id='".$_GET['strainid']."'";
								$resEXp = mysql_query($getExpSQL);
								$arrayExp= array();
								while($exp=mysql_fetch_assoc($resEXp))
								{
										$arrayExp[]=$exp['experience_name'];
                                }
								for($i=0;count($experience)>$i;$i++)
										{
											if(in_array($experience[$i],$arrayExp)){
											echo '<option selected="selected" value="'.$experience[$i].'">'.$experience[$i].'</option>';				}else{
											echo '<option value="'.$experience[$i].'">'.$experience[$i].'</option>';											}
											
										}
		      		?>	
							</select>  <!--</span>-->
                          </div>
                        </div>
                        <!--end of experience-->
                        
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                         <div class="addstraincon martop_btm2">
                        	<div class="headtext marbtm10"> Medicinal Uses </div>
                              <div class="select_wrap"> <select name="medicinaluse[]" id="medicinaluse" class="select_field" multiple="multiple">
                            	<?php	
								$medicinaluse_query ="select * from medicinal_use 
													  where strain_id='".$straindetail['strain_id']."'";
								$medicinaluse_data = mysql_query($medicinaluse_query);
								$arrMedi= array();
								while($medi=mysql_fetch_array($medicinaluse_data))
								{
											
									$arrMedi[]=$medi['medicinal_type'];
							  	}
									for($i=0;count($medicinaluse)>$i;$i++)
									{
											if(in_array($medicinaluse[$i],$arrMedi)){
											echo '<option selected="selected" value="'.$medicinaluse[$i].'">'.$medicinaluse[$i].'</option>';				}else{
											echo '<option value="'.$medicinaluse[$i].'">'.$medicinaluse[$i].'</option>';}
									}
								?>	
							</select>   </div>
                            
                            
                            
                        </div>
                        
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        
               <div class="addstraincon martop_btm2">
               <div class="headtext marbtm10"> Lab Tested </div>
                    <div class="add-con" id="testedbyinput"> 
                    <dl>
                        <dt> Tested By <br>
                        	<span class="adddisinput"> 
                        	<input type="text" placeholder="Enter Tested By" name="testedby" id="testedby" maxlength="55" value="<?php echo $straindetail['tested_by'];?>"/></span> 
                        </dt>
                        <dd> THC <br> 
                        	<span class="adddisinput">
                            <input placeholder="Enter THC" type="text" name="thc" id="thc" maxlength="5" value="<?php echo $straindetail['thc'];?>"/><span style="float:right;" > %</span></span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> CBD <br> 
                        	<span class="adddisinput"> 
                        	<input placeholder="Enter CBD" type="text" name="cbd" id="cbd" maxlength="5" value="<?php echo $straindetail['cbd'];?>"/><span style="float:right;"> %</span></span> 
                        </dt>
                        <dd> CBN <br> 
                        	<span class="adddisinput">
                            	<input placeholder="Enter CBN" type="text" name="cbd" id="cbd" maxlength="5" value="<?php echo $straindetail['cbn'];?>"/>
                                <span style="float:right;"> %</span></span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> THCa <br> 
                        <span class="adddisinput"> 
                        	<input placeholder="Enter THCa" type="text" name="tcha" id="tcha" maxlength="5" value="<?php echo $straindetail['thca'];?>"/>
                        <span style="float:right;"> %</span></span></dt>
                        <dd> CBHa <br> 
                        	<span class="adddisinput">
                            	<input placeholder="Enter CBHa" type="text" name="cbha" id="cbha" maxlength="5" value="<?php echo $straindetail['cbha'];?>"/>
                                <span style="float:right;"> %</span></span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> Moisture <br> <span class="adddisinput"> <input placeholder="Enter Moisture" type="text" name="moisture" id="moisture" maxlength="5" value="<?php echo $straindetail['moisture'];?>"/><span style="float:right;"> %</span></span></dt>
                    </dl>
                </div>
            </div>
                  
                <!--<div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div> -->
                  
                        
                        

                        <div style="clear:both;"></div>
                        <div style="float:right;"><span class="mar_rit2"> <a href="javascript:void(0);" id="addstrainsubmit" class="edit_btn"> Update Strain </a> </span></div>
                        <input type="hidden" name="update_strain" id="update_strain" value="update strain" />
                      </form>  
                    </div>
                </div>
            </div>
             <?php } else{ echo '<strong class="invalidstrain">Invalid Strain</strong>';}?>    
            </div>
        </div>
        <?php include('newLounge_right.php');?>
	</div>
</div>
<div class="footer"><?php include('footer.php');?></div>
<?php echo include('budfolio_footer.php');?>



<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
<script type="text/javascript" src="js/jquery.editable-select.pack.js"></script>
<script>

$(document).ready(function() {
	
	 $('.editable-select').editableSelect(
      {
        bg_iframe: true,
        onSelect: function(list_item) {
        /*alert('List item text: '+ list_item.text() +'<br> \
          Input value: '+ this.text.val());*/        }
      }
    );
   /* var select = $('.editable-select:first');
    var instances = select.editableSelectInstances();
    instances[0].addOption('Germany, value added programmatically');*/

	  

	$("#searchtext").live('keyup',function(){
		$("#searcherror").html("&nbsp;");
	});

});
	

$(function() {
	$('input').customInput();
	$("#smell").multiselect();
	$("#taste").multiselect();
	$("#experience").multiselect(); 
	$("#medicinaluse").multiselect(); 
  });

</script>
<div id="boxes"></div>
<div id="mask"></div> 
</body>

</html>
