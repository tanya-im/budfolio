<?php 
session_start();
// Include header,auth and config file. #C8C8C8
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include_once("function.php");
$_SESSION['scrollcounter']=1;
if(isset($_GET['dispid'])&& !empty($_GET['dispid']))
{
	$FWPflag = getFollowUpList($_GET['dispid'],$_SESSION['user_id']);
	$getDispancryQuery='SELECT * 
						FROM dispensaries 
						WHERE dispensary_id="'.$_GET['dispid'].'"';
	$getDispancrySql=mysql_query($getDispancryQuery);
	$getDispancryResult=mysql_fetch_array($getDispancrySql);
	
?>
<link rel="stylesheet" href="css/jquery-ui.css" />
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/organictabs.jquery.js"></script>
<script>
$(function() {
	$( "#tabs" ).tabs({
						beforeLoad: function( event, ui ) {
ui.jqXHR.error(function() {
ui.panel.html(
"Please wait while tab is loading." );});}});
});
</script>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container" id="inner_main_div">
 			<div class="cell_con">
              <div class="gray_headingbar">
              		<div class="add_dis_btn" style="float:left;position:relative;right:0px;"> 
                    	<a style="float:left; margin-left: 13px;margin-top: 2px;" name="back_lounge" onclick="goOneStepBAck();" class="edit_btn" href="javascript:void(0);"> Back </a>
                     </div>
                    <h2 style="width:73%;"><?php echo str_replace("\\", "", stripslashes(truncatestr($getDispancryResult['dispensary_name'],35)));?></h2>
                	<!-- for follow button-->
                	<span style="margin:12px;" class="dispensary_followbtn" id="disPopMF_<?php echo $getDispancryResult["dispensary_id"];?>">
    <?php if($FWPflag==0){?>
    <a href="javascript:void(0);" <?php if(!empty($_SESSION['user_id'])){?> class="disPopfolloup-on" <?php }else{?> class="alertRequest" <?php } ?> id="followupid_<?php echo $getDispancryResult["dispensary_id"];?>"><img src="images/new_web/follow_green_btn.png" > </a> 
    <?php }else{
		?><a href="javascript:void(0);"  <?php if(!empty($_SESSION['user_id'])){?> class="disPopfolloup-off" <?php }else{?> class="alertRequest" <?php } ?>  id="followupid_<?php echo $getDispancryResult["dispensary_id"];?>"><img src="images/new_web/unfollow_btn.png" /></a><?php } ?></span>
                	<!-- for follow button-->
                </div>           
                
              <div id="content_1" class="content">
                <div class="popupcell"> 
					<div class="store_pic">
            	<?php if(empty($getDispancryResult['image'])){?>
                <img src="images/css_images/storeimg.png">
				<?php }else{?>
                <img src="images/dispensary_images/original/<?php echo $getDispancryResult['image'];?>" />
                <?php } ?>
			</div>                
           			<div class="popupcell_left">
            	<span class="cellheading"><?php echo stripslashes($getDispancryResult['dispensary_name']);?></span><br/> 
               
                <?php echo ($getDispancryResult['street_address']!=''?stripslashes($getDispancryResult['street_address']):'');?> 
                <br />
                <?php echo ($getDispancryResult['city']!=''?stripslashes($getDispancryResult['city']):'');?>
                <?php echo ($getDispancryResult['state']!=''?', '.stripslashes($getDispancryResult['state']):'N/A');?>
                <?php echo ($getDispancryResult['zip']!=''?', '.$getDispancryResult['zip']:'N/A');?>
                <br/>
                Hours of operation : <?php echo ($getDispancryResult['hours_of_operation']!=''?$getDispancryResult['hours_of_operation']:'N/A'); ?>
                <br />
                Phone  <span class="sinlebar">|</span> <?php echo ($getDispancryResult['phone_number']!=''?$getDispancryResult['phone_number']:'N/A');?>
                <br />
                Email  <span class="sinlebar">|</span><?php echo ($getDispancryResult['email']!=''?$getDispancryResult['email']:'N/A');?>
			</div>
					<div class="popupcell_right"> 
            			<span style="font-weight:bold;">Bio - </span>  <br />
               			 <div style="min-height:100px;">
				<?php echo $getDispancryResult['bio']!=''?atRateUserNameWeb(stripslashes($getDispancryResult['bio'])):'N/A'; ?>
               </div> 
                	</div><!-- edn of popupcell_right-->
       		
            <!-- options -->
                <div  class="dispensry_info_options">
              <?php
			     $strGetSettingsSQL ='SELECT * 
				 					  FROM dispensary_settings 
									  WHERE dispensary_id="'.$getDispancryResult["dispensary_id"].'"';
				 $resSettings = mysql_query($strGetSettingsSQL)	 or die ($strGetSettingsSQL." : ".mysql_error());
			     $settingsData = mysql_fetch_assoc($resSettings);
			     $settingRows = mysql_num_rows($resSettings);
			     if($settingRows>0){
		 	?>
    	<?php if($settingsData['DeliveryService']=='1'){?>
        	<img src="images/new_web/BF_shiping_icon (2).png"  style="margin-left:77px;"/>&nbsp;
        <?php }?>
        <?php if($settingsData['StoreFront']=='1'){?>
        	<img src="images/new_web/BF_shope_icon (2).png" />&nbsp;
        <?php }?>
        <?php if($settingsData['AcceptCreditCard']=='1'){?> 
        	<img src="images/new_web/BF_debitcard_icon (2).png" />&nbsp;
        <?php }?> 
        <?php if($settingsData['AcceptATMonSite']=='1'){?>
        	<img src="images/new_web/BF_atmcard_icon (2).png" />&nbsp;
        <?php }?>
        <?php if($settingsData['18YearsOld']=='1'){?>
        	<img src="images/new_web/BF_18year_icon (2).png" />&nbsp;
        <?php }?>
      	<?php if($settingsData['21YearsOld']=='1'){?>
        	<img src="images/new_web/BF_21year_icon (2).png" />&nbsp;
        <?php }?>
       	<?php if($settingsData['HandicapAssesseble']=='1'){?>
        	<img src="images/new_web/BF_handicap_icon (2).png" />&nbsp;
        <?php }?> 
        <?php if($settingsData['Security']=='1'){?>
        	<img src="images/new_web/BF_security_icon (2).png" />&nbsp;
        <?php }?>
         
		<?php	 }// settingRows>0?>
          </div><!-- end of option-->
            		
                    <!-- menu-->
                    <div id="dsp_menupp">
       			 <div class="cellouter"><!-- cellouter
			<div class="cellable"> Menu </div>-->
           <div id="tabs"  >
            	<ul>
                	<li class="nav-one"><a href="#tabs-1"> Menu </a></li>
                    
                    
                    <li class="nav-two"><a href="my_strains_list.php?added_by=<?php echo $getDispancryResult['added_by']; ?>"><span class="marrit10"> Visual menu</span>
                    <span class="ratting_display"><?php echo getStrainsCount($getDispancryResult['added_by']);?></span> </a></li>
                    <li class="nav-three">
                    	<a href="reviews.php?dispensaryId=<?php echo $getDispancryResult['dispensary_id']; ?>"> <span class="marrit10"> Reviews </span> 
                     	<span class="ratting_display"><?php echo getReviewsCount($getDispancryResult['dispensary_id']);?></span> </a>
                    </li>
                    
                   <li class="nav-four"><a href="my_bud_thought_lists.php?added_by=<?php echo $getDispancryResult['added_by']; ?>"><span class="marrit10"> Budthought</span>
                    <span class="ratting_display"><?php echo getBudThoughtsCount($getDispancryResult['added_by']);?></span> </a></li>
                    <li class="nav-five">
                    <a href="dispensaryPhotos.php?dispensaryName=<?php echo base64_encode(stripslashes($getDispancryResult['dispensary_name'])); ?>">
                    	<span class="marrit10" >Photos </span>
                        <span class="ratting_display"><?php echo getPhotosCount(stripslashes($getDispancryResult['dispensary_name']));?></span></a></li>
                        
                        
                    <li class="nav-six"><a href="dispensaryFollowersList.php?dispensaryId=<?php echo $getDispancryResult['dispensary_id']; ?>"><span class="marrit10"> Followers</span>
                    <span class="ratting_display"><?php echo getFollowersCount($getDispancryResult['dispensary_id']);?></span> </a></li>
                    
               <li class="nav-seven last"><a <?php if(!empty($_SESSION['user_id'])){?> href="loc_review.php?dispensaryId=<?php echo $getDispancryResult['dispensary_id']; ?>" <?php }else{?> class="alertRequest" href="javascript:void(0);" <?php } ?>> <span class="marrit10" onclick="openScroller();"> Write a review </span> </a></li>
                </ul>
               <div class="list-wrap">
               <div id="tabs-1">
				<?php 
                $querone='SELECT * 
						  FROM menu_heading 
						  WHERE dispensary_id = "'.$_GET['dispid'].'" 
						  ORDER BY menu_heading_id ASC';
                $resone=mysql_query($querone)or die($querone." : ".mysql_error());
                $tblno=mysql_num_rows($resone);
				if($tblno>0)
				{
                	while($dataone=mysql_fetch_array($resone))
					{
						$querytwo='SELECT * 
								   FROM dispensaries_detail 
								   WHERE  menu_heading_id="'.$dataone['menu_heading_id'].'"
								   ORDER BY menu_heading_id asc'; 
						$restwo=mysql_query($querytwo)or die(mysql_error());
						$restwono=mysql_num_rows($restwo);
						$l=0;
						while($datatwo=mysql_fetch_array($restwo))
						{
							$data=unserialize($datatwo['dispensaries_detail_data']);
							if($l==0)
							{
							echo '<div class="tabcell"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="popuptab">';	
							$lengthColumn = count($data);
								//echo $data[$lengthColumn];
							  if($data[$lengthColumn-1]!='hide') // start of 6th may -->
							  {
									echo '<tr>';
									for($m=0;$m<count($data);$m++)
									{
										if($m==0)
										{
											echo '<td width="16%"><span class="orgtext"> '.str_replace("\\", "",$data[$m]).'</span></td>';	
										}
										else
										{
											echo '<td width="17%"><span class="orgtext">'.str_replace("\\", "",$data[$m]).'</span></td>';
										}
									}
									echo '</tr>';
							  }
							}
							else
							{
								$Queryuserno='SELECT distinct user_id 
											  FROM strains 
											  WHERE strain_name="'.str_replace("\\", "",$data[0]).'"
											  AND (dispensary_name= "'.stripslashes($getDispancryResult['dispensary_name']).'"
											  OR 	dispensary_id="'.$_GET['dispid'].'")
											  AND flag="active"';				
								
								$Sqluserno = mysql_query($Queryuserno)or die($Queryuserno." :- ".mysql_error()); 
								$Resuserno = mysql_num_rows($Sqluserno);
								if($data[$lengthColumn-1]!='hide') // <!-- start of 6th may -->
								{	
									if($Resuserno>0 || $restwono==($l+1))
									{
											echo '<tr class="nonbdr">';
									}
									else
									{
										echo '<tr class="bdrbtm">';
									}
									for($m=0;$m<count($data);$m++)
									{
										echo '<td>'.str_replace("\\", "",$data[$m]).'</td>';
									}	
									echo '</tr>';
								}
								if($Resuserno>0)
								{	
									echo '<tr><td colspan="5"';
									if($restwono==($l+1)){
										echo 'class="lastcellbdrnone" ';
									}
									else
									{
										echo 'class="lastcellbdr" ';
									}
									echo '>';
									
									echo '<div class="greentext padtop2 fllft"><span class="uratingtxt">';
									echo 'User Ratings('.str_replace("\\", "",$Resuserno).')&nbsp; &nbsp;';
									/*$Querysmell="select smell_rating,count(smell_rating) as rating from strains where strain_name='".$data[0]."' group by smell_rating ORDER BY rating desc LIMIT 0 , 1";
									$Sqlsmell=mysql_query($Querysmell)or die(mysql_error()); 
									$Ressmell=mysql_fetch_array($Sqlsmell);
									echo 'Smell : '.$Ressmell['smell_rating'].' &nbsp; | &nbsp;';
									$Querytaste="select taste_rate,count(taste_rate) as rating from strains where strain_name='".$data[0]."' group by taste_rate ORDER BY rating desc LIMIT 0 , 1";
									$Sqltaste=mysql_query($Querytaste)or die(mysql_error()); 
									$Restaste=mysql_fetch_array($Sqltaste);
									echo 'Taste : '.$Restaste['taste_rate'].' &nbsp; |  &nbsp;';*/
//strength									
$Querytaste='SELECT strength_rate,count(strength_rate) as strengthRate 
				 FROM strains 
				 WHERE strain_name="'.str_replace("\\", "",$data[0]).'" 
				 AND (dispensary_name= "'.stripslashes($getDispancryResult['dispensary_name']).'"
				 OR dispensary_id="'.$_GET['dispid'].'")
				 AND FLAG="active"
				 GROUP BY strength_rate
				 ORDER BY strengthRate desc  LIMIT 0 , 1';
			 
			   $Sqltaste=mysql_query($Querytaste)or die($Querytaste." : ".mysql_error()); 
			   $Restaste=mysql_fetch_array($Sqltaste);
			   echo 'Strength : '.$Restaste['strength_rate'].' &nbsp; |  &nbsp;';								
									
									
									
//avrage rating 
			$avrageRattingSQL ='SELECT avg( overall_rating ) AS avgsum
									FROM strains
									WHERE strain_name="'.str_replace("\\", "",$data[0]).'"
									AND (dispensary_name= "'.stripslashes($getDispancryResult['dispensary_name']).'"
									OR 	dispensary_id="'.$_GET['dispid'].'")
									AND FLAG = "active"';					   
						   
						   
  $SqlOverallRating_avg=mysql_query($avrageRattingSQL)or die($avrageRattingSQL." : ".mysql_error()); 
  $ResOverallRating_avg=mysql_fetch_array($SqlOverallRating_avg);
	
	$avg = $ResOverallRating_avg['avgsum'];			
									
									if(!empty($avg))
									{
										echo 'Rating</span><span class="countstar">'.$avg.'</span>';
									}else {	echo 'Rating</span><span class="countstar">0</span>'; }
									echo '</div>';
									echo '</td></tr>';
								}
							}
							$l++;
						}
						if($l>0){echo ' </table></div>';}
					}
				}
				else
				{
					echo '<div class="cell_dispensary center" style="padding:15px 0;">No menu detail is available. </div>';
				}	
				?>
                </div><!-- end of tab-1-->
            
               </div><!-- end of tabs-->
        </div><!-- cellouter-->
	</div>
        	</div>
            		<!-- menu end-->
         		</div>
	          </div>
        	</div>
         </div>   
	<?php include('newLounge_right.php');?>
    </div>
</div>

<div class="footer"><?php include('footer.php');?>
</div>
<?php include('budfolio_footer.php');
}//closing of while
//} //closing of if?>