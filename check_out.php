<?php 
//session_start();
// Include header,auth and config file. #C8C8C8
include_once("newHeader.php");
include_once("config.php");
require_once("auth.php");
include_once("function.php");

$pricelistQuery="select * from `dispensary_price_tbl` ";
$pricelistSql=mysql_query($pricelistQuery) or die(mysql_error());
$pricelist=mysql_fetch_array($pricelistSql);

?>
<style>
.txtcon a:hover{
	font-size:20px;
}
a {
    font-size: 15px;text-decoration:none;
}
a:hover{ font-size:15px; border:0px; outline:none;text-decoration:none;}

</style>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container" id="inner_main_div">
        
 			<div class="cell_con">
             <div class="error_txt">  <?php 
					//Login erro message or email confirmation message.
					if($_GET['status']=='Success')
					{ 
						echo '<p class="error_msg green fadinmsg">Your menu upgradation is successfully done. </p>';
					}
					if($_GET['status']=='fail')
					{
						echo '<p class="error_msg red fadinmsg">Your menu upgradation is not done.Please try again.</p>';
					} 
					
				?> </div>
            
            <div></div>
            <div class="gray_headingbar"> <h2>Upgrade</h2></div>
                <div class="setting_con">
                	<div class="checkout_headcon">
                        <div class="checkout_heading">Upgrade Now and Edit Your Dispensary Menu/profile anytime</div>  
                        <div class="fltrit"> <span class="qes_txt">Questions</span><span class="qes_icon">?</span> <span> <img src="images/new_web/phone_icon.png"> </span> <span class="ph_no">858-263-8658</span> </div>
                    </div>
                    
                    <div class="clear"></div>
                    
                    <!-- start of 1 months-->
                	<div class="txtcon txt_cnt">  
                    	<div class="txtcon_txt1"> 
                        	<span class="wit_txt">Monthly</span>
                            <span class="brw_txt"> $<?php echo $pricelist["monthly"] ?></span>
                            <span class="check_blktxt"> Billed every month </span>
                            <span class="grnamount_txt">  </span>
                   		</div> 
                        <img src="images/new_web/checkout_bg.png" class="scale-with-grid">
                        <!--upgrade_subscription.php?type=m -->
                        <a href="dispensary_selection.php?type=m" class="upgrade_btn"> Upgrade Now </a>
                    </div>
                    <!-- end of 1 months-->
                    
                    <!-- start of 6 months-->
                	<div class="txtcon txt_cnt">  
                    	<div class="txtcon_txt1"> 
                        	<span class="wit_txt">6 Months</span>
                            <span class="brw_txt"> $<?php echo $pricelist["half_yearly"] ?> </span>
                            <span class="check_blktxt"> Billed One-time </span>
                            <span class="grnamount_txt"> Save 5%  </span>
                            
                   		</div> 
                        <img src="images/new_web/checkout_bg.png" class="scale-with-grid">
                        
                        <a href="dispensary_selection.php?type=h" class="upgrade_btn"> Upgrade Now </a>
                    </div>
                	 <!-- end of 6 months-->
                     
                    <!-- start of 12 months-->
                    <div class="txtcon txt_cnt">  
                    	<div class="txtcon_txt1"> 
                        	<span class="wit_txt">12 Months</span>
                            <span class="brw_txt"> $<?php echo $pricelist["yearly"] ?></span>
                            <span class="check_blktxt"> Billed One-time </span>
                            <span class="grnamount_txt"> Save 10% </span>
                   		</div> 
                        <img src="images/new_web/checkout_bg.png" class="scale-with-grid">
                        
                        <a href="dispensary_selection.php?type=a" class="upgrade_btn"> Upgrade Now </a>
                    </div>
                    <!-- end of 12 months-->
                    <!--<br />
                    <div style="clear:both;margin-top: 24px;float: left;"><a href="payment.php?type=m">Click here to pay using PayFlow</a></div>-->
                    <div class="heading_brd">Upgrade Includes:</div>
                    <div class="check_list">
                    	<ul>
                        	<li> Dispensary listing </li>
                            <li> Menu  </li>
                            <li>Get Directions  </li>
                            <li> Direct Member communication </li>
                           <!-- <li> * 14-day trail upgrade </li>-->
                        </ul>
                    </div>
                    
                    <div class="check_list">
                    	<ul>
                        	<li> Social Media Plug-ins  </li>
                            <li> Share unlimited specials   </li>
                            <li> Share unlimited photos </li>
                          
                        </ul>
                    </div>
                    
					<div class="heading_brd">Terms:</div>
                    
                    <div class="check">
                    	<p>You may upgrade your dispensary profile at anytime. Doing so enables all the services listed above. Once you purchase an upgrade, your subscription to the applicable service will automatically renew month-to-month (unless otherwise stated) until you cancel your service by contacting Budfolio at 858-263-8658.

By upgrading your account you agree to pay the then-current applicable service fee listed on the site or quoted to you by Budfolio. Budfolio accepts payments through credit card through your profiles payment portal. If you pay by credit card and your profiles payment portal, Budfolio will automatically bill your credit card submitted in ordering the service on the date the service is activated, and each month thereafter, until you cancel your service. In the event you want to cancel your service, you must do so 30-days prior to your next billing date. You must contact your account manager, or Budfolio (858-263-8658) to cancel your service. If any fee cannot be charged to your credit card for any reason, Budfolio may provide you, via email or other method, notice of such non-payment and a link for you to update your payment information. If such non-payment is not remedied within seven (7) days after receiving such notice or non-payment, then Budfolio (A) may suspend applicable service(s) (B) may permanently revoke applicable service(s).

Full Site<a href='terms_condition.php'> / User terms here</a><br />Budfolio.com does not share, sell, rent, or trade personally identifiable information with third parties for promotional purposes.</p>
                    </div>
                    
                </div></div>
       </div>    
    
 <?php include('newLounge_right.php');?>
</div>
  </div>

<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>   