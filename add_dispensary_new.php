<?php 

// Include header,auth and config file. #C8C8C8
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
/*include('latLong.php');*/
include('function.php');
?>
<style>
.show_this_row
{
	background:#CCC;
	font-weight:bold;
	color:#000;	
}
.hide_this_row
{
	background:#0C6;
	font-weight:bold;
	color:#FFF;		
}
.content {
    min-height: 644px;
}
img
{
	margin-top: 9px;
}
.disphoto_con img {
    background: none repeat scroll 0 0 #FFFFFF;
    box-shadow: 0 0 5px #888888;
    height: 145px;
    margin: 4px 2% -4px -49%;
    padding: 4%;
    width: 130px;
}
.logout_btn a {
    color: #FFFFFF;
    float: right;
    font-family: 'Gotham-Book';
    font-size: 18px;
    margin: 0;
    padding: 0;
    position: absolute;
    right: 0;
    top: -4px;
    width: 16%;
}
a.yellow_btn {
    background-image: linear-gradient(to bottom, #FAE978, #E5CA15);
    border: 1px solid #FFF6B6;
    border-radius: 2px;
    color: #773E00;
    float: left;
    font-family: 'Gotham-Book';
    font-size: 18px;
    margin: 0;
    margin-left: -49%;
    margin-top: 4%;
    padding: 5px 10px;
    text-align: center;
    text-decoration: none;
    width: 41%;
}
</style>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div style="color:#2B6A03;">
    <?php if($_GET['data']=='success'){?>
    			<p class='green success_msg fadinmsg'>Thank you !!! <b></b> for upgrading menu services.<br>
				Now you can update or edit your menu . </p>
    	<?php }else if ($_GET['data']=='dberror'){?>
        		<p class='error_msg red fadinmsg'>Transaction is not inserted into DB.</p>
        <?php }else if ($_GET['data']=='nouser'){ ?>
        		<p class='error_msg red fadinmsg'>No Such user exist in our data base.</p>
        <?php } ?></div>
    	
    	<div class="inner_conlft scroll_container" id="inner_main_div">
			<div class="cell_con">
    <div class="gray_headingbar">
    	<div class="add_dis_btn" style="float:left;position:relative;right:0px;"> 
                    	<a style="float:left; margin-left: 13px;margin-top: 2px;" name="back_lounge" class="edit_btn"
                         href="my_menu_list.php"> Back </a>
        </div>	
     	<h2 style="width:85%;">  Add Dispensary Details </h2> </div>
         <div class="content" id="content_1" style="min-height:650px;">
         	<div class="edit_dis_con">
         
              <div class="addleftcon"> 
              <form action="add_dispansary_ajaximage.php" enctype="multipart/form-data" method="post" id="dispansary_add_img_form" name="dispansary_add_img_form">
                  <input class="hideme" type="file" name="disImg" id="disImg" />
              </form>
            <div class="disphoto_con"> <img class="scale-with-grid" src="images/new_web/profile_pic.png"> </div>   
            <div> <a id="addDispensaryImg" href="javascript:void(0);"class="yellow_btn"> Upload </a> </div> 
            <div class="clear">&nbsp;</div>
            </div>
                        
              <form name="add_dispensaryform" id="add_dispensaryform" method="post" action="add_dispensary_on_menu.php"> 
              
              <div class="addritcon"> 
                    	 <dl>
                            <dt> Customer Name 
                            	<span class="adddisinput">
                            	 	<input type="text" name="customername" id="customername" maxlength="50" tabindex="1" > 
                                 </span> 
                            </dt>
                            <dd> Hours of operation 
                            	<span class="adddisinput"> 
	                              <input type="text" name="hoursofoperation" id="hoursofoperation" maxlength="50" tabindex="6"> 							   </span>
                            </dd>
                         </dl>
                         
                         <dl>
                            <dt> Street address 
                            	<span class="adddisinput"> 
                                	<input type="text" tabindex="2" name="streetaddress" id="streetaddress" maxlength="50" >
                                </span>
                            </dt>
                            <dd> Phone number 
                            	<span class="adddisinput"> 
                                  <input type="text"  name="phonenumber" id="phonenumber" maxlength="50" tabindex="7">
                                </span> 
                           </dd>
                         </dl>
                         
                         <dl>
                            <dt> City 
                            	<span class="adddisinput"> 
                                	<input type="text" tabindex="3" name="city" id="city" maxlength="50" > 
                                </span> 
                            </dt>
                            <dd> Email 
                            	<span class="adddisinput"> 
                                  <input type="text"  name="contact_email" id="contact_email" maxlength="50" tabindex="8"> 
                                </span> 
                            </dd>
                         </dl>
                         
                         <dl>
                            <dt> State 
                            	<span class="adddisinput"> 
                                	<input type="text" name="state" id="state" maxlength="50"  tabindex="4"> 
                                </span> 
                            </dt>
                            <dd> Website 
                            	<span class="adddisinput"> 
                                	<input type="text" name="website" id="website" maxlength="50" tabindex="9"> 
                                 </span> 
                            </dd>
                         </dl>
                         
                         <dl>
                            <dt> Zipcode 
                            	<span class="adddisinput"> 
                                	<input type="text" name="zipcode" id="zipcode" maxlength="6" tabindex="5"> 
                                </span> 
                           </dt>
                          
                          <dd>&nbsp; </dd>
                         </dl>
                         
                         <!--adding bio-->
						<dl>
                        <dt style="padding-top:1px;" > Bio 
                      	<span class="regbio" style="margin-left:0px;margin-top:2px;">
                    <textarea placeholder="Enter Bio"  id="bio_dis" name="bio_dis" col="200" maxlength="300" ></textarea>
                    </span>
                    </dt>
                    <dd>&nbsp;</dd>
                    </dl> 
						<!--end of bio-->
                    </div>
              
              
              <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                     
                         <!-- share buttones-->
                          <div id="sharebtns"  style="margin:10px;">
                            <?php include_once("dispensary_fb_share.html"); ?>
                            <?php include_once("twitter.php"); ?>
                         	</div>  
                          <!--    share ennd-->
                           
                     <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                     
                      <!-- start of setting options-->
                      
                      <?php /* if($_SESSION['dispansary']==1){
					
					$getSettingsSQL = "SELECT * FROM dispensary_settings 
									   WHERE dispensary_id='".$_GET['disId']."'";
					$resSettings = mysql_query($getSettingsSQL) or die($getSettingsSQL." : ".mysql_error());
					$settings= mysql_fetch_assoc($resSettings);*/
					
					?>        
                        <div class="addstraincon martop_btm2">
                        	   <div class="gray_headingbar" style="margin-bottom:10px;"><h2 style="width:100%;"> Store Options</h2> </div>
                             
             <span class="check_con">
             <input name="sample" type="checkbox" class="checkbox" value="DeliveryService" id="delivery_service">  
             	 <label class="checkbox_conlable">Delivery Service </label> 
             </span>
              
             <span class="check_con"> 
             <input name="sample[]" type="checkbox" class="checkbox" value="StoreFront" id="store_front"> 
             <label class="checkbox_conlable">Store Front </label>
              </span>
			
             <span class="check_con">
             <input name="sample[]" type="checkbox" class="checkbox" value="AcceptCreditCard" id="credit_card">
             <label class="checkbox_conlable">Accept Credit Card </label> 
             </span>
             
             <span class="check_con"> 
             <input name="sample[]" type="checkbox" class="checkbox" value="AcceptATMonSite" id="accept_atm">
             <label class="checkbox_conlable">ATM on Site </label> 
             </span>
             
             <span class="check_con">
             <input name="sample[]" type="checkbox" class="checkbox" value="18YearsOld" id="age_18_year_old">
             <label class="checkbox_conlable">18 Years Old </label>
             </span>
             
             <span class="check_con">
             <input name="sample[]" type="checkbox" class="checkbox" value="21YearsOld" id="age_21_year_old">
             <label class="checkbox_conlable">21 Years Old </label> 
             </span>
                        
              <span class="check_con">
             <input name="sample[]" type="checkbox" class="checkbox" value="Security" id="security">
             <label class="checkbox_conlable">Security</label> </span>
             
             <span class="check_con">
        	 <input name="sample[]" type="checkbox" class="checkbox" value="HandicapAssesseble" id="handicap_assessable">
             <label class="checkbox_conlable">Handicap assessable </label> 
             </span>          
                        
                    <!--<span class="post_btn"> <a href="javascript:void(0);" class="edit_btn" id="UpdateDispSettings" style="display:none;"> Save </a> </span>-->    
              </div> 
                         
                    <!-- end of seetings options-->      
    		<!--  <div class="dividerline"> <img class="scale-with-grid" src="images/new_web/dividerline.png"> </div>-->
               <div class="gray_headingbar"> <h2> Enter your menu </h2> </div>          
             
                        
              <div class="admindispmenu">
              <?php include('menu_option.php'); ?>
              
              </div>
              
                    
                    <div class="lftmar50"> 
                           <input type="hidden" name="moptionntabelon" id="moptionntabelon" value="1" />
                        <span class="lftmar8"> 
                        	<a href="javascript:void(0);"  name="addmoreoption" id="addmoreoption" 
                            onclick="menuoption();"> 
                            	<img  height="25" src="images/new_web/add_btn1.png">
                             </a> 
                        </span>  
                        <span class="lftmar8"> 
                        	<a href="javascript:void(0);" name="removeoption" id="removeoption" onclick="remove_option();"> 
                            	<img width="146" height="25" src="images/new_web/remove_btn1.png"> 
                            </a> 
                       </span>
                        <span class="lftmar8"> 
                       		<a href="javascript:void(0);" onclick="validate_dispensary();" >
                            	<img width="55" height="25" src="images/new_web/save_btn.png"> 			
                            </a> 
                       </span>
                        <span class="lftmar8"> 
                            <a href="javascript:void(0);" onclick="goOneStepBAck();"> 
                              <img width="69" height="25" src="images/new_web/cancel_btn.png">
                            </a> 
                       </span>
                       </div>          
                  </form>      
               </div>         
                    </div>
    </div>
		</div>
    <?php include('newLounge_right.php');?>
    </div>
</div>    
<div class="footer"><?php include('footer.php');?>
</div>
<?php include('budfolio_footer.php');?>

<script type="text/javascript" src="js/row_col.js"></script>
<script type="text/javascript" src="js/row_col_for_edit.js"></script>
<script>
	(function($){
		$(window).load(function(){
			/*$("#content_1").mCustomScrollbar({
				scrollButtons:{
					enable:true
				}
			});
			$('.scroll_container').scrollExtend(
				{	'target': 'div#content_1',
					'url': 'menuContent.php?flag=1',
					'loadingIndicatorClass': 'scrollExtend-loading',
					'loadingIndicatorEnabled': true
				}
			);*/
		});
	})(jQuery);
	</script>
    <script>
$(function() {
	$( "#tabs" ).tabs({
						beforeLoad: function( event, ui ) {
ui.jqXHR.error(function() {
ui.panel.html(
"Couldn't load this tab." );});}});
});
</script>
<div id="boxes"></div>
<div id="mask"></div> 
</body>

</html>
