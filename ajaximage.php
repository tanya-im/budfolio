<?php session_start(); ob_start();

include_once("config.php");
include_once("function.php");
// define the image folder path
$path = "images/uploads/original/";
$path_medium="images/uploads/medium/";
$path_thumbnail="images/uploads/thumbnail/";
$path_cropped="images/uploads/cropped/";
$max_file_size = 1000 * 1024; #400kb
$nw = $nh =320; # image with # height
// Check request methode
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
	$name = $_FILES['photoimg']['name'];
	$size = $_FILES['photoimg']['size'];
	// check image exists or not
	if(strlen($name))
	{
		list($txt, $ext) = explode(".", $name);
		$extention = explode(".", $name);
		$arrayconunt=(count($extention)-1);
		if(!empty($extention[$arrayconunt]))
		{
			$ext=$extention[$arrayconunt];
		}
		// Check image formate valid or not  
		if(in_array(strtolower($ext),$valid_formats))
		{
			// check image size not more than 3 Mb.
			if($size< ImageSize)
			{
			$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
			 
			// Move to uploaded file from tem dir to destination dir.
			/*$a=move_uploaded_file($tmp, $path.$actual_image_name);
			if($a)
			{
			*/		//start of image crop	
					//$path = 'images/uploads/cropped/'. uniqid() . '.' . $ext;
					$path = 'images/uploads/original/'.$actual_image_name;
					$size = getimagesize($_FILES['photoimg']['tmp_name']);
				    $x = (int) $_POST['x'];
				    $y = (int) $_POST['y'];
				    $w = (int) $_POST['w'] ? $_POST['w'] : $size[0];
				    $h = (int) $_POST['h'] ? $_POST['h'] : $size[1];
					
					$data = file_get_contents($_FILES['photoimg']['tmp_name']);
					
					$vImg = imagecreatefromstring($data);
					
					
					
					$dstImg = imagecreatetruecolor($nw, $nh);
					imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
					imagejpeg($dstImg, $path);
					imagedestroy($dstImg);
					// end of image crop
					
					
					$src=$path;
					$dest_m=$path_medium.$actual_image_name;
					$dest_t=$path_thumbnail.$actual_image_name;
					error_reporting(E_ALL);
					resize_image($src,$dest_m,300,300);
					
					resize_image($src,$dest_t,100,100);
					
					
					
					$_SESSION['images'][]=$actual_image_name;
				
					$no=count($_SESSION['images']);
					$no=$no-1;
					echo '<span id="strainimageid_'.$no.'" class="previewremoveimage">&nbsp;</span>';
					echo "<img src='".$path."'  class='preview'>";
				/*}
				else	
				{
					echo "failed";
				}*/
			}
			else
			{
				echo "Image file size max 6  MB";					
			}
		}
		else
		{
			echo "Invalid file format..";	
		}
	}
	else
	{
		echo "Please select image..!";
	}		
	exit;
}
?>