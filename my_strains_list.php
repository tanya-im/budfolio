<?php
/*
	File name - lounge_strain_deatils.php
	File Description - to show the strain details
					   this file will go in if condition in lounge page
	date - 14th may
	  
*/ 
session_start();
include_once("config.php");
include_once("function.php");
include_once("timeAgoUtil.php");
?>
<style>
.cell_left a
{
	font-size:18px;
}
.cell_left a:hover
{
	font-size:18px;
}
</style>
<?php

if($_GET['added_by']!=0)
{

	//SQL query to get budfolio list
			
		$strBudfolioListQuery = 'SELECT strain_id,strain_name,post_date,
									 dispensary_name,species,overall_rating,
 			   						 smell_rating,taste_rate,strength_rate,
			                         description,user_id as userId
									 FROM strains 
									 WHERE flag="Active"
									 and user_id ="'.$_GET['added_by'].'" 
									 order by post_date desc';
			
			$arrayBudfolioListArray=array();
			$BudfolioListSQL = mysql_query($strBudfolioListQuery) or die(mysql_error()." : ".$strBudfolioListQuery );							            if(mysql_num_rows($BudfolioListSQL) > 0)
			{
				while($BudfolioList=mysql_fetch_array($BudfolioListSQL))
				{
					$querythree='select user_name,zip_code,state,photo_url 
		 			  			  from users 
					              where user_id="'.$_GET['added_by'].'" 
								  limit 0,1';
					$userdata=mysql_query($querythree);
					$udata=mysql_fetch_array($userdata);
					$date =funTimeAgo(strtotime($BudfolioList['post_date']));
					
					if(!empty($udata['photo_url']))
					{
						$img_src='images/profileimages/original/'.$udata['photo_url'];
					}
					else
					{ 
						$img_src="images/css_images/profile_pic.png";
					}

?>
		 			<div class="inn_cell">
         				<!-- start of grey cell-->
						<div class="gray_bg_cellnew">
        				 <span class="lounge_profile"> <img src="<?php echo $img_src;?>" class="scale-with-grid" ></span>
        	 			  <span class="name_link" style="margin-top:-7px;">
                          	 <h4> 
                             	<a href="user_popup_page.php?userId=<?php echo $BudfolioList['userId'];?>" id="userids_<?php echo $BudfolioList['userId'];?>"> <?php echo str_replace("\\", "",stripslashes(truncatestr($udata['user_name'],20)));?></a> 
                            </h4> 
              			  </span>
              			 <span class="datetime"> <?php echo $date;?> ago</span>
         				</div>
						<!-- end of grey cell-->
                  
                   <?php 
                      // fatching strain image 
                      $strain_images_preview_query='select * from strainimages 
                                                    where strain_id="'.$BudfolioList['strain_id'].'"
                                                    AND image_name!=""
                                                    order by image_id asc LIMIT 0,1';
                      $strain_preview_imgdata = mysql_query($strain_images_preview_query);
                      $strain_image_array= mysql_fetch_assoc($strain_preview_imgdata);
                      if($strain_image_array['image_name']!='')
                      {
                      ?><div class="loungeimg_new" style="height: 590px;position: relative;width: 100%;"> <img src="images/uploads/original/<?php echo $strain_image_array['image_name'];?>" class="scale-with-grid"> </div>
                      <?php
                      }else{
                      }
                      ?>
                      
                      <?php
                        $strLikeStrainSQL ='SELECT count(*) as count FROM tbl_strain_like 
                                            WHERE strain_id ="'.$BudfolioList['strain_id'].'"';
                        $resStrainSQL = mysql_query($strLikeStrainSQL) or die($strLikeStrainSQL." : ".mysql_error());
                        $rowCount = mysql_fetch_assoc($resStrainSQL);
                        $likeCount= $rowCount['count'];
                           
                        ?>
                      
                        <div class="status_cell"> 
                           <div class="status_con"> 
                              <span class="fleft new_marlft8"> <a href="javascript:void(0);" <?php if(!empty($_SESSION['user_id'])){?>class="like_strain_btn" <?php }else{?> class="alertRequest" <?php } ?> id="likeBudThought_<?php echo $BudfolioList['strain_id'];?>"> <img src="images/new_lounge/like_newicon.png">  </a></span> 	
                              <span class="fleft" id="like_count_<?php echo $BudfolioList['strain_id']; ?>" style="padding-top:5px;"><?php 
                              if($likeCount>0){echo $likeCount;}else{/* echo nthng*/ }?> </span>
                           </div> 
                           <div class="cell_right_new">
                                  <?php for($rating=1;$rating < 6;$rating++){
                                              if($BudfolioList['overall_rating']>=$rating) {?>
                                              <img src="images/css_images/start_sel.png">
                                  <?php 	} else {?>
                                              <img src="images/css_images/start_dsel.png">
                                  <?php	} }?>
                             </div>
                         </div>
                      
                      	<div class="cell_left">
            	<a href="strain_info_page.php?strain=<?php echo $BudfolioList['strain_id']; ?>" 
                id="strnid_<?php echo $BudfolioList['strain_id']; ?>" class="loung_straindetail1 scale-with-grid"><?php echo  str_replace("\\", "", stripslashes($BudfolioList['strain_name'])); ?></a></span><br> <?php echo stripslashes($BudfolioList['species']);?> <br> <?php 
					
			 $dispensaryName=stripslashes($BudfolioList['dispensary_name']);
			 $disNameArray = explode('-',$dispensaryName);
			if(count($disNameArray)>0)
			{
				$disName= $disNameArray[0];
				$disId =getDispensaryId($disName);
			}	
              $dispancry_link_query='select dispensary_id 
                                    from dispensaries 
                                    where LOWER(dispensary_name)="'.stripslashes($disName).'"
                                    Limit 0,1';
                  $dispancry_link_sql = mysql_query($dispancry_link_query);
                  $dispancry_link_result = mysql_fetch_array($dispancry_link_sql);
                  if(!empty($dispancry_link_result['dispensary_id']))
                  {
					  ?>
                      <a href='dispensary_info_page.php?dispid=<?php echo $disId;?>' id='displink_<?php echo $dispancry_link_result['dispensary_id'];?>' class='gray_link'><?php echo stripslashes($BudfolioList['dispensary_name']);?></a>
                  <?php }
                  else
                  {
                      echo stripslashes($BudfolioList['dispensary_name']);
                  }
              ?>
             </div>
                        
           			   <div class="arrownew"> <a 
			 <?php if(!empty($_SESSION['user_id'])){?> class="loung_straindetail1" href="strain_info_page.php?strain=<?php echo $BudfolioList['strain_id']; ?>"  <?php }else{?> class="alertRequest scale-with-grid" <?php } ?> id="strainid_<?php echo $BudfolioList['strain_id']; ?>"> 
<img src="images/new_web/arrow.png" class="scale-with-grid"> </a></div>
        
					</div>
                    <?php 
				}// end of while
			}// end of if
			else
			{
				echo '<div class="cell_dispensary center" style="padding:15px 0;">No Strains available. </div>';
			}
}else
{
	echo '<div class="cell_dispensary center" style="padding:15px 0;">No strain available.</div>';
}
				?>

