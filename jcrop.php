<div class="window" style="">
<link rel="stylesheet" type="text/css" href="css/jcrop_css/imgareaselect-animated.css" />
	
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/crop_js/jquery.imgareaselect.min.js"></script>
	<script type="text/javascript" src="js/jcrop_js/script.js"></script>

   <!-- <script type="text/javascript" src="js/mycustom.js"></script>-->
    
	<style>
	a, a h1{
		font-family: Georgia, "Times New Roman", Times, serif;
		font-size: 1.2em;
		color: #645348;
		
		text-decoration: none;
		font-weight: 100;
		padding: 10px;
	}
	body{
		font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
		text-transform: inherit;
		color: #582A00;d
		background: #E7EDEE;
		width: 100%;
		margin: 0;
		padding: 0;
	}
	.wrap{
		width: 97%;
		margin: 10px auto;
		padding: 10px 7px;
		background: white;
		border: 2px solid #DBDBDB;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		text-align: center;
		overflow: hidden;
	}
	img#uploadPreview{
		border: 0;
		border-radius: 3px;
		-webkit-box-shadow: 0px 2px 7px 0px rgba(0, 0, 0, .27);
		box-shadow: 0px 2px 7px 0px rgba(0, 0, 0, .27);
		margin-bottom: 30px;
		overflow: hidden;
	}
	input[type="submit"]{
		border-radius: 10px;
		background-color: #61B3DE;
		border: 0;
		color: white;
		font-weight: bold;
		font-style: italic;
		padding: 6px 15px 5px;
		cursor: pointer;
	}
	.button_upload
	{
		background-image: linear-gradient(to bottom, #FAE978, #E5CA15);
    border: 1px solid #FFF6B6;
    border-radius: 2px;
    color: #773E00;
    float: right;
    font-family: 'Gotham-Book';
    font-size: 18px;
    margin: 0;
    padding: 5px 10px;
    text-align: center;
    text-decoration: none;
	}
	</style>

<div class="wrap">

	<!-- image preview area-->
	<img id="uploadPreview" style="display:none;border:1px solid;" />
	
    
    
    <form id="imageform" method="post" enctype="multipart/form-data" action='ajaximage.php'>
						<input class="hideme" type="file" name="photoimg" id="uploadImage"/>
					<!--</form>
	 image uploading form 
	<form action="ajaximage.php" method="post" enctype="multipart/form-data">
		<input  type="file" accept="image/jpeg" name="photoimg" />-->
		<input type="button" value="Upload" id="upload_croped" class="button_upload">

		<!-- hidden inputs -->
		<input type="hidden" id="x" name="x" />
		<input type="hidden" id="y" name="y" />
		<input type="hidden" id="w" name="w" />
		<input type="hidden" id="h" name="h" />
	</form>
	
</div><!--wrap-->
</div>