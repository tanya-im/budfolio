<?php session_start(); ob_start();
// Include auth and config files.

include_once("config.php");
include_once("function.php");

// If user submit the add dispensary form 
if(isset($_POST))
{
	$date = date('Y-m-d H:i:s');
	
	//Check if fields empty redirect it to dispensary page with a error message.
	if(empty($_POST['customername']))
	{
		header('Location:dispensary_menu.php?validatedata=empty');exit();
	}
	
	$addressForLatLong= $zip.'+'.$_POST['city'].'+'.$_POST['state'];
	
	$latlongdata = getlatlongbyaddress($addressForLatLong);
	
	
		// Insert dispensary detail
	 	$query='insert into dispensaries set
					dispensary_name = "'.stripslashes(mysql_real_escape_string($_POST['customername'])).'",
					image ="'.mysql_real_escape_string($_SESSION['dispancryphotoimg']).'",
					street_address ="'.mysql_real_escape_string($_POST['streetaddress']).'",
					phone_number ="'.mysql_real_escape_string($_POST['phonenumber']).'",
					email ="'.mysql_real_escape_string($_POST['contact_email']).'",
					website ="'.mysql_real_escape_string($_POST['website']).'",
					city ="'.mysql_real_escape_string($_POST['city']).'",
					state ="'.mysql_real_escape_string($_POST['state']).'", 
					zip ="'.mysql_real_escape_string($_POST['zipcode']).'",
					hours_of_operation ="'.mysql_real_escape_string($_POST['hoursofoperation']).'",
					latitude ="'.$latlongdata['results'][0]['geometry']['location']['lat'].'",
					longitude ="'.$latlongdata['results'][0]['geometry']['location']['lng'].'",
					added_by ="'.mysql_real_escape_string($_SESSION['user_id']).'",
					bio ="'.mysql_real_escape_string($_POST['bio_dis']).'",
					date_time="'.$date.'"';
			
		$sql = mysql_query($query) or die($query." :".mysql_error());
		$lastid = mysql_insert_id();
		$dispensary_image = $_SESSION['dispancryphotoimg'];
		$_SESSION['dispancryphotoimg'] = '';
		
		
		// attach dispensary wid subscription 
					$checkSubsSQL = "SELECT 
										id,
										tpm.purchase_id AS sub_id,
										tpm.plan_type AS subscription_type,
										dispensary_id,
										start_date, expiry_date
								 FROM tbl_menu_purchase AS tpm
								 JOIN purchased_menu_table x ON tpm.purchase_id = x.purchase_id
								 WHERE 
										tpm.user_id = '".$_SESSION['user_id']."'
										AND ( NOW()
										BETWEEN start_date
										AND expiry_date
										)
								AND dispensary_id =0
								ORDER BY tpm.purchase_id ASC
								LIMIT 0 , 1";	 
							 
			 $resSubSQL = mysql_query($checkSubsSQL) or die($checkSubsSQL." : ".mysql_error()); 
					
			 $subDetail = mysql_fetch_assoc($resSubSQL);
					//print_r($subDetail);
					$subTYpe = $subDetail['subscription_type'];	
					$subDate = $subDetail['start_date'];
					$Id = $subDetail['id'];
					
			$UpdateSuBDisSQL ="UPDATE `purchased_menu_table`
							   SET  dispensary_id='".$lastid."',
							   claim_date=NOW()
							   WHERE id = '".$Id."'";		
			$exeUpdateSubDisSQL = mysql_query($UpdateSuBDisSQL) or die($UpdateSuBDisSQL." : ".mysql_error());	
		
			
		
		
		if(!empty($lastid))
		{
			
			// start of sharing on facebook and on twitter
			if(isset($_POST['fblogin']))
			{
				if($_POST['fblogin']=='facebookshare')
				{
					include_once('fb/fb_dispensary_share.php');
				}
			}
			if(isset($_POST['twlogin']))
			{
				if($_POST['twlogin']=='twittershare')
				{
					include_once('twitter/twtr_dispensary_share.php');
				}
			}	
			// end of sharing on facebbok and twitter
			
			if(isset($_POST['heading']))
			{
				$newcount=0;
				for($i=0;$i<count($_POST['heading']);$i++)
				{
					if(isset($_POST['menudetailno_'.($newcount+1)]))
					{
						$querone="insert into menu_heading(dispensary_id)values('".$lastid."')";
						$resone=mysql_query($querone)or die($querone.": ". mysql_error());
						$lastheadingid=mysql_insert_id();
						$rowno=count($_POST['menudetailno_'.($newcount+1)]);
						$j=0;
						$k=0;
						while($k< $rowno)
						{
							if(isset($_POST['menudetail_'.($newcount+1).'_'.$j]))
							{
								$rowsdata=serialize($_POST['menudetail_'.($newcount+1).'_'.$j]);
								$querone="insert into dispensaries_detail(dispensaries_detail_data,menu_heading_id)values ('".mysql_real_escape_string($rowsdata)."','".$lastheadingid."')";
								$resone=mysql_query($querone)or die($rowsdata." : ".mysql_error());
								$k++;
							}
							$j++;
						}
					}
					else
					{
						$i--;
					}
					$newcount++;
				}	
			}
			
			if(!empty($_POST['sample'])) 
			{
				 $delivery_service='0';
					  $store_front='0';
					  $credit_card='0';
					  $accept_atm='0';
					  $age_18_year_old ='0';
					  $age_21_year_old='0';
					  $security='0';
					  $handicap_assessable='0';
					  $recreational = '0';
    			foreach($_POST['sample'] as $options) 
				{
					if($options=='DeliveryService')
					{
					  $delivery_service='1';
					}
					if($options=='StoreFront')
					{
					  $store_front='1';
					}
					if($options=='AcceptCreditCard')
					{
					  $credit_card='1';
					}
					if($options=='AcceptATMonSite')
					{
					  $accept_atm='1';
					}
					if($options=='18YearsOld')
					{
					  $age_18_year_old='1';
					}
					if($options=='21YearsOld')
					{
					  $age_21_year_old='1';
					}
					if($options=='Security')
					{
					  $security='1';
					}
					if($options=='HandicapAssesseble')
					{
						 $handicap_assessable='1';
					}
					if($options=='recreational')
					{
						$recreational='1';
					}
					 
			
    			}
				
				
				$dispSettingSQL = "INSERT INTO dispensary_settings SET  
						DeliveryService='".$delivery_service."',
						StoreFront='".$store_front."',
						AcceptCreditCard='".$credit_card."',
						AcceptATMonSite='".$accept_atm."',
						18YearsOld='".$age_18_year_old."',
						21YearsOld='".$age_21_year_old."',
						Security='".$security."',
						HandicapAssesseble='".$handicap_assessable."',
						dispensary_id='".$lastid."'";
				
			$exedispensarySettingSQL = mysql_query($dispSettingSQL) or die ($dispSettingSQL." : ".mysql_error());
			
			
			}
			
			
			
			header('Location:edit_dispensary.php?disId='.$lastid.'&newdata=add');exit();
		}
		else
		{	
			header('Location:dispensary_menu.php?sql=unsuccess');exit();
		}

}