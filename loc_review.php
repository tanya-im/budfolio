<?php 
session_start();
include_once("config.php");
include_once("function.php");
?>
<style>


</style>
<div id="review">
	<form name="location_review_dis" id="location_review_dis" method="post" action="location_review_submit.php">
    <input type="hidden" name="dispensary_id" id ="dispensary_id" value="<?php echo $_GET['dispensaryId'];?>" />
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['user_id']?>" />
		<div class="reviewtable"> 
          <div>
<div class="write_head">
Location Review
</div>
<div class='write_maincell' >



<!--strart of first row-->
<DIV class="write_cell1">Strain Quality:
</DIV>
<div class="write_cell2"><div class="rating_wrapp">
                                <?php for($r=1;$r<6;$r++)
                                        { 
                                            if($straindetail['overall_rating']>=$r)
                                            {
                                                echo '<a href="javascript:void(0);" class="ratthis" id="ratting_'.$r.'">&nbsp;</a>';
                                            }
                                            else
                                            {
                                                echo '<a href="javascript:void(0);" class="ratthis" id="ratting_'.$r.'">&nbsp;</a>';
                                            }	
                                        }?>
                            </div>
                             <input type="hidden" name="strain_quality" id="strain_quality" value="<?php echo $straindetail['overall_rating'];?>"> </div>
<DIV class="write_cell3">Strain Variety
</DIV>
<div class="write_cell4"> <div class="rating_wrap_sv">
                                <?php for($r=1;$r<6;$r++)
                                        { 
                                            if($straindetail['overall_rating']>=$r)
                                            {
                                                echo '<a href="javascript:void(0);" class="rate_sv" id="sv_'.$r.'">&nbsp;</a>';
                                            }
                                            else
                                            {
                                                echo '<a href="javascript:void(0);" class="rate_sv" id="sv_'.$r.'">&nbsp;</a>';
                                            }	
                                        }?>
                            </div>
                             <input type="hidden" name="strain_variety" id="strain_variety" value="<?php echo $straindetail['overall_rating'];?>"></div>
</div><!--end of first row-->


<div class="write_maincell"><!--strart of 2nd row-->
<DIV class="write_cell1">Service:
</DIV>
<div class="write_cell2"> <div class="rating_wrap_service">
                                <?php for($r=1;$r<6;$r++)
                                        { 
                                            if($straindetail['overall_rating']>=$r)
                                            {
                                                echo '<a href="javascript:void(0);" class="rate_service" id="s_'.$r.'">&nbsp;</a>';
                                            }
                                            else
                                            {
                                                echo '<a href="javascript:void(0);" class="rate_service" id="s_'.$r.'">&nbsp;</a>';
                                            }	
                                        }?>
                            </div>
                             <input type="hidden" name="service" id="service" value="<?php echo $straindetail['overall_rating'];?>"></div>
<DIV class="write_cell3">Atmosphere:
</DIV>
<div class="write_cell4"><div class="rating_wrap_atmosphere">
                                <?php for($r=1;$r<6;$r++)
                                        { 
                                            if($straindetail['overall_rating']>=$r)
                                            {
                                                echo '<a href="javascript:void(0);" class="rate_atm" id="at_'.$r.'">&nbsp;</a>';
                                            }
                                            else
                                            {
                                                echo '<a href="javascript:void(0);" class="rate_atm" id="at_'.$r.'">&nbsp;</a>';
                                            }	
                                        }?>
                            </div>
                             <input type="hidden" name="atmosphere" id="atmosphere" value="<?php echo $straindetail['overall_rating'];?>"> </div>
</div><!--end of 2nd row-->

<!--start of 3rd row-->

<div class="padtopbtm10">  
              <span> <input name="r_1" type="checkbox" value="1" id="r_1"> I will shop here again </span> <br>
              <span> <input name="r_2" type="checkbox" value="1" id="r_2"> I would recommend this club to others </span> <br>
              <span> <input name="r_3" type="checkbox" value="1" id="r_3"> this is my first visit: </span> <br></div>
<!-- end of 3rd row-->


<div class="padtopbtm10">  
			 <div class="reviewcon_lft">
             	<div class="reviewtextcon"><textarea name="review_text" id="review_text" cols="" rows="" placeholder="Enter comments"> </textarea> 	</div> <br>
                <span class="limit10"> <strong>200 Character</strong> </span>
             </div>
             <div class="reviewcon_rit">
             	<a href="javascript:void(0);" id="post_write_review"> <img src="images/new_web/post_btn.png" width="74" height="34" ></a>
             </div>
          </div>
          	
</div> 
    	</div> 
	</form>
</div>
<script>
function starrating(rates,rate_for,id)
{
	if(rates==null)
	{
		var startno=$("#"+id).val();
	}
	else
	{
		var startno='ff';
	}
	for(var i=1;i<=startno;i++)
	{
		$("#"+rate_for+"_"+i).addClass("rated");
	}
}


/*
	strain Quality
*/
$(".ratthis").hover(
		function()
		{
			$(".rating_wrapp a").removeClass("rated");
			var startno=$(this).attr("id");
			var rateno=startno.split("_");
			for(var i=1;i<=rateno[1];i++)
			{
				$("#ratting_"+i).addClass("rated");
			}
		},
		function()
		{
			$(".rating_wrapp a").removeClass("rated");
			starrating(null,'ratting','strain_quality');
	});
	$(".ratthis").click(function(){
		var startno=$(this).attr("id");
		var rateno=startno.split("_");
		var rates=$("#strain_quality").val(rateno[1]);
		starrating(rateno[1],'ratting','strain_quality');
	});
	
/*
	Strain Variety
*/	
$(".rate_sv").hover(
		function()
		{
			$(".rating_wrap_sv a").removeClass("rated");
			var startno=$(this).attr("id");
			var rateno=startno.split("_");
			for(var i=1;i<=rateno[1];i++)
			{
				$("#sv_"+i).addClass("rated");
			}
		},
		function()
		{
			$(".rating_wrap_sv a").removeClass("rated");
			starrating(null,'sv','strain_variety');
	});
	$(".rate_sv").click(function(){
		var startno=$(this).attr("id");
		var rateno=startno.split("_");
		var rates=$("#strain_variety").val(rateno[1]);
		starrating(rateno[1],'sv','strain_variety');
	});	
	
/*
	Service
*/	
$(".rate_service").hover(
		function()
		{
			$(".rating_wrap_service a").removeClass("rated");
			var startno=$(this).attr("id");
			var rateno=startno.split("_");
			for(var i=1;i<=rateno[1];i++)
			{
				$("#s_"+i).addClass("rated");
			}
		},
		function()
		{
			$(".rating_wrap_service a").removeClass("rated");
			starrating(null,'s','service');
	});
	$(".rate_service").click(function(){
		var startno=$(this).attr("id");
		var rateno=startno.split("_");
		var rates=$("#service").val(rateno[1]);
		starrating(rateno[1],'s','service');
	});		

/*
	Atmosphere
*/	
$(".rate_atm").hover(
		function()
		{
			$(".rating_wrap_atmosphere a").removeClass("rated");
			var startno=$(this).attr("id");
			var rateno=startno.split("_");
			for(var i=1;i<=rateno[1];i++)
			{
				$("#at_"+i).addClass("rated");
			}
		},
		function()
		{
			$(".rating_wrap_atmosphere a").removeClass("rated");
			starrating(null,'at','atmosphere');
	});
	$(".rate_atm").click(function(){
		var startno=$(this).attr("id");
		var rateno=startno.split("_");
		var rates=$("#atmosphere").val(rateno[1]);
		starrating(rateno[1],'at','atmosphere');
	});		
</script>