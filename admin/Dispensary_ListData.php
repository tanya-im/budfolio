<?php
session_start();
include_once('../config.php');
include_once('../function.php');
$pageNum = 1;
$rowsPerPage = 100; 


	// for search product or company
	if(!empty($_REQUEST['search_dis']))
	{
		$searchText = mysql_real_escape_string($_REQUEST['search_dis']);
	}
	if($searchText!='')
	{
			$search_part=" AND (dispensary_name like '%".strtolower($searchText)."%'
						   || city like '%".strtolower($searchText)."%'
						   || state like '%".strtolower($searchText)."%')";
	}
	//end of search dispensary part

   // if $_REQUEST['page'] defined, use it as page number
	if(isset($_REQUEST['page'])){
    	 $pageNum = $_REQUEST['page'];
		 $startid = $rowsPerPage*($pageNum-1)+1;
	}

	// counting the offset
	$offset = ($pageNum - 1) * $rowsPerPage;
	
	// order by
	$strOrderBy=(isset($_REQUEST['orderby']))?$_REQUEST['orderby']:'dispensary_name';
	//get the order value
	$strOrder=(isset($_REQUEST['order']))?$_REQUEST['order']:'asc';

	$selDispensaryList = "SELECT * FROM `dispensaries` WHERE 1 ";
	
	if(isset($search_part) && $search_part!=''){
		
		$selDispensaryList.=$search_part;
	}
	$selDispensaryList.= " ORDER BY $strOrderBy $strOrder LIMIT  $offset,$rowsPerPage ";
	//echo $selDispensaryList;
	$selDispensaryRes = mysql_query($selDispensaryList)or die("Error: ".$selDispensaryList." ".mysql_error());	
	
	// for pagination
	$query = "SELECT COUNT(dispensary_id) AS `numrows` FROM `dispensaries` WHERE 1 ";
	
	
	if(isset($search_part)&&$search_part!=''){
		
		$query.=$search_part;
	}
	
	//echo $query;
	$result  = mysql_query($query) or die('Error, query failed');
	$row     = mysql_fetch_array($result, MYSQL_ASSOC);
	$numrows = $row['numrows'];
	$maxPage = ceil($numrows/$rowsPerPage);
	
	// by default we show first page


	$self = $_SERVER['PHP_SELF'];
	$nav  = '';

	

if($pageNum > 1){
	
    $page  = $pageNum - 1;
 	$prev  = " <a href='#' onclick='getdata(".$page.")' >[Prev]</a> ";
	$first = " <a href='#' onclick='getdata(1)'>[First Page]</a> ";		     
}else{
   $prev  = '&nbsp;'; // we're on page one, don't print previous link
   $first = '&nbsp;'; // nor the first page link
}

if ($pageNum < $maxPage){
	
   $page = $pageNum + 1;
   $next = " <a href='#' onclick='getdata(".$page.")'>[Next]</a> ";
   $last = " <a href='#' onclick='getdata(".$maxPage.")' >[Last Page]</a> ";  
}else{
   $next = '&nbsp;'; // we're on the last page, don't print next link
   $last = '&nbsp;'; // nor the last page link
}
if(mysql_num_rows($selDispensaryRes) > 0)
{
?>

<table class="borderall" style="border:1px solid;width:100%;">
<thead class="displihead">
		<tr>
            <th data-sort="int" id="disp_admin_header_one">&nbsp;</th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_two" class="bold"><a href="javascript:void(0);" onclick="fun_orderby('dispensary_name')">Customer Name</a></th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_three" class="bold"><a href="javascript:void(0);" onclick="fun_orderby('phone_number')">Phone Number</a></th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_four" class="bold"><a href="javascript:void(0);" onclick="fun_orderby('city')">City</a></th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_five" class="bold"><a href="javascript:void(0);" onclick="fun_orderby('zip')">Zip Code</a></th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_five" class="bold"><a href="javascript:void(0);" onclick="fun_orderby('state')">State</a></th>
			<th data-sort="date" id="disp_admin_header_six" class="bold"><a href="javascript:void(0);" onclick="fun_orderby('date_time')">Registration Date</a></th>
			<th id="disp_admin_header_six" class="bold center">Action</th>
		</tr></thead>
<?php 

	while($row=mysql_fetch_array($selDispensaryRes))
	{
        $HTML.='<tr id="displiid_'.$row['dispensary_id'].'">';
        if(empty($row['image']))
		{
			$HTML.='<td class="dispadmin_img"><img src="../images/dispensary_images/User_default.JPG"/></td>';
		}
		else
		{
			$HTML.='<td class="dispadmin_img"><img src="../images/dispensary_images/thumbnail/'.$row['image'].'"/></td>';
		}	
		if(empty($row['dispensary_name']))
		{
			$HTML.='<td class="disp_admin_header_two pdright">-</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_two pdright">'.str_replace("\\", "", stripslashes(truncatestr($row['dispensary_name']))).'</td>';
		}
		if(empty($row['phone_number']))
		{
			$HTML.='<td class="disp_admin_header_three pdright">-</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_three pdright">'.$row['phone_number'].'</td>';
		}
		if(empty($row['city']))
		{
			$HTML.='<td class="disp_admin_header_four pdright">-</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_four pdright">'.str_replace("\\", "", stripslashes($row['city'])).'</td>';
		}
		if(empty($row['zip']))
		{
			$HTML.='<td class="disp_admin_header_five pdright">-</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_five pdright">'.$row['zip'].'</td>';
		}
		if(empty($row['state']))
		{
			$HTML.='<td class="disp_admin_header_five pdright">-</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_five pdright">'.str_replace("\\", "", stripslashes($row['state'])).'</td>';
		}
		if(empty($row['date_time']))
		{
			$HTML.='<td class="disp_admin_header_five pdright">&nbsp;</td>';
		}
		else
		{
			$reg_date = explode(' ',$row['date_time'] );//date('Y-m-d',strtotime($row['date_time']))
			if($reg_date[0]=='0000-00-00'){
				$reg_date[0]='-';
			}
			
			$HTML.='<td class="disp_admin_header_six pdright">'.$reg_date[0].'</td>';
		}
		
		
		$HTML.='<td class="disp_admin_header_six center">';
        $HTML.='<a href="editdispensary_link.php?disp_id='.$row['dispensary_id'].'">Edit</a>';
        $HTML.='<a href="javascript:void(0);" id="dispid_'.$row['dispensary_id'].'" class="delete_disp">Delete</a>';
		if($row['claimed_date']!='0000-00-00' )
		{
			$HTML.='&nbsp;&nbsp;<div style="margin-top:20px;padding-bottom:35px;" id="deactive_'.$row['dispensary_id'].'">';
			if($row['flag']=='active')
			{
				$HTML.='<a href="javascript:void(0);" onclick="deactivateDis('.$row['dispensary_id'].');">Deactivate</a></div>';
			}else
			{
				$HTML.='<a href="javascript:void(0);" onclick="activateDis('.$row['dispensary_id'].');">Activate</a></div>';
			}

		}
		
		$HTML.='</td>';
		$HTML.='</tr>';
		
	}
	$HTML.='<tr><td style="font-size:16px;height:24px; text-align: center;">'.$first . $prev ." Showing page $pageNum of $maxPage pages " . $next . $last.'</td></tr>';
	$HTML.='</table>';	

	
}
else
{
?>
<table class="borderall" style="border:1px solid;width:100%;">
<thead class="displihead">
		<tr>
            <th  id="disp_admin_header_one">&nbsp;</th>
			<th id="disp_admin_header_two" class="bold">Customer Name</th>
			<th id="disp_admin_header_three" class="bold">Phone Number</th>
			<th id="disp_admin_header_four" class="bold">City</th>
			<th id="disp_admin_header_five" class="bold">Zip Code</th>
			<th id="disp_admin_header_five" class="bold">State</th>
			<th id="disp_admin_header_six" class="bold">Registration Date</th>
			<th id="disp_admin_header_six" class="bold center">Action</th>
		</tr></thead>
    
    
<?php	
	$HTML.='<tr><td style="text-align:center;list-style:none;">No dispensary Found</td></tr>';
}
?>

<?php
echo $HTML;die;
?>