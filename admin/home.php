<?php // 
// Include header,auth,ad_transaction and config file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
date_default_timezone_set('Pacific/Honolulu');
?>
<style>
form {
    float: left;
    margin-bottom: 10px;
    margin-left: 10px;
    width: 59%;
}
.div_loader > img
{
	top:250px;
}
.div_loader
{
	margin-bottom: 30%;
    margin-left: 46%;
    margin-top: 30%;
}
</style>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jqpagination.css" />

<script>
function getdata(page){      
	try{ 
	
		var targetURL = 'UserListData.php';
		$("#page").val(page);
		$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'   /></div>");
		$('html,body').animate({ scrollTop: 0 }, 'slow');
		search_xhr=$.get(targetURL,$("#dataToSend").serialize(),function(data){
			try{
				$('#userlisting').html(data);
			}catch(e){
				console.log(e)	;
			}
		});
	}catch(e){
		console.log(e)
	}
}


function search_user_here(){
	
	if($.trim($('#search_user').val())==''){
		alert("Please enter user name to search.");
		return false;
	}/*else{
		alert("cmpnyName "+$.trim($('#companyName').val()));
		alert("productName"+$.trim($('#productName').val()));
		return false;
	}*/
	
	getdata(1);
}

function search_user(char){


	$('#user_wid_this_char').val(char);
	
	getdata(1);
}

function confirmDelete(type)
{
	
	var agree = confirm("Are you sure you want to Delete "+type+" ?");
		if (agree)
		{
			return true ;
		}
		else
		{
			return false ;
		}
}


 //order script
function fun_orderby(field){
	try{
		
		if($('#orderby').val()==field){
			if($('#order').val() == 'asc'){
				$('#order').val('desc');
			}else{
				$('#order').val('asc');
			}
		}else{
			$('#order').val('asc');
		}
		$('#orderby').val(field);
		$('#page').val('1');
		getdata(1)
	}catch(e){
		handle_try_catch_error(e)	
	}
}

function sendReActivation(userid)
{
	
	$.post('sendReactivation.php',"userId="+userid,function(data){
		
		if(data=='emailsent')
		{
			$('#code_'+userid).text('-');
		}
	});
	
}
</script>


<?php 
$userlistQuery="select * from users order by user_id desc";
$userlistSql=mysql_query($userlistQuery) or die(mysql_error());
$count=mysql_num_rows($userlistSql);
if($count > 0)
{
   $paginationCount=getPagination($count);
}
?>


<div class="mid_wrap">
	<h1 class="center left100 fadheading">Users List</h1>
    <div class="adlisterror"></div>
	
    <div class="home_right_part">
    	<div class="left100">
        	<form  id="dataToSend" name="dataToSend">
           		<input id="order" type="hidden" value="desc" name="order">
           		<input id="orderby" type="hidden" value="user_id" name="orderby">
           		<input type="hidden" id="page" value="" name="page">
        	   <div style="width:800px;">
                  <div style="float: left; width: 45%;">
                  <input type="text" name="search_user" id="search_user" placeholder="Search User" onkeypress="searchKeyPress(event);"/>
		   		  <input type="button" name="search_user_but" id="search_user_but" value="Search" onclick="search_user_here();" />
                  	  <input type="button" name="reset" value="Reset" onclick="emptySearch();"/>
                  </div>
        		  <div style="float: left; font-size: 14px; margin-left: 0px;">        
 <span><a href="javascript:void(0);" onclick="search_user('a')" id="user_A">A</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('b');" id="user_A">B</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('c');" id="user_A">C</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('d');" id="user_A">D</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('e');" id="user_A">E</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('f');" id="user_A">F</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('g');" id="user_A">G</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('h');" id="user_A">H</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('i');" id="user_A">I</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('j');" id="user_A">J</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('k');" id="user_A">K</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('l');" id="user_A">L</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('m');" id="user_A">M</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('n');" id="user_A">N</a></span> <span><a href="javascript:void(0);" onclick="search_user('o');" id="user_A">O</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('p');" id="user_A">P</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('q');" id="user_A">Q</a></span> <span><a href="javascript:void(0);" onclick="search_user('r');" id="user_A">R</a></span> <span><a href="javascript:void(0);" onclick="search_user('s');" id="user_A">S</a></span> <span><a href="javascript:void(0);" onclick="search_user('t');" id="user_A">T</a></span> <span><a href="javascript:void(0);" onclick="search_user('u');" id="user_A">U</a></span> <span><a href="javascript:void(0);" onclick="search_user('v');" id="user_A">V</a></span> <span><a href="javascript:void(0);" onclick="search_user('w');" id="user_A">W</a></span> <span><a href="javascript:void(0);" onclick="search_user('x');" id="user_A">X</a></span> <span><a href="javascript:void(0);" onclick="search_user('y');" id="user_A">Y</a></span>
 <span><a href="javascript:void(0);" onclick="search_user('z');" id="user_A">Z</a></span>      </div>         
 				  <div style="float:left;margin-left:50px;"><a href="demoDownload.php">Export</a></div>
                  <input type="hidden" name="user_wid_this_char" id="user_wid_this_char" value="" />
               </div>
           		<input type="hidden" name="search" id="submit" value="search" />                              															   		   </form>
    	
       <table id="userlisting" border="1" cellpadding="5" cellspacing="0"></table>
           
        </div>
    </div>
</div>
<!-- include ajax file to strain title paging-->
<script type="text/javascript" src="js/new/jquery.jqpagination.js"></script>
<script type="text/javascript" src="js/dispensary_list_paging.js"></script>
<script type="text/javascript" src="js/new/user_list_script.js"></script>
<script>
$(document).ready(function(){
	
	getdata(1);
});

function emptySearch()
{
	$('#search_user').val('');
	getdata(1);
}
 function searchKeyPress(e)
    {
        // look for window.event in case event isn't passed in
        if (typeof e == 'undefined' && window.event) { e = window.event; }
        if (e.keyCode == 13)
        {
            document.getElementById('search_user_but').click();
        }
    }
</script>
<!--End-->
<?php include_once("footer.php"); ?>