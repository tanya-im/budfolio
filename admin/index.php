<?php include_once("header.php");
if(!empty($_SESSION['admin_name']))
{
	header("Location:landing_page_details.php");
}
?>
<div class="adminlogin_wrap">
	<div class="rpwd_wrapper">
        <h3>Admin Login</h3>
        <div class="admin_innerwrapper">
            <?php if(isset($_GET['loginvalidation'])){?>
                <div class="leftwc red fadinmsg">Username or password empty.</div>
            <?php } 
			if(isset($_GET['auth'])){ if($_GET['auth']=='false'){?>
                <div class="leftwc red fadinmsg">Username or password incorrect.</div>
            <?php }} ?>
            <!-- Admin login form-->
            <form method="post" name="adminlogin" id="adminlogin" action="adminlogin.php" onsubmit="return validateadminlogin();">  
            <div>
                <label>User Name </label>
                <input type="text" name="adminname" id="adminname" />
            </div>
            <div>
                <label>Password</label>
                <input type="password" name="adminpassword" id="adminpassword" />
            </div>
            <div class="fgbtn">
                <input type="submit" name="adminloginform" id="adminloginform" value="Login !" />
            </div>
            </form> 
            <!-- end admin login form-->   
        </div>
	</div>
</div>
<!-- Include footer file-->
<?php include_once("footer.php"); ?>
<!-- end -->
<!-- validate admin login form-->
<script>
function validateadminlogin()
{
	var adminname=document.getElementById("adminname").value;
	var adminpassword=document.getElementById("adminpassword").value;
	if(adminname=='')
	{
		document.getElementById('adminname').focus();
		alert('Please enter user name');
		return false;
	}
	if(adminpassword=='')
	{
		document.getElementById('adminpassword').focus();
		alert('Please enter password');
		return false;
	}
	return true;
}
</script>