<?php 
// Include header,auth,ad_transaction and config file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("AdminFunction.php");
include_once("DeleteOrDeactiveAd.php");
if(!empty($_GET['type']))
{
	$AdType=$_GET['type'];
}
else
{
	$AdType=FrontPageAd;
}
?>
<div class="mid_wrap">
	<h1 class="adheading"><?php if($AdType==FrontPageAd)echo 'Front Page Ad';if($AdType==FrontPageBottumAd)echo 'Front Page Bottum Ad';if($AdType==TopBanner)echo 'Top Banner Ad';if($AdType==BottomBanner)echo 'Bottom Banner Ad';?></h1>
    <div class="adlisterror"></div>
    <div class="addnewbtn">
    	<a href="<?php echo $AdType; ?>" class="add_btn" id="opendailogbox"  name="modal">Add New</a>
    </div>
    <div class="Adwrap">
		<form method="post" name="fornaddlistform" id="fornaddlistform">
        <input id="action" name="action" type="hidden" value="" />
        <table id="Adtable">
			<?php if(isset($_GET["page"])){	$page = intval($_GET["page"]);}
			else{	$page = 1;}?>
			<?php echo AdHeader();?>
			<?php echo ShowAdvertisementList($page,$AdType);?>
        </table>
        </form>    
        <?php echo AdPaging($page,$AdType); ?>    
    </div>
</div>
<div id="boxes"></div>
<div id="mask"></div>
<script type="text/javascript" src="js/ad_list_paging.js"></script>
<script type="text/javascript" language="javascript" src="js/select_all.js"></script>
<?php include_once("footer.php"); ?>