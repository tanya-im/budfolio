<?php 
// Include header,auth and config file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
include_once("dispensary_transaction.php");

// Get dispensary detail
$query="select * from dispensaries where dispensary_id='".$_GET['disp_id']."'";
$sql=mysql_query($query)or die(mysql_error());
$result=mysql_fetch_array($sql);
$rowno=mysql_num_rows($sql);
$_SESSION['dispensary_id']=$_GET['disp_id'];
if(empty($_GET['disp_id']))
{
	header("Location:dispensary_list.php");
}
if($rowno<1)
{
	header("Location:dispensary_list.php");
}


?>
<style>
.show_this_row
{
	background:#CCC;
	font-weight:bold;
	color:#000;	
}
.hide_this_row
{
	background:#0C6;
	font-weight:bold;
	color:#FFF;		
}
.reg_detail_twopart {
    float: left;
    margin-left: 5%;
    width: 40%;
}
<style>
th
{
	color:#069;
}
.modbtn{
	padding: 2px 5px;
	text-decoration: none;
	cursor: pointer;
	width: auto;
	height: 21px;
	border-radius: 4px;
	margin: 0px 13px 13px 66px;
	padding: 6px 5px 5px 5px;
	font-family: 'Gotham-Book';
	color: #fff;
	font-size: 16px;
	font-weight: normal;
	text-align: center;
	background-image: linear-gradient(to bottom, #8bac3a, #abca44);
}
</style>
<div class="mid_wrap">
<div style="font-weight: bold; float: right; padding-top: 20px; padding-right: 18px;"><a onclick="goOneStepBAck();" href="javascript:void(0);">BACK</a></div>
	<h1 class="center left100">Edit Dispensary</h1>
    <div class="left100">
    	<div class="displeftimgpart">
        	<div id="disp_image_preview">
            	<?php if(empty($result['image'])){?>
                	<img src="../images/dispensary_images/User_default.JPG" />
            	<?php } else{?>
                	<img src="../images/dispensary_images/original/<?php echo $result['image']; ?>" />
				<?php } ?>
            </div>
            <div class="left left100">
            	<!-- Dispensary images form-->
            	<form action="UpdateDispensaryImage.php" enctype="multipart/form-data" method="post" id="dispancryimageform">
					<span class="left left100">Upload Photo</span>
                    <input type="file" id="dispancryphotoimg" name="dispancryphotoimg">
				</form>
                <!-- end-->
            </div>
            <div class="tbboxdetail left left100">&nbsp;</div>
		</div>
    <!-- Dispensary images form -->    
	<form name="dispensaryfrm" id="dispensaryfrm" method="post" action="dispensary_transaction.php">
        <div class="dispfieldone">           
			<div class="twopart">
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Customer name</label>
                    <input type="text" value="<?php echo  stripslashes($result['dispensary_name']);?>" name="customername" id="customername" maxlength="50"/>
                    <input type="hidden" value="<?php echo $_GET['disp_id'];?>" name="dispid" id="dispid"/> 
                     <input type="hidden" value="do_update" name="do_update" id="do_update"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Street address</label>
                    <input type="text" value="<?php echo  stripslashes($result['street_address']);?>" name="streetaddress" id="streetaddress" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">City</label>
                    <input type="text" value="<?php echo  stripslashes($result['city']);?>" name="city" id="city" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">State</label>
                    <input type="text" value="<?php echo  stripslashes($result['state']);?>" name="state" id="state" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Zip code</label>
                    <input type="text" value="<?php echo  $result['zip'];?>" name="zipcode" id="zipcode" maxlength="10"/> 
                </div>
            </div>
            <div class="twopart">
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Hours of operation</label>
                    <input type="text" value="<?php echo  stripslashes($result['hours_of_operation']);?>" name="hoursofoperation" id="hoursofoperation" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Phone number</label>
                    <input type="text" value="<?php echo $result['phone_number'];?>" name="phonenumber" id="phonenumber" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Email</label>
                    <input type="text" value="<?php echo  $result['email'];?>" name="email_dis" id="email_dis" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Website</label>
                    <input type="text" value="<?php echo  $result['website'];?>" name="website" id="website" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Bio</label>
                    <textarea name="bio" id="bio" style="width:58%;" maxlength="50"><?php echo  stripslashes($result['bio']);?> </textarea>
                </div>
            </div>
            
            <div class="left100 ceneter">
            
              <?php 
					$getSettingsSQL = "SELECT * FROM dispensary_settings WHERE dispensary_id='".$_GET['disp_id']."'";
					$resSettings = mysql_query($getSettingsSQL) or die($getSettingsSQL." : ".mysql_error());
					$settings= mysql_fetch_assoc($resSettings);
					
					?> <div class="headtext marbtm10"><u> Select Options </u></div>
                  <table>
                  <tr style="width:100%;">           
             		<td style="width:20%;"><span class="check_con">
             <input name="sample" type="checkbox" class="checkbox" value="delivery_service" id="delivery_service" 
             <?php if($settings['DeliveryService']==1){?> checked="checked" <?php }?>>  			 <label class="checkbox_conlable">Delivery Service </label> </span></td>
             
            		<td style="width:20%;">
             <span class="check_con"> 
             <input name="sample[]" type="checkbox" class="checkbox" value="store_front" id="store_front" <?php if($settings['StoreFront']==1){?> checked="checked" <?php }?>> 
             <label class="checkbox_conlable">Store Front </label> </span></td>
			
                    <td style="width:20%;">
             <span class="check_con">
             <input name="sample[]" type="checkbox" class="checkbox" value="credit_card" id="credit_card" <?php if($settings['AcceptCreditCard']==1){?> checked="checked" <?php }?>>
             <label class="checkbox_conlable">Accept Credit Card </label> </span></td>
             
                    <td style="width:20%;">
             <span class="check_con"> 
             <input name="sample[]" type="checkbox" class="checkbox" value="accept_atm" id="accept_atm" <?php if($settings['AcceptATMonSite']==1){?> checked="checked" <?php }?>>
             <label class="checkbox_conlable">Accept ATM on Site </label> </span></td>
             </tr>
             <tr>
             <td>
             <span class="check_con">
             <input name="sample[]" type="checkbox" class="checkbox" value="age_18_year_old" id="age_18_year_old" <?php if($settings['18YearsOld']==1){?> checked="checked" <?php }?>>
             <label class="checkbox_conlable">18 Years Old </label> </span></td>
             
             <td><span class="check_con">
             <input name="sample[]" type="checkbox" class="checkbox" value="age_21_year_old" id="age_21_year_old" <?php if($settings['21YearsOld']==1){?> checked="checked" <?php }?>>
             <label class="checkbox_conlable">21 Years Old </label> </span></td>
                        
             <td> <span class="check_con">
             <input name="sample[]" type="checkbox" class="checkbox" value="security" id="security" <?php if($settings['Security']==1){?> checked="checked" <?php }?>>
             <label class="checkbox_conlable">Security</label> </span></td>
             
             <td><span class="check_con">
        <input name="sample[]" type="checkbox" class="checkbox" value="handicap_assessable" id="handicap_assessable" <?php if($settings['HandicapAssesseble']==1){?> checked="checked" <?php }?>>
             <label class="checkbox_conlable">Handicap assessable </label> </span></td>   
             
<a id="UpdateDispSettings" class="edit_btn" style="display:none;" href="javascript:void(0);"> Save </a>
             </tr>
             </table>  
           </div>
        </div>
        <!-- 23rd jan-->
        
        <?php 
		$getRegDetailSQL = "SELECT user_name,email_address,registration_date,
							phoneno,contact_name FROM users
							WHERE user_id ='".$result['added_by']."'
							LIMIT 0,1";
		$exeRegDetail= mysql_query($getRegDetailSQL) or die($getRegDetailSQL.":".mysql_error());
		$userRegData = mysql_fetch_assoc($exeRegDetail);
							
		?>
        <!-- registration details-->
        <div class="left100 ceneter">
            	<label class="left100 bold14 dispmenulbl">Registration Detail</label>
                
                <input type="hidden" value="<?php echo $result['added_by'];?>" name="hidden_added_by" id="hidden_added_by" maxlength="50"/>
            </div> <!-- 23rd jan ends here-->
        <div class="left100 ceneter">
            <div class="reg_detail_twopart">
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Contact name</label>
                    <input type="text" value="<?php echo  stripslashes($userRegData['contact_name']);?>" name="contact_name" id="contact_name" maxlength="50"/>
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Contact phone</label>
                    <input type="text" value="<?php echo  $userRegData['phoneno'];?>" name="user_phone_no" id="user_phone_no" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Email Address</label>
                    <input type="text" value="<?php echo  $userRegData['email_address'];?>" name="user_email_add" id="user_email_add" maxlength="50"/> 
                </div>
               
            </div>
            <div class="reg_detail_twopart">
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">User name</label>
                    <input type="text" value="<?php echo  stripslashes($userRegData['user_name']);?>" name="user_name" id="user_name" maxlength="50"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Registration date</label>
                    <input type="text" value="<?php echo $userRegData['registration_date'];?>" name="registration_date" id="registration_date" maxlength="50"/> 
                </div>
            <?php
			// find all the dispensary tht this user has added or clamied
			$getDisSQL= "SELECT GROUP_CONCAT(dispensary_name) as dispensary
						 FROM dispensaries 
						 WHERE added_by='".$result['added_by']."'";
			$exeGetDisSQL = mysql_query($getDisSQL) or die($getDisSQL." : ".mysql_query());			 			$disData = mysql_fetch_assoc($exeGetDisSQL);
			$disList = ($disData['dispensary']!='' or $disData['dispensary']!=null)?$disData['dispensary']:'';
                
                ?>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip">Dispensary Claimed</label>
                    <input type="text" value="<?php echo stripcslashes($disList);?>" name="email" id="email" maxlength="50"/> 
                </div>
                
            </div>
        </div>
        <!-- registration details ends here-->
        
        <!-- Payment details starts here-->
        <div class="left100 ceneter">
            <label class="left100 bold14 dispmenulbl">Payment Detail</label>
            <?php
//get the list of subscription of user
/*$strUserSubscriptionListSQL="SELECT plan_type, amount, start_date, expiry_date
							FROM `tbl_menu_purchase`, `purchased_menu_table` 
							WHERE tbl_menu_purchase.purchase_id =purchased_menu_table.purchase_id
							and purchased_menu_table.user_id='".$result['added_by']."'
							and purchased_menu_table.dispensary_id='".$_GET['disp_id']."'";*/
$strUserSubscriptionListSQL="SELECT `id`, 
 									 a.`purchase_id` as sub_id,
									 `dispensary_id`,
									 `start_date`,
									 `expiry_date`,
									 `occurence`,
									  a.`status`,
									  plan_type,
									  amount,
									  a.`comment_by_admin`
									  FROM `purchased_menu_table` as a, tbl_menu_purchase as b 
									  WHERE a.`purchase_id`=b.`purchase_id` 
									  AND a.user_id='".$result['added_by']."'
									  AND a.dispensary_id='".$_GET['disp_id']."'
									  ORDER BY start_date desc";						
	
  $strUserSubscriptionListEXE = mysql_query($strUserSubscriptionListSQL) or die ($strUserSubscriptionListSQL." : ".mysql_error());
?>
<?php if(mysql_num_rows($strUserSubscriptionListEXE)>0) {
		 ?>
       <table id="userlisting" style="width: 93%;margin-left: 4%;" border="1" cellpadding="5" cellspacing="0">
       		<thead>
            	<tr>
					
					<th data-sort="date" style="width:16%;text-align:center;">Subscriptions</th>
					<th style="width:8%;text-align:center;">Amount</th>
    				<th style="width:8%;text-align:center;">Start Date </th>
					<th data-sort="int" style="width:8%;text-align:center;">Expiry Date</th>
                    
                    <th data-sort="int" style="width:8%;text-align:center;">Status</th>
                    <th data-sort="int" style="width:8%;text-align:center;">Update</th>
                    
       			</tr>
             </thead>
			 <tbody>
             
           <?php  while($sub= mysql_fetch_assoc($strUserSubscriptionListEXE))
			{
				$isRecurringBYDb = $sub['isRecurring'];
				?>
				<tr id="subId_<?php echo $sub['sub_id'];?>" >
			<?php
				if($sub['plan_type']=='d')
				{
					$plantype=" 14 Day Trail";
				}else if($sub['plan_type']=='m')
				{
					$plantype="1 Month";
				}else if($sub['plan_type']=='h')
				{
					$plantype="6 Month";
				}
				else if($sub['plan_type']=='a')
				{
					$plantype="Annual";
				}else
				{
					$plantype='By admin';
				}
				
				
				if(!empty($sub['start_date']))
				{
					$strtd=explode(" ",$sub['start_date']);
					$sd=explode("-",$strtd[0]);
					$start_date=date ("M j, Y", mktime (0,0,0,$sd[1],$sd[2],$sd[0]));
				}
				if(!empty($sub['expiry_date']))
				{
					$expd=explode(" ",$sub['expiry_date']);
					$ed=explode("-",$expd[0]);
					$expiry_date=date ("M j, Y", mktime (0,0,0,$ed[1],$ed[2],$ed[0]));
				}
				?>
		
				
				<td class="center"><?php echo $plantype;?></td>
				<td class="center"><?php echo ($sub['amount']!='0'?$sub['amount']:'-');?></td>
				<td class="center"><?php echo $start_date;?></td>
				<td class="center"><?php echo $expiry_date;?> </td>
                
                <?php
					// check if subscription is running or not
					 
					 
				?>
                <td class="center"><?php /*echo $sub['status'];*/ $expdate=strtotime($sub['expiry_date']);$curdate=strtotime(date('Y-m-d h:i:s'));if($expdate<$curdate){echo "inactive";}else{echo "active";}
				?> 
                </td>
                <?php $dispensaryname=getDispensaryName($sub['dispensary_id']); ?>
				<td><span class="center modbtn" id="modify_<?php echo $sub['sub_id'];?>" onclick="editPaymentDetail(this.id,'<?php echo $plantype;?>','<?php echo ($sub['amount']!='0'?$sub['amount']:'-');?>','<?php echo $sub['start_date'];?>','<?php echo $sub['expiry_date'];?>','<?php echo $_GET['id']; ?>','<?php echo $sub['dispensary_id']; ?>','<?php echo $sub['status']; ?>','<?php echo str_replace("'", "@", stripslashes($sub['comment_by_admin'])); ?>' )">Modify</span></td>
          <?php
			}
	   ?>	
				</tr>
             </tbody>
             </table>
             
          <?php  }
 else
	  {?>
		  <div style="width:100%;font-size:16px;height:70px; text-align: center;padding-top:4%;">No Payment detail Found !!!</div>
	 <?php }
?>          
            
        </div><!-- 14 day trail-- end here-->
        
        <!-- start of upgrade detail -->
       
            
        <!-- end  of upgrade detail -->
        
        <!-- menu heading-->
        <div class="left100 ceneter">
            	<label class="left100 bold14 dispmenulbl">Menu Detail</label>
        </div> <!-- menu heading ends here-->

		<div class="admindispmenu"><?php include_once("edit_menu_option.php"); ?></div>    
        <div class="left100 padding30">    
            <div class="left100 right">
             	<input type="hidden" name="moptionntabelon" id="moptionntabelon" value="<?php echo $tblno;?>" />
                <input class="onlyrpd" type="button" value="Add More Option" name="addmoreoption" id="addmoreoption" onclick="menuoption();" />
                <input class="onlyrpd" type="button" value="Remove Option" name="removeoption" id="removeoption"/>
                <input class="onlyrpd" type="button" value="Update !" name="update" id="update" />
                <input class="onlyrpd" type="button" name="cancel" value="Cancel !" onclick="createnewdispcancel('<?php echo $path.'admin/dispensary_list.php';?>');" />
            </div>
        </div>
    </form>
    <!-- End dispensary form-->    
    </div>
    <div class="clear">&nbsp;</div>
</div>
<script>
/* function is used for edit payment detail functionality*/
function editPaymentDetail(id,plantype,amount,stdate,expdate,userID,dipensaryID,status,comment_by_admin){
	
		var validid=id.split("_");
		if(validid[1]=='')
		{
				return false;
		}
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
        // Load ad form  
		var id='#dialog';
		$.ajax({
					type: "POST",
					url: "edit_dispensary_pament_deatail.php",
					data: 'EditSubscription=EditSubscription&modifyID='+validid[1]+'&plantype='+plantype+'&amount='+amount+'&stdate='+stdate+'&expdate='+expdate+'&userID='+userID+'&dipensaryID='+dipensaryID+'&status='+status+'&comment_by_admin='+comment_by_admin,
					async:   false,
					success: function(data)
					{
						
						$("#boxes").html(data);
						//Set the popup window to center
						$(id).css('top',  winH/2-$(id).height()/2);
						$(id).css('left', winW/2-$(id).width()/2);
						//transition effect
						$(id).fadeIn(2000); 
					}
		});
}
</script>
<div id="boxes"></div>
<div id="mask"></div>
<?php include_once("footer.php"); ?>
<!-- Add Row and col js -->
<script type="text/javascript" src="js/row_col.js"></script>
<script type="text/javascript" src="js/row_col_for_edit.js"></script>
<script>

$('#update').click(function()
{
    var final = '';
    $('.checkbox:checked').each(function(){        
        var values = $(this).val();
        final +='&'+values+'='+values;
    });
   	disId = $('#dispid').val();
	final+='&dispId='+disId;
	
	$.ajax({
				type: "POST",
				url: "save_options.php",
				data: 'updateDispensarySettings=updateDispensarySettings'+final,
				async:   false,
				success: function(data)
				{
					if(data=='updated')
					{
						$('#dispensaryfrm').submit();
					}
					if(data=='notUpdated')
					{
						console.log('notupdate');
						window.location.href="dispensary_list.php?optionssql=updateunsuccess";	
					}
				}
		});
	

//$('#dispensaryform').submit();
/*});*/
});
$('body').on("click",'.hide_menu_row',function(){ 
	

	/*var table_row_no = $(this).attr("name");
	var id_hidden_field = table_row_no.split('_');

	if(this.checked)
	{
	    $('#hide_'+id_hidden_field[1]+'_'+id_hidden_field[2]).val('hide');
	
	}
	else
	{
		$('#hide_'+id_hidden_field[1]+'_'+id_hidden_field[2]).val('');
	}
	return false;*/
	console.log('abc');
	var table_row_no = $(this).attr("name"); console.log(table_row_no);
	var removedBracketID = table_row_no.split('[]');
	
	var id_hidden_field = removedBracketID[0].split('_');
	// alert('#hide_'+id_hidden_field[1]+'_'+id_hidden_field[2]);
	if($(this).hasClass('show_this_row'))
	{
		
	    $('#hide_'+id_hidden_field[1]+'_'+id_hidden_field[2]).val('hide');
		$(this).addClass('hide_this_row');
		$(this).removeClass('show_this_row');
		$(this).attr('title',"Show this row");
		$(this).val('H');
		//$(this).attr("name").css({'background':'#0C6'});
	
	}
	else
	{
		$('#hide_'+id_hidden_field[1]+'_'+id_hidden_field[2]).val('');
		$(this).addClass('show_this_row');
		$(this).removeClass('hide_this_row');
		$(this).attr('title',"Hide this row");
		$(this).val('S');
	}
})
</script>