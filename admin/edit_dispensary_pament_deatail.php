<?php session_start();
include_once('../config.php'); 
include_once("../function.php");
if(isset($_POST['EditSubscription']) && !empty($_POST['modifyID']))
{
	
?>
<div id="dialog" class="window">
	<a href="javascript:void(0);" class="close"><img src="../images/css_images/close_btn.png" width="37" height="37" alt="close" > </a>
	<div class="budthought_popup">
    	<div class="ad_errormsg"></div> 
        <div class="adfrm">
        	<!-- Add Ad detail form -->
        	<form name="adform" id="adform" method="post" onsubmit="">
			<div class="l_pagedeialfrm">
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100">
                    	Subscription</label>
                    <input type="text" name="subscriptiontype" id="subscriptiontype" value="<?php echo $_POST['plantype']; ?>" /> 
                    <input type="hidden" name="userId" id="userId" value="<?php echo $_POST['userID']; ?>" />
                    <input type="hidden" name="dispensaryId" id="dispensaryId" value="<?php echo $_POST['dipensaryID']; ?>" />
                    <input type="hidden" value="<?php echo $_POST['isRecurringBYDb']; ?>" id="isRecurringBYDb" />
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100">
                    	Dispensary Name</label>
                    <input type="text" name="dispensaryname" id="dispensaryname" value="<?php echo stripslashes(getDispensaryName($_POST['dipensaryID'])); ?>" />
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100">
                    	Start Date</label>
                    <input type="text" name="dbstartdate" id="dbstartdate" class="date-picker" value="<?php echo $_POST['stdate']; ?>" /> 
                    <input type="hidden" name="startdate" id="startdate" value="<?php echo $_POST['start_date']; ?>" /> 
                </div>
                
            </div>
            <div class="l_pagedeialfrm">     
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100">
                    	Amount</label>
                    <input type="text" name="amount" id="amount" value="<?php echo $_POST['amount']; ?>" />
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100">
                    	Status</label>
                         <input type="text" name="status" id="status" disabled="disabled" value="<?php /*echo $_POST['status'];*/$expdate=strtotime($_POST['expdate']);$curdate=strtotime(date('Y-m-d h:i:s'));if($expdate<$curdate){echo "inactive";}else{echo "active";}?>" />                  
                </div>
                
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100">Expiry Date</label>
                     <input type="text" name="dbexpirydate" class="date-picker" id="dbexpirydate" value="<?php echo $_POST['expdate']; ?>" />
                     <input type="hidden" name="expirydate" id="expirydate" value="<?php echo $_POST['expiry_date']; ?>" /> 
                </div>
                
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100">
                    	Your Comment</label>
                        <textarea id="comment" name="comment" style="width:59%;"  ><?php echo str_replace("@", "'",stripslashes($_POST['comment_by_admin'])); ?></textarea>
                </div>
                
                <div class="tbboxdetail left left100">
                	<input type="hidden" value="<?php echo $_POST['modifyID']; ?>" id="purchaseid" />
                	<input type="button" style="float: right;margin-right: 362px;margin-top: 26px;" value="Update Detail !" name="updateDetail" id="updateDetail">
                    <span class="loaderbar"><img src="../images/css_images/loading-bars.gif" /></span>
                </div>
           	</div>            
        </form>
        <!-- end-->
		</div>
	</div>	
</div>
<script>
$(document).ready(function(){
	
	$('#updateDetail').click(function(){
		
		var subscriptionType=$('#subscriptiontype').val();
		var amount=$('#amount').val();
		var dispensaryname=$('#dispensaryname').val();
		var status=$('#status').val();
		var purchaseID=$('#purchaseid').val();
		var isRecurringBYDb=$('#isRecurringBYDb').val();		
		var dbstdate=$('#dbstartdate').val();
		var dbexpdate=$('#dbexpirydate').val();
		var start_date=$('#startdate').val();
		var expiry_date=$('#expirydate').val();
		var userId=$('#userId').val();
		var dispensaryId=$('#dispensaryId').val();
		var comment=$('#comment').val();
		
		
		$.ajax({
					type: "POST",
					url: "update_dispensary_payment_detail.php",
					data: 'updateSubscription=updateSubscription&subscriptionType='+subscriptionType+'&amount='+amount+'&start_date='+start_date+'&expiry_date='+expiry_date+'&dispensaryname='+dispensaryname+'&isRecurringBYDb='+isRecurringBYDb+'&dbstdate='+dbstdate+'&dbexpdate='+dbexpdate+'&userId='+userId+'&dispensaryId='+dispensaryId+'&purchaseID='+purchaseID+'&status='+status+'&comment='+comment,
					async:   false,
					success: function(data)
					{
						window.location.href="editdispensary_link.php?id="+userId;
					}
		});
	
	});
	
});

$(function() {
   	$( ".date-picker" ).datepicker({dateFormat: "yy-mm-dd" });
});
</script>
<?php } ?>    