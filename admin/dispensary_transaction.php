<?php 

//print_r($_REQUEST);
// Include auth and config files.
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
$date = date('Y-m-d H:i:s');
//error_reporting(E_ALL);
// If admin user submit the add dispensary form 
if(isset($_POST['adddispbtn']))
{
	$date = date('Y-m-d H:i:s');
	//Check if fields empty redirect it to dispensary page with aerror message.
	if(empty($_POST['customername']))
	{
		header('Location:dispensary_link.php?validatedata=empty');exit();
	}
	$zip=mysql_real_escape_string($_POST['zipcode']);
	$latlongdata=getlatlong($zip);
	// check zip code is valid or not
	if($latlongdata['status']!='OK')
	{
		header('Location:front_page_ad.php?zipcode=invalid');exit();
	}
	
	// Insert dispensary detail
	 $query="insert into dispensaries
						(
						  dispensary_name,
						  image,
						  street_address,
						  phone_number,
						  email,
						  website,
						  city,
						  state,
						  zip,
						  hours_of_operation,
						  bio,
						  latitude,
						  longitude,
						  date_time
						)
						values(
						'".mysql_real_escape_string($_POST['customername'])."',
						'".mysql_real_escape_string($_SESSION['dispancryphotoimg'])."',
						'".mysql_real_escape_string($_POST['streetaddress'])."',
						'".mysql_real_escape_string($_POST['phonenumber'])."',
						'".mysql_real_escape_string($_POST['email'])."',
						'".mysql_real_escape_string($_POST['website'])."',
						'".mysql_real_escape_string($_POST['city'])."',
						'".mysql_real_escape_string($_POST['state'])."',
						'".mysql_real_escape_string($_POST['zipcode'])."',
						'".mysql_real_escape_string($_POST['hoursofoperation'])."',
						'".mysql_real_escape_string($_POST['bio'])."',
						'".$latlongdata['results'][0]['geometry']['location']['lat']."',
						'".$latlongdata['results'][0]['geometry']['location']['lng']."',
						'".$date."'
						)";
	$sql=mysql_query($query)or die(mysql_error());
	$lastid=mysql_insert_id();
	$_SESSION['dispancryphotoimg']='';
	if(!empty($lastid))
	{
		if(isset($_POST['heading']))
		{
			$newcount=0;
			for($i=0;$i<count($_POST['heading']);$i++)
			{
				if(isset($_POST['menudetailno_'.($newcount+1)]))
				{
					$querone="insert into menu_heading(dispensary_id)values('".$lastid."')";
					$resone=mysql_query($querone)or die(mysql_error());
					$lastheadingid=mysql_insert_id();
					$rowno=count($_POST['menudetailno_'.($newcount+1)]);
					$j=0;
					$k=0;
					while($k< $rowno)
					{
						if(isset($_POST['menudetail_'.($newcount+1).'_'.$j]))
						{
							$rowsdata=serialize($_POST['menudetail_'.($newcount+1).'_'.$j]);
							$querone="insert into dispensaries_detail
							(
							dispensaries_detail_data,
							menu_heading_id
							)values 
							(
							'".mysql_real_escape_string($rowsdata)."',
							'".$lastheadingid."'
							)";
							$resone=mysql_query($querone)or die(mysql_error());
							$k++;
						}
						$j++;
					}
				}
				else
				{
					$i--;
				}
				$newcount++;
			}	
		}
		header('Location:dispensary_list.php?sql=success');exit();
	}
	else
	{
		header('Location:dispensary_list.php?sql=unsuccess');exit();
	}
}

// If admin user submit the update dispensary form 
if(isset($_POST['do_update']))
{
	//Check if fields empty redirect it to dispensary page with aerror message.
	if(empty($_POST['customername']))
	{
		header('Location:dispensary_link.php?validatedata=empty');exit();
	}
	if(empty($_POST['dispid']))
	{
		header('Location:dispensary_link.php?emptydispid=emptydispid');exit();
	}
	$zip=mysql_real_escape_string($_POST['zipcode']);
	
	
	$addressForLatLong= $zip.'+'.$_POST['city'].'+'.$_POST['state'];
	
	$latlongdata = getlatlongbyaddress($addressForLatLong);
	
	//$latlongdata=getlatlong($zip);
	// check zip code is valid or not
	if($latlongdata['status']!='OK')
	{
		header('Location:front_page_ad.php?zipcode=invalid');exit();
	}
	
	
	// Insert dispensary detail
	$query="Update dispensaries set
	dispensary_name='".mysql_real_escape_string($_POST['customername'])."',
	street_address='".mysql_real_escape_string($_POST['streetaddress'])."',
	phone_number='".mysql_real_escape_string($_POST['phonenumber'])."',
	email='".mysql_real_escape_string($_POST['email_dis'])."',
	website='".mysql_real_escape_string($_POST['website'])."',
	city='".mysql_real_escape_string($_POST['city'])."',
	state='".mysql_real_escape_string($_POST['state'])."',
	zip='".mysql_real_escape_string($_POST['zipcode'])."',
	hours_of_operation='".mysql_real_escape_string($_POST['hoursofoperation'])."',
	bio='".mysql_real_escape_string($_POST['bio'])."',
	latitude='".$latlongdata['results'][0]['geometry']['location']['lat']."',
	longitude='".$latlongdata['results'][0]['geometry']['location']['lng']."'
 	where dispensary_id='".$_POST['dispid']."'"; 
	
	$sql=mysql_query($query)or die(mysql_error());
	
	/*if($_REQUEST['deftype_extendeddate']){
		$updateendDateSQL="UPDATE purchased_menu_table SET
				start_date='".mysql_real_escape_string($_POST['deftype_startdate'])."',
				expiry_date='".mysql_real_escape_string($_POST['deftype_extendeddate'])."'			
				WHERE dispensary_id='".$_REQUEST['dispid']."'";
		$sql=mysql_query($updateendDateSQL)or die(mysql_error());
	}else{
		$updateendDateSQL="UPDATE purchased_menu_table SET
				start_date='".mysql_real_escape_string($_POST['deftype_startdate'])."',
				expiry_date='".mysql_real_escape_string($_POST['expiry_date'])."'			
				WHERE dispensary_id='".$_REQUEST['dispid']."'";
		$sql=mysql_query($updateendDateSQL)or die(mysql_error());
	}
	
	$updateendDateSQL="UPDATE purchased_menu_table, tbl_menu_purchase SET
				plan_type='".mysql_real_escape_string($_POST['Upgradetype'])."',
				amount='".mysql_real_escape_string($_POST['Upgradetype_amount'])."',
				start_date='".mysql_real_escape_string($_POST['Upgradetype_startdate'])."',
				expiry_date='".mysql_real_escape_string($_POST['Upgradetype_expirydate'])."'			
				WHERE tbl_menu_purchase.purchase_id =purchased_menu_table.purchase_id
				AND dispensary_id='".$_REQUEST['dispid']."'";
	$sql=mysql_query($updateendDateSQL)or die(mysql_error());*/
	
	$lastid=$_REQUEST['dispid'];
	$_SESSION['dispancryphotoimg']='';
	
	if(!empty($lastid))
	{
		if(isset($_POST['heading']))
		{
			$GetAllDispensaryMenuHeadingIdsQuery="select menu_heading_id 
												  from menu_heading 
												  where dispensary_id='".$lastid."'";
			$GetAllDispensaryMenuHeadingIdsSql=mysql_query($GetAllDispensaryMenuHeadingIdsQuery)or die(mysql_error());
			while($ids=mysql_fetch_array($GetAllDispensaryMenuHeadingIdsSql))
			{
			   $RemoveExixtingDataQueryTwo="delete from dispensaries_detail 
											where  
											menu_heading_id='".$ids['menu_heading_id']."'";
			  $RemoveExixtingDataSqlTwo=mysql_query($RemoveExixtingDataQueryTwo)or die(mysql_error());
			}
			$RemoveExixtingDataQueryOne="delete from menu_heading where dispensary_id='".$lastid."'";
			$RemoveExixtingDataSqlOne=mysql_query($RemoveExixtingDataQueryOne)or die(mysql_error());
			if($RemoveExixtingDataSqlOne)
			{
				$newcount=0;
				for($i=0;$i<count($_POST['heading']);$i++)
				{
					if(isset($_POST['menudetailno_'.($newcount+1)]))
					{
						$querone="insert into menu_heading(dispensary_id)values('".$lastid."')";
						$resone=mysql_query($querone)or die(mysql_error());
						$lastheadingid=mysql_insert_id();
						$rowno=count($_POST['menudetailno_'.($newcount+1)]);
						$j=0;
						$k=0;
						while($k< $rowno)
						{
							if(isset($_POST['menudetail_'.($newcount+1).'_'.$j]))
							{
								$rowsdata=serialize($_POST['menudetail_'.($newcount+1).'_'.$j]);
								$querone="insert into dispensaries_detail(dispensaries_detail_data,menu_heading_id)values ('".mysql_real_escape_string($rowsdata)."','".$lastheadingid."')";
								$resone=mysql_query($querone)or die(mysql_error());
								$k++;
							}
							$j++;
						}
					}
					else
					{
						$i--;
					}
					$newcount++;
				}	
			}
		}
		else
		{
			$RemoveExixtingDataQueryOne="delete from menu_heading where dispensary_id='".$lastid."'";
			$RemoveExixtingDataSqlOne=mysql_query($RemoveExixtingDataQueryOne)or die(mysql_error());
		}
		
	if($_POST['hidden_added_by']!='' and $_POST['hidden_added_by']!='0')
	{
		// update user info 
		$updateUserSQL ="UPDATE users SET
			user_name='".mysql_real_escape_string($_POST['user_name'])."',
			email_address='".mysql_real_escape_string($_POST['user_email_add'])."',
			registration_date='".mysql_real_escape_string($_POST['registration_date'])."',
			phoneno='".mysql_real_escape_string($_POST['user_phone_no'])."',
			contact_name='".mysql_real_escape_string($_POST['contact_name'])."'
			WHERE user_id='".$_POST['hidden_added_by']."'";
			
		$exeUpdateUser= mysql_query($updateUserSQL) or die($updateUserSQL." : ".mysql_error());	
		
	}
	
		header('Location:editdispensary_link.php?disp_id='.$_POST['dispid']);exit();
	}
	else
	{
		header('Location:dispensary_list.php?sql=updateunsuccess');exit();
	}
}


?>