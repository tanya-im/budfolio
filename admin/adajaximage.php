<?php
session_start();
include_once("../function.php");
include_once("../config.php");
// define the image folder path
$path = "../images/ad_images/";
$path_medium="../images/ad_images/medium/";
$path_thumbnail="../images/ad_images/thumbnail/";
// Check request methode
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
	$name = $_FILES['adimg']['name'];
	$size = $_FILES['adimg']['size'];
	// check image exists or not
	if(strlen($name))
	{
		list($txt, $ext) = explode(".", $name);
		$extention = explode(".", $name);
		$arrayconunt=(count($extention)-1);
		if(!empty($extention[$arrayconunt]))
		{
			$ext=$extention[$arrayconunt];
		}
		// Check image formate valid or not 
		if(in_array(strtolower($ext),$valid_formats))
		{
			// check image size not more than 3 Mb.
			if($size< ImageSize)
			{
				$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
				$tmp = $_FILES['adimg']['tmp_name'];
				// Move to uploaded file from tem dir to destination dir.
				if(move_uploaded_file($tmp, $path.$actual_image_name))
				{
					$src=$path.$actual_image_name;
					$dest_m=$path_medium.$actual_image_name;
					$dest_t=$path_thumbnail.$actual_image_name;
					//resize_image($src,$dest_m,300,300);
					//resize_image($src,$dest_t,100,100);
					$_SESSION['adimg']=$actual_image_name;
					echo "<img src='../images/ad_images/".$actual_image_name."'  class='preview'>";
				}
				else	
				{
					echo "failed";
				}
			}
			else
			{
				echo "Image file size max 6  MB";					
			}
		}
		else
		{
			echo "Invalid file format..";	
		}
	}
	else
	{
		echo "Please select image..!";
	}		
	exit;
}
?>