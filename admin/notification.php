<?php // 
// Include header,auth,ad_transaction and config file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");

?>
<style>
form {
    float: left;
    margin-bottom: 10px;
    margin-left: 10px;
/*    width: 59%;*/
	width:71%;
}
.div_loader > img
{
	top:250px;
}
.div_loader
{
	margin-bottom: 30%;
    margin-left: 46%;
    margin-top: 30%;
}
.home_right_part1{ float:left; width:98%; padding:3% 2% 0 1%;}
</style>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jqpagination.css" />

<script>
function getdata(page){      
	try{ 
	
		var targetURL = 'notifyingUserList.php';
		$("#page").val(page);
		$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'/></div>");
		$('html,body').animate({ scrollTop: 0 }, 'slow');
		search_xhr=$.get(targetURL,$("#dataToSend").serialize(),function(data){
			try{
				$('#userlisting').html(data);
			}catch(e){
				console.log(e)	;
			}
		});
	}catch(e){
		console.log(e)
	}
}


function search_user_here(){
	
	if($.trim($('#search_user').val())==''){
		alert("Please enter user name to search.");
		return false;
	}
	
	getdata(1);
}

function search_user(char){

	$('#user_wid_this_char').val(char);
	
	getdata(1);
}

 //order script
function fun_orderby(field){
	try{
		
		if($('#orderby').val()==field){
			if($('#order').val() == 'asc'){
				$('#order').val('desc');
			}else{
				$('#order').val('asc');
			}
		}else{
			$('#order').val('asc');
		}
		$('#orderby').val(field);
		$('#page').val('1');
		getdata(1)
	}catch(e){
		handle_try_catch_error(e)	
	}
}



</script>


<?php 
$userlistQuery="select * from users order by user_id desc";
$userlistSql=mysql_query($userlistQuery) or die(mysql_error());
$count=mysql_num_rows($userlistSql);
if($count > 0)
{
   $paginationCount=getPagination($count);
}
?>


<div class="mid_wrap">
	<h1 class="center left100 fadheading">Notify Users</h1>
    <div class="adlisterror"></div>
	<div style="width:100%;">
    <form style="width:100%;">
       	<label style="font-size:14px; float:left; margin-left:2%;"><b>Notification Message:&nbsp;</b></label>
      	<span><textarea style="float:center;height: 58px;width: 771px;" id="notification" name="notification"></textarea></span>
        <div style="margin-top: 11px;text-align: center;">
       <input type="button" id="notifyAll" name="notifyAll" value="All User!" onclick="notify_all();"/>
        <input type="button" id="notifydisUser" name="notifydisUser" value="Dispansary User!" onclick="notify_dispensry_user();"/>
        <input type="button" id="notifydisUser" name="notifydisUser" value="Member User!" onclick="notify_member_user();"/>
        </div>
    </form>
    </div>
    
    <div style="clear: both;height: 25px;padding-top: 0;text-align: center;width: 100%;font-size:16px"> Or <br />
    		Notify to selected user
    </div>
    
    <div class="home_right_part1">
    	<div class="left100">
        	<form  id="dataToSend" name="dataToSend">
           		<input id="order" type="hidden" value="asc" name="order">
           		<input id="orderby" type="hidden" value="user_name" name="orderby">
           		<input type="hidden" id="page" value="" name="page">
                <input type="hidden" id="checkval" value="" name="checkval" />
        	   <div style="width:135%;">
                  <div style="float: left; width: 45%;">
                  <input type="text" name="search_user" id="search_user" placeholder="Search User" onkeypress="searchKeyPress(event);"/>
		   		  <input type="button" name="search_user_but" id="search_user_but" value="Search" onclick="search_user_here();" />
                  </div>
              
 				 <div style="float: right; font-size: 14px; margin-left: 0px;">
                 		 <input type="button" id="notify_selected" name="notify_selected" value="Notify selected user" onclick="notify_selected_user();"/>
                 </div>
                  <input type="hidden" name="user_wid_this_char" id="user_wid_this_char" value="" />
               </div>
           		<input type="hidden" name="search" id="submit" value="search" />                              															   		   </form>
    	
       <table id="userlisting" border="1" cellpadding="5" cellspacing="0">
       
       </table>
           
        </div>
    </div>
</div>
<!-- include ajax file to strain title paging-->
<script type="text/javascript" src="js/new/jquery.jqpagination.js"></script>
<script type="text/javascript" src="js/new/notification_script.js"></script>
<script>

$(document).ready(function(){
	Array.prototype.contains = function(k) {
    for(var p in this)
        if(this[p] === k)
            return true;
    return false;
}
	var pushedId = [];
	$(document).delegate("input.check:checkbox", "change", function () {
		
		var val=$(this).attr('id');
		$('#'+val).attr("checked",true);
		//id.push(val);
		
		var len=pushedId.length;
		
			if(!pushedId.contains(val)){
				
				pushedId.push(val);
				
			}else if(pushedId.contains(val)) {
				
				pushedId.splice($.inArray(val, pushedId),1);

			}
		
		$("#checkval").val(pushedId.toString());	
	});	
	
	
	});
	

$(document).ready(function(){
	
	getdata(1);
});
 function searchKeyPress(e)
 {
        // look for window.event in case event isn't passed in
        if (typeof e == 'undefined' && window.event) { e = window.event; }
        if (e.keyCode == 13)
        {
            document.getElementById('search_user_but').click();
        }
 }
 
 function notify_all(){      
	try{ 
	
		var targetURL = 'notifyToAllUsers.php';
		var notification = $('#notification').val();
		if(notification=='')
		{
			alert("Please enter notification to be send .");
		
			return false;
			
		}
		else
		{
			$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'/></div>");
			$('html,body').animate({ scrollTop: 0 }, 'slow');
			$.ajax({
				
				type: "POST",
				url: targetURL,
				data: 'send_to_all=send_to_all&notification='+notification,
				success: function(data)
				{	
					$('#notification').val('');
					console.log(data);
					alert('Notification is successfully sent.');
					getdata(1);
			    }
			});
		}
	}catch(e){
		console.log(e)
	}
}


function notify_dispensry_user(){      
try{ 
	
		var targetURL = 'notifyToUsers.php';
		var notification = $('#notification').val();
		if(notification=='')
		{
			alert("Please enter notification to be send .");
		
			return false;
		}
		else
		{
			$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'/></div>");
			$('html,body').animate({ scrollTop: 0 }, 'slow');
			$.ajax({
				
				type: "POST",
				url: targetURL,
				data: 'send_to_d_user=send_to_d_user&notification='+notification+'&user_type=2',
				success: function(data)
				{	
					console.log(data);
					$('#notification').val('');
					alert('Notification is successfully sent.');
					getdata(1);
					
			    }
			});
		}
	}catch(e){
		console.log(e)
	}
}
 
function notify_member_user(){      
try{ 
	
		var targetURL = 'notifyToMemberUsers.php';
		var notification = $('#notification').val();
		if(notification=='')
		{
			alert("Please enter notification to be send .");
		
			return false;
		}
		else
		{
			$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'/></div>");
			$('html,body').animate({ scrollTop: 0 }, 'slow');
			$.ajax({
				
				type: "POST",
				url: targetURL,
				data: 'send_to_member_user=send_to_member_user&notification='+notification+'&user_type=1',
				success: function(data)
				{	
					console.log(data);
					$('#notification').val('');
					alert('Notification is successfully sent.');
					getdata(1);
			    }
			});
		}
	}catch(e){
		console.log(e)
	}
}
 
function notify_selected_user(){
      
try{ 
	
		var targetURL = 'notifyToSpecificUsers.php';
		var notification = $('#notification').val();
		
		var userids = $('#checkval').val();
		
		if(userids=='')
		{
			alert("Please Select User From List to Send Notification.");
			return false;
		}
		
		if(notification=='')
		{
			alert("Please enter notification to be send .");
			return false;
		}
		else
		{
			$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'/></div>");
			$('html,body').animate({ scrollTop: 0 }, 'slow');
			$.ajax({
				
				type: "POST",
				url: targetURL,
				data: 'send_to_selected_user=send_to_selected_user&notification='+notification+'&user_ids='+userids,
				success: function(data)
				{	
					console.log(data);
					$('#notification').val('');
					alert('Notification is successfully sent.');
					getdata(1);
					
			    }
			});
		}
	}catch(e){
		console.log(e)
	}
}
 
</script>
<!--End-->
<?php include_once("footer.php"); ?>