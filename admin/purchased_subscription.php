<?php 
// Include header,auth,ad_transaction and config file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");

?>
<style>
form {
    float: left;
    margin-bottom: 10px;
    margin-left: 10px;
    width: 59%;
}
.div_loader > img
{
	top:250px;
}
.div_loader
{
	margin-bottom: 30%;
    margin-left: 46%;
    margin-top: 30%;
}
.home_right_part {
	width:98%%;
}
.center
{
	 text-align: center;
}
</style>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jqpagination.css" />

<script>
function getdata(page){      
	try{ 
	
		var targetURL = 'subscriptionListData.php';
		$("#page").val(page);
		$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'   /></div>");
		$('html,body').animate({ scrollTop: 0 }, 'slow');
		search_xhr=$.get(targetURL,$("#dataToSend").serialize(),function(data){
			try{
				
			
				$('#userlisting').html(data);
			}catch(e){
				console.log(e)	;
			}
		});
	}catch(e){
		console.log(e)
	}
}


function search_user_here(){
	
	if($.trim($('#search_user').val())==''){
		alert("Please enter user name to search.");
		return false;
	}/*else{
		alert("cmpnyName "+$.trim($('#companyName').val()));
		alert("productName"+$.trim($('#productName').val()));
		return false;
	}*/
	
	getdata(1);
}

function search_user(char){


	$('#user_wid_this_char').val(char);
	
	getdata(1);
}

function confirmDelete(type)
{
	
	var agree = confirm("Are you sure you want to Delete "+type+" ?");
		if (agree)
		{
			return true ;
		}
		else
		{
			return false ;
		}
}


 //order script
function fun_orderby(field){
	try{
		
		if($('#orderby').val()==field){
			if($('#order').val() == 'asc'){
				$('#order').val('desc');
			}else{
				$('#order').val('asc');
			}
		}else{
			$('#order').val('asc');
		}
		$('#orderby').val(field);
		$('#page').val('1');
		getdata(1)
	}catch(e){
		handle_try_catch_error(e)	
	}
}

function sendReActivation(userid)
{
	
	$.post('sendReactivation.php',"userId="+userid,function(data){
		
		if(data=='emailsent')
		{
			$('#code_'+userid).text('-');
		}
	});
	
}

$(document).ready(function(){
	
	$(document).on("click","#monthlyPriceEdit",function(){
		var newPrice=$("#monthlyPrice").val();
		var targetURL = "changeDispensaryPrice.php?fun=month&newMonthlyPrice="+newPrice;
		$.ajax({
					type: "POST",
					url: (targetURL),
					cache : "false",
					success: function(data)
					{
						alert("Monthly price successfully changed");
					}
		});
		
	});
	
	$(document).on("click","#halfyearlyPriceEdit",function(){
		
		var newPrice=$("#halfyearlyPrice").val();
		var targetURL = "changeDispensaryPrice.php?fun=halfyear&halfyearlyPrice="+newPrice;
		$.ajax({
					type: "POST",
					url: (targetURL),
					cache : "false",
					success: function(data)
					{
						alert("Halfyearly price successfully changed");
					}
		});
		
	});
	
	$(document).on("click","#yearlyPriceEdit",function(){
		
		var newPrice=$("#yearlyPrice").val();
		var targetURL = "changeDispensaryPrice.php?fun=year&yearlyPrice="+newPrice;
		$.ajax({
					type: "POST",
					url: (targetURL),
					cache : "false",
					success: function(data)
					{
						alert("Yearly price successfully changed");
					}
		});
		
	});

});

</script>


<?php 
$userlistQuery="select * from `tbl_menu_purchase` order by user_id desc";
$userlistSql=mysql_query($userlistQuery) or die(mysql_error());
$count=mysql_num_rows($userlistSql);
if($count > 0)
{
   $paginationCount=getPagination($count);
}

$pricelistQuery="select * from `dispensary_price_tbl` ";
$pricelistSql=mysql_query($pricelistQuery) or die(mysql_error());
$pricelist=mysql_fetch_array($pricelistSql);

?>


<div class="mid_wrap">
	<h1 class="center left100 fadheading">Purchased Subscription</h1>
    <div class="adlisterror"></div>
    
	
    
    <div class="home_right_part" style="width:98%;">
    
    <div style="height:100px;">
    
    	<span style="float:left;margin-left: 18%;">
        	<div>
            	<span><b>Monthly:</b><br /><br />$<input type="text" id="monthlyPrice" style="height:25px;" value="<?php echo $pricelist["monthly"] ?>"  />
                </span>
                <span style="float:right;margin-right: 32%;"><br /><input type="button" id="monthlyPriceEdit" value="Edit"/></span>
            </div>
        </span>
        
        <span style="float:left;margin-left: 5%;">
        	<div>
            	<span><b>Half Yearly:</b><br /><br />$<input type="text" id="halfyearlyPrice" style="height:25px;" value="<?php echo $pricelist["half_yearly"] ?>" /></span>
                <span style="float:right;margin-right: 32%;"><br /><input type="button" id="halfyearlyPriceEdit" value="Edit"/></span>
            </div>
        </span>
        
        <span style="float:left;margin-left: 5%;">
        	<div>
            	<span><b> Yearly:</b><br /><br /> $<input type="text" id="yearlyPrice" style="height:25px;" value="<?php echo $pricelist["yearly"] ?>" /></span>
                <span style="float:right;margin-right: 32%;"><br /><input type="button" id="yearlyPriceEdit" value="Edit"/></span>	
            </div>
       </span>
    </div>
    
    	<div class="left100">
        	<form  id="dataToSend" name="dataToSend">
           		<input id="order" type="hidden" value="desc" name="order">
           		<input id="orderby" type="hidden" value="user_id" name="orderby">
           		<input type="hidden" id="page" value="" name="page">
               <!--   <input type="text" name="search_user" id="search_user" placeholder="Search User" onkeypress="searchKeyPress(event);"/>
		   		  <input type="button" name="search_user_but" id="search_user_but" value="Search" onclick="search_user_here();" />-->
        	    <input type="hidden" name="search" id="submit" value="search" />                              															   		   </form>
    	
       <table id="userlisting" border="1" cellpadding="5" cellspacing="0"></table>
           
        </div>
    </div>
</div>
<!-- include ajax file to strain title paging-->
<script type="text/javascript" src="js/new/jquery.jqpagination.js"></script>
<script type="text/javascript" src="js/dispensary_list_paging.js"></script>
<script type="text/javascript" src="js/new/user_list_script.js"></script>
<script>
$(document).ready(function(){
	
	getdata(1);
});
 
</script>
<!--End-->
<?php include_once("footer.php"); ?>