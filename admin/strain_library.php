<?php 
// Include header,auth,config and dispensary_transaction file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
?>
<style>
#userlisting tr td {
height:100px;
}
#userlisting tr th 
{
	text-align:center;
	background-color:#FFFFFF;
}
#userlisting tr td 
{
	min-height:135px;
}

form {
    float: left;
    margin-bottom: 10px;
    margin-left: 10px;
    width: 59%;
}
</style>

<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jqpagination.css" />
<script>
function getdata(page){      
	try{ 
	
		var targetURL = 'strainLibContent.php';
		$("#page").val(page);
		$('#userlisting').html("<img src='images/lightbox-ico-loading.gif' style='margin-top:200px;'  />");
		$('html,body').animate({ scrollTop: 0 }, 'slow');
		search_xhr=$.get(targetURL,$("#dataToSend").serialize(),function(data){
			try{
				$('#userlisting').html(data);
				//$("#searchResult table tbody tr:even").css("background","#DFDFDF");
				
				
			}catch(e){
				console.log(e)	;
			}
		});
	}catch(e){
		console.log(e)
	}
}


function search_strainLibeary(){
	
	if($.trim($('#search_sl').val())==''){
		alert("Please enter strain name to search.");
		return false;
	}
	
	getdata(1);
}


function confirmDelete(type)
{
	
	var agree = confirm("Are you sure you want to Delete "+type+" ?");
		if (agree)
		{
			return true ;
		}
		else
		{
			return false ;
		}
}


 //order script
function fun_orderby(field){
	try{
		
		if($('#orderby').val()==field){
			if($('#order').val() == 'asc'){
				$('#order').val('desc');
			}else{
				$('#order').val('asc');
			}
		}else{
			$('#order').val('asc');
		}
		$('#orderby').val(field);
		$('#page').val('1');
		getdata(1)
	}catch(e){
		handle_try_catch_error(e)	
	}
}
</script>


        
<div class="mid_wrap">
	<h1 class="center disp_title">Strain Libeary List</h1>
    <div class="add_new_link"><a name="strain_modal"  id="opendailogbox" class="add_btn" href="add_strain.php">Add New Strain</a></div><!--onclick="open_modal();"-->
    
    <form  id="dataToSend" name="dataToSend">
           <input id="order" type="hidden" value="asc" name="order">
           <input id="orderby" type="hidden" value="strain_name" name="orderby">
           <input type="hidden" id="page" value="" name="page">

           <div style="width:50%;"><input type="text" name="search_sl" id="search_sl"/>
		   		<input type="button" name="search_sl_but" id="search_sl_but" value="Search" onclick="search_strainLibeary();" />
           </div>
           <input type="hidden" name="search" id="submit" value="search" />                              															   </form>  
    
    
    
    <div id="dispremovemsg">
    <?php
	 if($_REQUEST['status']=='alreadyexist')
	 {?>
		<p> Strain name already exist.</p>
	 <?php }
	?>
    
    </div>
    <?php 
	// Error message and status message
	if(isset($_GET['sql']))
	{
		if($_GET['sql']=='success')
		{
			echo '<div class="dispmsg green fadinmsg">Dispensary added successfully.</div>';
		}
		if($_GET['sql']=='unsuccess')
		{
			echo '<div class="dispmsg red fadinmsg">Dispensary not added successfully.Please try again</div>';
		}
		if($_GET['sql']=='updatesuccess')
		{
			echo '<div class="dispmsg green fadinmsg">Dispensary updated successfully.</div>';
		}
		if($_GET['sql']=='updateunsuccess')
		{
			echo '<div class="dispmsg red fadinmsg">Dispensary not updated successfully.Please try again</div>';
		}
	}
	if(isset($_GET['zipcode']))
	{
		if($_GET['zipcode']=='invalid')
		{
			echo '<div class="dispmsg red fadinmsg">Invalid zip code .Please try again with valid zip code.</div>';
		}
	}
	// End messages
	?>
    <div class="disp_list" >
  	  <ul id="userlisting" class="borderall"></ul>
    </div>
 </div>       
     
        
       
        
		    
<script src="js/Ad.js"></script>
<script type="text/javascript" src="js/dispensary_list_paging.js"></script>
<script>

$(document).ready(function(){
	
	getdata(1);
});
</script>
<!--End-->
<div id="boxes"></div>
<div id="mask"></div>
<?php include_once("footer.php"); ?>