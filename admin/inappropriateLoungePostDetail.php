<?php 
// Include header,auth,ad_transaction and config file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
?>

<style>
form {
    float: left;
    margin-bottom: 10px;
    margin-left: 10px;
    width: 59%;
}
.div_loader > img
{
	top:250px;
}
.div_loader
{
	margin-bottom: 30%;
    margin-left: 46%;
    margin-top: 30%;
}
.home_right_part {
	width:98%%;
}
.center
{
	 text-align: center;
}
</style>

<script>
function getdata(page){  
    
	try{ 
		$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'   /></div>");
		$('html,body').animate({ scrollTop: 0 }, 'slow');
		$("#page").val(page);
		
		$.ajax({
					type: "POST",
					url: "loungePostListData.php",
					//data: '',
					success: function(data)
					{
						$('#postsListing').html(data);
						return false;
					}
			});
	
	
	
		/*var targetURL = 'loungePostListData.php';
		$("#page").val(page);
		$('#userlisting').html("<div class='div_loader'><img src='../images/css_images/loading.gif'   /></div>");
		$('html,body').animate({ scrollTop: 0 }, 'slow');
		search_xhr=$.get(targetURL,$("#dataToSend").serialize(),function(data){
			try{
				
			
				$('#postsListing').html(data);
			}catch(e){
				console.log(e)	;
			}
		});*/
	}catch(e){
		console.log(e)
	}
}

/*//order script
function fun_orderby(field){
	try{
		
		if($('#orderby').val()==field){
			if($('#order').val() == 'asc'){
				$('#order').val('desc');
			}else{
				$('#order').val('asc');
			}
		}else{
			$('#order').val('asc');
		}
		$('#orderby').val(field);
		$('#page').val('1');
		getdata(1)
	}catch(e){
		handle_try_catch_error(e)	
	}
}*/
</script>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jqpagination.css" />

<div class="mid_wrap">
	<h1 class="center left100 fadheading">Inappropriate Lounge Posts</h1>
    <div class="adlisterror"></div>
	
    <div class="home_right_part" style="width:98%;">
    	<div class="left100">
        	<form  id="dataToSend" name="dataToSend" style="width:0;">
           		<input id="order" type="hidden" value="desc" name="order">
           		<input id="orderby" type="hidden" value="user_id" name="orderby">
           		<input type="hidden" id="page" value="" name="page">
               <!--   <input type="text" name="search_user" id="search_user" placeholder="Search User" onkeypress="searchKeyPress(event);"/>
		   		  <input type="button" name="search_user_but" id="search_user_but" value="Search" onclick="search_user_here();" />-->
        	    <input type="hidden" name="search" id="submit" value="search" />                              															   		   </form>
    	
       <table id="postsListing" border="1" cellpadding="5" cellspacing="0"></table>
           
        </div>
    </div>
</div>
<!-- include ajax file to strain title paging-->
<script type="text/javascript" src="js/new/jquery.jqpagination.js"></script>
<script type="text/javascript" src="js/dispensary_list_paging.js"></script>
<script type="text/javascript" src="js/new/user_list_script.js"></script>
<script>
$(document).ready(function(){
	
	getdata(1);
});

function viewBudthoughtDetail(id){
	
		var validid=id.split("_");
		if(validid[1]=='')
		{
				return false;
		}
		
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
        // Load ad form  
		var id='#dialog';
		$.ajax({
					type: "POST",
					url: "showInappropriateBudthought.php",
					data: 'ShowBudthought=ShowBudthought&modifyID='+validid[1],
					async:   false,
					success: function(data)
					{
						
						$("#boxes").html(data);
						//Set the popup window to center
						$(id).css('top',  winH/2-$(id).height()/2);
						$(id).css('left', winW/2-$(id).width()/2);
						//transition effect
						$(id).fadeIn(2000); 
					}
		});
}

function viewStrainDetail(id){
	
		var validid=id.split("_");
		if(validid[1]=='')
		{
				return false;
		}
		
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
        // Load ad form  
		var id='#dialog';
		$.ajax({
					type: "POST",
					url: "showInappropriateStrain.php",
					data: 'ShowStrain=ShowStrain&modifyID='+validid[1],
					async:   false,
					success: function(data)
					{
						
						$("#boxes").html(data);
						//Set the popup window to center
						$(id).css('top',  winH/2-$(id).height()/2);
						$(id).css('left', winW/2-$(id).width()/2);
						//transition effect
						$(id).fadeIn(2000); 
					}
		});
}

function hideshowBudthoughtDetail(id){  
    
	try{ 
	
		var validid=id.split("_");
		if(validid[1]=='')
		{
				return false;
		}
		
		var actionText=$("#"+id).text();
		if(actionText=="Hide"){
		
		$.ajax({
					type: "POST",
					url: "hide_show_Posts.php",
					data: 'fun=HideBud&budId='+validid[1],
					success: function(data)
					{
						$("#"+id).text("Show");
						window.location.href="inappropriateLoungePostDetail.php";
						return false;
					}
			});
			
		}else{
			
		$.ajax({
					type: "POST",
					url: "hide_show_Posts.php",
					data: 'fun=ShowBud&budId='+validid[1],
					success: function(data)
					{
						$("#"+id).text("Hide");
						window.location.href="inappropriateLoungePostDetail.php";
						return false;
					}
			});
		
		}
	
	}catch(e){
		console.log(e)
	}
}

function hideshowStrainDetail(id){  
    
	try{ 
	
		var validid=id.split("_");
		if(validid[1]=='')
		{
				return false;
		}
		var actionText=$("#"+id).text();
		
		if(actionText=="Hide"){
		
		$.ajax({
					type: "POST",
					url: "hide_show_Posts.php",
					data: 'fun=HideStrain&strainId='+validid[1],
					success: function(data)
					{
						$("#"+id).text("Show");
						window.location.href="inappropriateLoungePostDetail.php";
						return false;
					}
			});
			
		}else{
			
		$.ajax({
					type: "POST",
					url: "hide_show_Posts.php",
					data: 'fun=ShowStrain&strainId='+validid[1],
					success: function(data)
					{
						$("#"+id).text("Hide");
						window.location.href="inappropriateLoungePostDetail.php";
						return false;
					}
			});
		
		}
	
	}catch(e){
		console.log(e)
	}
}
</script>
<!--End-->
<div id="boxes"></div>
<div id="mask"></div> 
<?php include_once("footer.php"); ?>