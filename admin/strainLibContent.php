<?php
session_start();
include_once('../config.php');
include_once('../function.php');
// by default we show first page
$pageNum = 1;
$rowsPerPage = 100; 
$selCompany_search='';

	// for search product or company
	if(!empty($_REQUEST['search_sl']))
	{
		$strainLibName = mysql_real_escape_string($_REQUEST['search_sl']);
	}
	
	if($strainLibName!='')
		{
			$selStrainLib_search=" AND strain_name like '%".strtolower($strainLibName)."%' ";
	}
	
			
	
	// if $_REQUEST['page'] defined, use it as page number
	if(isset($_REQUEST['page'])){
    	 $pageNum = $_REQUEST['page'];
		$startid = $rowsPerPage*($pageNum-1)+1;
		
	}

	// counting the offset
	$offset = ($pageNum - 1) * $rowsPerPage;
	
	// order by
	$strOrderBy=(isset($_REQUEST['orderby']))?$_REQUEST['orderby']:'strain_name';
	//get the order value
	$strOrder=(isset($_REQUEST['order']))?$_REQUEST['order']:'asc';

	$selSTrainLibrary = "SELECT * FROM `strain_library` WHERE status='0' ";
	
	if(isset($selStrainLib_search)&&$selStrainLib_search!=''){
		
		$selSTrainLibrary.=$selStrainLib_search;
	}
	
	$selSTrainLibrary.= " ORDER BY $strOrderBy $strOrder LIMIT  $offset,$rowsPerPage ";
	
	
	

	$selStrainLIbRes = mysql_query($selSTrainLibrary)or die("Error: ".$selSTrainLibrary." ".mysql_error());	
	
	// for pagination
	$query = "SELECT COUNT(strain_lib_id) AS `numrows` FROM `strain_library` WHERE 1 ";
	
	
	if(isset($selStrainLib_search)&&$selStrainLib_search!=''){
		
		$query.=$selStrainLib_search;
	}
	
	
	$result  = mysql_query($query) or die('Error, query failed');
	$row     = mysql_fetch_array($result, MYSQL_ASSOC);
	$numrows = $row['numrows'];
	$maxPage = ceil($numrows/$rowsPerPage);
	
	// by default we show first page


	$self = $_SERVER['PHP_SELF'];
	$nav  = '';

	

if($pageNum > 1){
	
    $page  = $pageNum - 1;
 	$prev  = " <a href='#' onclick='getdata(".$page.")' >[Prev]</a> ";
	$first = " <a href='#' onclick='getdata(1)'>[First Page]</a> ";		     
}else{
   $prev  = '&nbsp;'; // we're on page one, don't print previous link
   $first = '&nbsp;'; // nor the first page link
}

if ($pageNum < $maxPage){
	
   $page = $pageNum + 1;
   $next = " <a href='#' onclick='getdata(".$page.")'>[Next]</a> ";
   $last = " <a href='#' onclick='getdata(".$maxPage.")' >[Last Page]</a> ";  
}else{
   $next = '&nbsp;'; // we're on the last page, don't print next link
   $last = '&nbsp;'; // nor the last page link
}
?>
	<table class="borderall" style="width:99.9%;">
	<thead class="displihead">
		<tr>
            
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_two" class="bold">				             <a href="javascript:void(0);" onclick="fun_orderby('strain_name')">Strain Name </a>
            </th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_three" class="bold" style="width:9%;">Species
            </th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_four" class="bold" style="width:9%;">   Apearance
            </th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_five" class="bold" style="width:12%"> Lineage
            </th>
			<th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_five" class="bold" style="width:9%;">            Smell
            </th>
            <th data-sort="moveBlanks" data-sort-desc="moveBlanksDesc" id="disp_admin_header_five" class="bold" style="width:8%;">             Taste
            </th>
			<th data-sort="date" id="disp_admin_header_six" class="bold"><a href="javascript:void(0);" onclick="fun_orderby('date_time')">Added Date</a></th>
			<th id="disp_admin_header_six" class="bold center">Action</th>
		</tr></thead>
<?php 
if(mysql_num_rows($selStrainLIbRes)>0)	
{
	while($row=mysql_fetch_array($selStrainLIbRes))
	{
        $HTML.='<tr id="strainLIbid_'.$row['strain_lib_id'].'">';
		if(empty($row['strain_name']))
		{
			$HTML.='<td class="disp_admin_header_two pdright">&nbsp;</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_two pdright" style="width:14%;text-align:center;">'.str_replace("\\", "",truncatestr($row['strain_name'])).'</td>';
		}
		if(empty($row['species']))
		{
			$HTML.='<td class="disp_admin_header_three pdright" style="width:9%;">&nbsp;</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_three pdright" style="width:9%;text-align:center;">'.str_replace("\\", "",$row['species']).'</td>';
		}
		if(empty($row['lineage']))
		{
			$HTML.='<td class="disp_admin_header_four pdright">&nbsp;</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_four pdright">'.str_replace("\\", "",$row['lineage']).'</td>';
		}
		if(empty($row['apearance']))
		{
			$HTML.='<td class="disp_admin_header_five pdright" style="width:11%;">-</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_five pdright" style="width:11%;">'.str_replace("\\", "",$row['apearance']).'</td>';
		}
		if(empty($row['smell']))
		{
			$HTML.='<td class="disp_admin_header_five pdright" style="width:9%;">&nbsp;</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_five pdright" style="width:9%;">'.str_replace("\\", "",$row['smell']).'</td>';
		}
		
		if(empty($row['taste']))
		{
			$HTML.='<td class="disp_admin_header_five pdright">&nbsp;</td>';
		}
		else
		{
			$HTML.='<td class="disp_admin_header_five pdright">'.str_replace("\\", "",$row['taste']).'</td>';
		}
		if(empty($row['date_time']))
		{
			$HTML.='<td class="disp_admin_header_five pdright">&nbsp;</td>';
		}
		else
		{
			$reg_date = explode(' ',$row['date_time'] );//date('Y-m-d',strtotime($row['date_time']))
			if($reg_date[0]=='0000-00-00'){
				$reg_date[0]='-';
			}
			
			$HTML.='<td class="disp_admin_header_six pdright">'.$reg_date[0].'</td>';
		}
		
		
		$HTML.='<td class="disp_admin_header_six center">';
        $HTML.='<a href="editstrain_lib.php?strain_id='.$row['strain_lib_id'].'">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        $HTML.='<a href="javascript:void(0);" id="slibid_'.$row['strain_lib_id'].'" class="delete_slib">Delete</a>';
		$HTML.='</td>';
		$HTML.='</tr>';
		
	}
	$HTML.='<tr><td style="font-size:16px;height:24px; text-align: center;">'.$first . $prev ." Showing page $pageNum of $maxPage pages " . $next . $last.'</td></tr>';
	$HTML.='</table>';	
}
else
{/*
	$HTML='<li class="displihead">
            <div id="disp_admin_header_one">&nbsp;</div>
			<div id="disp_admin_header_two" class="bold">Customer Name</div>
			<div id="disp_admin_header_three" class="bold">Phone Number</div>
			<div id="disp_admin_header_four" class="bold">City</div>
			<div id="disp_admin_header_five" class="bold">Zip Code</div>
			<div id="disp_admin_header_six" class="bold center">Action</div>
		</li>';*/
	$HTML.='<tr><td style="text-align:center;list-style:none;">No strain found.</td></tr>';
}
echo $HTML;die;
?>