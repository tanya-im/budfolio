<?php include_once("header.php");
include_once("../config.php");
if(!isset($_SESSION['admin_name']))
{
	header("Location:home.php");
}
?>
<style>
#msg{
	margin-top: 25px;
    text-align: center;
	}
	
.admin_innerwrapper label {
    float: left;
    font-weight: bold;
    padding-left: 50px;
    text-align: left;
    width: 55%;
}
</style>

<?php 

$getAdminID = "SELECT user_id, user_name from users where user_type='3'";
$res = mysql_query($getAdminID);
$adminData = mysql_fetch_assoc($res);
$id = $adminData['user_id'];


?>
<div class="adminlogin_wrap">
<div id="msg" <?php if($_GET['resetpwd']=='unsuccess' || $_GET['resetusername']=='unsuccess' ){?> style="color:#A45900;" <?php } ?>>
<?php 
	if($_GET['resetpwd']=='success')
	{
		echo "Your password is changed successfully.";
	}
	else if($_GET['resetpwd']=='unsuccess')
	{
		echo "Your password is not changed.Please try again. ";
		
	}if($_GET['resetusername']=='success')
	{
		echo "Username is changed successfully. ";
		
	}if($_GET['resetusername']=='unsuccess')
	{
		echo "Username is not changed.Please try again. ";
	}
?>

</div>
	<div class="rpwd_wrapper">
        <h3>Change Password</h3>
        <div class="admin_innerwrapper">
            <?php if(isset($_GET['loginvalidation'])){?>
                <div class="leftwc red fadinmsg">Username or password empty.</div>
            <?php } 
			if(isset($_GET['auth'])){ if($_GET['auth']=='false'){?>
                <div class="leftwc red fadinmsg">Username or password incorrect.</div>
            <?php }} ?>
            <!-- Admin login form-->
            <form method="post" name="forgotpassword" id="forgotpassword" action="forgotpassword.php" onsubmit="return validatefgpwd();">  
                	<div>
                		<label>Enter New Password</label>
                    	<input type="password" name="fgpassword" id="fgpassword" />
                        <input type="hidden" name="admin_id" id="admin_id" value="<?php echo $id;?>" />
                        
                    </div>
                    <div>
                    	<label>Enter Confirm Password</label>
                    	<input type="password" name="confirmfgpassword" id="confirmfgpassword" />
                	</div>
                    <div class="fgbtn">
                    	<input type="submit" name="fgpwd" id="fgpwd" value="Save !" />
                       
                    </div>
                </form>  
            <!-- end admin login form-->   
        </div>
	</div>
    
    <div class="rpwd_wrapper">
        <h3>Change Username</h3>
        <div class="admin_innerwrapper">
            <?php if(isset($_GET['loginvalidation'])){?>
                <div class="leftwc red fadinmsg">Username or password empty.</div>
            <?php } 
			if(isset($_GET['auth'])){ if($_GET['auth']=='false'){?>
                <div class="leftwc red fadinmsg">Username or password incorrect.</div>
            <?php }} ?>
            <!-- Admin login form-->
            <form method="post" name="changeusername" id="changeusername" action="forgotpassword.php" onsubmit="return validateusername();">  
                	<div>
                		<label>Current Username</label>
                    	<input type="text" name="current_name" id="current_name" value="<?php echo $adminData['user_name'];?>" />
                        <input type="hidden" name="id_admin" id="id_admin" value="<?php echo $id;?>" />
                        
                    </div>
                    <div>
                    	<label>New Username</label>
                    	<input type="text" name="new_username" id="new_username" />
                	</div>
                    <div class="fgbtn">
                    	<input type="submit" name="username" id="username" value="Save !" />
                       
                    </div>
                </form>  
            <!-- end admin login form-->   
        </div>
	</div>
</div>
<!-- Include footer file-->
<?php include_once("footer.php"); ?>
<!-- end -->
<!-- validate admin login form-->
<script>

function validateusername()
{
	var newUsername=document.getElementById("new_username").value;
	if(newUsername=='')
	{
		document.getElementById('new_username').focus();
		alert('Please enter New Username');
		return false;
	}
	return true;
}

function validatefgpwd()
{
	var fgpassword=document.getElementById("fgpassword").value;
	var confirmfgpassword=document.getElementById("confirmfgpassword").value;
	if(fgpassword=='')
	{
		document.getElementById('fgpassword').focus();
		alert('Please enter password');
		return false;
	}
	if(confirmfgpassword=='')
	{
		document.getElementById('confirmfgpassword').focus();
		alert('Please enter confirm password');
		return false;
	}
	if(fgpassword!=confirmfgpassword)
	{
		document.getElementById("fgpassword").value='';
		document.getElementById("confirmfgpassword").value='';
		document.getElementById('fgpassword').focus();
		alert('Confirm password do not match');
		
		return false;
	}
	return true;
}

</script>