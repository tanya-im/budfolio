<?php 
// Include header,auth,config and dispensary_transaction file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
?>
<style>
#userlisting tr td {
height:100px;
}

#userlisting tr th 
{
	text-align:center;
	background-color:#FFFFFF;
}
form {
    float: left;
    margin-bottom: 10px;
    margin-left: 10px;
    width: 100%;
}
</style>

<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jqpagination.css" />
<script>
function getdata(page){      
	try{ 
	
		var targetURL = 'Dispensary_ListData.php';
		$("#page").val(page);
		$('#userlisting').html("<img src='../images/css_images/loading.gif' style='margin-top:150px;margin-left:300px;'  />");
		$('html,body').animate({ scrollTop: 0 }, 'slow');
		search_xhr=$.get(targetURL,$("#dataToSend").serialize(),function(data){
			try{
					$('#userlisting').html(data);
				}catch(e)
				{
					console.log(e)	;
				}
		});
	}catch(e){
		console.log(e)
	}
}


function search_disp(){
	
	if($.trim($('#search_dis').val())==''){
		alert("Please enter text to search.");
		return false;
	}
	
	getdata(1);
}


function confirmDelete(type)
{
	
	var agree = confirm("Are you sure you want to Delete "+type+" ?");
		if (agree)
		{
			return true ;
		}
		else
		{
			return false ;
		}
}


 //order script
function fun_orderby(field){
	try{
		
		if($('#orderby').val()==field){
			if($('#order').val() == 'asc'){
				$('#order').val('desc');
			}else{
				$('#order').val('asc');
			}
		}else{
			$('#order').val('asc');
		}
		$('#orderby').val(field);
		$('#page').val('1');
		getdata(1)
	}catch(e){
		handle_try_catch_error(e)	
	}
}

//deactivate dispensary
function deactivateDis(dis_id)
{
	if(dis_id==''){
		return false;
	}else
	{
		$.post('deactivate_dispensary.php','disp_id='+dis_id+'&deactive=deactive',function(data){
			
			if(data=='deactivated')
			{
				$('#deactive_'+dis_id).html('<a href="javascript:void(0);" onclick="activateDis('+dis_id+');">Activate</a>');		
			}
			
		});
	}
}

function activateDis(dis_id)
{
	if(dis_id==''){
		return false;
	}else
	{
		$.post('deactivate_dispensary.php','disp_id='+dis_id+'&activate=activate',function(data){
			
			if(data=='activated')
			{
				$('#deactive_'+dis_id).html('<a href="javascript:void(0);" onclick="deactivateDis('+dis_id+');">Deactivate</a>');
			}
		});
	}

}
</script>        
<div class="mid_wrap">
	<h1 class="center disp_title">Dispensary List</h1>
    <div class="add_new_link"><a href="dispensary_link.php">Add New Dispensary</a></div>
    <form  id="dataToSend" name="dataToSend">
           <input id="order" type="hidden" value="asc" name="order">
           <input id="orderby" type="hidden" value="dispensary_name" name="orderby">
           <input type="hidden" id="page" value="" name="page">

           <div style="width:98%;"><input type="text" name="search_dis" id="search_dis"/>
		   		<input type="button" name="search_dis_but" id="search_dis_but" value="Search" onclick="search_disp();" />
            <!--    <div class="add_new_link"><a href="claimed_dispensary.php">Claimed Dispensary</a></div> -->
                <input type="button" name="reset" value="Reset" onclick="emptySearch();"/> 
                 <div class="add_new_link"><b><a href="downloadDispensaryExcel.php">Export</a></b></div>
           </div>
           <input type="hidden" name="search" id="submit" value="search" />                             															   </form>
    
    
    <div id="dispremovemsg"></div>
    <?php 
	// Error message and status message
	if(isset($_GET['sql']))
	{
		if($_GET['sql']=='success')
		{
			echo '<div class="dispmsg green fadinmsg">Dispensary added successfully.</div>';
		}
		if($_GET['sql']=='unsuccess')
		{
			echo '<div class="dispmsg red fadinmsg">Dispensary not added successfully.Please try again</div>';
		}
		if($_GET['sql']=='updatesuccess')
		{
			echo '<div class="dispmsg green fadinmsg">Dispensary updated successfully.</div>';
		}
		if($_GET['sql']=='updateunsuccess')
		{
			echo '<div class="dispmsg red fadinmsg">Dispensary not updated successfully.Please try again</div>';
		}
	}
	if(isset($_GET['zipcode']))
	{
		if($_GET['zipcode']=='invalid')
		{
			echo '<div class="dispmsg red fadinmsg">Invalid zip code .Please try again with valid zip code.</div>';
		}
	}
	// End messages
	?>
    <div class="disp_list" >
  	  <ul id="userlisting" class="borderall"></ul>
    </div>
 </div>       
     
    
       
        
		    
<script src="js/Ad.js"></script>
<script type="text/javascript" src="js/new/jquery.jqpagination.js"></script>
<!--<script type="text/javascript" src="js/new/scripts.js"></script>
generic demo script
<script src="./demo/js/demo.js"></script>
<!-- include ajax file to strain title paging-->
<script type="text/javascript" src="js/dispensary_list_paging.js"></script>
<script>
$(document).ready(function(){
	
	getdata(1);
});
function emptySearch()
{
	$('#search_dis').val('');
	getdata(1);
}
</script>
<!--End-->
<?php include_once("footer.php"); ?>