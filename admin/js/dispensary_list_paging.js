/*function changePagination(pageId,liId,searchTxt){
	
	
      $(".flash").show();
      $(".flash").fadeIn(400).html('<img src="../images/imagesforcss/ajax-loading.gif" />');
      var status='active';
	  if($('#active_list').is(':checked'))
	  { 
	  	status='active'; 
	  }
	  if($('#deactive_list').is(':checked'))
	  { 
	  	status='deactive'; 
	  }
	  var dataString = 'pageId='+ pageId+'&searchtxt='+searchTxt;
      $.ajax({
      type: "GET",
      url: "Dispensary_ListData.php",
      data: dataString,
      cache: false,
      success: function(result)
	  		{
			 
               $("#userlisting").html(result);
			}
      });
}*/

$(document).ready(function()
{ 
	$(".user_list").fadeIn("slow");
	$("body").on("click",'.delete_disp',function(){
		t=confirm("Are you sure you want to delete this Customer Detail?");
		if(t==true)
		{
			$("#dispremovemsg").removeClass("dispremovemsg");
			$(".fadinmsg").hide();
			var id=$(this).attr("id");
			var dispid=id.split("_");
			if(dispid[1]!='')
			{
				$.ajax({
					type: "POST",
					url: "DeleteDisp.php",
					data: 'dispid='+dispid[1],
					cache: false,
					success: function(result)
					{
						if(result=='delete')
						{
							$("#displiid_"+dispid[1]).remove();
							$("#dispremovemsg").addClass("dispremovemsg");
							$("#dispremovemsg").show();
							$("#dispremovemsg").html("Customer Detail Delete Successfully.").delay(5000).fadeOut("slow");
						}
					}
				});
			}
		}
	});
	
	
	$("body").on("click",'.delete_slib',function(){
		t=confirm("Are you sure you want to delete this Strain From Strain Library?");
		if(t==true)
		{
			$("#dispremovemsg").removeClass("dispremovemsg");
			$(".fadinmsg").hide();
			var id=$(this).attr("id");
			var slibid=id.split("_");
		
			if(slibid[1]!='')
			{
				$.ajax({
					type: "POST",
					url: "delete_strain_lib.php",
					data: 'slibid='+slibid[1],
					cache: false,
					success: function(result)
					{
						
						if(result=='delete')
						{
							$("#strainLIbid_"+slibid[1]).remove();
							$("#dispremovemsg").addClass("dispremovemsg");
							$("#dispremovemsg").show();
							$("#dispremovemsg").html("Strain is deleted Successfully.").delay(5000).fadeOut("slow");
						}
					}
				});
			}
		}
	});
	
	
	
	$("body").on("click",'.delete_user',function(){
		t=confirm("Are you sure you want to delete this user?");
		if(t==true)
		{
			$("#dispremovemsg").removeClass("dispremovemsg");
			$(".fadinmsg").hide();
			var id=$(this).attr("id");
			var userid=id.split("_");
		
			if(userid[1]!='')
			{
				$.ajax({
					type: "POST",
					url:  "delete_user.php",
					data: 'deleteUser=deleteUser&userid='+userid[1],
					cache: false,
					success: function(result)
					{
						if(result=='delete')
						{
							$("#UserId_"+userid[1]).remove();
							$("#dispremovemsg").addClass("dispremovemsg");
							$("#dispremovemsg").show();
							$("#dispremovemsg").html("Strain is deleted Successfully.").delay(5000).fadeOut("slow");
						}
					}
				});
			}
		}
	});
});

/*function search_disp()
{
	searchText =$('#search_dis').val();

	changePagination('0','first',searchText);
	
}


<!-- first time called-->
$(document).ready(function()
{ 
	searchText =$('#search_dis').val();
	changePagination('0','first',searchText);
});*/