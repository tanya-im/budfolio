var fieldName='frantAdDeactivate[]';

function selectall(){
  var i=document.fornaddlistform.elements.length;
  var e=document.fornaddlistform.elements;
  var name=new Array();
  var value=new Array();
  var j=0;
  for(var k=0;k<i;k++)
  {
    if(document.fornaddlistform.elements[k].name==fieldName)
    {
      if(document.fornaddlistform.elements[k].checked==true){
        value[j]=document.fornaddlistform.elements[k].value;
        j++;
      }
    }
  }
  checkSelect();
}
function selectCheck(obj)
{
 var i=document.fornaddlistform.elements.length;
  for(var k=0;k<i;k++)
  {
    if(document.fornaddlistform.elements[k].name==fieldName)
    {
      document.fornaddlistform.elements[k].checked=obj;
    }
  }
  selectall();
}

function selectallMe()
{
  if(document.fornaddlistform.frant_ad_Deactivate_all.checked==true)
  {
   selectCheck(true);
  }
  else
  {
    selectCheck(false);
  }
}
function checkSelect()
{
 var i=document.fornaddlistform.elements.length;
 var berror=true;
  for(var k=0;k<i;k++)
  {
    if(document.fornaddlistform.elements[k].name==fieldName)
    {
      if(document.fornaddlistform.elements[k].checked==false)
      {
        berror=false;
        break;
      }
    }
  }
  if(berror==false)
  {
    document.fornaddlistform.frant_ad_Deactivate_all.checked=false;
  }
  else
  {
    document.fornaddlistform.frant_ad_Deactivate_all.checked=true;
  }
}

function AdConfirmationMsgDeact()
{
	var c=confirm("Are you sure you want to deactivate the selected Advertise?");
	if(c==true)
	{
		$("#action").val("deactivate");
		$("#fornaddlistform").submit();
	}
	else{ return false;}
}
function AdConfirmationMsgDelete()
{
	var c=confirm("Are you sure you want to delete the selected Advertise?");
	if(c==true)
	{
		$("#action").val("delete");
		$("#fornaddlistform").submit();
	}
	else{ return false;}
}
function ShowUpdateAdRow(id)
{
	if(id=='')
	{
		return false;	
	}
	$.ajax({
			type: "POST",
			url: "ShowUpdateAdRow.php",
			data: 'showUpdateAdRow=showUpdateAdRow&adid='+id,
			success: function(data)
			{
				if(data=='invalidid')
				{
					
				}
				else
				{
					$("#adliid_"+id).html(data);	
				}
			}
	});	
}

function ShowAddedAdRow(id)
{
	if(id=='')
	{
		return false;	
	}
	$.ajax({
			type: "POST",
			url: "ShowUpdateAdRow.php",
			data: 'showUpdateAdRow=showUpdateAdRow&adid='+id,
			success: function(data)
			{
				if(data=='invalidid')
				{
					
				}
				else
				{
					$("#Adtable tbody").prepend('<tr id="adliid_'+id+'">'+data+'</tr>');	
				}
			}
	});	
}
function showLoader()
{
	//Get the screen height and width
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();
	
	//Set heigth and width to mask to fill up the whole screen
	$('#loderbox').css({'width':maskWidth,'height':maskHeight});
	
	//transition effect		
	$('#loderbox').fadeIn(1000);
	$('#loderbox').fadeTo("slow",0.6);

	//Get the window height and width
	var winH = $(window).height();
	var winW = $(window).width();
	$("#loderimg").html('<img src="images/css_images/loader.gif" alt="Uploading...."/>');
	$("#loderimg").css('top',winH/2-$("#loderimg").height()/2);
	$("#loderimg").css('left', winW/2-$("#loderimg").width()/2);
	$('#loderimg').fadeIn("slow");
}
function hideLoader()
{
	$('#loderbox').fadeOut("slow");
	$("#loderimg").fadeOut('slow');	
}