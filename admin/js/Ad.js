$(document).ready(function(){ 
	$("body").on('click','.dlete_ad',function(){
	t=confirm("Are you sure you want to delete this advertisement?");
	if(t==true)
	{
		var id=$(this).attr("id");
		var adid=id.split("_");
		if(adid[1]!='')
		{
			$.ajax({
					type: "POST",
					url: "AdAction.php",
					data: 'AdDelete=AdDelete&adid='+adid[1],
					cache: false,
					success: function(result)
					{
						if(result=='Deleted')
						{
							$("#adliid_"+adid[1]).remove();
							$(".adlisterror").html('Ad delete successfully.');
						}
						if(result=='NotDeactivate')
						{
							$(".adlisterror").html('Ad not deleted successfully. Please try again.');
						}
					}
				});
			}
		}
	});
	
	$("body").on('click','.deactive_ad',function(){
	t=confirm("Are you sure you want to deactivate this advertisement?");
	if(t==true)
	{
		var id=$(this).attr("id");
		var adid=id.split("_");
		if(adid[1]!='')
		{
			$.ajax({
					type: "POST",
					url: "AdAction.php",
					data: 'Addeactivate=Addeactivate&adidDeactivate='+adid[1],
					cache: false,
					success: function(result)
					{
						if(result=='Deactivate')
						{
							$("#statusid_"+adid[1]).html('deactive');
							$(".adlisterror").html('Ad deactive successfully.');
						}
						if(result=='NotDeactivate')
						{
							$(".adlisterror").html('Ad not deactive successfully. Please try again.');
						}
					}
			});
		}
	}
	});
});


function goOneStepBAck()
{
	window.history.go(-1);
}

$(document).ready(function() {	

	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
        // Load ad form  
		var id='#dialog'; 
		var type=$(this).attr('href');   
		$.ajax({
					type: "POST",
					url: "AdForm.php",
					data: 'AdForm=AdForm&type='+type,
					async:   false,
					success: function(data)
					{
						$("#boxes").html(data);
						//Set the popup window to center
						$(id).css('top',  winH/2-$(id).height()/2);
						$(id).css('left', winW/2-$(id).width()/2);
						//transition effect
						$(id).fadeIn(2000); 
					}
		});
		
		
		//select all the a tag with name equal to modal
		
	});
	
	//select all the a tag with name equal to modal
	$('body').on('click','.adedit',function(e) {
		//Cancel the link behavior
		e.preventDefault();
		var adid=$(this).attr("id");
		var advalidid=adid.split("_");
		if(advalidid[1]=='')
		{
				return false;
		}
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
        // Load ad form  
		var id='#dialog';
		$.ajax({
					type: "POST",
					url: "AdEditForm.php",
					data: 'AdEditForm=AdEditForm&adid='+advalidid[1],
					async:   false,
					success: function(data)
					{
						$("#boxes").html(data);
						//Set the popup window to center
						$(id).css('top',  winH/2-$(id).height()/2);
						$(id).css('left', winW/2-$(id).width()/2);
						//transition effect
						$(id).fadeIn(2000); 
					}
		});
	});
	
	
	
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});			

	$(window).resize(function () {
	 
 		var box = $('#boxes .window');
 
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
      
        //Set height and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});
               
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        box.css('top',  winH/2 - box.height()/2);
        box.css('left', winW/2 - box.width()/2);
	});
	
	$("body").on("click","#save_detail",function(){
		var CustomerName=$("#CustomerName").val();
		var WebsiteUrl=$("#WebsiteUrl").val();
		var StartingZipCode=$("#StartingZipCode").val();
		var ActiveViewRadius=$("#ActiveViewRadius").val();
		var MaxPageViews=$("#MaxPageViews").val();
		var datepicker=$("#datepicker").val();
		var adtype=$("#adtype").val(); 
		var zipsetting=$("#zipsetting").val(); 
		if(CustomerName=='')
		{
			$(".ad_errormsg").html("Please Fill the Customer Name");
			$("#CustomerName").focus();
			return false;
		}
		if(WebsiteUrl=='http://' || WebsiteUrl=='')
		{
			$(".ad_errormsg").html("Please Fill the Website Url");
			$("#WebsiteUrl").focus();
			return false;
		}
		if(StartingZipCode=='')
		{
			$(".ad_errormsg").html("Please Fill the Starting Zip Code");
			$("#StartingZipCode").focus();
			return false;
		}
		if(StartingZipCode!='')
		{
			var numbers = /^[0-9,.]+$/;  
			if(StartingZipCode.match(numbers))  
			{  
				 
			}  
			else  
			{  
				$(".ad_errormsg").html("Starting zip code must have numeric values only");
				
				return false;  
			}
		}
		if(ActiveViewRadius=='')
		{
			$(".ad_errormsg").html("Please Fill the Active View Radius");
			$("#ActiveViewRadius").focus();
			return false;
		}
		if(ActiveViewRadius!='')
		{
			var numbers = /^[0-9,.]+$/;  
			if(ActiveViewRadius.match(numbers))  
			{  
				 
			}  
			else  
			{  
				$(".ad_errormsg").html("Active view radius must have numeric values only");
				$("#ActiveViewRadius").focus();  
				return false;  
			} 	
		}
		if(MaxPageViews=='')
		{
			$(".ad_errormsg").html("Please Fill the Max Page Views");
			$("#MaxPageViews").focus();
			return false;
		}
		if(datepicker=='')
		{
			$(".ad_errormsg").html("Please Fill the Expire Date");
			$("#datepicker").focus();
			return false;
		}
		var status='';
		if($('#adstatusact').is(':checked'))
		{ 
			 status='active';
		}
		if($('#adstatusdeact').is(':checked'))
		{ 
			status='deactive'; 
		}
		$(".loaderbar").show();
		$.ajax({
					type: "POST",
					url: "AddAdDetail.php",
					data: 'AddAdDetail=AddAdDetail&type='+adtype+'&CustomerName='+CustomerName+'&WebsiteUrl='+WebsiteUrl+'&StartingZipCode='+StartingZipCode+'&ActiveViewRadius='+ActiveViewRadius+'&MaxPageViews='+MaxPageViews+'&datepicker='+datepicker+'&status='+status+'&zipsetting='+zipsetting,
					async:   false,
					success: function(data)
					{
						var lasid=data.split("_");
						if(lasid[0]=='InvalidZipCode')
						{
							$(".ad_errormsg").html("Please Enter Valid ZipCode.");
							$(".loaderbar").fadeOut("slow");
							return false;
						}
						if(lasid[0]=='AdvertisementSave')
						{
							$(".adlisterror").html("Advertisement save successfully.");
							ShowAddedAdRow(lasid[1]);
						}
						if(lasid[0]=='AdvertisementNotSave')
						{
							$(".adlisterror").html("Advertisement not save. Please try again.");	
						}
						closeme();
					}
		});
	});
	
	$("body").on("click","#update_detail",function(){
		var CustomerName=$("#CustomerName").val();
		var WebsiteUrl=$("#WebsiteUrl").val();
		var StartingZipCode=$("#StartingZipCode").val();
		var ActiveViewRadius=$("#ActiveViewRadius").val();
		var MaxPageViews=$("#MaxPageViews").val();
		var datepicker=$("#datepicker").val();
		var adtype=$("#adtype").val();
		var zipsetting=$("#zipsetting").val(); 
		if(CustomerName=='')
		{
			$(".ad_errormsg").html("Please Fill the Customer Name");
			$("#CustomerName").focus();
			return false;
		}
		if(WebsiteUrl=='http://' || WebsiteUrl=='')
		{
			$(".ad_errormsg").html("Please Fill the Website Url");
			$("#WebsiteUrl").focus();
			return false;
		}
		if(StartingZipCode=='')
		{
			$(".ad_errormsg").html("Please Fill the Starting Zip Code");
			$("#StartingZipCode").focus();
			return false;
		}
		if(StartingZipCode!='')
		{
			var numbers = /^[0-9,.]+$/;  
			if(StartingZipCode.match(numbers))  
			{  
				 
			}  
			else  
			{  
				$(".ad_errormsg").html("Starting zip code must have numeric values only");
				$("#StartingZipCode").focus();  
				return false;  
			}
		}
		if(ActiveViewRadius=='')
		{
			$(".ad_errormsg").html("Please Fill the Active View Radius");
			$("#ActiveViewRadius").focus();
			return false;
		}
		if(ActiveViewRadius!='')
		{
			var numbers = /^[0-9,.]+$/;  
			if(ActiveViewRadius.match(numbers))  
			{  
				 
			}  
			else  
			{  
				$(".ad_errormsg").html("Active view radius must have numeric values only");
				$("#ActiveViewRadius").focus();  
				return false;  
			} 	
		}
		if(MaxPageViews=='')
		{
			$(".ad_errormsg").html("Please Fill the Max Page Views");
			$("#MaxPageViews").focus();
			return false;
		}
		if(datepicker=='')
		{
			$(".ad_errormsg").html("Please Fill the Expire Date");
			$("#datepicker").focus();
			return false;
		}
		var status='';
		if($('#adstatusact').is(':checked'))
		{ 
			 status='active';
		}
		if($('#adstatusdeact').is(':checked'))
		{ 
			status='deactive'; 
		}
		var id=$("#adidforupdate").val();
		$(".loaderbar").show();
		$.ajax({
					type: "POST",
					url: "AddAdDetail.php",
					data: 'UpdateAd=UpdateAd&adid='+id+'&CustomerName='+CustomerName+'&WebsiteUrl='+WebsiteUrl+'&StartingZipCode='+StartingZipCode+'&ActiveViewRadius='+ActiveViewRadius+'&MaxPageViews='+MaxPageViews+'&datepicker='+datepicker+'&status='+status+'&zipsetting='+zipsetting,
					async:   false,
					success: function(data)
					{
						if(data=='InvalidZipCode')
						{
							$(".ad_errormsg").html("Please Enter Valid ZipCode.");
							$(".loaderbar").fadeOut("slow");
							return false;
						}
						if(data=='AdvertisementUpdate')
						{
							$(".adlisterror").html("Advertisement updated successfully.");
							ShowUpdateAdRow(id);
						}
						if(data=='AdvertisementNotUpdate')
						{
							$(".adlisterror").html("Advertisement not update. Please try again.");	
						}
						closeme();
					}
		});
	});
	//if mask is clicked
	$('body').on('click','.close',function () {
		closeme();
	});	
	
});

function closeme()
{
	$("#mask").fadeOut("slow");
	$('.window').fadeOut("slow");
}

$(function(){
      // Helper function to convert a string of the form "Mar 15, 1987" into
      // a Date object.
      var date_from_string = function(str){
        var months = ["jan","feb","mar","apr","may","jun","jul",
                      "aug","sep","oct","nov","dec"];
        var pattern = "^([a-zA-Z]{3})\\s*(\\d{2}),\\s*(\\d{4})$";
        var re = new RegExp(pattern);
        var DateParts = re.exec(str).slice(1);

        var Year = DateParts[2];
        var Month = $.inArray(DateParts[0].toLowerCase(), months);
        var Day = DateParts[1];
        return new Date(Year, Month, Day);
      }

      var moveBlanks = function(a, b) {
        if ( a < b ){
          if (a == "")
            return 1;
          else
            return -1;
        }
        if ( a > b ){
          if (b == "")
            return -1;
          else
            return 1;
        }
        return 0;
      };
      var moveBlanksDesc = function(a, b) {
        // Blanks are by definition the smallest value, so we don't have to
        // worry about them here
        if ( a < b )
          return 1;
        if ( a > b )
          return -1;
        return 0;
      };

      var table = $("table").stupidtable({
        "date":function(a,b){
          // Get these into date objects for comparison.

          aDate = date_from_string(a);
          bDate = date_from_string(b);

          return aDate - bDate;
        },
        "moveBlanks": moveBlanks,
        "moveBlanksDesc": moveBlanksDesc,
      });

      table.on("beforetablesort", function (event, data) {
        // data.column - the index of the column sorted after a click
        // data.direction - the sorting direction (either asc or desc)
        $("#msg").text("Sorting index " + data.column)
      });

      table.on("aftertablesort", function (event, data) {
        var th = $(this).find("th");
        th.find(".arrow").remove();
        var dir = $.fn.stupidtable.dir;

        var arrow = data.direction === dir.ASC ? "&uarr;" : "&darr;";
        th.eq(data.column).append('<span class="arrow">' + arrow +'</span>');
      });

      $("tr").slice(1).click(function(){
       // $(".awesome").removeClass("awesome");
       // $(this).addClass("awesome");
      });

    });
	
	
	function open_modal()
	{
		
		
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
        // Load ad form  
		var id='#dialog_strain'; 
		var type=$(this).attr('href');   
		$.ajax({
					type: "POST",
					url: "add_strain.php",
					data: 'add_strain=add_strain',
					async:   false,
					success: function(data)
					{
						
						$("#boxes").html(data);
						//Set the popup window to center
						$(id).css('top',  winH/2-$(id).height()/2);
						$(id).css('left', winW/2-$(id).width()/2);
						//transition effect
						$(id).fadeIn(2000); 
					}
		});
	}
	
	
	
$("body").on("click","#Save_strain",function(){
		var StrainName=$("#StrainName").val();
		var species=$("#species").val();
		var apearance=$("#apearance").val();
		var lineage=$("#lineage").val();
		var smell=$("#smell").val();
		var taste=$("#taste").val();
		
		if(StrainName=='')
		{
			$(".ad_errormsg").html("Please Fill the strain Name");
			$("#StrainName").focus();
			return false;
		}
		if(species=='')
		{
			$('.ad_errormsg').html('Please enter the species name').css('color','#A45900');
			return false;
		}
		if(lineage=='')
		{
			$('.ad_errormsg').html('Please enter the lineage name').css('color','#A45900');
			return false;
		}
		
		$(".loaderbar").show();
		$.ajax({
					type: "POST",
					url: "AddStrain.php",
					data: 'AddStrain=AddStrain&StrainName='+StrainName+'&species='+species+'&apearance='+apearance+'&lineage='+lineage+'&smell='+smell+'&taste='+taste,
					async:   false,
					success: function(data)
					{
						
						if(data=='alreadyExist')
						{
							//$(location).attr('href','strain_library.php?status=alreadyexist');
						//alert('in already');
							window.location ="strain_library.php?status=alreadyexist";
							/*$(".ad_errormsg").html("Strain name already exist. Please try another name.").css('color','#A45900').delay(5000).fadeOut("slow");
;	*/
							return false;
						}
						else if(data=='NotSave')
						{
							$(".ad_errormsg").html("Strain is not save. Please try again.").css('color','#A45900').delay(5000).fadeOut("slow");
;	
							return false;
							
						}
						else if(data=='save')
						{
							
							$(".ad_errormsg").html("Strain added successfully.").css('color','#A45900').delay(5000).fadeOut("slow");
							
						}
						closeme();
					}
		});
	});	
	


$("body").on("click","#edit_strain",function(){
		var StrainName=$("#StrainName").val();
		var species=$("#species").val();
		var apearance=$("#apearance").val();
		var lineage=$("#lineage").val();
		var smell=$("#smell").val();
		var taste=$("#taste").val();
		var strain_lib_id = $('#strain_lib_id').val();
		
		if(StrainName=='')
		{
			$(".ad_errormsg").html("Please Fill the strain Name");
			$("#StrainName").focus();
			return false;
		}
		if(species=='')
		{
			$('.ad_errormsg').html('Please enter the species name').css('color','#A45900');
			return false;
		}
		if(lineage=='')
		{
			$('.ad_errormsg').html('Please enter the lineage name').css('color','#A45900');
			return false;
		}
		
		$(".loaderbar").show();
		$.ajax({
					type: "POST",
					url: "AddStrain.php",
					data: 'UpdateStrain=UpdateStrain&StrainName='+StrainName+'&species='+species+'&apearance='+apearance+'&lineage='+lineage+'&smell='+smell+'&taste='+taste+'&strain_lib_id='+strain_lib_id,
					async:   false,
					success: function(data)
					{
						
						
						if(data=='notupdate')
						{
							$(".ad_errormsg").html("Strain is not updated. Please try again.").css('color','#A45900').delay(5000).fadeOut("slow");
;	
							return false;
							
						}
						else if(data=='update')
						{
							
							$(".ad_errormsg").html("Strain updated successfully.").css('color','#A45900').delay(5000).fadeOut("slow");
						window.location.href="strain_library.php";	
							
						}
						closeme();
					}
		});
	});	