$(document).ready(function(){ 
	$('body').on('change','#landingpageimg',function(){ 
		$("#disp_image_preview").html('');
		$("#disp_image_preview").html('<img src="../../images/css_images/loader.gif" alt="Uploading...."/>');
		$("#landingpageimageform").ajaxForm({target: '#disp_image_preview',success:allimage_preview()}).submit();
	});
	
	$('body').on('change','#adimg',function(){ 
		$("#ad_image_preview").html('');
		$("#ad_image_preview").html('<img src="../images/css_images/loader.gif" alt="Uploading...."/>');
		$("#adimageform").ajaxForm({target: '#ad_image_preview'}).submit();
	});
	
	$('body').on('change','#dispancryphotoimg',function(){ 
		$("#disp_image_preview").html('');
		$("#disp_image_preview").html('<img src="../images/css_images/loader.gif" alt="Uploading...."/>');
		$("#dispancryimageform").ajaxForm({target: '#disp_image_preview'}).submit();
	});
	
/*	$('body').on('click','.deactuser',function(){ 
		var id=$(this).attr("id");
		var userrid=id.split("_");
		if(userrid[1]=='')
		{
			return false;
		}
		$(".adlisterror").fadeIn("slow")
		$.ajax({
				type: "POST",
				url: "DeactivateUser.php",
				data: 'deactivatruser=deactivatruser&userid='+userrid[1],
				async:   false,
				success: function(data)
				{
					alert(data);
					$("#uid_"+userrid[1]).html(data);
					if(data=='deactive'){
					$(".adlisterror").html('User deactivated successfully');
					}
					if(data=='active'){
					$(".adlisterror").html('User activated successfully');
					}
				}
		});
	});*/
	$('body').on('click','.deactuser',function(){ 
		var id=$(this).attr("id");
		var userrid=id.split("_");
		if(userrid[1]=='')
		{
			return false;
		}
		$(".adlisterror").fadeIn("slow")
		$.ajax({
				type: "POST",
				url: "DeactivateUser.php",
				data: 'deactivatruser=deactivatruser&userid='+userrid[1],
				async:   false,
				success: function(data)
				{
					//alert(data);
					
					
				
					if(data=='Deactive'){
						showData ='Active';
						$(".adlisterror").html('User deactivated successfully');
					}
					if(data=='Active'){
						showData ='Deactive';
						$(".adlisterror").html('User activated successfully');
					}
						$("#uid_"+userrid[1]).html(showData);
				}
		});
	});
});
function allimage_preview()
{
	$.ajax({
				type: "POST",
				url: "show_landing_page_images.php",
				data: 'all_image=show',
				async:   false,
				success: function(data)
				{
					$(".images_preview_ldetail").html(data);
				}
		});
}

function validate()
{
	var CustomerName=$("#CustomerName").val();
	var WebsiteUrl=$("#WebsiteUrl").val();
	var StartingZipCode=$("#StartingZipCode").val();
	var ActiveViewRadius=$("#ActiveViewRadius").val();
	var MaxPageViews=$("#MaxPageViews").val();
	var datepicker=$("#datepicker").val();
	if(CustomerName=='')
	{
		$("#CustomerName").focus();
		alert("Please Fill the Customer Name");
		return false;
	}
	if(WebsiteUrl=='http://')
	{
		$("#WebsiteUrl").focus();
		alert("Please Fill the Website Url");
		return false;
	}
	if(WebsiteUrl=='')
	{
		$("#WebsiteUrl").focus();
		alert("Please Fill the Website Url");
		return false;
	}
	if(StartingZipCode=='')
	{
		$("#StartingZipCode").focus();
		alert("Please Fill the Starting Zip Code");
		return false;
	}
	if(ActiveViewRadius=='')
	{
		$("#ActiveViewRadius").focus();
		alert("Please Fill the Active View Radius");
		return false;
	}
	if(ActiveViewRadius!='')
	{
		var numbers = /^[0-9,.]+$/;  
		if(ActiveViewRadius.match(numbers))  
		{  
			 
		}  
		else  
		{  
			alert('Active view radius must have numeric values only');  
			$("#ActiveViewRadius").focus();  
			return false;  
		} 	
	}
	if(MaxPageViews=='')
	{
		$("#MaxPageViews").focus();
		alert("Please Fill the Max Page Views");
		return false;
	}
	if(datepicker=='')
	{
		$("#datepicker").focus();
		alert("Please Fill the Expire Date");
		return false;
	}
	return true;
}


function validate_dispensary()
{
	var customername=$("#customername").val();
	var streetaddress=$("#streetaddress").val();
	var city=$("#city").val();
	var state=$("#state").val();
	var zipcode=$("#zipcode").val();
	var hoursofoperation=$("#hoursofoperation").val();
	var phonenumber=$("#phonenumber").val();
	var email=$("#email").val();
	var website=$("#website").val();
	var directions=$("#directions").val();
	if(customername=='')
	{
		$("#customername").focus();
		alert("Please Fill the Customer Name.");
		return false;
	}
	if(streetaddress=='')
	{
		$("#streetaddress").focus();
		alert("Please Fill the street address.");
		return false;
	}
	if(city=='')
	{
		$("#city").focus();
		alert("Please Fill the city.");
		return false;
	}
	if(state=='')
	{
		$("#state").focus();
		alert("Please Fill the state.");
		return false;
	}
	if(zipcode=='')
	{
		$("#zipcode").focus();
		alert("Please Fill the Zip Code");
		return false;
	}
	if(hoursofoperation=='')
	{
		$("#hoursofoperation").focus();
		alert("Please Fill the Hours of operation.");
		return false;
	}
	if(phonenumber=='')
	{
		$("#phonenumber").focus();
		alert("Please Fill the phone number.");
		return false;
	}
	if(email=='')
	{
		$("#email").focus();
		alert("Please Fill the email address.");
		return false;
	}
	if(website=='http://')
	{
		$("#website").focus();
		alert("Please Fill the Website Url");
		return false;
	}
	if(website=='')
	{
		$("#website").focus();
		alert("Please Fill the Website Url");
		return false;
	}
	return true;
}

$(document).ready(function () {
	$('body').on('click',".removelpageimage",function(){
  		var r=confirm("You want to remove this picture?");
		if (r==true)
		{
			var imgid=$(this).attr("id");
			var id=imgid.split("_");
			if(id[0]=='sessionstrainimageid')
			{
				$("#sessionimagewapper_"+id[1]).remove();
				$.ajax({
						type: "POST",
						url: "show_landing_page_images.php",
						data: 'temp_imageid='+id[1],
						async:   false,
						success: function(data)
						{
							if(data=='remove')
							{
								$("#sessionimagewapper_"+id[1]).remove();
							}
							else
							{
								alert("Please try again.");
							}
						}
				});
			}
			else
			{
				var imgname=id[0].split("@#@");
				if(imgname[0]=='')
				{
					return false;	
				}
				$.ajax({
						type: "POST",
						url: "show_landing_page_images.php",
						data: 'imgdata='+imgname[0]+'&imageid='+id[1],
						async:   false,
						success: function(data)
						{
							if(data=='remove')
							{
								$("#imagewapper_"+id[1]).remove();
							}
							else
							{
								alert("Please try again.");
							}
						}
				});
			}	
		}
		else
		{
			return false;
		}
	});
});
function validate_disp()
{
	alert($("#headingtext").val());
	if($("#headingtext").val()=='')
	{
		$("#headingtext").focus();
		alert("Please enter heading text");
		return false;
	}
	tinyMCE.triggerSave();
	return true;
}