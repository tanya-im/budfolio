<?php session_start();
include_once("../config.php");
include_once("../function.php");
// If admihn user submit the ad detail form  
if(isset($_POST['AddAdDetail']))
{
	$customer_name=mysql_real_escape_string($_POST['CustomerName']);
	$website_url=mysql_real_escape_string($_POST['WebsiteUrl']);
	$starting_zipcode=mysql_real_escape_string($_POST['StartingZipCode']);
	$active_view_radius=mysql_real_escape_string($_POST['ActiveViewRadius']);
	$max_page_views=mysql_real_escape_string($_POST['MaxPageViews']);
	$ad_expire_date=mysql_real_escape_string($_POST['datepicker']);
	$image_name=mysql_real_escape_string($_SESSION['adimg']);
	$ad_type=mysql_real_escape_string($_POST['type']);
	$status=mysql_real_escape_string($_POST['status']);
	$zip_setting=mysql_real_escape_string($_POST['zipsetting']);
// check zip code is valid or not	
	$latlongdata=getlatlong($starting_zipcode);
	if($latlongdata['status']!='OK')
	{
		echo 'InvalidZipCode';die;
	}
	$date = date('Y-m-d');
// Insert the ad detail 
	$query="insert into advertisement(customer_name,image_name,website_url,starting_zipcode,zipcode_setting, active_view_radius,max_page_views,created_date,ad_expire_date,ad_type,status,latitude,longitude)values('".$customer_name."','".$image_name."','".$website_url."','".$starting_zipcode."','".$zip_setting."','".$active_view_radius."','".$max_page_views."','".$date."','".$ad_expire_date."','".$ad_type."','".$status."','".$latlongdata['results'][0]['geometry']['location']['lat']."','".$latlongdata['results'][0]['geometry']['location']['lng']."')";
	$sql=mysql_query($query)or die(mysql_error());
	$_SESSION['adimg']='';
	$id=mysql_insert_id();
// If query run successfully show success message else show error message
	if($sql)
	{
		echo 'AdvertisementSave_'.$id;
	}
	else
	{
		echo 'AdvertisementNotSave';
	}
	die;
}

// If admin user submit the update ad form
if(isset($_POST['UpdateAd']) && !empty($_POST['adid']))
{	
	$customer_name=mysql_real_escape_string($_POST['CustomerName']);
	$website_url=mysql_real_escape_string($_POST['WebsiteUrl']);
	$starting_zipcode=mysql_real_escape_string($_POST['StartingZipCode']);
	$active_view_radius=mysql_real_escape_string($_POST['ActiveViewRadius']);
	$max_page_views=mysql_real_escape_string($_POST['MaxPageViews']);
	$ad_expire_date=mysql_real_escape_string($_POST['datepicker']);
	$image_name=mysql_real_escape_string($_SESSION['adimg']);
	$status=mysql_real_escape_string($_POST['status']);
	$zip_setting=mysql_real_escape_string($_POST['zipsetting']);
	$latlongdata=getlatlong($starting_zipcode);
	// check zip code is valid or not
	if($latlongdata['status']!='OK')
	{
		echo 'InvalidZipCode';die;
	}
	
	// Update the ad detail
	$query="update advertisement set customer_name='".$customer_name."',website_url='".$website_url."',starting_zipcode='".$starting_zipcode."',zipcode_setting='".$zip_setting."', active_view_radius='".$active_view_radius."',max_page_views='".$max_page_views."', ad_expire_date='".$ad_expire_date."',status='".$status."',latitude='".$latlongdata['results'][0]['geometry']['location']['lat']."',longitude='".$latlongdata['results'][0]['geometry']['location']['lng']."',image_name='".$image_name."' where advertisement_id='".$_POST['adid']."'";
	$sql=mysql_query($query)or die(mysql_error());
	$_SESSION['adimg']='';
	// If query run successfully show success message else show error message
	if($sql)
	{
		echo 'AdvertisementUpdate';
	}
	else
	{
		echo 'AdvertisementNotUpdate';
	}
	die;
}
?>