<?php session_start(); ob_start();

 date_default_timezone_set('Pacific/Honolulu');
/* echo date_default_timezonse_get();
 echo "<br>";*/


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Budfolio</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
    <script src="js/jmin.js"></script>
	<script>$(document).ready(function(){$(".fadinmsg").delay(7000).fadeOut("slow");});</script>
</head>
<body>
<div id="wrapper">
	<div class="header">
    	<div class="header_left">
        	<img src="../images/imagesforcss/Budfolio-header-opt.jpg" alt="header" width="1024px">
        </div>
        <?php 
			if(isset($_SESSION['admin_name']))
			{?>
				<div class="header_right">
        			<span class="logout"><a href="logout.php">Logout</a></span>
            	</div>
		<?php }?>	
    </div>
    <div class="main_menu">
    	<ul class="main_menu_ul">
        	<li class="first"><a href="siteStats.php">Home</a></li>
            <li class="first"><a href="home.php">Users List </a></li>
            <li><a href="landing_page_details.php">Front Page Detail</a></li>
            <li><a href="dispensary_list.php">Dispensaries</a></li>
            <li><a href="strain_library.php">Strain Template</a></li>
            <li><a href="Advertisment.php?type=FrontPageAd">Front Page Ad</a></li>
            <li><a href="Advertisment.php?type=FrontPageBottumAd">Front Page Bottum Ad</a></li>
            <li><a href="Advertisment.php?type=TopBanner">Top Banner</a></li>
            <li><a href="Advertisment.php?type=BottomBanner">Bottom Banner</a></li>
            <li><a href="inappropriateLoungePostDetail.php">Lounge Posts</a></li>
            <li><a href="purchased_subscription.php">Subscriptions</a></li>
            <li><a href="notification.php">Notifications</a></li>
            <li class="last"><a href="changePwd.php">Setting</a></li>
        </ul>
    </div>