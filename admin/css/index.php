<?php 
// Include header and config files. 
include_once("newHeader.php");
include_once("config.php");?>
<link href="css/new_style.css" rel="stylesheet" type="text/css" />

    <div id="wraper">
        <div id="new_main_con">
        	<div class="indexmsg">
    			<p id="regerrormsgonindex" ><?php include_once("indexerror.php");?></p>
			</div>
            <div class="new_home_conlft">
                <div class="home_headconlft"> 
                    <div class="home_heading"> Welcome to Budfolio</div> 
                    <span class="home_txtcon"> Start a strain journal and join the social network for the cannabis community.</span>
                    <span class="fltrit"> <a href="new_learnmore.php" class="new_greenlink"> Learn more </a> </span>
                    <div class="clear"></div>
                    <span> <a href="#"><img src="images/new_home/app_store_btn.png"></a> &nbsp;<a href="#"><img src="images/new_home/googleplay_btn.png"></a> </span>	
                    <br><br>
                </div><!--end of home_headconlft-->
                
                <div class="home_headconrit"> <img src="images/new_home/phone.png"> </div>
                <div><span class="search_heading">Find local dispensary & delivery</span>
                    <span class="wid70"> <div class="newinput-append"> <div class="new_searchbox"> <img src="images/new_web/serach_icon.png" class="scale-with-grid"> <input type="text" id="sl_search_text" name="sl_search_text" onkeypress="searchKeyPress(event);"> </div> <a href="javascript:void(0);" class="menusearchbtn_home new_search_btn pushtop9" id="search_here"> Search </a> </div></span></div>
                <div class="new_btmad img"> <img src="images/ad_images/budfoliobanner1000x174banner.jpg"> </div>
            </div>
            <!--end of new_home_conlft-->
            <div class="home_conrit"> 
            <div id="new_example-two">
               <ul class="new_nav">
                  <li class="nav-one"><a href="javascript:void(0);" id="member_login" class="current">Members</a></li><!--href="#featured2"-->
                  <li class="nav-two"><a href="javascript:void(0);" id="dispensary_login">Dispensary</a></li><!--href="#core2"-->
               </ul>
              <div id="login_reg_content">  
              
               <?php include('member_login_form.php'); ?>
             </div><!-- end of id="login_reg_content-->
             </div>	
                
            </div>
            <!-- end of right panel -->
            <div class="bottom_link">
            <div class="bottom_linkblock">
                <span class="listheading">Apps</span>	
                <ul>
                    <li><a href="https://itunes.apple.com/us/app/budfolio/id687645140?mt=8">iPhone</a></li>
                    <li><a href=" https://play.google.com/store/apps/details?id=com.budfolio.app&hl=en">Android</a></li>
                </ul>
            </div>
            
            <div class="bottom_linkblock">
                <span class="listheading">About Us</span>
                <ul>
                    <li><a href="https://budfolio.com/qa">Budfolio</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </div>
            
            <div class="bottom_linkblock">
                <span class="listheading">Social</span>
                <ul>
                    <li><a href="https://twitter.com/budfolio">Twitter</a></li>
                    <li><a href="https://www.facebook.com/pages/Budfolio/126841610710369">Facebook</a></li>
                    <li><a href="https://instagram.com/strainrater">Instagram</a></li>
                    <li><a href="#">Youtube</a></li>
                </ul>
            </div>
            
            <div class="bottom_linkblock">
                <span class="listheading">Dispensary</span>
                <ul>
                    <li><a href="javascript:void(0);">Marketing</a></li>
                    <li><a href="https://budfolio.com/qa">Register</a></li>
                    <li><a href="https://budfolio.com/qa">Sign in</a></li>
                </ul>
            </div>
            
            <div class="bottom_linkblock">
                <span class="listheading">Advertise</span>
                <ul>
                    <li><a href="https://budfolio.com/qa/outereMenu.php">Dispensaries</a></li>
                </ul>
            </div>
    
        </div>
        </div>
	</div>
    
	<div class="footer">        
        <div class="clear">&nbsp;</div>
        <div class="fllft"> © 2013 Budfolio LLC </div>
        <div class="footerlink">
            <ul>
                <li> <a href="mailto:advertising.budfolio@gmail.com" target="_new">Advertisers</a> </li>
                <li>| </li>
                <li> <a href="production/terms_condition.php">Terms</a> </li>
            </ul>
        </div>
	</div>
</div>
<?php include('budfolio_footer.php');?>
<script type="text/javascript" src="js/new_web.js"></script>
<script>
	$(function() {
		$("#new_example-two").organicTabs({
			"speed": 200
		});
	});
</script>
<script>
$(document).ready(function(){
	
	$('#member_login').click(function(){
		
		$.ajax({
				type: "POST",
				url: "member_login_form.php",
				success: function(data)
				{
					$("#login_reg_content").html(data);
				}
		});
		
	})
	
	$('#dispensary_login').click(function(){
	
		$.ajax({
				type: "POST",
				url: "dispensary_login_form.php",
				success: function(data)
				{
					$("#login_reg_content").html(data);
				}
		});
	
	})
	
	$('.menusearchbtn_home').click(function(){
		var sl_search_text = $('#sl_search_text').val();
		if(sl_search_text=='')
		{
			alert("Please enter dispensary name to search");
		}else
		{
			window.location.href="outereMenu.php?search="+sl_search_text;
		}
	})
	
	$("#loginpassword").keypress(function(event) {
    	if (event.which == 13) {
       		 event.preventDefault();
        	$(".submit_login").click();
    	}else
		{
			console.log("in else");
		}
	});
	$('#dispensary_password').live("keypress",function(event) {
		
    	if (event.which == 13) {
       		 event.preventDefault();
        	$(".disp_login").click();
    	}else
		{
			
			console.log("in else");
		}
	});


})


</script>
</body>
</html>
