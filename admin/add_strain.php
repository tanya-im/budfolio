<?php 
// Include header,auth,config and dispensary_transaction file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
include_once("../values_array.php");
?>
<style>
form
{
	width:90%;
}
select
{
	width:80%;
}
</style>


<div>
	<div style="font-weight: bold; float: right; padding-top: 20px; padding-right: 18px;"><a onclick="goOneStepBAck();" href="javascript:void(0);">BACK</a></div>
	
    <div class="strainlib_heading">Add New Strain</div> 
    	<div class="budthought_popup">
        <div class="ad_errormsg"></div> 
    	
        <div class="strainfrm" >
        	<!-- Add Ad detail form -->
            <form method="post" >
			<div class="l_pagedeialfrm" style="padding: 0 32%;">
                <div class="strainboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Strain Name</label>		<input placeholder="Strain Name " type="text" name="StrainName" id="StrainName" /> 
                </div>
                <div class="strainboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>species</label>				<input type="text" placeholder="Species"  name="species" id="species" value="<?php echo $_REQUEST['strain_id']?>" />
                </div>
                
                <div class="strainboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Lineage</label>			<input placeholder="Lineage" type="text" name="lineage" id="lineage"/> 
                </div>     
                <div class="strainboxdetail left left100">
                    <label class="tbox_tip120 left left100">Smell</label>
                    <input placeholder="" type="text" name="smell" id="smell"/> 				</div>
                <div class="strainboxdetail left left100">
                    <label class="tbox_tip120 left left100">Taste</label>
                    <input placeholder="" type="text" name="taste" id="taste"/> 
                </div>
                <div class="strainboxdetail left left100">
                    <label class="tbox_tip120 left left100">Apearance</label><br />			
                    <textarea placeholder="Enter apearance" style=" height: 156px;width: 297px;" name="apearance" id="apearance" ></textarea>
                </div>
            </div>
            <div class="l_pagedeialfrm">
            
            	<!--<div class="strainboxdetail left left100">
                    <label class="tbox_tip120 left left100">Effects :</label>
                    <select name="Effects" id="Effects"  > 
                    <option value="">Please Select</option>
                    <?php  
					for($i=0;count($experience)>$i;$i++)
						{
							echo '<option value="'.$experience[$i].'">'.$experience[$i].'</option>';
						}
					?>
                    </select>
                </div>-->
                
               <!-- <div class="strainboxdetail left left100">
                    <label class="tbox_tip120 left left100">Medicinal Use : </label>
                    <select name="MedicinalUse" id="MedicinalUse" >
                    <option value="">Please Select</option>  
                    <?php  
					for($i=0;count($medicinaluse)>$i;$i++)
						{
							echo '<option value="'.$medicinaluse[$i].'">'.$medicinaluse[$i].'</option>';
						}
					?>
                    </select>
                </div>-->
                
            </div>  
            <div style="clear:both; text-align: center;"><input type="submit" name="Save_strain" value="Save Strain" id="Save_strain"></div>          
        </form>
        <!-- end-->
		</div>
	</div>	
</div>

<?php include_once("footer.php"); ?>