<?php 


// stats of site
// Include header,auth,ad_transaction and config file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
$userlistQuery="select * from users order by user_id desc";
$userlistSql=mysql_query($userlistQuery) or die(mysql_error());
$count=mysql_num_rows($userlistSql);
if($count > 0)
{
   $paginationCount=getPagination($count);
}
//Total visit
$totalvisitsQuery="select count(*) as totalvisits from visits_detail";
$totalvisitsSql=mysql_query($totalvisitsQuery)or die(mysql_error());
$totalvisitsRes=mysql_fetch_array($totalvisitsSql);

//Todays visit
$dailyvisitsQuery="select count(*) as dailyvisits from visits_detail where date='".date("Y-m-d")."'";
$dailyvisitsSql=mysql_query($dailyvisitsQuery)or die(mysql_error());
$dailyvisitsRes=mysql_fetch_array($dailyvisitsSql);

// Get Last 7 days visits
$date = date("Y-m-d");// current date
$last7thdaydate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . " -7 day"));
$lastonethdaydate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . " -1 day"));
// Get Last 7 days visits
$lastweekvisitsQuery="select count(*) as dailyvisits from visits_detail where date between '".$last7thdaydate."' and '".$lastonethdaydate."'";
$lastweekvisitsSql=mysql_query($lastweekvisitsQuery)or die(mysql_error());
$lastweekvisitsRes=mysql_fetch_array($lastweekvisitsSql);

// Get Last 30 days visits
$last30thdaydate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . " -30 day"));
$last30daysQuery="select count(*) as dailyvisits from visits_detail where date between '".$last30thdaydate."' and '".$lastonethdaydate."'";
$last30daysSql=mysql_query($last30daysQuery)or die(mysql_error());
$last30daysRes=mysql_fetch_array($last30daysSql);

//Unique visitors total
$totaluniquevisitsQuery="select DISTINCT ip_address from visits_detail";
$totaluniquevisitsSql=mysql_query($totaluniquevisitsQuery)or die(mysql_error());
$totaluniquevisitsRes=mysql_num_rows($totaluniquevisitsSql);

//Unique visitors last 7 days
$last7daysuniquevisitsQuery="select DISTINCT ip_address  from visits_detail where date between '".$last7thdaydate."' and '".$lastonethdaydate."'";
$last7daysuniquevisitsSql=mysql_query($last7daysuniquevisitsQuery)or die(mysql_error());
$last7daysuniquevisitsRes=mysql_num_rows($last7daysuniquevisitsSql);

//Unique visitors last 30 days
$last30daysuniquevisitsQuery="select DISTINCT ip_address  from visits_detail  where date between '".$last30thdaydate."' and '".$lastonethdaydate."'";
$last30daysuniquevisitsSql=mysql_query($last30daysuniquevisitsQuery)or die(mysql_error());
$last30daysuniquevisitsRes=mysql_num_rows($last30daysuniquevisitsSql);

//Unique visitors last 60 days
$last60thdaydate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . " -60 day"));
$last60daysuniquevisitsQuery="select  DISTINCT ip_address from visits_detail  where date between '".$last60thdaydate."' and '".$lastonethdaydate."'";
$last60daysuniquevisitsSql=mysql_query($last60daysuniquevisitsQuery)or die(mysql_error());
$last60daysuniquevisitsRes=mysql_num_rows($last60daysuniquevisitsSql);
?>
<style>
#site_stat th
{
	width:20%;
}
#site_stat td
{
	width:20%;
	text-align:center;
}
</style>
<div class="mid_wrap">
	<h1 class="center left100 fadheading">Budfolio Stats</h1>
    <div class="adlisterror"></div>
	
    <div class="home_right_part">
    	<div class="left100">
			
            <div class="visits_list left100">
        		<div style="margin-top:50px;"><h3>Site Stats</h3>
        	<table id ="site_stat" style="width:100%;">
            	<tr style="width:100%;">
            		<th>Total Users</th>
                	<th>Site Visits Total</th>
                	<th>Site Visits Today</th>
                	<th>Site Visits Last 7 Days</th>
                	<th>Site Visits Last 30 days</th>
               </tr>
               <tr>
               		<td><?php echo $count; ?></td>
                    <td><?php echo $totalvisitsRes[0]; ?></td>
                    <td><?php echo $dailyvisitsRes[0]; ?></td>
                    <td><?php echo $lastweekvisitsRes[0]; ?></td>
                    <td><?php echo $last30daysRes[0]; ?></td>
               </tr>     
           </table>
            </div>
           		<div style="margin-top:50px;"><h3>Unique Visitors</h3>
        		<table id ="unique_stat" style="width:100%;">
				<tr>
            		<th>Total</th>
                	<th>Unique Visits Last 7 Days</th>
                	<th>Unique Visits Last 30 days</th>
                	<th>Unique Visits Last 60 days</th>
				</tr>
                <tr>
                	<td><?php echo $totaluniquevisitsRes; ?></td>
                    <td><?php echo $last7daysuniquevisitsRes; ?></td>
                    <td><?php echo $last30daysuniquevisitsRes; ?></td>
                    <td><?php echo $last60daysuniquevisitsRes; ?></td>
                 </tr>   
               </table>     	
            
        	</div>
        	</div>
    	</div>
</div>
</div>
<!-- include ajax file to strain title paging-->
<script type="text/javascript" src="js/user_list_paging.js"></script>
<!--End-->
<?php include_once("footer.php"); ?>
