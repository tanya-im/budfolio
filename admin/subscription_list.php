<?php 
// Include header,auth,ad_transaction and config file.
include_once("header.php");
include_once("auth.php");
include_once("../config.php");
include_once("../function.php");
?>
<!-- datePicker required styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="css/datePicker.css">
       <!-- <link rel="stylesheet" type="text/css" media="screen" href=" css/demo.css">-->
		
<?php
//get the list of subscription of user
$strUserSubscriptionListSQL="SELECT `id`, 
 									 a.`purchase_id` as sub_id,
									 `dispensary_id`,
									 `start_date`,
									 `expiry_date`,
									 `occurence`,
									  a.`status`,
									  plan_type,
									  amount,
									  a.`comment_by_admin`,
									  b.recurrning as isRecurring 
									  FROM `purchased_menu_table` as a, tbl_menu_purchase as b 
									  WHERE a.`purchase_id`=b.`purchase_id` 
									  AND a.user_id='".$_GET['id']."'
									  ORDER BY start_date desc";
	
  $strUserSubscriptionListEXE = mysql_query($strUserSubscriptionListSQL) or die ($strUserSubscriptionListSQL." : ".mysql_error());
?>
<style>
th
{
	color:#069;
}
.modbtn{
	padding: 2px 5px;
	text-decoration: none;
	cursor: pointer;
	width: auto;
	height: 21px;
	border-radius: 4px;
	margin: 0px 13px 13px 13px;
	padding: 6px 5px 5px 5px;
	font-family: 'Gotham-Book';
	color: #fff;
	font-size: 16px;
	font-weight: normal;
	text-align: center;
	background-image: linear-gradient(to bottom, #8bac3a, #abca44);
}

</style>
<div class="mid_wrap">
<div style="font-weight: bold; float: right; padding-top: 20px; padding-right: 18px;"><a onclick="goOneStepBAck();" href="javascript:void(0);">BACK</a></div>
	<h1 class="center left100 fadheading"> Subscription List Of <?php echo getUserName($_GET['id']); ?></h1>
    <div class="adlisterror"></div>
	
    <div style="width:98%;margin-left:12px;border:1px solid #999999;text-align:right;" id="getNewSubscription">
    <a href="javascript:void(0);" onclick="" > Give new Subscription </a>
    </div>
    
    <div style="width:98%;height:50px;border:1px solid #999999;margin-left:12px;display:none;clear:both;" id="new_subscription">
    <form id="new_sub" name="new_sub" method="post" style="width:100%;">
    	 <div style="width:33%;float:left;margin-left:10%;padding-top:10px;"><label class="tbox_tip">Start Date</label>
         <input type="text" value="" name="new_start_date" id="new_start_date" class="date-pick"/>
         </div>
        
         <div style="width:33%;float:left;padding-top:10px;">
         <label class="tbox_tip">End Date</label>
         <input type="text" value="" name="new_end_date" id="new_end_date" class="date-pick" />
         </div>
        
        <div style="width:20%;float:right;padding-top:10px;">
         <input type="hidden" id="u_id" name="u_id" value="<?php echo $_GET['id']; ?>" />
         </div>
         <input type="button" value="Submit" id="give_sub" />         
    </form>
    
    </div>
    
    
    
    <div class="home_right_part" style="width:98%;">
    	<div class="left100">
        	<form  id="dataToSend" name="dataToSend">
           		<input id="order" type="hidden" value="asc" name="order">
           		<input id="orderby" type="hidden" value="a.id" name="orderby">
           		<input type="hidden" id="page" value="" name="page">
                   <!--<input type="text" name="search_user" id="search_user" placeholder="Search User" onkeypress="searchKeyPress(event);"/>
		   		 <input type="button" name="search_user_but" id="search_user_but" value="Search" onclick="search_user_here();" />-->
        	    <input type="hidden" name="search" id="submit" value="search" />                              															   		   </form>
     <?php if(mysql_num_rows($strUserSubscriptionListEXE)>0)
	 {?>
       <table id="userlisting" border="1" cellpadding="5" cellspacing="0">
       		<thead>
            	<tr>
					<th style="width:10%;text-align:center;">S No.</th>
					<th data-sort="date" style="width:16%;text-align:center;">Subscriptions</th>
					<th style="width:8%;text-align:center;">Amount</th>
    				<th style="width:8%;text-align:center;">Start Date </th>
					<th data-sort="int" style="width:8%;text-align:center;">Expiry Date</th>
                    <th data-sort="int" style="width:15%;text-align:center;">Dispensary</th>
                    <th data-sort="int" style="width:8%;text-align:center;">Status</th>
                    <th data-sort="int" style="width:8%;text-align:center;">Update</th>
                    <th data-sort="int" style="width:8%;text-align:center;">Recurring Payment</th>
       			</tr>
             </thead>
			 <tbody>
       <?php 
	   		$count=1;
	   		while($sub= mysql_fetch_assoc($strUserSubscriptionListEXE))
			{
				$isRecurringBYDb = $sub['isRecurring'];
				?>
				<tr id="subId_<?php echo $sub['sub_id'];?>" >
			<?php
				if($sub['plan_type']=='d')
				{
					$plantype=" 14 Day Trail";
				}else if($sub['plan_type']=='m')
				{
					$plantype="1 Month";
				}else if($sub['plan_type']=='h')
				{
					$plantype="6 Month";
				}
				else if($sub['plan_type']=='a')
				{
					$plantype="Annual";
				}else
				{
					$plantype='By admin';
				}
				
				
				if(!empty($sub['start_date']))
				{
					$strtd=explode(" ",$sub['start_date']);
					$sd=explode("-",$strtd[0]);
					$start_date=date ("M j, Y", mktime (0,0,0,$sd[1],$sd[2],$sd[0]));
				}
				if(!empty($sub['expiry_date']))
				{
					$expd=explode(" ",$sub['expiry_date']);
					$ed=explode("-",$expd[0]);
					$expiry_date=date ("M j, Y", mktime (0,0,0,$ed[1],$ed[2],$ed[0]));
				}
				?>
		
				<td class="center"><?php echo $count++;?></td>
				<td class="center"><?php echo $plantype;?></td>
				<td class="center"><?php echo ($sub['amount']!='0'?$sub['amount']:'-');?></td>
				<td class="center"><?php echo $start_date;?></td>
				<td class="center"><?php echo $expiry_date;?> </td>
                <td class="center"><?php echo stripslashes(getDispensaryName($sub['dispensary_id']));?> </td>
                <?php
					// check if subscription is running or not
					 
					 
				?>
                <td class="center"><?php /*echo $sub['status'];*/ $expdate=strtotime($sub['expiry_date']);$curdate=strtotime(date('Y-m-d h:i:s'));if($expdate<$curdate){echo "inactive";}else{echo "active";}
				?> 
                </td>
                <?php $dispensaryname=getDispensaryName($sub['dispensary_id']); ?>
				<td><span class="center modbtn" id="modify_<?php echo $sub['sub_id'];?>" onclick='editSubscriptionListDetail(this.id,"<?php echo $plantype;?>","<?php echo ($sub['amount']!='0'?$sub['amount']:'-');?>","<?php echo $start_date;?>","<?php echo $expiry_date;?>","<?php echo $isRecurringBYDb;?>","<?php echo $sub['start_date'];?>","<?php echo $sub['expiry_date'];?>","<?php echo $_GET['id']; ?>","<?php echo $sub['dispensary_id']; ?>","<?php echo $sub['status']; ?>","<?php echo $sub['comment_by_admin']; ?>" )'>Modify</span></td>                
                
                <td class="center"><input type="checkbox" id="plan_<?php echo $sub['sub_id'];?>" class="recurring_payment" <?php if($isRecurringBYDb=='true'){?> checked="checked" <?php }else if($plantype=='By admin' ||($sub['plan_type']=='d')){ ?> disabled="disabled" <?php }?>/></td>
				</tr>
                		
		<?php
			}
	   ?>	
       		</tbody>
       </table>
<?php }else
	  {?>
		  <div style="width:100%;font-size:16px;height:70px; text-align: center;padding-top:4%;">No user Found !!!</div>
	 <?php }
?>    
        </div>
    </div>
</div>
<!-- include ajax file to strain title paging-->

<script type="text/javascript" src="js/new/jquery.jqpagination.js"></script>
<script type="text/javascript" src="js/dispensary_list_paging.js"></script>
<script type="text/javascript" src="js/new/user_list_script.js"></script>
<script type="text/javascript" src="js/date.js"></script>
<script type="text/javascript" src="js/jquery.datePicker.js"></script>

<script>
$(document).ready(function(){
	
	
	$('#getNewSubscription').click(function()
	{
  		$('#new_subscription').slideToggle('slow', function() {
    	// Animation complete.
  		});
    });
	
	 <!-- page specific scripts -->
	
			Date.firstDayOfWeek = 0;
			Date.format = 'yyyy-mm-dd';
            $(function()
            {
				$('.date-pick').datePicker()
            });

	
	
	$('#give_sub').click(function(){
		console.log('call8ing');
		var start_date  = $('#new_start_date').val();
		var end_date= $('#new_end_date').val();
		var user_id = $('#u_id').val();
		if(start_date=='')
		{
			alert('Please enter start date of subscription.');
			return false;
		}
		if(end_date=='')
		{
			alert('Please enter end date of subscription.')
			return false;
			
		}
		/*console.log(new Date(start_date).getTime());
		console.log(new Date(start_date).getTime());
		if(start_date > end_date)
		{
			alert('Start date must be smaller then the end date.');
			return false;
		}*/
		$.ajax({
			  type: "POST",
			  url: "give_new_subscription.php",
			  data: "data=data&start="+start_date+'&end='+end_date+'&u_id='+user_id,
			  cache: false,
			  success: function(result)
					{
						if(result==0)
						{
							alert('Start date must be smaller then the end date.');
							return false;
						}
						else if(result==1)
						{
							 alert('Subscription is successfully given to user.');
							 window.location="subscription_list.php?id=<?php echo $_GET['id']; ?>";
						}
					}
	});
	});
	
	//getdata(1);
});

$(document).ready(function(e) {
    $('.recurring_payment').bind('click',function(){
		
		var plan=$(this).attr("id");
		var id =plan.split("_"); 
		var planId=id[1];
			if($(this).is(":checked"))
			{
				console.log('CHECKED the reactivate recurring profile');
				$.ajax({
				type: "POST",
				url: "../Paypal-PayFlow1/reactivate_recurring_profile.php",
				data: 'reactivate_profile=reactivate_profile&payment_id='+planId,
				success: function(data1)
				{
					response =data1.split('_');
					
					if(response[0]=='Success')
					{
						$.ajax({
						type: "POST",
						url: "../reactivate_success.php",
						data: 'payment_id='+planId+'&tran_detail='+response[1],
						success: function(data)
						{	
							if(data=='1')
							{
								alert('Your recurruring payment is reactivated now.')
							}
							else
							{
								alert('Please Try again to reactivate .')
							}
						}
						});
					}else
					{
						alert('Please try again to reactivate your recurrning profile.');
					}
				}
				});
				//return false;
			}
			else
			{
				$.ajax({
				type: "POST",
				url: "../Paypal-PayFlow1/cancelation_recurring_profile.php",
				data: 'delete_profile=delete_profile&payment_id='+planId,
				success: function(data1)
				{
					response =data1.split('_');
					if(response[0]=='Success')
					{
						$.ajax({
						type: "POST",
						url: "../cancle_success.php",
						data: 'payment_id='+planId+'&tran_detail='+response[1],
						success: function(data2)
						{	
						
							if(data2=='1')
							{
								alert('Your recurruring payment is deactivated now.')
							}
							else
							{
								alert('Please Try again .')
							}
						}
						});
					}else
					{
						alert('Please try again to deactivate your recurrning profile.');
					}
					
					
				}
				});
			}
		
	});
});

/*---------------------function edit subscription list detail----------------------------------*/
function editSubscriptionListDetail(id,plantype,amount,start_date,expiry_date,isRecurringBYDb,stdate,expdate,userID,dipensaryID,status,comment_by_admin){
	
		var validid=id.split("_");
		if(validid[1]=='')
		{
				return false;
		}
		
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
        // Load ad form  
		var id='#dialog';
		$.ajax({
					type: "POST",
					url: "edit_subscription_list.php",
					data: 'EditSubscription=EditSubscription&modifyID='+validid[1]+'&plantype='+plantype+'&amount='+amount+'&start_date='+start_date+'&expiry_date='+expiry_date+'&isRecurringBYDb='+isRecurringBYDb+'&stdate='+stdate+'&expdate='+expdate+'&userID='+userID+'&dipensaryID='+dipensaryID+'&status='+status+'&comment_by_admin='+comment_by_admin,
					async:   false,
					success: function(data)
					{
						
						$("#boxes").html(data);
						//Set the popup window to center
						$(id).css('top',  winH/2-$(id).height()/2);
						$(id).css('left', winW/2-$(id).width()/2);
						//transition effect
						$(id).fadeIn(2000); 
					}
		});
}
/*------------------------------------------------------------------------------------*/
</script>
<div id="boxes"></div>
<div id="mask"></div> 

<?php include_once("footer.php"); ?>