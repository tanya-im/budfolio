<?php
include_once('../config.php'); // MySQL database connection file.
$DB_TBLName = "users";    //MySQL Table Name
$filename = "UsersList"; //File Name

$sql = "SELECT users.user_id as SNo, users.user_name, DATE_FORMAT(users.registration_date, '%M %d %Y') as registration_date, users.email_address, (CASE users.user_type WHEN 1 THEN 'Member User' WHEN 2 THEN 'Dispensary User' ELSE 'more' END) as User_type, DATE_FORMAT(users.last_login_date, '%M %d %Y') as last_login_date, (

SELECT count( strain_id )
FROM strains
WHERE user_id = users.user_id AND flag='active'
) AS total_strains, (

SELECT count( bud_thought_id )
FROM tbl_bud_thought
WHERE user_id = users.user_id AND delete_status='0'
) AS total_bud_thought
FROM `users`
GROUP BY users.user_id
ORDER BY users.user_name";
$result = @mysql_query($sql)
    or die("Couldn't execute query:<br>" . mysql_error(). "<br>" . mysql_errno());
$file_ending = "xls";

//header info for browser
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=$filename.xls");
header("Pragma: no-cache");
header("Expires: 0");
/*******Start of Formatting for Excel*******/
//define separator (defines columns in excel & tabs in word)
$sep = "\t"; //tabbed character
//start of printing column names as names of MySQL fields
for ($i = 0; $i < mysql_num_fields($result); $i++) {
echo mysql_field_name($result,$i) . "\t";
}

print("\n");
//end of printing column names
//start while loop to get data
    while($row = mysql_fetch_row($result))
    {
        $schema_insert = "";
        for($j=0; $j<mysql_num_fields($result);$j++)
        {
            if(!isset($row[$j]))
                $schema_insert .= "NULL".$sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]".$sep;
            else
                $schema_insert .= "".$sep;
        }
        $schema_insert = str_replace($sep."$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }
?>