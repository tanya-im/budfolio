<?php if(isset($_POST['AdForm'])){?>
<div id="dialog" class="window">
	<a href="javascript:void(0);" class="close"><img src="../images/css_images/close_btn.png" width="37" height="37" alt="close" > </a>
	<div class="budthought_popup">
    	<div class="ad_errormsg"></div> 
    	<div class="imgleftpart"><?php include_once('Ad_imageForm.php'); ?></div>
        <div class="adfrm">
        	<!-- Add Ad detail form -->
            <form name="adform" id="adform" method="post" onsubmit="return validate();">
			<div class="l_pagedeialfrm">
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Customer Name</label>				<input placeholder="" type="text" name="CustomerName" id="CustomerName"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Website Url</label>				<input type="text" placeholder="http://example.com"  name="WebsiteUrl" id="WebsiteUrl" />
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Starting Zip Code</label>			<input placeholder="" type="text" name="StartingZipCode" id="StartingZipCode"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100">Zip Code Setting</label>				
                    <textarea placeholder="Enter zip codes by comma  separated.      Ex: 94130,94501,94502" name="zipsetting" id="zipsetting"></textarea>
                </div>
                
            </div>
            <div class="l_pagedeialfrm">     
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Active View Radius</label>
                    <input placeholder="" type="text" name="ActiveViewRadius" id="ActiveViewRadius"/><span class="miles">miles</span> 				</div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Max Page Views</label>
                    <input placeholder="" type="text" name="MaxPageViews" id="MaxPageViews"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Expire Date</label>
                    <input placeholder="" type="text" name="AdExpireDate" id="datepicker"/> 
                </div>
                <div class="tbboxdetail left left100">
                    <label class="tbox_tip120 left left100"><span class="mdred">*</span>Status</label>
                    <input type="radio" checked="checked" name="Status" value="active" id="adstatusact"/>
                    <span>Active</span>
                    <input type="radio" name="Status" value="deactive" id="adstatusdeact"/>
                    <span>Deactive</span>
                </div>
                <div class="tbboxdetail left left100">
                    <input type="button" value="Save Detail !" name="save_detail" id="save_detail">
                	<input type="hidden" id="adtype" value="<?php echo $_POST['type'] ?>" />
                	<span class="loaderbar"><img src="../images/css_images/loading-bars.gif" /></span>
                </div>
            </div>            
        </form>
        <!-- end-->
		</div>
	</div>	
</div>
<script>
$(function() {
   	$( "#datepicker" ).datepicker({dateFormat: "yy-mm-dd" });
});</script>
<?php } ?>     