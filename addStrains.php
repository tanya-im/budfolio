<?php
// Include header,auth,transection and constant array file.  
include_once("newHeader.php");
require_once("auth.php");
include_once("strain_transection.php");
include_once("values_array.php");
$_SESSION['images']=array();
$_SESSION['primaeyimages']='';

require_once 'includes/mobileDetect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
include_once('includes/config_strain.php');

?>
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel= "stylesheet" type="text/css" href="css/jquery.editable-select.css" />
<link rel="stylesheet" type="text/css" href="css/crop_css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/crop_css/imgareaselect-default.css" />
<link rel="stylesheet" type="text/css" href="css/crop_css/styles.css" />

<script type="text/javascript" src="js/customInput.jquery.js"></script>

<script type="text/javascript">
			var useMobile=<?php if($deviceType=='tablet'||$deviceType=='phone') { echo 'true';} else {echo 'false';}?>;
</script>
<style>
.ui-multiselect ui-widget ui-state-default ui-corner-all{width:50%;}
.pultop107{margin:107px 0px 0px 15px; float:left;}
#preview img {left: 0;top: 0;}
#preview {clear: both;display: block;float: left;overflow: hidden;position: relative;}
</style>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    <?php 
				// Status or error messages.
				if(isset($_GET['strainstatus']))
				{
					if($_GET['strainstatus']=='added')
					{
						echo '<span class="error_msg_all fadinmsg green"> Strain added successfully.</span>';
					}
					if($_GET['strainstatus']=='notadded')
					{
						echo '<span class="error_msg_all fadinmsg red">Strain not added. Please try again.</span>';
					}
				}
				// End messages
			?>
    	<div class="inner_conlft">
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2>Add Strain </h2>  </div>
                <div id="content_1" class="content">
                	<div class="edit_dis_con">
                    <span id="strainameval" class="error_msg_all red"></span>
                        <div class="addstraincon martop_btm2">
                    

                        	<div class="addstrain_photocon">
                            	<div id="previewwrapper" class="comman"><div id="preview" class="add_pic" ><img src="images/css_images/ad_img.png" style="position:relative;" /></div></div>
	
                            	<div class="straim_images"><?php include("show_strain_images.php"); ?></div>
                   		<a href="javascript:void(0);"  onclick="show_wrapper();" class="edit_btn " id="addstrainimagebox"  > Add Photo  </a>	
                           </div>
                             
                      <div id="showHideDivForCropImage" class="hidden" style="visibility:hidden;height:0px;"><?php include_once('strain_jcrop.php'); ?></div>   
                      <div class="dividerline" style="margin:2%;"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>  
					    <form method="post" name="strainopration" id="strainopration" onsubmit="return add_strain_validation();">  
							<!-- share buttones-->
                           <div id="sharebtns"  style="margin:10px;">
                    	<?php include_once("fb.html"); ?>
                        <?php include_once("twitter.php"); ?>
                    </div>  
                           <!-- share ennd-->
                    
                          
                            <dl>
                                <dt> Strain Name <span class="adddisinput"> 
                                  <select name="strainname" id="strainname" class="editable-select">
                                  <?php
								  $strStrainLibSQl = "SELECT * FROM strain_library";
								  $res = mysql_query($strStrainLibSQl);
                                  while($rowSl= mysql_fetch_assoc($res))
								  {
								  	 echo '<option value="'.str_replace("\\", "",stripslashes($rowSl['strain_name'])).'">'.str_replace("\\", "",stripslashes($rowSl['strain_name'])).'</option>';
								  }
								?>	
                                </select>
                                </span> </dt>
                                <dd> Dispensary <span class="adddisinput">
                                <select name="dispensensary" id="dispensensary" class="editable-select">
                                <?php 
								$strGetDispSQL = "SELECT dispensary_id,dispensary_name FROM `dispensaries`";
								$responseDis = mysql_query($strGetDispSQL);
								while($dis=mysql_fetch_assoc($responseDis))
								{ 
									echo '<option value="'.str_replace("\\", "",stripslashes($dis['dispensary_name'])).'">'.str_replace("\\", "",stripslashes($dis['dispensary_name'])).'</option>';
								}
								?>
                                </select>
                                </span> </dd>
                            </dl>
                             
                            <dl>
                                <dt> Species  <span class="adddisinput"> <select name="species" id="species">
                            	<option value="">Please select species</option> 
                                <?php	for($i=0;count($species)>$i;$i++)
										{
											echo '<option value="'.$species[$i].'">'.$species[$i].'</option>';
										}
								?>	
							</select>  </span> </dt>
                                <dd> Lineage  <span class="adddisinput"> <input type="text" placeholder="Enter Lineage"  name="linage" id="linage"  maxlength="55"> </span> </dd>
                            </dl>
                             
                            <dl>
                                <dt> Consumption Method  <span class="adddisinput"> <select name="consumption" id="consumption">
                                	<option value="">Please select consumption</option> 
                                	<?php for($i=0;count($consumption)>$i;$i++)
										{
									echo '<option value="'.$consumption[$i].'">'.$consumption[$i].'</option>';
										}
									?>
                                </select>  </span> </dt>
                                <dd>&nbsp;  </dd>
                             </dl>
                        </div>
                        
                      <div class="addstraincon martop_btm2">
                <div class="headtext marbtm10"> Notes</div>
                <div class="add-con"> 
                    <textarea placeholder="Enter Description" rows="3" cols="57" maxlength='1000' name="description" id="description"></textarea>
                        <div class="dbox_msg">Max 1000 characters.</div>  
                </div>
            </div>    
                        
                        
    
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        <div class="addstraincon martop_btm2">
                        	<div class="headtext"> Tell us about your Bud </div>
                             <dl>
                             	<dt> Smell<br /> <span class="select_wrap"> <select name="smell[]" id="smell" multiple="multiple" class="select_field">
                            	 <?php for($i=0;count($smell)>$i;$i++){
                                    echo '<option value="'.$smell[$i].'">'.$smell[$i].'</option>';
                                }?>
                            </select>  </span> </dt>
                                    
                                <dd> Taste <br /> 
                                <span class="select_wrap"> 
                                	<select name="taste[]" id="taste" multiple="multiple" class="select_field">
                            	<?php for($i=0;count($taste)>$i;$i++){
                                    echo '<option value="'.$taste[$i].'">'.$taste[$i].'</option>';
                                }?>
                            </select> </span> </dt>
                             </dl>
                             
                             <dl>
                                <dt> Strength  <span class="adddisinput"> <select name="strength" id="strength"> 
                        	<option value="">Please select strength</option> 
                            <?php	for($i=0;count($strength)>$i;$i++){
                                echo '<option value="'.$strength[$i].'">'.$strength[$i].'</option>';
                            }?>
                        </select>  </span> </dt>
                                    
                                <dd> Rating  <br> <span class="martop_btm2 floatlft"><div class="rating_wrapp">
                                <?php for($r=1;$r<6;$r++)
                                        { 
                                            if($straindetail['overall_rating']>=$r)
                                            {
                                                echo '<a href="javascript:void(0);" class="ratthis rated" id="ratting_'.$r.'">&nbsp;</a>';
                                            }
                                            else
                                            {
                                                echo '<a href="javascript:void(0);" class="ratthis" id="ratting_'.$r.'">&nbsp;</a>';
                                            }	
                                        }?>
                            </div>
                             <input type="hidden" name="overall" id="overall" value="<?php echo $straindetail['overall_rating'];?>"> </span> </dd>
                             </dl>
                        </div>
                        
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        <div class="addstraincon martop_btm2">
                        <div class="headtext marbtm10"> Experiences </div>
                          <div  class="select_wrap" >
                        	<select name="experience[]" id="experience"  multiple="multiple" class="select_field">
                            	<?php	for($i=0;count($experience)>$i;$i++)
										{
											echo '<option value="'.$experience[$i].'">'.$experience[$i].'</option>';
										}
								?>	
							</select>  <!--</span>-->
                          </div>
                        </div>
                        
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        <div class="addstraincon martop_btm2">
                        	<div class="headtext marbtm10"> Medicinal Uses </div>
                            	<div class="select_wrap">
                                  <select name="medicinaluse[]" id="medicinaluse" class="select_field" multiple="multiple">							
								  <?php	for($i=0;count($medicinaluse)>$i;$i++)
										{
											echo '<option value="'.$medicinaluse[$i].'">'.$medicinaluse[$i].'</option>';
										}
								?>	
									</select>  
                            </div>
                        </div>
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        
               <div class="addstraincon martop_btm2">
               <div class="headtext marbtm10"> Lab Tested </div>
                    <div class="add-con" id="testedbyinput"> 
                    <dl>
                        <dt> Tested By <br>
                        	<span class="adddisinput"> 
                        	<input type="text" placeholder="Enter Tested By" name="testedby" id="testedby" maxlength="55"/></span> 
                        </dt>
                        <dd> THC <br> 
                        	<span class="adddisinput">
                            <input placeholder="Enter THC" type="text" name="thc" id="thc" maxlength="5"/><span style="float:right;"> %</span></span>
                        </dd>
                    </dl>
                    <dl>
                        <dt> CBD <br> 
                        	<span class="adddisinput"> 
                        	<input placeholder="Enter CBD" type="text" name="cbd" id="cbd" maxlength="5"/><span style="float:right;"> %</span></span> 
                        </dt>
                        <dd> CBN <br> 
                        	<span class="adddisinput">
                            	<input placeholder="Enter CBN" type="text" name="cbn" id="cbn" maxlength="5"/>
                                <span style="float:right;"> %</span></span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> THCa <br> 
                        <span class="adddisinput"> 
                        	<input placeholder="Enter THCa" type="text" name="tcha" id="tcha" maxlength="5"/>
                        <span style="float:right;"> %</span></span></dt>
                        <dd> CBHa <br> 
                        	<span class="adddisinput">
                            	<input placeholder="Enter CBHa" type="text" name="cbha" id="cbha" maxlength="5"/>
                               <span style="float:right;"> %</span></span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> Moisture <br> <span class="adddisinput"> <input placeholder="Enter Moisture" type="text" name="moisture" id="moisture" maxlength="5"/><span style="float:right;"> %</span></span></dt>
                    </dl>
                </div>
            </div>
                  
                  <div style="clear:both;"></div>
                        <div><span class="mar_rit2"> <a href="javascript:void(0);" id="addstrainsubmit" class="edit_btn addstrainsubmit"> Add Strain </a> </span></div>
                          <input type="hidden" name="add_strain" id="add_strain" value="add strain" />
                      </form>  
                    </div>
                </div>
            </div>
        </div>
        
        <?php include('newLounge_right.php');?>
    </div>
</div>
<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>

<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
<script type="text/javascript" src="js/jquery.editable-select.pack.js"></script>
<script type="text/javascript" src="js/crop_js/jquery.imgareaselect.min.js"></script>
		<script type="text/javascript" src="js/crop_js/strain_effcts.js"></script>
		<?php if($deviceType=='tablet'||$deviceType=='phone') { ?>
		<script type="text/javascript" src="js/crop_js/touch.js"></script>
		<?php } ?>
<script>

$(document).ready(function() {
	
	 $('.editable-select').editableSelect(
      {
        bg_iframe: true,
        onSelect: function(list_item) {
        /*alert('List item text: '+ list_item.text() +'<br> \
          Input value: '+ this.text.val());*/        }
      }
    );
  

	  

	$("#searchtext").live('keyup',function(){
		$("#searcherror").html("&nbsp;");
	});

});
function show_wrapper()
{
	if($('#showHideDivForCropImage').hasClass("hidden"))
	{
		$('#showHideDivForCropImage').css({'visibility':'visible','height':'auto'});
		$('#showHideDivForCropImage').removeClass('hidden');
		$('#showHideDivForCropImage').addClass('show');
	}
	else if ($('#showHideDivForCropImage').hasClass("show"))
	{
		$('#showHideDivForCropImage').css({'visibility':'hidden','height':'0px'});
		$('#showHideDivForCropImage').removeClass('show');
		$('#showHideDivForCropImage').addClass('hidden');
	}
	
}	

$(function() {
	$('input').customInput();
	$("#smell").multiselect();
	$("#taste").multiselect();
	$("#experience").multiselect(); 
	$("#medicinaluse").multiselect(); 
  });

</script>
<div id="boxes"></div>
<div id="mask"></div> 
</body>

</html>
