<?php 
// Define all constant in array 
$species=array("Indica",
				"Hybrid",
				"Sativa",
				"Hybrid - Indica Dominate",
				"Hybrid - Sativa Dominate",
				"Hybrid - Sativa");

/*
,"Sativa dom","Indica 70 / Sativa 30","Indica 60 / Sativa 40","Sativa 60 / Indica 40","Sativa 95 / Indica 5","Delete 	Indica 50 / Sativa 50","Sativa / Indica","Indica 80 / Sativa 20","Indica 90 / Sativa 10"

$smell=array("Citrus","Earthy","Fruity","Floral","Pine","Skunky","Spicy","Woodsy","Peppery","Berries","Kush","Sweet");
*/
$smell= array("Blueberry",
			   "Berries",
			   "Citrus",
			   "Earthy",
			   "Floral",
			   "Fruity",
			   "Hash",
			   "Lemon",
			   "Piney",
			   "Raspberry",
			   "Skunky",
			   "Spicy",
			   "Sweet",
			   "Woodsy",
			   "Peppery",
			   "Peppery"
			   );

$taste = array(
				"Blueberry",
				"Berries",
				"Chemicals",
				"Citrus",
				"Floral",
				"Fruity",
				"Kushy",
				"Lemon",
				"Mango",
				"Harsh",
				"Hash",
				"Peppery",
				"Raspberry",
				"Sweet",
				"Wood"
				);

$strength = array(
				    "very weak",
					"weak",
					"strong",
					"very strong"
			);

$overall = array(
					"very bad",
					"bad",
					"good",
					"very good");


$experience = array(

"Anxious",
"Aroused",
"Calming",
"Creative",
"Cotton Mouth",
"Dizzy",
"Dry Eyes",
"Energetic",
"Euphoric",
"Focused",
"Giggly",
"Happy",
"Headache",
"Hungry",
"Lazy / Lethargic",
"Paranoid",
"Relaxed",
"Sleepy",
"Social",
"Tingly",
"Uplifted"


					  );

$consumption = array(
						"Pipe",
						"Water pipe",
						"Joint",
						"Blunt",
						"Vaporizer",
						"Dab",
						"Baked good",
						"Tincture",
						"Topical",
						"Beverage",
						"Shatter",
						"Oil",
						"Capsule"
						);

$medicinaluse = array(
					   "ADD/ADHD",
"Alzheimer's",
"Anorexia",
"Anxiety",
"Arthritis",
"Asthma",
"Bipolar Disorder",
"Cachexia",
"Cancer",
"Chron's Disease",
"Cramps",
"Depression",
"Eye Pressure",
"Epilepsy",
"Fatigue",
"Fibromyalgia",
"Gastrointestinal Disorder",
"Glaucoma",
"Headaches",
"HIV/AIDS",
"Hypertension",
"Inflammation",
"Insomnia",
"Appetite",
"Migraines",
"Multiple Sclerosis",
"Muscular Dystrophy",
"Muscle Spasms",
"Nausea",
"Pain",
"Parkinson's",
"Phantom Limb Pain",
"PMS",
"PTSD",
"Seizures",
"Spasticity",
"Spinal Cord Injury",
"Stress",
"Tinnitus",
"Tourette's Syndrome"

				);
$profileTYpe = array("Flower","Concentrate","Edible");
?>