<?php
include_once("config.php");
include_once("function.php");
include_once("timeAgoUtil.php");
include_once("lounge_functions.php");
include_once("emojify.php");

$agent = strtolower($_SERVER['HTTP_USER_AGENT']); // Put browser name into local variable

if (preg_match("/iphone/", $agent)) { // Apple iPhone Device
    // Set style sheet variable value to target your iPhone style sheet
    

} else if (preg_match("/android/", $agent)) { // Google Device using Android OS
    // Set style sheet variable value to target your Android style sheet
    
}else if (preg_match("/ipad/", $agent)) { // Google Device using Android OS
    // Set style sheet variable value to target your Android style sheet
    
}else{
	include_once("socialPageHeader.php");
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml"><head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
<meta name="viewport" content="width=device-width, user-scalable=no">
<title></title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<style>

.budthought_top{
	width: 98.2%;
	/*height: 31px;*/
	margin: 0px;
	padding: 10px 1%;
	float: left;
	text-align: center;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#e4e4e4));
	background-image: -webkit-linear-gradient(top, #f1f1f1, #e4e4e4);
	background-image: -moz-linear-gradient(top, #f1f1f1, #e4e4e4);
	background-image: -ms-linear-gradient(top, #f1f1f1, #e4e4e4);
	background-image: -o-linear-gradient(top, #f1f1f1, #e4e4e4);
	background-image: linear-gradient(to bottom, #f1f1f1, #e4e4e4);
}
#userImage{
	/*width:50%;*/
}
.msg_cell_inner {
    border-bottom: 1px solid #c8c8c8;
    float: left;
    margin: -1px -17px 5px;
    padding: 5px 2%;
    width: 98%;
}
.comment_here_tb {
   
    float: left;
    height: 36px;
    width: 95%;
}
div.loungeimg_new{
	position: relative;
	float:left;
	margin-left: 24%;
}
.user_comment a {
	font-size: 14px;
	color: #009933;
	text-decoration: none;
}
.msg_cell_bud {
	float: left;
	margin: 18px 89px 23px;
	padding: 10px 2%;
	width: 81%;
	word-wrap: break-word;
	background-color: palegoldenrod;
}
.username_new {
    color: #000;
    font-family: 'Gotham-Book';
    font-size: 14px;
    font-weight: bold;
    margin: 7px 0 0;
}
.user_comment {
	margin: 5px 12px 0;
	padding: 0px;
	float: left;
	font-family: 'Gotham-Book';
	font-size: 14px;
	color: #666;
	width: 100%;
}
.status_cell {
	width: 98.2%;
	height: 19px;
	/* margin-top: 10px; */
	padding: 10px 1%;
	float: left;
	border-top: 1px solid #999;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#f1f1f1), to(#e4e4e4));
	background-image: -webkit-linear-gradient(top, #f1f1f1, #e4e4e4);
	background-image: -moz-linear-gradient(top, #f1f1f1, #e4e4e4);
	background-image: -ms-linear-gradient(top, #f1f1f1, #e4e4e4);
	background-image: -o-linear-gradient(top, #f1f1f1, #e4e4e4);
	background-image: linear-gradient(to bottom, #f1f1f1, #e4e4e4);
	filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#f1f1f1, endColorstr=#e4e4e4);
}
.lounge_comment textarea {
    border: 1px solid #c8c8c8;
    float: left;
    font-family: 'Gotham-Book';
    font-size: 14px;
    height: 19px;
    margin: 2px 1% 11px;
    padding: 5px;
    resize: none;
    width: 90%;
}

.backbutton{    background-image: linear-gradient(to bottom, #fff3a4, #e6cb16);
    border: 1px solid #fff6b6;
    border-radius: 4px;
    color: #2c4e01;
    float: left;
    font-family: 'Gotham-Book';
    font-size: 20px;
    font-weight: normal;
    height: 22px;
    list-style: none outside none;
    margin: 0 4px 0 0;
    padding: 10px;
    text-align: center;
    text-decoration: none;
    text-shadow: 1px 1px 1px #fff9d4;
}

@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 480px) {
.comment_here_tb {
    border: 1px solid;
    float: left;
    height: 25px;
    width: 100%;
}
	
#userImage{
	/*width:20%;*/
	/*margin-left:-35%;*/
}
a.username{
	/*margin-left:-20%;*/
	}
div.loungeimg_new{
	position: relative;
	float:left;
	margin-left: 0% !important;
	}
.msg_cell_bud {
	float: left;
	margin: 0% !important;
	padding: 10px 2%;
	width: 96%;
	background-color: palegoldenrod;
}
.status_con {
	margin: 0px 15px 0px 0px;
	padding: 0px;
	float: right;
	font-weight: bold;
}
.inn_cell{
	
	width:100% !important;}
}
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) {
	
div.loungeimg_new{
	position: relative;
	float:left;
	margin-left: 6%;
}
.msg_cell_bud {
	float: left;
	margin: 18px 49px 23px;
	padding: 10px 2%;
	width: 81%;
	word-wrap: break-word;
	background-color: palegoldenrod;
}
.comment_here_tb {
    border: 1px solid;
    float: left;
    height: 25px;
    width: 92%;
}
}
</style>

</head>

<?php 



?>

<body >
<div id="tabs-1" style="">
<?php if (preg_match("/iphone/", $agent) || preg_match("/ipad/", $agent)) {
	
	$base64encoded_ciphertext = $_SERVER['QUERY_STRING'];

	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	$key = 'a16byteslongkey!a16byteslongkey!';
	
	$plaintext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($base64encoded_ciphertext), MCRYPT_MODE_ECB);
	
	$param=trim($plaintext);
	$paramValues=explode('_',$param);
	
	$budthoughtId=$paramValues[0];
	$userId=$paramValues[1];
	
	
	//$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $budthoughtId, MCRYPT_MODE_ECB);
	//$base64encoded_ciphertext1 = base64_encode($ciphertext); 
    
?>
<div style="height: 30px;background-color:#000; width:100%;padding: 7% 1% 1% 1%;">
<a href="https://itunes.apple.com/us/app/budfolio/id687645140?mt=8" style="font-size:18px; color:white;height:30px; width:100%;"><center><u>Download Budfolio for free!</u></center></a>
</div>
<div style="height: 30px;">
<div class="backbutton" style="height:30px;padding-left:0 !important; width:98%; color:#06C;">
<a href="#" style="float: left; font-size:18px; text-decoration:none; color:#2C4E01; margin-left:1%;"> Budfolio</a>
<a href="budfolio://?bt=<?php echo $budthoughtId; ?>" style="float: right; font-size:18px;text-decoration:none; color:#2C4E01; margin-right:1%;"> Open in App!</a>
</div>

</div>

    
<?php
} else if (preg_match("/android/", $agent)) { 

	$base64encoded_ciphertext = $_SERVER['QUERY_STRING'];

	$iv ='fedcba9876543210';
	$key = 'a16byteslongkey!a16byteslongkey!';

	$plaintext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($base64encoded_ciphertext), MCRYPT_MODE_CBC,$iv);
	
	$param=trim($plaintext);
	//echo "param---".$param."<br>";
	$paramValues=explode('_',$param);
	
	$budthoughtId=$paramValues[0];
	$userId=$paramValues[1];
	//echo "budthoughtId--".$budthoughtId."---userId---".$userId;

?> 

<div style="height: 30px;background-color:#000; width:100%;padding: 7% 1% 1% 1%;">
<a href="https://play.google.com/store/apps/details?id=com.budfolio.app&hl=en" style="font-size:18px; color:white;height:30px; width:100%;"><center><u>Download Budfolio for free!</u></center></a>
</div>
<div style="height: 30px;">
<div class="backbutton" style="height:30px;padding-left:0 !important; width:98%; color:#06C;">
<a href="#" style="float: left; font-size:18px; text-decoration:none; color:#2C4E01; margin-left:1%;"> Budfolio</a>
<a href="budfolio://?bt=<?php echo $budthoughtId; ?>" style="float: right; font-size:18px;text-decoration:none; color:#2C4E01; margin-right:1%;"> Open in App!</a>
</div>

</div>
<!--<span style="float:left;">BudFolio</span> 
<a href="budfolio://?bt=<?php echo $_GET['budthoughtId']; ?>" class="edit_btn" style="float: right;height: 30px;padding-top: 18px;width: 100%;"> Open in App!
</a>-->
<?php    
}
?>
<center>    <div class="inn_cell" id="budthought">
			<?php
			//production
            //$budthoughtId='6654';
            //$userId='2903';
			
			//qa1
            //$budthoughtId='2084';
            //$userId='353';
	
            $budThoughtSQL = "SELECT * 
                                  FROM `tbl_bud_thought` 
                                  where `bud_thought_id`='".$budthoughtId."'";
            $budThoughtData= mysql_query($budThoughtSQL);
            $row=mysql_num_rows($budThoughtData);
            
            $userDetailSQL = "SELECT user_id 
                                  FROM `users` 
                                  where `user_id`='".$userId."'";
            $userDetail= mysql_query($userDetailSQL);
            $row2=mysql_num_rows($userDetail);
            
            $straindata= mysql_fetch_assoc($budThoughtData);
            if($row>0 && $row2>0){
            //query to get the like count and comment count	
                $countCmmntSQL = "SELECT count(*) as commentCount 
                                  FROM `tbl_comments` 
                                  where `bud_thought_id`='".$budthoughtId."'";
                $resultCommentCount= mysql_query($countCmmntSQL);
                $rowComment= mysql_fetch_assoc($resultCommentCount);
                $commentCount= $rowComment['commentCount'];
                
                //query to fetch like count
                $countLikeSQL = "SELECT count(*) as likeCount 
                                 FROM `tbl_like_thoughts` 
                                 where `bud_thought_id`='".$budthoughtId."'";
                $resultLikeCount= mysql_query($countLikeSQL);
                $rowLike= mysql_fetch_assoc($resultLikeCount);
                $likeCount= $rowLike['likeCount'];
                       
                $querythree="select user_name,zip_code,state,photo_url 
                             from users 
                             where user_id='".$userId."' 
                             limit 0,1";
                $userdata=mysql_query($querythree);
                $udata=mysql_fetch_array($userdata);
                
                if(!empty($udata['photo_url']))
                {
                    $img_src='images/profileimages/original/'.$udata['photo_url'];
                }
                else
                { 
                    $img_src="images/css_images/profile_pic.png";
                }
                ?>
        
        <div class="budthought_top"> <span class="lounge_profile" id="lounge_profile" style="float: left;"> <img src="<?php echo $img_src;?>"  width="40" height="40" class="scale-with-grid" id="userImage"></span> <span class="name_link"><a href="javascript:void(0);" class="username" id="userids_<?php echo $userId;?>"> <?php echo str_replace("\\", "", truncatestr($udata['user_name'],20));?></a></span> <span class="datetime" style="margin:1%;"> <?php echo funTimeAgo(strtotime($straindata['post_date']));?> ago</span>
        </div>
        
        <div class="loungeimg_new" style="">
        <?php if($straindata['picture_url']){ ?>
         <img src="<?php echo 'images/budthought/'.$straindata['picture_url'];?>"  class="scale-with-grid"> 
         <?php } ?>
        </div>
        
        <div class="msg_cell_bud"> <span class="user_comment"><?php echo emojify(stripslashes(atRateUserNameWeb($straindata['bud_thought'])));?></span> 
        </div>
        
        <div class="status_cell">
        
        	<div style="float:left;margin-left: 2%;">
        		<span class="fleft" id="markbudthoght" style="padding-top:0px; float:right;">
                </span>
        		<a href="javascript:void(0);" style="font-size: 15px;text-decoration: none;color: #009933;" id="InappropriateBud_<?php echo $userId?>" onclick="newLoungeAlert();" class="InappropriateBud" ><img src="images/new_lounge/report_inappropriate.png" style="height: 20px;"  />
            	</a>
            
        	</div>
            
            <div class="status_con" style="margin-right: 7%;"> 
            <span class="fleft" style="float:right; " id="comment_count_<?php echo $budthoughtId; ?>"><?php if($commentCount>0){echo $commentCount;}else{/*display nthng*/}?>
             </span> 
             <span class="fleft new_marlft8" style="float:right;
"><a href="javascript:void(0);" class="show-comment show_post_btn" onclick="newLoungeAlert();" id="showComments_<?php echo $budthoughtId; ?>"> <img src="images/new_lounge/chat_newicon.png"></a> </span> 
             
           </div>
           
           <div class="status_con">
              <span class="fleft new_marlft8" style="float:right;/*margin-right: 3%;*/">
              <!--onclick='likeBudthought(this.id,"<?php echo $userId; ?>")'-->
               <a href="javascript:void(0);" id="likeBudThought_<?php echo $budthoughtId;?>" onclick="newLoungeAlert();" > <img src="images/new_lounge/like_newicon.png"> 
               </a>
               <span class="fleft" style="float:right; margin-right:-3%;" id="like_count_<?php echo $budthoughtId; ?>"> <?php if($likeCount>0){echo $likeCount;}else{/* echo nthng*/ }?></span>
              </span>
           </div>
           
        </div>
        <!--<div  class="lounge_comment comments_here " id="comments_here_<?php echo $budthoughtId;?>"></div>-->
        <div id="totalComments" style="width:99%; top:100% height:50px;">
        
        </div>
        <!--<div  class="lounge_comment comments_here " id="comments_here_<?php echo $budthoughtId;?>"></div>-->
        
        
        
</div></center>
<?php }else{ echo '<div class="cell_dispensary center" style="padding:15px 0;">No detail is available. </div>'; } ?>
<script src="js/jquery.min.js" type="text/javascript" ></script>
<script>

function newLoungeAlert(){
	
	var val =confirm("Sorry! Please Login or Register to use this functionality.");
	if(val==true){
		window.location.assign("https://budfolio.com/qa1/")
	}
	else{
		
	}
}

//for like budthought 
/*function likeBudthought(Bid,Uid){
	
	var BudthoughtId=Bid.split("_");	
	var targetURL = "likeBudThoughtViaSocialApp.php";
	
	$.post(targetURL,{'budThoughtId':BudthoughtId[1], 'userId':Uid},function(data){
		try{
				
			$('#like_count_'+BudthoughtId[1]).html(data);
		}catch(e){
			console.log(e)	;
		}
	});

}*/
//show-comment
/*function showComments(budID){
	//var budID=id.split("_");
	//console.log(budID[1]);
	var targetURL = "seeCommentsViaSocialApp.php";
	$.post(targetURL,{'budThoughtId':budID},function(data){
		try{
				
				$("#showComments_"+budID).addClass("hide-comment");
				$("#showComments_"+budID).removeClass("show-comment");
				$('#totalComments').html(data);
				
		   }catch(e)
		   {
				console.log(e)	;
		   }
		});
	
}*/

/*$(document).ready(function(){

$(".show-comment").live('click',function(){
	
	var strainid=$(this).attr("id");
	var budID=strainid.split("_");
	//console.log(budID[1]);
	var targetURL = "seeCommentsViaSocialApp.php";
	$.post(targetURL,{'budThoughtId':budID[1]},function(data){
		try{
				
				$("#showComments_"+budID[1]).addClass("hide-comment");
				$("#showComments_"+budID[1]).removeClass("show-comment");
				$('#totalComments').html(data);
				
		   }catch(e)
		   {
				console.log(e)	;
		   }
		});
})	

$(".hide-comment").live('click',function(){
		var strainid=$(this).attr("id");
		var id =strainid.split("_");
		if(id[1]=='')
		{
			return false;
		}
		$("#showComments_"+id[1]).addClass("show-comment");
		$("#showComments_"+id[1]).removeClass("hide-comment");
		$('#totalComments').html("");
})	
	
});


function post_comment(budthoughtId)
{
		var budCommentText=$("#budCommentText").val();
			
		if(budCommentText=='')
		{
				alert("Please enter your comment");
				$("#budCommentText").focus();
				return false;
		}
			
		var targetURL = "addComment.php";
		$.post(targetURL,{'budThoughtId':budthoughtId,'budCommentText':budCommentText},function(data){
		try{
				$("#budCommentText"+budthoughtId).val('');
				$('#comment_count_'+budthoughtId).html(data);
				showComments(budthoughtId);
					
		}catch(e){
					console.log(e)	;
		}
	});
}*/

/*function submit_comment(event,budthoughtId)
{
	if (event.which == 13)
	{
       	event.preventDefault();
        post_comment(budthoughtId);
		showComments(budthoughtId);
    }else
	{
		console.log("in else");
	}
}*/


</script>
</div>
</body>
</html>