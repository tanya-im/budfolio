
    <?php
    //Php Time_Ago Script v1.0.0
 
    function funTimeAgo($time_ago)
    {
    $cur_time=time();
	//echo date('Y-m-d H:i:s');
	
    $time_elapsed = $cur_time - $time_ago;
	//echo "time diff ".$time_elapsed;
    $seconds = $time_elapsed ;
    $minutes = round($time_elapsed / 60 );
 
    $hours = round($time_elapsed / 3600);
    $days = round($time_elapsed / 86400 );
    $weeks = round($time_elapsed / 604800);
  /*  $months = round($time_elapsed / 2600640 );
    $years = round($time_elapsed / 31207680 );*/
    // Seconds
    if($seconds <= 60 )
    {
    	return $seconds." sec";
    }
    //Minutes
    else if($minutes <=60)
    {
		 if($minutes==1){
			return "1 min";
		}else{
			return $minutes." mins";
	    }
    }
    //Hours
    else if($hours <=24)
    {
		 if($hours==1){
			 return "1 hour";
		}else{
			return $hours." hours";
		}
    }
    //Days
    else if($days <= 7)
    {
		if($days==1)
		{
			return "1 day";
		}else{
			 return $days." days";
		}
    } //Weeks
    else  /*($weeks <= 4.3)*/
    {
		if($weeks==1)
		{
			return "1 Week";
		}else{
			 return $weeks." weeks";
		}
    }
   
    }
?>
    

