<?php 
// check authentication if user name or id empty redirect to login page.

if(empty($_SESSION['user_name']) || empty($_SESSION['user_id']))
{
	session_unset($_SESSION['user_name']);
	session_unset($_SESSION['user_id']);
	header('Location:index.php');
}
?>