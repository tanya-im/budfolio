<?php
// Include header,auth and config file.
include_once("newHeader.php");
require_once("auth.php");
include_once("config.php");
include_once("function.php");
include_once("timeAgoUtil.php");
include_once("lounge_functions.php"); 
include_once("check_subscription_for_lounge.php");
$_SESSION['loungecounter']=1;
$_SESSION['searchtext']='';
?>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    
      
      <?php
	if($_SESSION['dispansary']==1)
	{	
		if($timeDiffrence > 0 and $subTYpe=='d')
		{
				$isSubscription='1';
	?>
                <div id ="scroll" style="color:#2B6A03;margin-bottom: 6px;
    margin-top: 0;"><marquee scrolldelay="200">14-day trial is active:<a href="check_out.php"> Click here to upgrade </a> and continue sharing photos, specials, and more in the Lounge</marquee></div>
                <?php 
		}
		else if ($timeDiffrence < 0 and $subTYpe=='d' )
		{
				$isSubscription='0';		
					
	?>
				<div id ="scroll" style="color:#2B6A03;margin-bottom: 6px;margin-top: 0;"> <marquee scrolldelay="200"> Your trial period has ended:<a href="check_out.php"> Click here to upgrade</a> and continue sharing photos, specials, and more in the Lounge</marquee></div>
	<?php
        }
		else if ($timeDiffrence < 0 and $subTYpe!='d' ){
		$isSubscription='0';		
					
	?>
				<div id ="scroll" style="color:#2B6A03;margin-bottom: 6px;margin-top: 0;"> <marquee scrolldelay="200"> Your subscription has ended:<a href="check_out.php"> Click here to upgrade</a> and continue sharing photos, specials, and more in the Lounge</marquee></div>
	<?php
        }
	}// end of $_session['dispensary']
		?>
    
    
    
    	<div class="inner_conlft scroll_container">
        <div id="msg"></div>
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2> Lounge</h2> 
                  <div class="refresh_button"> <a href="javascript:void(0);" onClick="window.location.reload()"> <img src="images/new_web/bf_refresh_btn.png" class="scale-with-grid"> </a> </div>
                     
               <?php 
			    if(($_SESSION['dispansary']==1) and !empty($_SESSION['user_id']))  
             	 { 
				 
				 ?>   
                 <div class="budthough_btn"> <a <?php if($timeDiffrence>0) { ?> href="bud_thought_form.php" <?php }else{ ?>
                 href="javascript:void(0);" onclick="subscription_over_alert();" <?php }   ?>> <img src="images/new_web/budthought_btn.png" class="scale-with-grid"> </a> </div>
               <?php }
			   else if  ($_SESSION['user_id'] and $_SESSION['dispensary']!=1)
			   {
				   
				?>
                <div class="budthough_btn"> <a href="bud_thought_form.php"><img src="images/new_web/budthought_btn.png" class="scale-with-grid"> </a> </div>
			   <?php
               }
			   ?></div>
                <div class="greenbar_bg"><?php include('Lounge_Filter.php');?> </div>
                <div class="greenbar_bg_new"><div class="input-append"> <div class="searchbox" style="width:626px;"> <img src="images/new_web/serach_icon.png" class="scale-with-grid"> <input type="text" id="sl_search_text" name="sl_search_text" placeholder="Username, strain, hashtags"> </div> <a href="javascript:void(0);" id="search_here" class="pushtop9 search_btn <?php if($filename=='outerLounge.php' || $filename=='newLounge.php'){?>lounge_search <?php }else if($filename=='loungeFollowupList.php'){?>loungeFollowSearch<?php  }else {?> loungeMessageSearch <?php } ?>" > Search <!--<img src="images/new_web/search_btn.png" class="scale-with-grid">--> </a> </div></div>
                <div id="content_1" class="content">
	<?php 
	// Show lounge listing for all users
	// Show lounge listing for all users
	$perpage = PerPageForLoungMyLocal;
	$results = loungeUserFollowers(1,$_SESSION['user_id'],$perpage);//function 
	$rows = mysql_num_rows($results);
	if($rows)
	{
		$i = 0;
        while($straindata=mysql_fetch_array($results))
        {	
			$i++;
			$dateTime = explode(' ',$straindata['post_date']);
			$date = date('d F Y/h:i A',strtotime($straindata['post_date']));//conevrting date time into string format
			if($straindata['TYPE']==1) 	
			{
				//includeing external file for showing strains
				include("newloung_strain_details.php");
			}//end of if(type==1 //strain)
			else{//includeing external file for showing bud thoughts 
				include('newLounge_budThought_details.php');	 
			}//end of else//bud thought
		}//end of while
	}
	else{
			echo '<div class="cell"><center style="padding-top:25px;">No result found.</center></div>';
	}?>
	</div>            	
    <!--    End loung elisting -->
			</div>
    	</div>
    	 <?php include('newLounge_right.php');?>
	</div>
</div>
<div class="footer"><?php include('footer.php');?></div>
<?php  include('budfolio_footer.php');?>

<script>

/* function to mark strain post inappropriate*/
function markInappropriateStrain(strainid,userid,postinguserid){
	
	console.log("strainid::"+strainid+"---userid::"+userid+"---postinguserid"+postinguserid);
	var count=0;
	$.ajax({
					type: "POST",
					url: "markInappropriate.php",
					data: 'strainid='+strainid+'&userid='+userid+'&postinguserid='+postinguserid+'&postType=2',
					success: function(data)
					{
						console.log(data);
						if(data!="You already marked as report Inappropriate."){
							alert("Post Reported as Inappropriate.");
							//$("#markstrain_"+strainid).text(count);
						}else{
							alert("You already marked as report Inappropriate.");
						}
						//console.log("buddata::"+data);
						
						//window.location.href="newLounge.php";
						return false;
					}
			});
}
/* function to mark Budthought post inappropriate*/
function markInappropriateBudthought(budthoughtid,userid,postinguserid){
	
	console.log("budthoughtid::"+budthoughtid+"---userid::"+userid+"---postinguserid"+postinguserid);
	var count=0;
	$.ajax({
					type: "POST",
					url: "markInappropriate.php",
					data: 'budthoughtid='+budthoughtid+'&userid='+userid+'&postinguserid='+postinguserid+'&postType=1',
					success: function(data)
					{
						console.log(data);
						if(data!="You already marked as report Inappropriate."){
							alert("Post Reported as Inappropriate.");
							//$("#markbudthoght_"+budthoughtid).text(count);
						}else{
							alert("You already marked as report Inappropriate.");
						}
						//console.log("straindata::"+data);
						
						//window.location.href="newLounge.php";
						return false;
					}
			});
}


	(function($){
		$(window).load(function(){
			/*$("#content_1").mCustomScrollbar({
				scrollButtons:{
					enable:true
				}
			});*/
			$('.scroll_container').scrollExtend(
			{	
			'target': 'div#content_1',
			'url': 'loungeFollowUpAjax.php',
			'loadingIndicatorClass': 'scrollExtend-loading',
			'loadingIndicatorEnabled': true
			});
		});
	})(jQuery);

$(document).ready(function() {	

	$("#searchtext").live('keyup',function(){
		$("#searcherror").html("&nbsp;");
	});
	

});
</script>


<script type="text/javascript" src="js/loungeComment.js"></script>
<div id="boxes"></div>
<div id="mask"></div> 
</body>

</html>