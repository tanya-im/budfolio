<?php session_start();?>
<div id="boxes" >
<!-- ----------Registration form start here-----------onsubmit=" return dis_registration_validate();"----------------->
<form name="dis_registration" id="dis_registration"  autocomplete="Disabled">    
    <div id="dispensary_register" class="window">
        <div class="userpopup_header"> <span class="headertxt"> Dispensary Profile Registration  </span></div>
        <a href="javascript:void(0);" class="close" onclick="closeStrain();">
        	<img src="images/css_images/close_btn.png" width="37" height="37" alt="close" id="cls" />
        </a>
        <div class="reg-con">
        	<p id="regerrormsg"></p>
            <dl>
                <dt><span class="mdred">*</span>Username</dt>
                <dd> <span class="registerinput"> <input type="text" name="username" id="username" tabindex="1" maxlength="25" placeholder="Enter Username" value="<?php echo $_SESSION['username'];?>" /> </span> </dd>
                <div class="username_check"></div>
            </dl>
            
            <dl>
                <dt><span class="mdred">*</span>Password </dt>
                <dd> <span class="registerinput"> <input type="password" name="password" id="password" tabindex="2"placeholder="Enter Password" /> </span>
                 <div class="username_check"></div>
                 </dd>
            </dl>
            
            <dl>
                <dt><span class="mdred">*</span>Confirm Password </dt>
                <dd> <span class="registerinput"> <input type="password" name="confirmpassword" id="confirmpassword" tabindex="3" placeholder="Enter Confirm Password" /> </span> 
                 <div class="username_check"></div>
                </dd>
            </dl>
            
            <dl>
                <dt><span class="mdred">*</span>Email</dt>
                <dd> <span class="registerinput"> <input type="text" placeholder="Enter Email Address" name="email" id="email" tabindex="4" maxlength="55" value="<?php echo $_SESSION['email'];?>"/> </span></dd>
                <div class="username_check"></div>
            </dl>
            
            <dl>
                <dt><span class="mdred">*</span>Business Name</dt>
                <dd> <span class="registerinput"> <input type="text" name="businessname" id="businessname" tabindex="5" maxlength="50" placeholder="Enter Business Name" /> </span> </dd>
                <div class="username_check space"></div>
            </dl>
            
       
            
            <dl>
                <dt><span class="mdred">*</span>Zip Code</dt>
                <dd> <span class="registerinput"> <input type="text" placeholder="Enter Zip Code" name="zipcode" id="zipcode" tabindex="6" maxlength="8" value="<?php echo $_SESSION['zipcode'];?>"/> </span> </dd>
				<div class="username_check"></div>
            </dl>
          
            <dl>
                <dt>Phone No</dt>
                <dd> <span class="registerinput"> <input type="text" placeholder="Enter Phone No" name="phoneno" id="phoneno" tabindex="7" maxlength="12" /> </span> 
                 <div class="username_check"></div>
                </dd>
            </dl>
			
            <!-- 3 checkboxes-->
            <dl>
            	<dt> <input type="checkbox" name="i_am_18" id="i_am_18" tabindex="8" /></dt>
                <dd><span class="newmartop10">I am at least 18 years old</span></dd>
            </dl>
			<dl>
            	<dt> <input type="checkbox" name="up_to_date" id="up_to_date" tabindex="9" /></dt>
                <dd><span class="newmartop10">Stay up-to-date with budfolio news ?</span></dd>
            </dl>
            <dl>
            	<dt> <input type="checkbox" name="t_and_c" id="t_and_c" onclick="hideerrormsg();" tabindex="10"/></dt>
                <dd><span class="newmartop10">I agree to terms and conditions </span></dd>
                <div class="fllft martop10"> 
                    	<a href="javascript:void(0);" id="tandcmsg" class="gray_link"> Terms & Conditions </a>
                    </div>
            </dl>
			<!-- end of three checkBoxes-->
            
           
            <!--<dl>
                <dt>&nbsp;</dt>
                <dd> 
                    <div class="fllft martop10"> 
                        <input type="radio" onclick="hideerrormsg();" name="t_and_c" value="yes" id="t_and_c_yes" tabindex="10" > Yes &nbsp; &nbsp;
                        <input type="radio"  checked="checked" name="t_and_c" value="no" id="t_and_c_no" tabindex="11"> No
                    </div> 
                    <div class="martop5 fllft"> &nbsp; 
                        <img src="images/css_images/reg_divid.png" width="2" height="32" > &nbsp;  
                    </div> 
                    <div class="fllft martop10"> 
                    	<a href="javascript:void(0);" id="tandcmsg" class="gray_link"> Terms & Conditions </a>
                    </div>
                </dd>
            </dl>-->
            <dl>
            	<span class="tcmsg">By registering for budfolio you agree that you are at least 18 years of age and that you are a legal medicinal marijuana under your current state laws.</span>
            </dl>
            <dl>
                <dt>&nbsp;</dt>
                <dd>
                	<a  style="height:20px;float:left;"class="edit_btn martop10" type="button" name="dis_reg_submit" 
                    id="dis_reg_submit" tabindex="11">Create Member !</a>
                	<input type="hidden" value="available" name="unameflag" id="unameflag" />
                    <input type="hidden" value="available" name="emailflag" id="emailflag" />
                </dd>
            </dl>
    	</div>
	</div>        
</form> <!------------------------- Registration form end here------------->
</div>