<?php ob_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

<title>Budfolio Home</title>

<!-- Include css and js files-->
<link rel="stylesheet" type="text/css" href="css/style.css" />

<link rel="stylesheet" type="text/css" href="css/scroller.css" >

<link rel="stylesheet" href="css/jquery-ui.css" />


 
<script type="text/javascript" src="https://budfolio.com/qa1/js/jquery.min.js"></script>
</head>
<body>
	<div class="main_con">
    <header> 
	<div class="logo"> <img src="images/new_web/new_logo.png" border="0" ></div>
   

	    
    </header>
<?php 
// Check parameter valid or not.
include_once("config.php");
if(isset($_GET['fgtpwd']) && isset($_GET['cid']))
{
	if(!empty($_GET['fgtpwd']) && !empty($_GET['cid']))
	{
		echo  $queryone="select user_id,user_name from users where user_id='".$_GET['cid']."' and forgot_password='".$_GET['fgtpwd']."'";
		$res=mysql_query($queryone);
		$data=mysql_fetch_array($res);
		if(!empty($data[0]))
		{
		?>
        <!--  Reset password form-->
        <div class="reset_pwd">
        	<div class="rpwd_wrapper">
            	<h3>Forgot password</h3>
                <div class="rpwd_innerwrapper">
                <form method="post" name="forgotpassword" id="forgotpassword" action="forgotpassword.php" onsubmit="return validatefgpwd();">  
                	<div>
                		<label>Enter new password</label>
                    	<input type="password" name="fgpassword" id="fgpassword" />
                        <input type="hidden" name="uid" id="uid" value="<?php echo $_GET['cid'];?>" />
                        <input type="hidden" name="fgcode" id="fgcode" value="<?php echo $_GET['fgtpwd'];?>" />
                    </div>
                    <div>
                    	<label>Enter confirm password</label>
                    	<input type="password" name="confirmfgpassword" id="confirmfgpassword" />
                	</div>
                    <div class="fgbtn">
                    	<input type="submit" name="fgpwd" id="fgpwd" value="Save !" />
                        <input type="button" name="fgpwdcancel" id="fgpwdcancel" value="Cancel !" onclick="fgpwdcancelbtn();" />
                    </div>
                </form>    
                </div>
            </div>
        </div>
        <!-- end reset password form-->
<?php
		}
		else
		{
			//header('Location:http://budfolio.com/index.php?resetpwd=invaliddata');
		}         
	}
	else
	{
		//header('Location:http://budfolio.com/index.php?resetpwd=emptydata');
	}
}
else
{
	//header('Location:http://budfolio.com/index.php?resetpwd=invaliduser');
}
?>
<!-- Validate the reset password fields-->
<script>
function validatefgpwd()
{
	var fgpassword=document.getElementById("fgpassword").value;
	var confirmfgpassword=document.getElementById("confirmfgpassword").value;
	if(fgpassword=='')
	{
		document.getElementById('fgpassword').focus();
		alert('Please enter password');
		return false;
	}
	if(confirmfgpassword=='')
	{
		document.getElementById('confirmfgpassword').focus();
		alert('Please enter confirm password');
		return false;
	}
	if(fgpassword!=confirmfgpassword)
	{
		document.getElementById('confirmfgpassword').focus();
		alert('Confirm password do not match');
		return false;
	}
	return true;
}
function fgpwdcancelbtn()
{
	window.location.href="https://budfolio.com/qa1/index.php";	
}
</script>
</body>
</html>