<?php 
session_start();
include_once("config.php");
include_once("function.php");
	/*$strGetReviewSQL  = "SELECT * FROM tbl_dispensary_review 
						 WHERE dispensary_id='".$_GET['dispensaryId']."'
						 ORDER BY date_time DESC";*/
						 
	$dispensaryName = getDispensaryName($_GET['dispensaryId']);					 
						 
	$strGetReviewSQL='SELECT tdr.review_id,
								tdr.dispensary_id,
								tdr.user_id,
								tdr.strain_quality,
								tdr.strain_variety,
								tdr.date_time as d,
								tdr.service,
								tdr.atmosphere,
								tdr.privacy,
								tdr.pricing,
								tdr.review_text,
								tdr.rc_shop_here_again,
								tdr.rc_recommend_club,
								tdr.rc_first_visit,
								0 As type
						 FROM tbl_dispensary_review tdr
						 WHERE tdr.dispensary_id="'.$_GET['dispensaryId'].'"
						 UNION
						 SELECt s.strain_id,
						 		s.dispensary_id,
								s.user_id,
								s.strain_name,
								s.dispensary_name,
								s.post_date as d,
								s.species,
								s.overall_rating,
								NULL AS privacy,
								NULL AS pricing,
								NULL AS review_text,
								NULL AS rc_shop_here_again,
								NULL AS rc_recommend_club,
								NUll AS rc_first_visit,
								1 AS TYPE
						 FROM strains s		
						 INNER JOIN users ON users.user_id = s.user_id
			   			 WHERE s.flag = "active"
			  			 AND users.flag ="active"
			   			 AND users.privacy = "public"	
						 AND s.dispensary_name like "%'.mysql_real_escape_string($dispensaryName).'%" 
						 AND s.dispensary_id="'.$_GET['dispensaryId'].'"
						 ORDER BY d desc';					 
						 
	$resGetreviews = mysql_query($strGetReviewSQL) or die($strGetReviewSQL." : ".mysql_error());
	
?>
<style>
.reviewtable table {
    float: left;
    
}
</style>
<div id="core2">
	<div class="reviewtable"> 
<?php 
	
	if(mysql_num_rows($resGetreviews)>0)
	{
		while($reviewRow = mysql_fetch_assoc($resGetreviews))
		{
			$dateTime = explode(' ',$reviewRow['date_time']);
			$date = date('d F Y / h:i A',strtotime($reviewRow['date_time']));//conevrting date time into string format
			if($reviewRow['type']==0)
			{
?>
                                  	
        <table width="100%" cellpadding="0" cellspacing="1" border="0">
        	<tr class="padtopbtm10" >
              <td colspan="5">
            	<div class="gray_bg_cell">
                
                 <?php 
			  
			 $userPic= getUserImagePath($reviewRow['user_id']);
			  
			  
		if($userPic!='')
		{
?>		<!--<div class="dis_review_img">-->
			<div class="gray_header_user_img">
            	<img src="<?php echo 'images/profileimages/thumbnail/'.$userPic;?>" 
                class="scale-with-grid" > 
            </div>
            <!--</div>-->
<?php 	}
		?>
                
                
                	<div class="name_link"> <a 
        href="user_popup_page.php?userId=<?php echo $reviewRow['user_id'];?>" id="userids_<?php echo $reviewRow['user_id'];?>"><?php echo str_replace("\\", "",stripslashes(getUserName($reviewRow['user_id'])));?></a>
                    </div>
                </div>
              </td>
            </tr>
      	   <tr class="padtopbtm10">
          	 <!-- <td rowspan="2" width="20%"> </td>-->
              <td width="20%" style="text-align:center;">Strain Quality:</td>
              <td width="20%" > 
			  <?php for($rating=1;$rating < 6;$rating++){
                    if($reviewRow['strain_quality']>=$rating) {?>
              			<img src="images/css_images/start_sel.png" width="21" height="21">
              <?php 	} else {?>
              			<img src="images/css_images/start_dsel.png" width="21" height="21">
              <?php	} }?>
              </td>

              <td width="20%">Strain Variety:</td>
              <td width="20%">  
			  <?php for($rating=1;$rating < 6;$rating++){
                    if($reviewRow['strain_variety']>=$rating) {?>
              			<img src="images/css_images/start_sel.png" width="21" height="21">
              <?php 	} else {?>
              			<img src="images/css_images/start_dsel.png" width="21" height="21">
              <?php	} }?> </td>
          </tr>
          
           <tr class="padtopbtm10">
              <td width="20%" align="center">Service:</td>
              <td width="20%" > 
			  <?php for($rating=1;$rating < 6;$rating++){
                    if($reviewRow['service']>=$rating) {?>
              			<img src="images/css_images/start_sel.png" width="21" height="21">
              <?php 	} else {?>
              			<img src="images/css_images/start_dsel.png" width="21" height="21">
              <?php	} }?>
              </td>

              <td width="20%">Atmosphere:</td>
              <td width="20%">  
			  <?php for($rating=1;$rating < 6;$rating++){
                    if($reviewRow['atmosphere']>=$rating) {?>
              			<img src="images/css_images/start_sel.png" width="21" height="21">
              <?php 	} else {?>
              			<img src="images/css_images/start_dsel.png" width="21" height="21">
              <?php	} }?> </td>
            </tr>
            
           <tr class="padtopbtm10">
              <td width="20%" align="center">Price:</td>
              <td width="20%" > 
			  <?php for($rating=1;$rating < 6;$rating++){
                    if($reviewRow['pricing']>=$rating) {?>
              			<img src="images/css_images/start_sel.png" width="21" height="21">
              <?php 	} else {?>
              			<img src="images/css_images/start_dsel.png" width="21" height="21">
              <?php	} }?>
              </td>

              <td width="20%">Privacy:</td>
              <td width="20%">  
			  <?php for($rating=1;$rating < 6;$rating++){
                    if($reviewRow['privacy']>=$rating) {?>
              			<img src="images/css_images/start_sel.png" width="21" height="21">
              <?php 	} else {?>
              			<img src="images/css_images/start_dsel.png" width="21" height="21">
              <?php	} }?> </td>
            </tr> 
            
            <!-- for 3 options-->
            <tr class="padtopbtm10">
              <td colspan="1" align="center"><input type="checkbox" <?php if($reviewRow['rc_shop_here_again']=='1'){?> checked="checked" disabled="disabled"<?php }?> /> I will shop here again </td>
              <td colspan="2" align="center"><input type="checkbox" <?php if($reviewRow['rc_recommend_club']=='1'){?> checked="checked" <?php }?> disabled="disabled"/>  I would recommend this club to others  </td>
              <td colspan="1" align="center"><input type="checkbox" <?php if($reviewRow['rc_first_visit']=='1'){?> checked="checked" <?php }?> disabled="disabled"/> 
              this is my first visit </td>
		 </tr> 
            <!-- end of 3 options-->                            
        
            <tr class="dividerbtm">
              <td colspan="5" > <span class="biohead"> Review </span> <span class="biodis"> <?php echo $reviewRow['review_text'];?>. </span></td>
            </tr>
                                     
            <!--<tr> <td colspan="5" class="blank">&nbsp; </td> </tr>-->
                                        
         </table>  
    
                                
 <?php 	} // end of $reviewRow['type']==0
 			else
			{
				include('review_strain.php');
			}
 
 
 		}//end of while loop 
 	}else// end of (mysql_num_rows($resGetreviews)>0)
	 {
	  echo '<div class="cell_dispensary center" style="padding:15px 0;">No reviews available. </div>';
 	}?>
                             </div>
                         </div>