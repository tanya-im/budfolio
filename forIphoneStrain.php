<?php
include_once("config.php");
include_once("function.php");
include_once("emojify.php");

$agent = strtolower($_SERVER['HTTP_USER_AGENT']); // Put browser name into local variable

if (preg_match("/iphone/", $agent)) { // Apple iPhone Device
    // Set style sheet variable value to target your iPhone style sheet
    

} else if (preg_match("/android/", $agent)) { // Google Device using Android OS
    // Set style sheet variable value to target your Android style sheet
    
}else if (preg_match("/ipad/", $agent)) { // Google Device using Android OS
    // Set style sheet variable value to target your Android style sheet
    
}else{
	include_once("socialPageHeader.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml"><head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
<!--<meta name="viewport" content="width=device-width, user-scalable=no">-->
<meta name="viewport" content="width=device-width,initial-scale=0.7,maximum-scale=0.7,minimum-scale=0.7,user-scalable=no" />
<title></title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/style_one.css" />
<link rel="stylesheet" type="text/css" href="css/style1.css" />

<link rel="stylesheet" type="text/css" href="css/scroller.css" >
<link rel="stylesheet" href="css/jquery-ui.css" />

<link rel="stylesheet" type="text/css" href="css/stylenew.css" />
<style>
.cell_con {
width: 134%;
background: #fff;
float: left;
}
.strpopupdesc {
    float: left;
    text-align: justify;
	width: 100% !important;
}
.edit_btn{
	float: left;
	height: 30px;
	padding-top: 18px;
	text-align: center;
    width: 100%;
}
.celladd_popup {
    float: left;
    margin: 5px 39px;
    min-height: 88px;
    min-width: 100%;
    padding: 6px;
}
.inner_conlft {
	margin: 0px 1% 0px 2%;
	padding: 5px;
	float: left;
	width: 71%;
	min-height: 500px;
}
#wraper {
	background: url(../images/new_web/gray_backgroundrepeat.jpg) repeat center top;
	width: 100%;
	margin: 0px auto;
	padding: 0px;
}

.backbutton{    background-image: linear-gradient(to bottom, #fff3a4, #e6cb16);
    border: 1px solid #fff6b6;
    border-radius: 4px;
    color: #2c4e01;
    float: left;
    font-family: 'Gotham-Book';
    font-size: 20px;
    font-weight: normal;
    height: 22px;
    list-style: none outside none;
    margin: 0 4px 0 0;
    padding: 10px;
    text-align: center;
    text-decoration: none;
    text-shadow: 1px 1px 1px #fff9d4;
}

@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 480px)  {

.add_pic_popup {
	/* width: 132px; */
	width: 25%;
	height: 132px;
	float: left;
	margin: 5px 10px;
	padding: 4px;
	background: #fff;
	border: 1px solid #FFFFFF;
	box-shadow: 0 2px 10px 2px #CCCCCC;
}	
#main_con {
	background: #e1e1e1;
	width: 100%;
	margin: 0 0 45px;
	padding: 3% 0px 10px;
	float: left;
}	
a.edit_btn {
    background-image: linear-gradient(to bottom, #fae978, #e5ca15);
    border: 1px solid #fff6b6;
    border-radius: 2px;
	height: 30px;
    color: #773e00;
    float: left;
    font-family: 'Gotham-Book';
    font-size: 18px;
    margin: 0;
    padding: 5px 0;
    text-align: center;
    text-decoration: none;
	
}
.inner_conlft {
    float: left;
    margin: 0 1% 0 0;
    min-height: 500px;
    padding: 5px;
    width: 99%;
}	
.celladd_popup {
    float: left;
    margin: 5px 25px;
    min-height: 88px;
    min-width: 100%;
    padding: 6px;
}
.backtoapp{
	width:100%;
}
.inputBox{
	width:100%
	}
.cell_con {
    background: none repeat scroll 0 0 #fff;
    float: left;
    width: 100%;
}
span #experience { 

	word-wrap: break-word;
}
#wraper {
	background: url(../images/new_web/gray_backgroundrepeat.jpg) repeat center top;
	width: 100%;
	margin: 0px auto;
	padding: 0px;
}
}

@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) {


a.edit_btn {
    background-image: linear-gradient(to bottom, #fae978, #e5ca15);
    border: 1px solid #fff6b6;
    border-radius: 2px;
	height: 30px;
    color: #773e00;
    float: left;
    font-family: 'Gotham-Book';
    font-size: 18px;
    margin: 0;
    padding: 5px 0;
    text-align: center;
    text-decoration: none;
	
}	

.backtoapp{
	
}
.cell_con {
    background: none repeat scroll 0 0 #fff;
    float: left;
    width: 100%;
}

.inner_conlft {
    float: left;
    margin: 0 1% 0 0;
    min-height: 500px;
    padding: 5px;
    width: 100%;
}
#wraper {
	background: url(../images/new_web/gray_backgroundrepeat.jpg) repeat center top;
	width: 100%;
	margin: 0px auto;
	padding: 0px;
}
#main_con {
	background: #e1e1e1;
	width: 100%;
	margin: 0 0 45px;
	padding: 3% 0px 10px;
	float: left;
}	
	
}
</style>

</head>
<?php 



?>
<body >
<?php if (preg_match("/iphone/", $agent) || preg_match("/ipad/", $agent)) { 

	$base64encoded_ciphertext = $_SERVER['QUERY_STRING'];
	
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	$key = 'a16byteslongkey!a16byteslongkey!';
	
	$plaintext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($base64encoded_ciphertext), MCRYPT_MODE_ECB);
	
	$strainId=trim($plaintext);
	
	//$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $strainId, MCRYPT_MODE_ECB);
	//$base64encoded_ciphertext1 = base64_encode($ciphertext);
   
?>
<div style="height: 30px;background-color:#000; width:100%;padding: 7% 1% 1% 1%;">
<a href="https://itunes.apple.com/us/app/budfolio/id687645140?mt=8" style="font-size:18px; color:white;height:30px; width:100%;"><center><u>Download Budfolio for free!</u></center></a>
</div>
<div style="height: 30px;">

<div class="backbutton" style="height:30px;padding-left:0 !important; width:98%; color:#06C;">
<a href="#" style="float: left; font-size:18px; text-decoration:none; color:#2C4E01; margin-left:1%;"> Budfolio</a>
<a href="budfolio://?st=<?php echo $strainId; ?>" style="float: right; font-size:18px;text-decoration:none; color:#2C4E01; margin-right:1%;"> Open in App!</a>
</div>

</div>
    
<?php
} else if (preg_match("/android/", $agent)) { 

$base64encoded_ciphertext = $_SERVER['QUERY_STRING'];

$iv ='fedcba9876543210';
$key = 'a16byteslongkey!a16byteslongkey!';

$plaintext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($base64encoded_ciphertext), MCRYPT_MODE_CBC,$iv);
$strainId=trim($plaintext);

?>    
<div style="height: 30px;background-color:#000; width:100%;padding: 7% 1% 1% 1%;">
<a href="https://play.google.com/store/apps/details?id=com.budfolio.app&hl=en" style="font-size:18px; color:white;height:30px; width:100%;"><center><u>Download Budfolio for free!</u></center></a>
</div>
<div style="height: 30px; ">

<div class="backbutton" style="height:30px;padding-left:0 !important; width:98%; color:#06C;">
<a href="#" style="float: left; font-size:18px; text-decoration:none; color:#2C4E01; margin-left:1%;"> Budfolio</a>
<a href="budfolio://?st=<?php echo $strainId; ?>" style="float: right; font-size:18px;text-decoration:none; color:#2C4E01; margin-right:1%;"> Open in App!</a>
</div>

</div>
<?php    
}
?>
<?php

if(isset($strainId)&& !empty($strainId))
{
	$getStrainQuery="select * from strains where flag='Active'and strain_id='".$strainId."'";
	$getStrainSql=mysql_query($getStrainQuery);
	while($getStrainResult=mysql_fetch_array($getStrainSql))
	{
		$strain_images_preview_query="select * from strainimages 
									  where strain_id='".$strainId."'
									  AND image_name!=''
		 							  order by image_id asc LIMIT 0,3";
		$strain_preview_imgdata=mysql_query($strain_images_preview_query);
		$images_list_no=mysql_num_rows($strain_preview_imgdata);
		$img=array();
		$imarray=0;
		while($strain_preview_images_list=mysql_fetch_array($strain_preview_imgdata))
		{
			$img[$imarray]=$strain_preview_images_list['image_name'];
			$imarray++;
		}
		$exp_query="select * from experience where strain_id='".$straindetail['strain_id']."'";
		$exp_data=mysql_query($exp_query);
		$medicinaluse_query="select * from medicinal_use where strain_id='".$strainId."'";
		$medicinaluse_data=mysql_query($medicinaluse_query);


?>
<style>
a.edit_btn:hover
{
	font-size:18px;
}
.addinputDescription {
	margin: 0px;
	padding: 2px 5px 2px 5px;
	float: left;
	width: 99%;
	background: #fff;
	border: 1px solid #ccc;
	word-wrap: break-word;
	min-height: 27px;
}
</style>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container" id="inner_main_div">
 			<div class="cell_con">
              <div class="gray_headingbar">
              		<h2 ><center><?php echo str_replace("\\", "",stripslashes($getStrainResult['strain_name']));?></center></h2></div>
              	 <div id="content_1" class="content">
                 		<div class="add_container">
            <div class="celladd_popup">
                <div class="photo_sec">
                	<!--<h4 class="marbtm5 marlft5"> View Strain</h4>-->
                    <?php for($i=0;$i<3;$i++){
							if(empty($img[$i]))
							{
								echo '<div class="add_pic_popup"> <div class="add_pic_inner">  </div> </div>';
                     		}else {?>
                    			<div class="add_pic_popup"> <div class="add_pic_inner"><a class="fancybox" 
                                rel="gallery1" href="<?php echo $path;?>images/uploads/original/<?php echo $img[$i];?>"><img src="images/uploads/original/<?php echo $img[$i];?>" /></a> </div> </div>
                    <?php } }?>            
				</div>
			</div>
            <div class="content_1">
				<div class="celladd"> 
					<div class="add-con"> 
						<dl>
							<dt> Strain Name <br>
                                <span class="addinput"> 
                                    <input type="text" class="inputBox" value="<?php echo str_replace("\\", "",stripslashes($getStrainResult['strain_name']));?>"  disabled="disabled"/> 
                                </span>
							</dt>
                            <dd> Dispensary <br> 
                                <span class="addinput"> 
                              <?php 
							 if($getStrainResult['dispensary_id']==0)
							 {
								 	$id = str_replace("\\", "",stripslashes(getDispensaryId($getStrainResult['dispensary_name'])));
							 }else
							 {
									 $id = $getStrainResult['dispensary_id'];
							 }
							 if(!empty($id)){
                           ?>     
                                
                             <a href='dispensary_info_page.php?dispid=<?php echo $id;?>' target='_blank'>							                                  <input type="text" class="inputBox" value="<?php echo str_replace("\\", "",stripslashes($getStrainResult['dispensary_name']));?>" /></a>
                             <?php  }?>
                                </span> 
                            </dd>
						</dl>
						<dl>
							<dt> Species <br> 
                            	<span class="addinput"> 
                                	<?php if(!empty($getStrainResult['species'])){
												echo str_replace("\\", "",stripslashes($getStrainResult['species']));
									} ?> 
								</span> 
							</dt>
                            <dd> Lineage <br> 
                            	<span class="addinput"> 
                            	<input type="text" class="inputBox" value="<?php echo str_replace("\\", "",stripslashes($getStrainResult['linage']));?>" disabled="disabled"/></span></dd>
						</dl>
						<dl>
							<dt> Consumption Method <br> 
                            	<span class="addinput"> 
                                	<?php if(!empty($getStrainResult['consumption_name'])){
												echo str_replace("\\", "",stripslashes($getStrainResult['consumption_name']));
									} ?> 
								</span> 
							</dt>
							<dd>&nbsp; </dd>
						</dl>
					</div>
				</div>
				<div class="celladd">
					<h5 class="martop5 marlft5"> Bud Description </h5>
					<div class="add-con"> 
                        <dl>
                        	<dt> Smell <br> <span class="addinputDescription"> <?php if(!empty($getStrainResult['smell_rating'])){ echo str_replace("\\", "",stripslashes($getStrainResult['smell_rating']));} ?></span> </dt>
                        
                            <dd> Taste <br> <span class="addinputDescription"> <?php if(!empty($getStrainResult['taste_rate'])){	echo str_replace("\\", "",stripslashes($getStrainResult['taste_rate']));} ?></span> </dd>
                            </dl>
							<dl>
								<dt> Strength <br> <span class="addinput"><?php if(!empty($getStrainResult['strength_rate'])){	echo str_replace("\\", "",stripslashes($getStrainResult['strength_rate']));} ?></span> </dt>
								<dd> Rating <br> <span class="martop5 fllft">
									<?php for($r=1;$r<6;$r++){
                            			if($getStrainResult['overall_rating']>=$r)
										{
											echo '<img src="images/css_images/selstar_big.png" width="27" height="27"> ';		}else{
											echo '<img src="images/css_images/dselstar_big.png" width="25" height="27">';							}	
                            		}
								?> </span> </dd>
							</dl>
						</div>
				</div>
                <div class="celladd">
                    <h5 class="martop5 marlft5">  Experiences</h5>
                   <span id="experience" style="padding-top:5px;font-size: 13px;
font-family: 'Gotham-Book';color: #757575; 
font-weight: normal;word-wrap: break-word;"> 
                     <?php 
						$exp_query="select * from experience 
						where strain_id='".$getStrainResult['strain_id']."'";
						$exp_data = mysql_query($exp_query);
						$i=0;
						$len=mysql_num_rows($exp_data);
						while($exp_list = mysql_fetch_assoc($exp_data))
						{	if($i==$len-1){
								$exp_value .= str_replace("\\", "",stripslashes($exp_list['experience_name']));		
							}else
							{
								$exp_value .= str_replace("\\", "",stripslashes($exp_list['experience_name'])).",";
							}
							$i++;
						}	echo str_replace("\\", "",stripslashes($exp_value));		?> 
						</span> 
                    
                </div>
                <div class="celladd">
                    <h5 class="martop5 marlft5">  Medicinal Uses</h5>
                    <span style="padding-top:5pxfont-size: 13px;
font-family: 'Gotham-Book';color: #757575;
font-weight: normal;"> 
                                <?php	
								$medicinaluse_query = "select * from medicinal_use 
													   where strain_id='".$getStrainResult['strain_id']."'";
							
								$medicinaluse_data = mysql_query($medicinaluse_query);
								$i=0;
								
								$len=mysql_num_rows($medicinaluse_data);
								while($medicinaluse_list = mysql_fetch_array($medicinaluse_data))
								{
									if($i==$len-1){
										$medi_name .= str_replace("\\", "",stripslashes($medicinaluse_list['medicinal_type']));
									}else
									{
										$medi_name .= str_replace("\\", "",stripslashes($medicinaluse_list['medicinal_type'])).",";
									}$i++;
								}	
								echo str_replace("\\", "",stripslashes($medi_name)); ?>	
							</span>
                </div>
                <div class="add-con" id="testedbyinput"> 
                    <dl>
                        <dt> Tested By <br> <span class="addinput"> 
                        <input type="text" class="inputBox" value="<?php echo str_replace("\\", "",stripslashes($getStrainResult['tested_by']));?>" disabled="disabled" /></span> </dt>
                        <dd> THC <br> <span class="addinput"><?php echo $getStrainResult['thc'];
						if(!empty($getStrainResult['thc'])){?>%<?php } ?></span> </dd>
                    </dl>
                    <dl>
                        <dt> CBD <br> <span class="addinput"> <?php echo $getStrainResult['cbd'];
						if(!empty($getStrainResult['cbd'])){?>%<?php } ?></span> </dt>
                        <dd> CBN <br> <span class="addinput"><?php echo $getStrainResult['cbn'];
						if(!empty($getStrainResult['cbn'])){?>%<?php } ?></span> </dd>
                    </dl>
                    <dl>
                        <dt> THCa <br> <span class="addinput"> <?php echo $getStrainResult['thca'];
						if(!empty($getStrainResult['thca'])){?>%<?php } ?></span></dt>
                        <dd> CBHa <br> <span class="addinput"><?php echo $getStrainResult['cbha'];
						if(!empty($getStrainResult['cbha'])){?>%<?php } ?></span> </dd>
                    </dl>
                    <dl>
                        <dt> Moisture <br> <span class="addinput"><?php echo $getStrainResult['moisture'];
						if(!empty($getStrainResult['moisture'])){?>%<?php } ?></span></dt>
                    </dl>
                </div>
                <div class="celladd">
					<h5 class="martop5 marlft5">Description </h5>
					<div class="add-con"> 
                        <dl class="strpopupdesc"><?php echo str_replace("\\", "",stripslashes($getStrainResult['description']));?></dl>
					</div>
				</div>
			</div>
		</div>
        </div>
        </div>
        </div>
	
    </div>
</div>

<?php 
	}
 }
	?>
</body>
</html>