<?php
//add strain.php bach up budfolio
// Include header,auth,transection and constant array file.  
include_once("newHeader.php");
require_once("auth.php");
include_once("strain_transection.php");
include_once("values_array.php");
$_SESSION['images']=array();
$_SESSION['primaeyimages']='';
?>
<div class="inner-con">
	<?php  include_once("left_sidebar.php");?>
    <div class="innercon_centeradd">
    	<span id="strainameval" class="error_msg_all red"></span>
    	<div class="add_container">
        	<?php 
				// Status or error messages.
				if(isset($_GET['strainstatus']))
				{
					if($_GET['strainstatus']=='added')
					{
						echo '<span class="error_msg_all fadinmsg green"> Strain added successfully.</span>';
					}
					if($_GET['strainstatus']=='notadded')
					{
						echo '<span class="error_msg_all fadinmsg red">Strain not added. Please try again.</span>';
					}
				}
				// End messages
			?>
            <div class="celladd">
            	<div class="photo_sec addrev">
                	<h4 class="marbtm5 marlft5"> Add New Strain</h4>
                    <div class="choosemsg"><label>Choose primary image</label></div>
                    <div id="previewwrapper" class="comman"><div id="preview" class="add_pic"><img src="images/css_images/ad_img.png" /></div></div>
                    <div class="straim_images"><?php include_once("show_strain_images.php"); ?></div>
                    <a href="javascript:void(0);" id="addstrainimagedilogbox" class="photo_btn"> Add Photo  </a>
					<form id="imageform" method="post" enctype="multipart/form-data" action='ajaximage.php'>
						<input class="hideme" type="file" name="photoimg" id="photoimgadd" />
					</form>
                </div>
		<form method="post" name="strainopration" id="strainopration" onsubmit="return add_strain_validation();">
                <div id="primaryradionwapper">
                    	<span class="spone"><input type="radio" value="0" name="primaeryimg" id="primaeryimgone" /></span>
                        <span class="sptwo"><input type="radio" value="1" name="primaeryimg" id="primaeryimgtwo" /></span>
                        <span class="spthree"><input type="radio" value="2" name="primaeryimg" id="primaeryimgtwo" /></span>
                	<div id="sharebtns">
                    	<?php include_once("fb.html"); ?>
                        <?php include_once("twitter.php"); ?>
                    </div>         
                </div>
                <div class="add-con"><!-- Add strain form srat-->
       				<dl>
						<dt> <span class="mdred">*</span>Strain Name <br/><span class="addinput"> <input type="text" placeholder="Enter Strain Name" name="strainname" id="strainname" maxlength="55"> </span></dt>
						<dd> Dispensary <br/> <span class="addinput"> <input type="text" placeholder="Enter Dispensary" name="dispensensary" id="dispensensary" maxlength="55"> </span> </dd>
					</dl>
                    <dl>
                    	<dt> Species <br><span class="addinput"> 
                        	<select name="species" id="species">
                            	<option value="">Please select species</option> 
                                <?php	for($i=0;count($species)>$i;$i++)
										{
											echo '<option value="'.$species[$i].'">'.$species[$i].'</option>';
										}
								?>	
							</select></span> 
						</dt>
                        <dd> Lineage <br>
                        	<span class="addinput">
                        		<input type="text" placeholder="Enter Lineage"  name="linage" id="linage"  maxlength="55"> 
							</span>
						</dd>
					</dl>
                    <dl>
                        <dt> Consumption Method <br> 
                        	<span class="addinput"> 
                                <select name="consumption" id="consumption">
                                	<option value="">Please select consumption</option> 
                                	<?php for($i=0;count($consumption)>$i;$i++)
										{
									echo '<option value="'.$consumption[$i].'">'.$consumption[$i].'</option>';
										}
									?>
                                </select> 
                        	</span> 
						</dt>
                        <dd>&nbsp; </dd>
                    </dl>
				</div>
			</div>
            <div class="celladd">
                <h5 class="martop5 marlft5"> Tell us about your Bud </h5>
                <div class="add-con"> 
                    <dl>
                        <dt> Smell <br> <span class="addinput"> 
                            <select name="smell" id="smell">
                            	<option value="">Please select smell</option> 
                                <?php for($i=0;count($smell)>$i;$i++){
                                    echo '<option value="'.$smell[$i].'">'.$smell[$i].'</option>';
                                }?>
                            </select> </span> </dt>
                            
                        <dd> Taste <br> <span class="addinput"> 
                            <select name="taste" id="taste">
                            	<option value="">Please select taste</option>  
                                <?php for($i=0;count($taste)>$i;$i++){
                                    echo '<option value="'.$taste[$i].'">'.$taste[$i].'</option>';
                                }?>
                            </select> </span> </dd>
                    </dl>
                    <dl>
                    <dt> Strength <br> <span class="addinput">
                        <select name="strength" id="strength"> 
                        	<option value="">Please select strength</option> 
                            <?php	for($i=0;count($strength)>$i;$i++){
                                echo '<option value="'.$strength[$i].'">'.$strength[$i].'</option>';
                            }?>
                        </select> </span> </dt>
                    <dd> Rating <br>
                        <span class="martop5 fllft"> 
                            <div class="rating_wrapp">
                                <?php for($r=1;$r<6;$r++)
                                        { 
                                            if($straindetail['overall_rating']>=$r)
                                            {
                                                echo '<a href="javascript:void(0);" class="ratthis rated" id="ratting_'.$r.'">&nbsp;</a>';
                                            }
                                            else
                                            {
                                                echo '<a href="javascript:void(0);" class="ratthis" id="ratting_'.$r.'">&nbsp;</a>';
                                            }	
                                        }?>
                            </div>
                             <input type="hidden" name="overall" id="overall" value="<?php echo $straindetail['overall_rating'];?>">
                        </span>
                    </dd>
                </dl>
                </div>
            </div>
            <div class="celladd">
                <h5 class="martop5 marlft5"> Experiences </h5>
                <div class="add-con">
                    <span class="addselect exchecbox">
                        <!--<select name="experience[]" multiple="multiple"> 
                            <?php for($i=0;count($experience)>$i;$i++){
                                echo '<option value="'.$experience[$i].'">'.$experience[$i].'</option>';
                            }?>
                        </select> -->
                        <?php
						for($i=0;count($experience)>$i;$i++)
						{
							echo '<div><input type="checkbox" name="experience[]" value="'.$experience[$i].'">';
							echo '<label>'.$experience[$i].'</label></div>';
						}
						?>
                    </span>
                </div>
            </div>
            <div class="celladd">
                <h5 class="martop5 marlft5">  Medicinal Uses</h5>
                <div class="add-con"> 
                    <span class="addselect musechbox"> 
                        <!--<select multiple="multiple" name="medicinaluse[]"> 
                            <?php for($i=0;count($medicinaluse)>$i;$i++)
                                  {
                                     echo '<option value="'.$medicinaluse[$i].'">'.$medicinaluse[$i].'</option>';
                                  }
                            ?>		
                        </select> -->
                        <?php
                        for($i=0;count($medicinaluse)>$i;$i++)
                        {
                        echo '<div>';
							echo '<input type="checkbox" name="medicinaluse[]" value="'.$medicinaluse[$i].'">';
                        	echo '<label>'.$medicinaluse[$i].'</label>';
						echo '</div>';
						}	 
                        ?>
                    </span>    
                </div>
            </div>
            <div class="celladd">
                <h5 class="martop5 marlft5"> Lab Tested </h5>
                    <div class="add-con" id="testedbyinput"> 
                    <dl>
                        <dt> Tested By <br>
                        	<span class="addinput"> 
                        	<input type="text" placeholder="Enter Tested By" name="testedby" id="testedby" maxlength="55"/></span> 
                        </dt>
                        <dd> THC <br> 
                        	<span class="addinput">
                            <input placeholder="Enter THC" type="text" name="thc" id="thc" maxlength="5"/>%</span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> CBD <br> 
                        	<span class="addinput"> 
                        	<input placeholder="Enter CBD" type="text" name="cbd" id="cbd" maxlength="5"/>%</span> 
                        </dt>
                        <dd> CBN <br> 
                        	<span class="addinput">
                            	<input placeholder="Enter CBN" type="text" name="cbd" id="cbd" maxlength="5"/>
                                %</span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> THCa <br> 
                        <span class="addinput"> 
                        	<input placeholder="Enter THCa" type="text" name="tcha" id="tcha" maxlength="5"/>
                        %</span></dt>
                        <dd> CBHa <br> 
                        	<span class="addinput">
                            	<input placeholder="Enter CBHa" type="text" name="cbha" id="cbha" maxlength="5"/>
                                %</span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> Moisture <br> <span class="addinput"> <input placeholder="Enter Moisture" type="text" name="moisture" id="moisture" maxlength="5"/>%</span></dt>
                    </dl>
                </div>
            </div>
            <div class="celladd">
                <h5 class="martop5 marlft5"> Description</h5>
                <div class="add-con"> 
                    <textarea placeholder="Enter Description" rows="3" cols="57" maxlength='1000' name="description" id="description"></textarea>
                        <div class="dbox_msg">Max 1000 characters.</div>  
                </div>
            </div>
            <div class="add_top"> 
                <a href="javascript:void(0);" id="addstrainsubmit" class="edit_btn addstrainsubmit"><span class="fllft padrit4"> <img src="images/css_images/add_btn.png" width="20" height="20" > </span> <span class="fllft"> Add Strain </span> </a>  
                <input type="hidden" name="add_strain" id="add_strain" value="add strain" />
            </div>
            <!--<div class="add_top">
            	<input type="submit" name="addStrain" value="Add Strain" class="addstrainbtn" />
			</div>-->
            </form>        
		</div>
	</div>
	<!-- Inlude right sidebar--><?php include_once("right_sidebar.php");?><!-- End right sidebar-->
</div>
<?php include_once("footer.php");?>
<script>
$(".addstrainsubmit").click(function(){
	$("#addstrainsubmit").removeClass("addstrainsubmit");
	$("#strainopration").submit();
});
</script>