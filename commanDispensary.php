<?php 
session_start();

$FWPflag = getFollowUpList($menudata["dispensary_id"],$_SESSION['user_id']);
$latLong = getLatLong();

?>

<div class="dispensary_cell" id="d_<?php echo $menudata['dispensary_id'];?>">
	<div class="blank_bg_cell" > 
    	 <span class="name_link"> 
    <a style="min-width:63%;" <?php if(!empty($_SESSION['user_id'])){ //class="dispensary-popup"?>  href="dispensary_info_page.php?dispid=<?php echo $menudata['dispensary_id'];?>" <?php }else{?> class="alertRequest" <?php } ?> id="menudisp_<?php echo $menudata['dispensary_id']; ?>"><?php echo stripslashes($menudata['dispensary_name']); ?></a>	
     </span>
     	 <span style="font-size:18px;"> <?php echo ceil($menudata['distance']); ?> miles away </span>
     	 <span class="dispensary_followbtn" id="disMF_<?php echo $menudata["dispensary_id"];?>">
    <?php if($FWPflag==0){?>
    <a href="javascript:void(0);" <?php if(!empty($_SESSION['user_id'])){?> class="disMenufolloup-on" <?php }else{ ?> class="alertRequest" <?php } ?> id="followupid_<?php echo $menudata["dispensary_id"];?>"> 
    <img src="images/new_web/follow_green_btn.png" /> </a> 
    <?php }else{
		?><a href="javascript:void(0);" <?php if(!empty($_SESSION['user_id'])){?> class="disMenufolloup-off" <?php }else{?> class="alertRequest" <?php } ?> id="followupid_<?php echo $menudata["dispensary_id"];?>"><img src="images/new_web/unfollow_btn.png" /></a><?php } ?></span>
     </div>
   
    <div style="clear:both"></div>
	<!--Div for street address-->
    <div class="dis_iconcon showThis countyname" style=" border-bottom: 1px solid #CCCCCC;border-top: 1px solid #CCCCCC;  float: left; height: 35px;  margin: 10px 0 0 -14px;    padding: 6px 0 0;    text-align: center;    width: 104%;">
    	<?php $showaddress = stripcslashes($menudata['street_address']);
			  if($showaddress!='')
			  {
			  		   $showaddress .=" ".stripcslashes($menudata['city']);
			  }else
			  {
				       $showaddress = stripcslashes($menudata['city']);
			  }
			  if($showaddress!='')
			  {
			   		   $showaddress .=", ".stripcslashes($menudata['state']);
              }else
			  {
				  		 $showaddress .=stripcslashes($menudata['state']);
			  }
              if($showaddress!='')
			  {
    		           $showaddress .=($menudata['zip']!=''||$menudata['zip']!='na')?
					   ", ".$menudata['zip']:'';
			  }
			  echo $showaddress;
    ?>
    </div>
    <!-- end of street address-->
    
    <div style="clear:both"></div>
   
    <div class="dispensary_img">
    <?php 
		$showH=138;$showW=655;
		
		if(empty($menudata['image']))
		{
			$img ='images/css_images/imgbase.png';
		}else
		{
			$img ='images/dispensary_images/original/'.$menudata['image'];
		}
	
		list($width, $height, $type, $attr) = getimagesize($img);
		
		if($height <= $showH){
			$marginTB = ($showH-$height) /2;//setting margin top bottom 
		}else{
			$marginTB =0;
		}
		
		if($width <= $showW){
			$marginLR = ($showW-$width)/2;
		}else
		{
			$marginLR =0;
		}
		
	
	?>
    <img  src="<?php echo $img;?>">
    </div>
    
    <!-- edit box of bio-->
    <div class="dis_iconcon showThis" style="min-height: 150px;margin-top: 10px;padding-left: 8px;width: 55%;word-wrap:break-word;"><?php echo atRateUserNameWeb(stripcslashes($menudata['bio']));?></div>
    <!-- edit box of bio-->
	 <div style="clear:both"></div>
   
    
   <?php // 27th aug
	$settingRows=0;
		if(!empty($_SESSION['dispansary']) || !empty($_SESSION['user_id']))
		{
			
			if($menudata['added_by']!='0'){	

		  		 $strGetSettingsSQL ="SELECT * 
				 					  FROM dispensary_settings 
									  WHERE 
									  	dispensary_id='".$menudata["dispensary_id"]."'";
				 $resSettings = mysql_query($strGetSettingsSQL)	 or die ($strGetSettingsSQL." : ".mysql_error());
			     $settingsData = mysql_fetch_assoc($resSettings);
			     $settingRows = mysql_num_rows($resSettings);
			     if($settingRows>0){
		 ?>
    	<div class="showThis" style="float: right;margin-top: 0px;width: 60%;text-align:center;">
        
        <?php if($settingsData['DeliveryService']=='1'){?>
        	<img src="images/new_web/BF_shiping_icon (2).png" />&nbsp;
        <?php }?>
        <?php if($settingsData['StoreFront']=='1'){?>
        	<img src="images/new_web/BF_shope_icon (2).png" />&nbsp;
        <?php }?>
        <?php if($settingsData['AcceptCreditCard']=='1'){?> 
        	<img src="images/new_web/BF_debitcard_icon (2).png" />&nbsp;
        <?php }?> 
        <?php if($settingsData['AcceptATMonSite']=='1'){?>
        	<img src="images/new_web/BF_atmcard_icon (2).png" />&nbsp;
        <?php }?>
        <?php if($settingsData['18YearsOld']=='1'){?>
        	<img src="images/new_web/BF_18year_icon (2).png" />&nbsp;
        <?php }?>
      	<?php if($settingsData['21YearsOld']=='1'){?>
        	<img src="images/new_web/BF_21year_icon (2).png" />&nbsp;
        <?php }?>
       	<?php if($settingsData['HandicapAssesseble']=='1'){?>
        	<img src="images/new_web/BF_handicap_icon (2).png" />&nbsp;
        <?php }?> 
        <?php if($settingsData['Security']=='1'){?>
        	<img src="images/new_web/BF_security_icon (2).png" />&nbsp;
        <?php }?>
        
    	</div>
        
	<?php	 }// settingRows>0
			} // end of 	if($menudata['added_by']!='0'){
		} 
	?>
        
    <!-- div for edit-delete and claim -->
     <div class="dis_bottom_cell" style="float:left;margin-left: -13px;width: 102%;padding:5px;">
     <?php if(!empty($_SESSION['dispansary']) )
		   {
			   
			 if($_SESSION['user_id'] == $menudata['added_by'])
			 {
				 
				 //if logged in user is owner of ths profile then check the subscription 
				 //on which its purchased id running or expired
				$getTheSubOFDisSQl = "SELECT id,
											 tpm.purchase_id AS sub_id,
											 tpm.user_id as user_id,
											 tpm.plan_type AS subscription_type,
											 start_date,
											 expiry_date,
											 x.status as status,
											 x. occurence as occurence,
											 tpm.recurrning as isRecurring
									 FROM tbl_menu_purchase AS tpm
								 	 JOIN purchased_menu_table x ON tpm.purchase_id = x.purchase_id 
									 WHERE x.user_id='".$_SESSION['user_id']."'
									 AND dispensary_id='".$menudata["dispensary_id"]."' 
									 AND x.status='active'
									 LIMIT 0,1";
				$exeTheSubOFDisSQl = mysql_query($getTheSubOFDisSQl) or die(mysql_error()." : ".$getTheSubOFDisSQl);
				
				$dispSubs = mysql_fetch_assoc($exeTheSubOFDisSQl);
				$isRecurringBYDb = $dispSubs['isRecurring'];
				$planType = $dispSubs['subscription_type'];	
				if ($dispSubs['status']=='active'){
					
					//$planDate = $dispSubs['start_date'];	
					$currentDate =strtotime(date('Y-m-d h:i:s'));
					$expiryDateOfSubscription=strtotime($dispSubs['expiry_date']);
				}
				/*if ($dispSubs['status']=='active')
				{
					$isRecurringBYDb = $dispSubs['isRecurring'];
					$planType = $dispSubs['subscription_type'];	
					$planDate = $dispSubs['start_date'];	
					$currentTime =time();	
					$planStartTimeInSec = strtotime($dispSubs['start_date']);
					
					if($planType=='a')// for 1 year subscription
					{
						$deductFromDays =365;
						$lastDateOfSubscription = $planStartTimeInSec+365*24*60*60;
					}
					elseif($planType=='h')// for 1/2 year subscription
					{
						$deductFromDays =182;
						$expiryDateOfSubscription = $planStartTimeInSec+182*24*60*60;
					}else if($planType=='m')	// for 1 months subscription
					{
						$deductFromDays =30;
						$expiryDateOfSubscription = $planStartTimeInSec+30*24*60*60;
					}else if($planType=='d') // after registration 14 days trail
					{
						$deductFromDays =14;
						$expiryDateOfSubscription = $planStartTimeInSec+14*24*60*60;
					}else if($planType=='0'||$planType=='')
					{
						
						//$expiryDateOfSubscription = $planStartTimeInSec+strtotime($dispSubs['expiry_date']);
						$expiryDateOfSubscription = strtotime($dispSubs['expiry_date']);
					}
				    $timeRemainingToExpire = $expiryDateOfSubscription-$currentTime;
				}*/
				
				$startDt= explode(" ",$dispSubs['start_date']);
				$sdt=explode("-",$startDt[0]);
				$StartDate=date ("M j, Y", mktime (0,0,0,$sdt[1],$sdt[2],$sdt[0]));
				
				
                $expiry=explode(" ",$dispSubs['expiry_date']);
				$dt=explode("-",$expiry[0]);
				$expiryDate=date ("M j, Y", mktime (0,0,0,$dt[1],$dt[2],$dt[0]));
			?>
			 
                 <span class="editicon_btn showThis"> <a <?php if($expiryDateOfSubscription > $currentDate){?> href="edit_dispensary.php?disId=<?php echo $menudata["dispensary_id"];?>"  class="Menu_dis_edit" id="EditDis_<?php echo $menudata["dispensary_id"];?>" <?php  }else{?> onclick="alert('Please upgrade your subscription to access the menu');" <?php } ?>> <img src="images/new_web/edit_btn.png"> Edit</a> </span>             
                 
       			 <span class="deleteicon_btn showThis" ><a href="javascript:void(0);" <?php if($expiryDateOfSubscription > $currentDate){?> class="Menu_dis_delete" id="deleteDisId_<?php echo $menudata["dispensary_id"];?>" <?php  }else{?> onclick="alert('Please upgrade your subscription to access the menu');" <?php } ?>> <img src="images/new_web/delete_btn.png"></a> </span>	
                 
                 
 <span class="claim_btn" id="cld_<?php echo $_SESSION['dispansary']; ?>_<?php echo $menudata["dispensary_id"];?>"><a href="javascript:void(0);" <?php if($expiryDateOfSubscription > $currentDate){?>  id="<?php echo $_SESSION['dispansary'] ?>_<?php echo $menudata["dispensary_id"];?>_yes" class="claim_profile" <?php  }else{?> onclick="alert('Please upgrade your subscription to access the menu');" <?php } ?> ><img src="images/new_web/unclaim_profile_btn.png" class="scale-with-grid" ></a></span> 
 
            
      <?php }// end of if($_SESSION['user_id']==$menudata['added_by'])
			 else if(($_SESSION['user_id'] != $menudata['added_by']) and $menudata['added_by']=='0' )
			 {	 //echo "1"; 
	  			if($expiryDateOfSubscription < $currentDate)// if any type of subscription is expired
				{
					//echo "2";
				?>	
                	<span class="claim_btn" id="cld_<?php echo $_SESSION['dispansary']; ?>_<?php echo $menudata["dispensary_id"];?>"> 						<a href="javascript:void(0);" id="<?php echo $_SESSION['dispansary'] ?>_<?php echo $menudata["dispensary_id"];?>_no" onClick="alert('Please upgrade your subscription to access the menu');"><img src="images/new_web/claim_profile_btn.png"  class="scale-with-grid" > </a> 
                    </span>
	  	  <?php }
		  		else// if any type of subscription is running
				{//echo "3";
				?>	
                <span class="claim_btn" id="cld_<?php echo $_SESSION['dispansary']; ?>_<?php echo $menudata["dispensary_id"];?>"> 					<a href="javascript:void(0);" id="<?php echo $_SESSION['dispansary'] ?>_<?php echo $menudata["dispensary_id"];?>_no" class="claim_profile claim_this"><img src="images/new_web/claim_profile_btn.png" class="scale-with-grid" > 
               </a> </span>
               
		<?php }
			}// end of else if(($_SESSION['user_id'] != $menudata['added_by']) and $menudata['added_by']=='0' )
			 else if(($_SESSION['user_id'] != $menudata['added_by']) and $menudata['added_by']!='0' )
			 {?>
            	<span class="claim_btn" id="cld_<?php echo $_SESSION['dispansary']; ?>_<?php echo $menudata["dispensary_id"];?>"> 
             	<a href="javascript:void(0);" onclick="showClaimedAlert();" id="<?php echo $_SESSION['dispansary'] ?>_<?php echo $menudata["dispensary_id"];?>_no"><img src="images/new_web/claimed_btn.png" class="scale-with-grid" ></a></span>
	 <?php } ?> 
	 
       
      	<?php }//end of if(!empty($_SESSION['dispansary']) ) ?>
      
      <?php if(empty($_SESSION['user_id']) )
		{
		 ?>	<span class="claim_btn"  
            id="cld_<?php echo $_SESSION['dispansary']; ?>_<?php echo $menudata["dispensary_id"];?>"> 
            <?php if($menudata['added_by']!='0'){
			?>
			  <a href="javascript:void(0);" onclick="showClaimedAlert();"
          	   id="<?php echo $_SESSION['dispansary'] ?>_<?php echo $menudata["dispensary_id"];?>_no">
                 <img src="images/new_web/claimed_btn.png" class="scale-with-grid" > 
               </a>
  <?php }else{?>
           	 <a href="javascript:void(0);" 
          		id="<?php echo $_SESSION['dispansary'] ?>_<?php echo $menudata["dispensary_id"];?>_no" 
                class="claim_profile">
                <img src="images/new_web/claim_profile_btn.png" class="scale-with-grid" > 
             </a>
	<?php   } ?>
			 </span> 
	<?php   }
	?>
    </div>
    <!-- end of div for edit-delete and claim -->
   <?php  if(!empty($_SESSION['dispansary']) )
		   {
			 if($_SESSION['user_id'] == $menudata['added_by'])
			 {
				 ?>
    <div class="dis_bottom_cell" style="float:left;margin-left: -13px;width: 102%;padding:5px;">
     <span class="editicon_btn showThis" style="float:left;color:#2B6A03;margin-top:5px;margin-left: 2%;">Start Date : <?php echo $StartDate;?></span>
                <span class="editicon_btn showThis" style="float:left;color:#2B6A03;margin-top:5px;margin-left: 2%;">&nbsp; |&nbsp; End Date : <?php echo $expiryDate;?></span>
                 <span class="editicon_btn showThis" style="float:left;color:#2B6A03;margin-top:5px;margin-left: 2%;"> &nbsp;|&nbsp; <a href="check_out.php"  style="font-size:16px;color:#2B6A03;"> Upgrade </a></span>
                   
               <span class="editicon_btn showThis" style="float:left;color:#2B6A03;margin-top:3px;margin-left: 0%;"> &nbsp;|&nbsp;
				  <?php if($planType!='d'){
				  			if($isRecurringBYDb=='true'){ ?>
                            <input id="mail_<?php echo $dispSubs['sub_id']; ?>" type="button" onclick="sendingMailToAdmin(this.id)" value="Stop Recurring" style="font-size:16px;color:#2B6A03;width:85%;"/>
						<?php }
                  			else if($isRecurringBYDb=='false'){ ?>
                            <input id="mail_<?php echo $dispSubs['sub_id']; ?>" onclick="sendingMailToAdmin(this.id)" type="button" value="Start Recurring" style="font-size:16px;color:#2B6A03;width:85%;"/> 
						<?php }
				}	 else{ ?>
							<input id="mail_<?php echo $dispSubs['sub_id']; ?>" onclick="sendingMailToAdmin(this.id)" type="button" value="Start Recurring" style="font-size:16px;color:#2B6A03;width:85%;" disabled="disabled" />                         	
				<?php } ?>
				  </span>
              
    </div>
    <?php }
		   }?>
</div>