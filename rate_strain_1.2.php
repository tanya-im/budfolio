<?php
include_once("config.php");
include_once("function.php");
include_once("values_array.php");

 $strain_query ="select * from strains 
				     where strain_id ='". $_GET['strain_id']."'";
	$straindata = mysql_query($strain_query);
$numrows = mysql_num_rows($straindata);
$straindetail = mysql_fetch_assoc($straindata);
/*print_r($straindetail);*/
$smellArray = explode(',',$straindetail['smell_rating']);

$tasteArray =explode(',',$straindetail['taste_rate']);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
 <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/scroller.css" >
<link rel="stylesheet" href="css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel= "stylesheet" type="text/css" href="css/jquery.editable-select.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<style>
.cell_dispensary
{
	width:82%;
}
.martop_btm2 
{
	padding-left:5%;
}
.content
{
	height:1000px;
}
.percent_sign
{
	float:right;
	margin-top:-18px;
}
</style>

</head>

<body ><div class="inner_conlft" style="width:95%;">
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2>Add Strain Rating</h2>  </div>
                <div id="content_1" class="content" style="min-height:1070px;">
                	<div class="edit_dis_con">
                    <span id="strainameval" class="error_msg_all red"></span>
                       
                       
						<form method="post" name="strainopration" id="strainopration" action="add_rating.php">  
							
                    	<div class="addstraincon martop_btm2">
                        	<div class="headtext"> Tell us about your Bud </div>
                             <dl>
                               <dt> Smell <br /> <span class="select_wrap"> <select name="smell[]" id="smell" multiple="multiple" class="select_field">
                                 <?php for($i=0;count($smell)>$i;$i++){
									 
										if(in_array($smell[$i],$smellArray)){
												echo '<option selected="selected" value="'.$smell[$i].'">'.$smell[$i].'</option>';						}else{
													
												echo '<option value="'.$smell[$i].'">'.$smell[$i].'</option>';							
										}
										}
								?>
                            </select>  </span> </dt>
                                 <dd> Taste <br /> 
                                <span class="select_wrap"> 
                                	<select name="taste[]" id="taste" multiple="multiple" class="select_field">
                                <?php for($i=0;count($taste)>$i;$i++){
											if(in_array($taste[$i],$tasteArray))
											{
												echo '<option selected="selected" value="'.$taste[$i].'">'.$taste[$i].'</option>';							}else{
												echo '<option value="'.$taste[$i].'">'.$taste[$i].'</option>';
											}
										}
								?>
                            </select> </span> </dt>
                             </dl>
                             
                             <dl>
                                <dt> Strength  <span class="adddisinput"> <select name="strength" id="strength"> 
                        	<option value="">Please select strength</option>
                          <?php	for($i=0;$i<count($strength);$i++)
								{
									if($straindetail['strength_rate']==$strength[$i]){
											echo '<option selected="selected" value="'.$strength[$i].'">'.$strength[$i].'</option>';				}else{
											echo '<option value="'.$strength[$i].'">'.$strength[$i].'</option>';
										}
									}
							?>
                        </select>  </span> </dt>
                                    
                                <dd> Rating  <br> <span class="martop_btm2 floatlft"><div class="rating_wrapp">
                                <?php for($r=1;$r<6;$r++){ 
                            			if($straindetail['overall_rating']>=$r)
										{
											echo '<a href="javascript:void(0);" class="ratthis rated" id="ratting_'.$r.'">&nbsp;</a>';		}else{
											echo '<a href="javascript:void(0);" class="ratthis" id="ratting_'.$r.'">&nbsp;</a>';							}	
                            		}
								?>
                            </div>
                             <input type="hidden" name="overall" id="overall" value="<?php echo $straindetail['overall_rating'];?>"> </span> </dd>
                             </dl>
                        </div>
                        
                        
                        <!--start of profile type-->
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        <div class="addstraincon martop_btm2">
                        <div class="headtext marbtm10"> Profile Type  </div>
                          <span class="adddisinput"> 
                        	<select name="profiletype" id="profileType" >
                            <option value="">Please select profile type</option> 
                            	<?php	for($i=0;count($profileTYpe)>$i;$i++)
										{
											if($straindetail['profile_type']==$profileTYpe[$i])
											{
											echo '<option  selected="selected" value="'.$profileTYpe[$i].'">'.$profileTYpe[$i].'</option>';									}
											else
											{
												echo '<option value="'.$profileTYpe[$i].'">'.$profileTYpe[$i].'</option>';
											}
										}
								?>	
							</select>  <!--</span>-->
                          </span>
                        </div>
                        <!--end  of profile type-->
                        
                        
                        
                        
                        
                        
                        
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        <div class="addstraincon martop_btm2">
                        <div class="headtext marbtm10"> Experiences </div>
                          <div  class="select_wrap" >
                        	<select name="experience[]" id="experience"  multiple="multiple" class="select_field">
                            	<?php	
								$getExpSQL="SELECT * FROM experience 
								WHERE strain_id='".$straindetail['strain_id']."'";
								$resEXp = mysql_query($getExpSQL);
								$arrayExp= array();
								while($exp=mysql_fetch_assoc($resEXp))
								{
										$arrayExp[]=$exp['experience_name'];
                                }
								for($i=0;count($experience)>$i;$i++)
										{
											if(in_array($experience[$i],$arrayExp)){
											echo '<option selected="selected" value="'.$experience[$i].'">'.$experience[$i].'</option>';				}else{
											echo '<option value="'.$experience[$i].'">'.$experience[$i].'</option>';											}
											
										}
		      		?>	
							</select>  <!--</span>-->
                          </div>
                        </div>
                        
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        <div class="addstraincon martop_btm2">
                        	<div class="headtext marbtm10"> Medicinal Uses </div>
                            	<div class="select_wrap">
                                  <select name="medicinaluse[]" id="medicinaluse" class="select_field" multiple="multiple">							
								  <?php	
								$medicinaluse_query ="select * from medicinal_use 
													  where strain_id='".$straindetail['strain_id']."'";
								$medicinaluse_data = mysql_query($medicinaluse_query);
								$arrMedi= array();
								while($medi=mysql_fetch_array($medicinaluse_data))
								{
											
									$arrMedi[]=$medi['medicinal_type'];
							  	}
									for($i=0;count($medicinaluse)>$i;$i++)
									{
											if(in_array($medicinaluse[$i],$arrMedi)){
											echo '<option selected="selected" value="'.$medicinaluse[$i].'">'.$medicinaluse[$i].'</option>';				}else{
											echo '<option value="'.$medicinaluse[$i].'">'.$medicinaluse[$i].'</option>';}
									}
								?>	
									</select>  
                            </div>
                        </div>
                        <div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div>
                        
                        
               <div class="addstraincon martop_btm2">
               <div class="headtext marbtm10"> Lab Tested </div>
                    <div class="add-con" id="testedbyinput"> 
                    <dl>
                        <dt> Tested By <br>
                        	<span class="adddisinput"> 
                        	<input type="text" placeholder="Enter Tested By" name="testedby" id="testedby"  
                            value="<?php echo $straindetail['tested_by'];?>" maxlength="55"/></span> 
                        </dt>
                        <dd> THC <br> 
                        	<span class="adddisinput">
                            <input placeholder="Enter THC" type="text" name="thc" id="thc" value="<?php echo $straindetail['thc'];?>" maxlength="5"/><span class="percent_sign"> %</span></span>
                        </dd>
                    </dl>
                    <dl>
                        <dt> CBD <br> 
                        	<span class="adddisinput"> 
                        	<input placeholder="Enter CBD" type="text" name="cbd" id="cbd"  value="<?php echo $straindetail['cbd'];?>" maxlength="5"/><span class="percent_sign"> %</span></span> 
                        </dt>
                        <dd> CBN <br> 
                        	<span class="adddisinput">
                            	<input placeholder="Enter CBN" type="text" name="cbn" id="cbn" value="<?php echo $straindetail['cbn'];?>" maxlength="5"/>
                                <span class="percent_sign"> %</span></span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> THCa <br> 
                        <span class="adddisinput"> 
                        	<input placeholder="Enter THCa" type="text" name="tcha" id="tcha" value="<?php echo $straindetail['thca'];?>" maxlength="5"/>
                        <span class="percent_sign"> %</span></span></dt>
                        <dd> CBHa <br> 
                        	<span class="adddisinput">
                            	<input placeholder="Enter CBHa" type="text" name="cbha" id="cbha" value="<?php echo $straindetail['cbha'];?>" maxlength="5"/>
                               <span class="percent_sign"> %</span></span> 
                        </dd>
                    </dl>
                    <dl>
                        <dt> Moisture <br> <span class="adddisinput"> <input placeholder="Enter Moisture" value="<?php echo $straindetail['moisture'];?>" type="text" name="moisture" id="moisture" maxlength="5"/><span class="percent_sign"> %</span></span></dt>
                    </dl>
                </div>
            </div>
                  
                <!--<div class="dividerline"> <img src="images/new_web/dividerline.png" class="scale-with-grid" > </div> -->
                  <div style="clear:both;"></div>
                        <div><span class="mar_rit2"> <a href="javascript:void(0);" id="addstrainsubmit" class="edit_btn addstrainsubmit"> Add Rating </a> </span></div>
                          <input type="hidden" name="strain_id" id="strain_id" value="<?php echo $_GET['strain_id'];?>" />
                      </form>  
                    </div>
                </div>
            </div>
        </div>
        
<script type="text/javascript" src="js/jquery-ui.js"></script>   
<script type="text/javascript" src="js/customInput.jquery.js"></script>    
<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
<script type="text/javascript" src="js/jquery.editable-select.pack.js"></script>
<script type="text/javascript" src="js/mycustom.js"></script>
<script>

$(document).ready(function() {
	 $('.editable-select').editableSelect(
      {
        bg_iframe: true,
        onSelect: function(list_item) {
        /*alert('List item text: '+ list_item.text() +'<br> \
          Input value: '+ this.text.val());*/        }
      }
    );
});
	

$(function() {
	$('input').customInput();
	$("#smell").multiselect();
	$("#taste").multiselect();
	$("#experience").multiselect(); 
	$("#medicinaluse").multiselect(); 
  });
</script>        
</body>
</html>