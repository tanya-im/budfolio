<?php
/*
	File name - lounge_strain_deatils.php
	File Description - to show the strain details
					   this file will go in if condition in lounge page
	date - 14th may
	  
*/ 
session_start();
include_once("config.php");
include_once("function.php");
include_once("timeAgoUtil.php");
?>
<?php
if($_GET['added_by']!='0')
{
		$strLoungeListQuery = 'SELECT 
		  								bud_thought_id,
		  								bud_thought,
								 		post_date,
								 		tbt.user_id as userId, 
										picture_url
								 FROM tbl_bud_thought tbt
								 INNER JOIN users ON users.user_id = tbt.user_id
								 WHERE users.flag = "active"
								 AND  privacy = "public"
								 AND tbt.user_id="'.$_GET['added_by'].'"
								 AND delete_status="0"
								 ORDER BY post_date DESC';
						   

			$LoungeListSQL = mysql_query($strLoungeListQuery) or die($strLoungeListQuery." :".mysql_error());			            //executing strLoginSQL
			
			$arrayLoungeListArray=array();
			
			if(mysql_num_rows($LoungeListSQL) > 0)
			{
				while($straindata = mysql_fetch_array($LoungeListSQL))
				{	
					// Get user detail
					$userdetail = GetUserDetail($straindata['userId']);
					
					//for user profile image path
					$getPathSQL = 'SELECT photo_url 
								   FROM users 
								   WHERE user_id="'.$straindata['userId'].'"';
					$response = mysql_query($getPathSQL) 
								or die($getPathSQL." : ".mysql_error());
					$row = mysql_fetch_assoc($response);
 					if(!empty($row['photo_url']))
					{
						$userImgPath = BASEURL."images/profileimages/thumbnail/".$row['photo_url'];
						$userImgOriginal=BASEURL."images/profileimages/original/".$row['photo_url'];
					}else
					{
						$userImgPath = '';
						$userImgOriginal='';
					}
					//end of user image
					
					?>
					<div class="inn_cell" id="budthought_<?php echo $straindata['bud_thought_id'];?>">
	<?php 
         //query to get the like count and comment count	
		$countCmmntSQL = 'SELECT count(*) as commentCount 
						  FROM `tbl_comments` 
						  where `bud_thought_id`="'.$straindata['bud_thought_id'].'"';
		$resultCommentCount= mysql_query($countCmmntSQL);
		$rowComment= mysql_fetch_assoc($resultCommentCount);
		$commentCount= $rowComment['commentCount'];
		
		//query to fetch like count
		$countLikeSQL = 'SELECT count(*) as likeCount 
						 FROM `tbl_like_thoughts` 
						 where `bud_thought_id`="'.$straindata['bud_thought_id'].'"';
		$resultLikeCount= mysql_query($countLikeSQL);
		$rowLike= mysql_fetch_assoc($resultLikeCount);
		$likeCount= $rowLike['likeCount'];
			   
        $querythree='select user_name,zip_code,state,photo_url 
					 from users 
					 where user_id="'.$straindata['userId'].'" 
					 limit 0,1';
		$userdata=mysql_query($querythree);
		$udata=mysql_fetch_array($userdata);
		
		if(!empty($udata['photo_url']))
        {
			$img_src='images/profileimages/original/'.$udata['photo_url'];
		}
		else
		{ 
			$img_src="images/css_images/profile_pic.png";
		}
		?>
        
        <div class="gray_bg_cellnew"> <span class="lounge_profile"> <img src="<?php echo $img_src;?>" class="scale-with-grid" ></span> <span class="name_link"><a href="user_popup_page.php?userId=<?php echo $straindata['userId'];?>" id="userids_<?php echo $straindata['userId'];?>"> <?php echo str_replace("\\", "",truncatestr($udata['user_name'],20));?></a></span> <span class="datetime"> <?php echo funTimeAgo(strtotime($straindata['post_date']));?> ago</span></div>
        <!--<div class="gray_bg_cell"> <span class="name_link">
       <a 
        href="user_popup_page.php?userId=<?php echo $straindata['userId'];?>" id="userids_<?php echo $straindata['userId'];?>"> <?php echo truncatestr($udata['user_name'],20);?></a> 
        
        </span> <span class="datetime"> <?php echo funTimeAgo(strtotime($straindata['post_date']));?> ago</span></div>-->
        <!-- bud thought image-->
        <?php 
		if(!empty($straindata['picture_url']))
		{
		?>
			<div class="loungeimg_new" style="height: 590px;position: relative;width: 100%;">
            	<?php
				$showH=695;$showW=695;
		
		
	
		list($width, $height, $type, $attr) = getimagesize($img_src);
		//echo $height." : ".$attr;
		if($height <= $showH){
			$marginTB = ($showH-$height) /2;//setting margin top bottom 
		}else{
			$marginTB =0;
		}
		
		if($width <= $showW){
			$marginLR = ($showW-$width)/2;
		}else
		{
			$marginLR =0;
		}
		
				?>
            
         <img src="<?php echo 'images/budthought/'.$straindata['picture_url'];?>"  class="scale-with-grid"> 
         <!-- style="margin:<?php //echo $marginTB."px ".$marginLR; ?>px;"-->
            </div>
		<?php
		}
		?>
        
        
      
           <div class="msg_cell_bud"> <span class="user_comment"><?php echo str_replace("\\", "",atRateUserNameWeb(stripslashes($straindata['bud_thought'])));?></span> </div>
        
        <div class="status_cell">
        
        <!-- <div class="status_con"> <a href="javascript:void(0);" id="addCommentBudThought_<?php echo $straindata['bud_thought_id']; ?>" <?php if(!empty($_SESSION['user_id'])){?> class="addcomment_btn show_post_btn"  <?php }else{?>  class="addcomment_btn"onclick="alertRequest();" <?php } ?>> Add Comment </a> </div>-->
				
				<!--added on 6th sept-->	
			    <?php if($_SESSION['user_id']==$straindata['userId']){?>    
                <!-- for delete buttom-->  
                <div class="status_con"> 
                <a href="javascript:void(0);" id="deleteBudThought_<?php echo $straindata['bud_thought_id'];?>" class="delete_BT"> <img src="images/new_web/RecycleBin.png"  style="margin-top:-4px;" /></a>  </div>             
                <!--end of delete button-->
              <?php  } ?>
        
         <div class="status_con"> 
             <span class="fleft new_marlft8"><a href="javascript:void(0);" <?php if(!empty($_SESSION['user_id'])){
				 ?> class="show-comment show_post_btn" <?php  }else{?> class="alertRequest" <?php } ?> id="showComments_<?php echo $straindata['bud_thought_id']; ?>"> <img src="images/new_lounge/chat_newicon.png"></a> </span> <span class="fleft" id="comment_count_<?php echo $straindata['strain_id']; ?>"><?php if($commentCount>0){echo $commentCount;}else{/*display nthng*/}?></span> </div>
                 
             <div class="status_con">
              <span class="fleft new_marlft8">
               <a href="javascript:void(0);" id="likeBudThought_<?php echo $straindata['bud_thought_id'];?>" <?php if(!empty($_SESSION['user_id'])){?> class="like_bud" <?php }else{?> class="alertRequest" <?php } ?>> <img src="images/new_lounge/like_newicon.png"> 
               </a>
              </span>
              <span class="fleft" id="like_count_<?php echo $straindata['strain_id']; ?>"> <?php if($likeCount>0){echo $likeCount;}else{/* echo nthng*/ }?></span>
              </div> 
             
             
             
             
        </div>
        
      <div  class="lounge_comment comments_here " id="comments_here_<?php echo $straindata['bud_thought_id']; ?>"></div>

</div>
<?php 
				}// end of whhile
			}// enf of if
			else
			{
				echo '<div class="cell_dispensary center" style="padding:15px 0;">No Budthought available. </div>';
			}
} //end iof if
else
{
	echo '<div class="cell_dispensary center" style="padding:15px 0;">No Budthoughts available. </div>';
	
}
			?>
