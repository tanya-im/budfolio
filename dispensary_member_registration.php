<?php 


// Include header,auth and config file. #C8C8C8
include_once("newHeader.php");
//require_once("auth.php");
include_once("config.php");
include_once("function.php");
?>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container" id="inner_main_div">
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2> Dispensary Profile Registration</h2>
                </div>
                <div id="content_1" class="content">
                
                <form name="dis_registration" id="dis_registration"  autocomplete="Disabled"> 
                <input type="hidden" name='twitter_id' id='twitter_id' value="<?php echo (isset($_SESSION['twitter_id'])?$_SESSION['twitter_id']:'');?>" />
                   
    <div id="dispensary_register" class="window">
        
        <div class="reg-con">
        	<p id="regerrormsg"></p>
            <span class="brw_heading">1. User Information</span>
            <dl>
                <dt><span class="mdred">*</span>Username</dt>
                <dd> <span class="registerinput"> <input type="text" name="username" id="username" tabindex="1" maxlength="25" placeholder="Enter Username" value="<?php echo (isset($_POST['username'])?$_POST['username']:'');?>" /> </span> </dd>
                <div class="username_check"></div>
            </dl>
            
            <dl>
                <dt><span class="mdred">*</span>Password </dt>
                <dd> <span class="registerinput"> <input type="password" name="password" id="password" tabindex="2"placeholder="Enter Password" value="<?php echo (isset($_POST['password'])?$_POST['password']:'');?>" /> </span>
                 <div class="username_check"></div>
                 </dd>
            </dl>
            
            <dl>
                <dt><span class="mdred">*</span>Confirm Password </dt>
                <dd> <span class="registerinput"> <input type="password" name="confirmpassword" id="confirmpassword" tabindex="3" placeholder="Enter Confirm Password" value="<?php echo (isset($_POST['confirmpassword'])?$_POST['confirmpassword']:'');?>" /> </span> 
                 <div class="username_check"></div>
                </dd>
            </dl>
            
            <dl>
                <dt><span class="mdred">*</span>Email</dt>
                <dd> <span class="registerinput"> <input type="text" placeholder="Enter Email Address" name="email" id="email" tabindex="4" maxlength="55" value="<?php echo (isset($_POST['email'])?$_POST['email']:'');?>"/> </span></dd>
                <div class="username_check"></div>
            </dl>
              <span class="brw_heading">2. Business Information  <span style="font-size:12px;">(*used to verify business)</span></span>
              
             <dl>
                <dt><span class="mdred">*</span>Contact Name</dt>
                <dd> <span class="registerinput"> <input type="text" name="contactname" id="contactname" tabindex="5" maxlength="50" placeholder="Enter contact Name" /> </span> </dd>
                <div class="username_check space"></div>
            </dl> 
              
            <dl>
                <dt><span class="mdred">*</span>Business Name</dt>
                <dd> <span class="registerinput"> <input type="text" name="businessname" id="businessname" tabindex="5" maxlength="50" placeholder="Enter Business Name" /> </span> </dd>
                <div class="username_check space"></div>
            </dl>
            
       
            
            <dl>
                <dt><span class="mdred">*</span>Zip Code</dt>
                <dd> <span class="registerinput"> <input type="text" placeholder="Enter Zip Code" name="zipcode" id="zipcode" tabindex="6" maxlength="8" value="<?php echo (isset($_POST['zipcode'])?$_POST['zipcode']:'');?>"/> </span> </dd>
				<div class="username_check"></div>
            </dl>
          
            <dl>
                <dt><span class="mdred">*</span>Phone No</dt>
                <dd> <span class="registerinput"> <input type="text" placeholder="Enter Phone No" name="phoneno" id="phoneno" tabindex="7" maxlength="12" /> </span> 
                 <div class="username_check"></div>
                </dd>
            </dl>
			
            <!-- 3 checkboxes-->
            <dl>
            	<dt> <input type="checkbox" name="i_am_18" id="i_am_18" tabindex="8"
                 <?php if($_POST['i_am_18']=='on'){ ?> checked="checked" <?php } ?> /></dt>
                <dd><span class="newmartop10">I am at least 18 years old</span></dd>
            </dl>
			<dl>
            	<dt> <input type="checkbox" name="up_to_date" id="up_to_date" 
                tabindex="9" <?php if($_POST['up_to_date']=='on'){ ?> checked="checked" <?php }?> /></dt>
                <dd><span class="newmartop10">Stay up-to-date with budfolio news ?</span></dd>
            </dl>
            <dl>
            	<dt> <input type="checkbox" name="t_and_c" id="t_and_c" onclick="hideerrormsg();" tabindex="10" <?php if($_POST['t_and_c']=='on'){ ?> checked="checked" <?php }?>/></dt>
                <dd><span class="newmartop10">I agree to terms and conditions </span></dd>
                <!--<div class="fllft martop10"> 
                    	<a href="javascript:void(0);" id="tandcmsg" class="gray_link"> Terms & Conditions </a>
                    </div>-->
            </dl>
			<!-- end of three checkBoxes-->
            
            <dl> <dt></dt> <dd><a href="javascript:void(o);" id="tandcmsg" class="gray_link"> Terms &amp; Conditions </a></dd> </dl>
            
            
            <dl>
            	<span class="tcmsg">By registering for budfolio you agree that you are at least 18 years of age and that you are a legal medicinal marijuana under your current state laws.</span>
            </dl>
            <dl>
                <dt>&nbsp;</dt>
                <dd>
                	<a  style="height:20px;float:left;"class="edit_btn martop10" type="button" name="dis_reg_submit" id="dis_reg_submit" tabindex="11">Create Member !</a>
                	<input type="hidden" value="available" name="unameflag" id="unameflag" />
                    <input type="hidden" value="available" name="emailflag" id="emailflag" />
                </dd>
            </dl>
    	</div>
	</div>        
</form> <!------------------------- Registration form end here------------->
</div>
			</div>
       </div>    
    
 <?php include('newLounge_right.php');?>
</div>
  </div>

<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>   