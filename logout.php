<?php 
// Unset session if user logedout and redirect to login page. 
session_start();

session_unset($_SESSION);

header('Location:index.php');
?>