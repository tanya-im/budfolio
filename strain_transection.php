<?php 
include_once("config.php");
// If add strain form submited.
if(isset($_POST['add_strain']))
{

	for($i=0;$i<count($_POST['smell']);$i++)
	{
		$commaSepratedSmell = implode(',',$_POST['smell']);
	}
	for($i=0;$i<count($_POST['taste']);$i++)	
	{
		$commaSepratedTaste = implode(',',$_POST['taste']);
	}

	$date = date('Y-m-d H:i:s');
	$queryone="insert into strains(user_id,
				strain_name,
				dispensary_name,
				species,
				linage,
				smell_rating,
				taste_rate,
				strength_rate,
				overall_rating,
				consumption_name,
				tested_by,
				thc,
				cbd,
				cbn,
				thca,
				cbha,
				moisture,
				description,
				post_date)
				values('".$_SESSION['user_id']."',
				'".mysql_real_escape_string($_POST['strainname'])."',
				'".mysql_real_escape_string($_POST['dispensensary'])."',
				'".mysql_real_escape_string($_POST['species'])."',
				'".mysql_real_escape_string($_POST['linage'])."',
				'".mysql_real_escape_string($commaSepratedSmell)."',
				'".mysql_real_escape_string($commaSepratedTaste)."',
				'".mysql_real_escape_string($_POST['strength'])."',
				'".mysql_real_escape_string($_POST['overall'])."',
				'".mysql_real_escape_string($_POST['consumption'])."',
				'".mysql_real_escape_string($_POST['testedby'])."',
				'".mysql_real_escape_string($_POST['thc'])."',
				'".mysql_real_escape_string($_POST['cbd'])."',
				'".mysql_real_escape_string($_POST['cbn'])."',
				'".mysql_real_escape_string($_POST['tcha'])."',
				'".mysql_real_escape_string($_POST['cbha'])."',
				'".mysql_real_escape_string($_POST['moisture'])."',
				'".mysql_real_escape_string($_POST['description'])."',
				'".$date."')";
				$strain_status=mysql_query($queryone) or die(mysql_error());
	$strain_id=mysql_insert_id();
	$strainname=mysql_real_escape_string($_POST['strainname']);
	// insert experience detal
	if(!empty($_POST['experience']) && !empty($strain_id))
	{
		for($i=0;$i<count($_POST['experience']);$i++)
		{
			$querytwo="insert into experience(experience_name,strain_id)values('".mysql_real_escape_string($_POST['experience'][$i])."','".$strain_id."')";
			
			$experience_status=mysql_query($querytwo)or die(mysql_error());
		}

		
	}
	// insert medicinaluse detal
	if(!empty($_POST['medicinaluse']) && !empty($strain_id))
	{
		for($i=0;$i<count($_POST['medicinaluse']);$i++)
		{
			$querythree="insert into medicinal_use(medicinal_type,strain_id)values('".mysql_real_escape_string($_POST['medicinaluse'][$i])."','".$strain_id."')";
			$medicinaluse_status=mysql_query($querythree)or die(mysql_error());
		}
    }
	
	// if dispensary name is entered manually then update dispensaryId in strain date -19th feb
			 
			 $dispancry_link_query="select dispensary_id 
                                    from dispensaries 
                                    where LOWER(dispensary_name)='".$_POST['dispensensary']."'
                                    Limit 0,1";
             $dispancry_link_sql = mysql_query($dispancry_link_query);
             if(mysql_num_rows($dispancry_link_sql)==1){
				 $dispancry_link_result = mysql_fetch_assoc($dispancry_link_sql);
				 $disId = $dispancry_link_result['dispensary_id'];
				 
				 //update dispensary id in starin 
				 $updateDispIDSQL= "UPDATE strains
				 					SET dispensary_id='".$disId."'
									WHERE strain_id='".$strain_id."'";
				$updateDispensaryId=mysql_query($updateDispIDSQL) or die($updateDispIDSQL." : ".mysql_error());
									
			 }else
			 {
				 // dispensary id will saved as "0"
			 }
	 //end of 19th feb 
	
	
	
	// insert strain images
	$primary='';
	if(!empty($_SESSION['images']) && !empty($strain_id))
	{
		$brack=0;
		for($i=(count($_SESSION['images'])-1);$i>=0;$i--)
		{
			if(!empty($_SESSION['images'][$i]))
			{
				if($_POST['primaeryimg']==$brack){
					$primary=1;
					$pimgname=$_SESSION['images'][$i];
				}
				else{
					$primary=0;
				}
				if(empty($pimgname))
				{
					$pimgname=$_SESSION['images'][$i];
				}
				$queryfour="insert into strainimages(image_name,primary_image,strain_id)values('".mysql_real_escape_string($_SESSION['images'][$i])."','".$primary."','".$strain_id."')";
				$image_status=mysql_query($queryfour)or die(mysql_error());
				$brack++;
			}
			if($brack==3)
			{
				 break;
			}
		}
	}
	unset($_SESSION['images']);
	if($strain_status)
	{	
		if(isset($_POST['fblogin']))
		{
			if($_POST['fblogin']=='facebookshare')
			{
				include_once('fb/fbshare.php');
			}
		}
		if(isset($_POST['twlogin']))
		{
			if($_POST['twlogin']=='twittershare')
			{
				include_once('twitter/twitterpost.php');
			}
		}
		if($_POST['add_strain']=='liberary')
		{	
		  header('Location:strain_liberary.php?strainstatus=added');	
		}else
		{
			header('Location:home.php?strainstatus=added');
		}
	}
	else
	{
		header('Location:home.php?strainstatus=notadded');exit();
	}
}


// ---------------If update strain form submited.-----------------------

if(isset($_POST['update_strain']))
{
	for($i=0;$i<count($_POST['smell']);$i++)
	{
		$commaSepratedSmell = implode(',',$_POST['smell']);
	}
	for($i=0;$i<count($_POST['taste']);$i++)	
	{
		$commaSepratedTaste = implode(',',$_POST['taste']);
	}
	
	
	$date = date('Y-m-d H:i:s');
	// Update all strain detail
	$queryone="UPDATE strains SET 
			   strain_name ='".mysql_real_escape_string($_POST['strainname'])."',
			   dispensary_name ='".mysql_real_escape_string($_POST['dispensensary'])."',
			   species ='".mysql_real_escape_string($_POST['species'])."',
			   linage ='".mysql_real_escape_string($_POST['linage'])."',
			   smell_rating ='".mysql_real_escape_string($commaSepratedSmell)."',
			   taste_rate ='".mysql_real_escape_string($commaSepratedTaste)."',
			   strength_rate ='".mysql_real_escape_string($_POST['strength'])."',
			   overall_rating ='".mysql_real_escape_string($_POST['overall'])."',
			   consumption_name ='".mysql_real_escape_string($_POST['consumption'])."',
			   tested_by='".mysql_real_escape_string($_POST['testedby'])."',
			   thc='".mysql_real_escape_string($_POST['thc'])."',
			   cbd='".mysql_real_escape_string($_POST['cbd'])."',
			   cbn='".mysql_real_escape_string($_POST['cbn'])."',
			   thca='".mysql_real_escape_string($_POST['tcha'])."',
			   cbha='".mysql_real_escape_string($_POST['cbha'])."',
			   moisture='".mysql_real_escape_string($_POST['moisture'])."',
			   description='".mysql_real_escape_string($_POST['description'])."', 
			   update_date ='".$date."' where strain_id='".$_SESSION['strain_id']."'";
	$strain_status = mysql_query($queryone) or die(mysql_error());
	$strain_id = $_SESSION['strain_id'];
	
	// Update experience detail
	if(!empty($_POST['experience']))
	{
		
		$querytwo="delete from experience where strain_id='".$_SESSION['strain_id']."'";
		$experience_status=mysql_query($querytwo)or die(mysql_error());
		for($i=0;$i<count($_POST['experience']);$i++)
		{
			$querytwo="insert into experience(experience_name,strain_id)values('".mysql_real_escape_string($_POST['experience'][$i])."','".$_SESSION['strain_id']."')";
			$experience_status=mysql_query($querytwo)or die(mysql_error());
		}
		
		
	}
	
	// Update medicinaluse detail
	if(!empty($_POST['medicinaluse']))
	{
		$querythree="delete from medicinal_use where strain_id='".$_SESSION['strain_id']."'";
		$medicinaluse_status=mysql_query($querythree)or die(mysql_error());
		for($i=0;$i<count($_POST['medicinaluse']);$i++)
		{
			 $querythree="insert into medicinal_use(medicinal_type,strain_id)values('".mysql_real_escape_string($_POST['medicinaluse'][$i])."','".$_SESSION['strain_id']."')";
			$medicinaluse_status=mysql_query($querythree)or die(mysql_error());
		}
	}
	
	
	
	// if dispensary name is entered manually then update dispensaryId in strain date -19th feb
			 
			 $dispancry_link_query="select dispensary_id 
                                    from dispensaries 
                                    where LOWER(dispensary_name)='".$_POST['dispensensary']."'
                                    Limit 0,1";
             $dispancry_link_sql = mysql_query($dispancry_link_query);
             if(mysql_num_rows($dispancry_link_sql)==1){
				 $dispancry_link_result = mysql_fetch_assoc($dispancry_link_sql);
				 $disId = $dispancry_link_result['dispensary_id'];
				 
				 //update dispensary id in starin 
				 $updateDispIDSQL= "UPDATE strains
				 					SET dispensary_id='".$disId."'
									WHERE strain_id='".$_SESSION['strain_id']."'";
				$updateDispensaryId=mysql_query($updateDispIDSQL) or die($updateDispIDSQL." : ".mysql_error());
									
			 }else
			 {
				 // dispensary id will saved as "0"
			 }
			 //end of 19th feb 


	$strain_id = $_SESSION['strain_id'];
	//print_r($_SESSION['images']);
	//print_r($_POST);
	// insert strain images
	if(!empty($_SESSION['images']) && !empty($strain_id))
	{
		$deleteimahesQuery="delete from strainimages where strain_id ='".$strain_id."' LIMIT 3";
		$deleteimahesSql=mysql_query($deleteimahesQuery)or die(mysql_error());
		
		$brack=0;
		$imgseqarray=array();
		for($i=0;count($_SESSION['images'])>$i;$i++)
		{
			if($_POST['primaeryimg']==$i){
				$primary=1;
			}
			else{
				$primary=0;
			}
			$queryfour="insert into strainimages(image_name,primary_image,strain_id)values('".mysql_real_escape_string($_SESSION['images'][$i])."','".$primary."','".$strain_id."')";
			$image_status=mysql_query($queryfour)or die(mysql_error());
		}
	}
	unset($_SESSION['images']);
	if($strain_status)
	{
		if(isset($_GET['page']))
		{
			header('Location:home.php?strainstatus=updated&page='.$_GET['page']);exit();
		}
		else
		{
			header('Location:home.php?strainstatus=updated');exit();
		}
	}
	else
	{
		if(isset($_GET['page']))
		{
			header('Location:home.php?strainstatus=notupdated&page='.$_GET['page']);exit();
		}
		else
		{
			header('Location:home.php?strainstatus=notupdated');exit();
		}	
	}
}
?>