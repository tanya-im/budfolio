<?php
// Include header,auth and config file.
include_once("newHeader.php");
include_once("config.php");
include('function.php');
$_SESSION['menucounter']=1;
$_SESSION['searchtext']='';
$_SESSION['datacount']='';

$ip_addr = $_SERVER['REMOTE_ADDR'];
	$geoplugin = unserialize( file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip_addr) );

	if ( is_numeric($geoplugin['geoplugin_latitude']) && is_numeric($geoplugin['geoplugin_longitude']) ) 
	{
		$userLat = $geoplugin['geoplugin_latitude'];
    	$userLong = $geoplugin['geoplugin_longitude'];
		$_SESSION['current_lat']=$userLat;
		$_SESSION['current_long']=$userLong;
	}else
	{
	
	echo "no lat long found";
	}
?>
<link rel="stylesheet" href="css/jquery-ui.css" />
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/organictabs.jquery.js"></script>


<style>
a
{
	text-decoration:none;
}
</style>
<div id="wraper">
	<div class="clear"></div> 
	<div id="main_con">
    	<div class="inner_conlft scroll_container" id="inner_main_div">
 			<div class="cell_con">
            	<div class="gray_headingbar"> <h2> Dispensary List</h2>  </div>
                <div class="greenbar_bg"><?php include('menu_fillters.php');?> </div>
               <div id="content_1" class="content">
                
	  <?php 	$Counterquery = "SELECT 
										dispensary_id,   
      									dispensary_name,
      									city,
      									state,
      									zip,
      									image,
      									phone_number,
      									added_by,
										latitude,
									    longitude,
										bio,
								 		street_address,
		  ( 3959 * acos( cos( radians( ".$userLat." ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(".$userLong.") ) + sin( radians( ".$userLat." ) ) * sin( radians( latitude ) ) ) ) AS distance
		                
		   				 			FROM dispensaries 
						 			WHERE flag='Active'
						 			ORDER BY distance, dispensary_name";
									
							
		$CounterRes = mysql_query($Counterquery);
		$CounterData = mysql_num_rows($CounterRes);
		$_SESSION['datacount']=$CounterData;
		$_SESSION['query'] =	$Counterquery;
		if($_SESSION['datacount']>0)
		{
			 include('menuContent.php');
           
	    }else{ echo '<div class="emptycell"><center style="padding-top:25px;">No result found.</center></div>'; 				       }?>		
        </div>
	        </div>
        </div>
	<?php include('newLounge_right.php');?>
    </div>
</div>

<div class="footer"><?php include('footer.php');?></div>
<?php include('budfolio_footer.php');?>

<script type="text/javascript" src="js/row_col.js"></script>
<script type="text/javascript" src="js/row_col_for_edit.js"></script>
<script>
	(function($){
		$(window).load(function(){
			$('.scroll_container').scrollExtend(
				{   'target': 'div#content_1',
					'url': 'menuContent.php?flag=1',
					'loadingIndicatorClass': 'scrollExtend-loading',
					'loadingIndicatorEnabled': true
				}
			  );
		});
	})(jQuery);
	</script>
    <script>
$(function() {
	$( "#tabs" ).tabs({
						beforeLoad: function( event, ui ) {
ui.jqXHR.error(function() {
ui.panel.html(
"Couldn't load this tab. We'll try to fix this as soon as possible. " +
"If this wouldn't be a demo." );});}});
});
</script>
<div id="boxes"></div>
<div id="mask"></div> 
</body>
<script type="text/javascript">
// Run the script on DOM ready:
$(document).ready(function(){
	<?php if(isset($_GET['search'])){?>
		$('#sl_search_text').val('<?php echo $_GET['search'];?>');
		$('.menusearchbtn').click();
		
	<?php }?>
});
</script>
</html>
